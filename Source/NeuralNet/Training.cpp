//own
#include "AiLab/NeuralNet.h"
#include "DatabaseHandler/DatabaseHandler.h"
#include "Scraper/Scraper.h"
#include "Source/FileIO/Config/ConfigIo.h"
#include "Source/FileIO/FileIO.h"
//#include "Source/Statistics/CardStatistics/PreMatchCardStats.h"
#include "Training.h"

//std
#include <array>
#include <map>
#include <vector>

namespace sports_prediction_lab
{
	//I didn't delete this code because it shows how to set up a neural net training 

	//using namespace ai_lab;
	//using namespace scraper;
	//using namespace database_handler::v1;
	//using sports_lab_common::SpMatch;
	//using sports_lab_common::CardsTrainingSample;
	//using sports_lab_common::DataSet;

	//using std::endl;
	//using std::map;
	//using std::vector;
	//
	//void GenerateTrainingSamples()
	//{
	//	using namespace competitions;
	//	const vector<std::array<utils::String, 2>> competitions
	//	{
	//		//{countries::AUSTRALIA, australia::A_LEAGUE},
	//		////{countries::CHINA, china::CSL},
	//		{countries::ENGLAND, england::CHAMPIONSHIP},
	//		//{countries::ENGLAND, england::PREMIER_LEAGUE},
	//		{countries::FRANCE, france::LIGUE_1},
	//		{countries::GERMANY, germany::BUNDESLIGA},
	//		{countries::ITALY, italy::SERIE_A},
	//		{countries::NETHERLANDS, netherlands::EREDIVISIE},
	//		////{countries::SCOTLAND, scotland::PREMIERSHIP},
	//		{countries::SPAIN, spain::PRIMERA_DIVISION},
	//		////{countries::USA, usa::MLS}
	//	};

	//	const int numSeasonsForGeneralStats = 2;
	//	const int numSeasonsForStatsAgainstEachOther = 4;
	//	const int maxNumTrainingsamples = -1;
	//	const int firstTrainingSeason = 2018;

	//	for (const auto&[country, competition] : competitions)
	//	{
	//		TCOUT << "Generating training samples for " << country << " " << competition << "..." << std::endl;
	//		vector<CardsTrainingSample> trainingSamples = PreMatchCardStats::CalculateTrainingSample(
	//			country,
	//			competition,
	//			numSeasonsForGeneralStats,
	//			numSeasonsForStatsAgainstEachOther,
	//			maxNumTrainingsamples,
	//			firstTrainingSeason
	//		);

	//		InsertTrainingSamples(trainingSamples);
	//	}
	//}

	//void Train()
	//{
	//	vector<ai_lab::back_propagation::TrainingTask> tasks;
	//	const utils::String countryLit{STR("country") };
	//	const utils::String competitionLit{STR("competition") };

	//	using namespace file_io;
	//	for (const auto& trainingTask : config::GetConfig().trainingTasks)
	//	{
	//		BackPropParams params;
	//		params.learningRate = trainingTask.learningRate;
	//		params.momentum = trainingTask.momentum;
	//		params.testSamplingFrequency = trainingTask.testSamplingFrequency;
	//		params.miniBatchSize = trainingTask.miniBatchSize;
	//		params.setUpInputLayerScaling = trainingTask.setUpInputLayerScaling;
	//		params.weightInitialisation = WeightInitialisation::FromString(trainingTask.weightInitialisation);
	//		params.neuralNetJsonFilePath =
	//			file_io::OUTPUT_FOLDER_NAME + trainingTask.neuralNetJsonFilePath;
	//		params.trainingErrorsFilePath =
	//			file_io::OUTPUT_FOLDER_NAME + trainingTask.trainingErrorsFilePath;
	//		params.errorDistributionFilePath =
	//			file_io::OUTPUT_FOLDER_NAME + trainingTask.errorDistributionFilePath;

	//		params.stopConditions.minNumIterations = trainingTask.stopConditions.minNumIterations;
	//		params.stopConditions.maxNumIterations = trainingTask.stopConditions.maxNumIterations;
	//		params.stopConditions.generalisationError = trainingTask.stopConditions.generalisationError;
	//		params.stopConditions.stopIfNotImprovedEpochRatio = trainingTask.stopConditions.stopIfNotImprovedEpochRatio;

	//		const utils::String& country = trainingTask.country;
	//		const utils::String& competition = trainingTask.competition;

	//		vector<ai_lab::SpTrainingSample> trainingSamples = QueryTrainingSamples(country, competition);

	//		Mlp mlp;
	//		for (const auto& layer : trainingTask.mlp.layers)
	//		{
	//			//TODO: is this correct? Shouldnt it be an if-else insted? It adds to layers if there is an outputfunction
	//			mlp.AddLayer(layer.activationFunction, layer.numNeurons);
	//			if (layer.outputFunction)
	//			{
	//				mlp.AddLayer(layer.activationFunction, *layer.outputFunction, layer.numNeurons);
	//			}
	//		}
	//		mlp.notes[countryLit] = country;
	//		mlp.notes[competitionLit] = competition;

	//		tasks.push_back({
	//			mlp,
	//			trainingSamples,
	//			params,
	//			trainingTask.times });
	//	}


	//	back_propagation::TrainParallel(tasks, 4);

	//	for (const auto& task : tasks)
	//	{
	//		InsertNeuralNet(
	//			task.mlp.notes.at(countryLit),
	//			task.mlp.notes.at(competitionLit),
	//			sports_lab_common::TrainingTypes::PreMatchCardStats,
	//			STR(""),
	//			task.mlp);
	//	}
	//}
}