#pragma once
//lib
#include "DatabaseHandler/DatabaseHandler.h"

namespace sport_monks_scraping
{
	void FullScrape();
	void QuickScrape();
	void FixMissingData();

	bool ScrapeTeamsById(SQLite::Database& db, const std::set<int64_t>& teamIds);
}