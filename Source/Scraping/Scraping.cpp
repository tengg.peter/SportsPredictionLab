#include "DatabaseHandler/DatabaseHandler.h"
#include "SportsPredictionLab/Source/Scraping/Scraping.h"
#include "Scraper/Scraper.h"

#include <thread>

namespace sport_monks_scraping
{
	using namespace database_handler;
	using namespace database_handler::common;
	using namespace database_handler::v2;
	using namespace scraper::sport_monks;

	using std::endl;
	using std::vector;
	using utils::date_and_time::DateTime;

	namespace
	{
		void LogTooManyAttempts(const std::string& functionName)
		{
			utils::Logger::LogInfo(functionName + ": " + TooManyAttemptsException::TOO_MANY_ATTEMPTS, true);
		}

		bool Continents(SQLite::Database& db)
		{
			TCOUT << "\nScraping continents..." << endl;
			InsertOr(common::OnConflict::Ignore, db, ScrapeAllContinents());
			return true;
		}
		bool Countries(SQLite::Database& db, bool forceDownload)
		{
			TCOUT << "\nScraping countries..." << endl;
			InsertOr(common::OnConflict::Ignore, db, ScrapeAllCountries(forceDownload));
			return true;
		}
		bool Leagues(SQLite::Database& db, bool forceDownload)
		{
			TCOUT << "\nScraping leagues..." << endl;
			InsertOr(common::OnConflict::Replace, db, ScrapeAllLeagues(forceDownload));
			return true;
		}
		bool Seasons(SQLite::Database& db)
		{
			TCOUT << "\nScraping seasons..." << endl;
			InsertOr(common::OnConflict::Replace, db, ScrapeAllSeasons(true /*forceDownload*/));
			return true;
		}
		void RemoveBelgianPhantomMatches(std::set<UpFixture, FixtureCmp>& fixtures)
		{
			for (auto it = fixtures.begin(); fixtures.end() != it;)
			{
				if (const auto& time = (*it)->date_time;
					208 /*Belgium Pro League*/ == (*it)->sm_league_id &&
					23 == time.hour && 0 == time.minute && 0 == time.second && /*is at 23:00 UTC*/
					DateTime::Now() < time /*is in the future*/)
				{
					utils::Logger::LogInfo("Belgian phantom match found: " + std::to_string((*it)->sm_id), true /*toConsoleToo*/);
					it = fixtures.erase(it);
				}
				else
				{
					++it;
				}
			}
		}

		bool Fixtures(SQLite::Database& db, bool scrapeAllSeasons, bool forceDownload)
		{
			TCOUT << "\nScraping fixtures..." << endl;
			bool emptyDb = 0 == QueryCount<Fixture>(db);
			std::set<std::string> years;
			DateTime latestDate;

			if (!emptyDb)
			{
				latestDate = QueryExtremeDate(db, "fixtures", "date_time", "status = 'NS' OR status = 'TBA'", "ASC");
				years.insert(std::to_string(latestDate.year));
			}

			for (const auto& upSeason : QueryAll<Season>(db))
			{
				const auto split = utils::string_operations::SplitByEach(utils::string_conversion::UtilsStringToStdString(upSeason->name), { "/" });
				if (scrapeAllSeasons || split[0] >= std::to_string(latestDate.year))
				{
					//if (split[0] < "2020")
					//{
					//	continue;
					//}

					years.insert(split[0]);
					if (1 < split.size())
					{
						years.insert(split[1]);
					}
				}
			}

			try
			{
				bool firstYear = true;
				std::string startDate;
				std::string endDate;

				for (const auto& year : years)
				{
					if (!scrapeAllSeasons && firstYear && !emptyDb)
					{
						startDate = latestDate.ToSQLiteDateFormat();
						TCOUT << "\nThe earliest date to update is " << startDate.c_str() << endl;
						firstYear = false;
					}
					else
					{
						startDate = year + "-01-01";
					}
					endDate = year + "-12-31";

					TCOUT << "Scraping fixtures of year " << year.c_str() << endl;
					utils::date_and_time::StopWatch timer;
					timer.Start();
					auto fixtures = ScrapeFixturesBetweenDates(startDate, endDate, forceDownload);
					RemoveBelgianPhantomMatches(fixtures);
					InsertOr(common::OnConflict::Replace, db, fixtures);
					timer.Stop();
					TCOUT << "Insertion took " << timer.Elapsed<std::chrono::seconds>() << " seconds." << endl;
				}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Fixtures");
				return false;
			}
			return true;
		}
		bool Commentaries(SQLite::Database& db)
		{
			TCOUT << "\nScraping commentaries..." << endl;
			std::set<int64_t> fixtureIdsToScrape = QueryWhereNotIn(
				db,
				"fixtures",
				"sm_id",
				"commentaries",
				"sm_fixture_id",
				"commentaries = 1");

			if (fixtureIdsToScrape.empty())
			{
				TCOUT << "Commentaries are up to date." << endl;
				return true;
			}
			TCOUT << fixtureIdsToScrape.size() << " fixtures were found to scrape commentaries for." << endl;

			try
			{
				int count = 0;
				for (const int64_t fixtureId : fixtureIdsToScrape)
				{
					InsertOr(common::OnConflict::Ignore, db, ScrapeCommentaries(fixtureId, true));
					if (0 == ++count % 10 || fixtureIdsToScrape.size() == count)
					{
						TCOUT << "Scraped commentaries for " << count << "/" << fixtureIdsToScrape.size() << " fixtures." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Commentaries");
				return false;
			}
			return true;
		}
		bool Teams(SQLite::Database& db, bool forceDownload)
		{
			TCOUT << "\nScraping teams..." << endl;
			const auto missingSeasonIds = QueryWhereNotIn(db, "seasons_teams", "seasons_sm_id", "seasons", "sm_id");
			const auto missingTeamIds = QueryWhereNotIn(db, "seasons_teams", "teams_sm_id", "teams", "sm_id");
			const auto activeSeasonIds = QuerySingleColumnFromTable(db, "seasons", "sm_id", "is_current_season = 1");

			TCOUT << "Seasons missing from DB: " << missingSeasonIds.size() << endl;
			TCOUT << "Active seasons to update: " << activeSeasonIds.size() << endl;

			std::set<int64_t> seasonIdsToScrape{ missingSeasonIds };
			seasonIdsToScrape.insert(activeSeasonIds.cbegin(), activeSeasonIds.cend());

			try
			{
				vector<vector<int64_t>> seasonTeamIds;
				std::set<UpTeam, TeamCmp> allTeamsSet;
				int count = 0;
				for (const auto seasonId : seasonIdsToScrape)
				{
					TCOUT << "Scraping teams of season " << ++count << "/" << seasonIdsToScrape.size() << endl;
					auto teamSet = ScrapeTeamsBySeason(seasonId, forceDownload);
					for (const auto& upTeam : teamSet)
					{
						seasonTeamIds.emplace_back(vector<int64_t>{ seasonId, upTeam->sm_id });
					}
					while (!teamSet.empty())
					{
						allTeamsSet.insert(std::move(teamSet.extract(teamSet.begin())));
					}
					TCOUT << "Scraped " << allTeamsSet.size() << " teams in total." << endl;
				}
				InsertIntoSwitchTable(db, "seasons_teams", seasonTeamIds);
				InsertOr(common::OnConflict::Ignore, db, allTeamsSet, true);

				//repeat query, because the missing teams could have been inserted by the previos loop
				allTeamsSet.clear();
				for (const auto teamId : QueryWhereNotIn(db, "seasons_teams", "teams_sm_id", "teams", "sm_id"))
				{
					allTeamsSet.insert(ScrapeTeam(teamId, forceDownload));
				}
				if (!allTeamsSet.empty())
				{
					TCOUT << "Found " << allTeamsSet.size() << " missing teams. Inserting them to the db..." << endl;
					InsertOr(common::OnConflict::Ignore, db, allTeamsSet, true);
				}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Teams");
				return false;
			}
			return true;
		}
		bool PlayerPerformances(SQLite::Database& db)
		{
			TCOUT << "\nScraping player performances..." << endl;
			try
			{
				const auto oldSeasonTeams = QuerySeasonsTeamsForPlayerPerformances(db, false);
				const auto activeSeasonTeams = QuerySeasonsTeamsForPlayerPerformances(db, true);

				TCOUT << "Scraping old seasons..." << endl;
				int inserted = 0;
				for (const auto& seasonTeam : oldSeasonTeams)
				{
					auto playerPerformancesSet = ScrapePlayerPerformances(seasonTeam.seasonId, seasonTeam.teamId, false);
					InsertOr(common::OnConflict::Ignore, db, playerPerformancesSet);
					inserted += playerPerformancesSet.size();
					TCOUT << "Total player performances inserted: " << inserted << std::endl;
				}

				TCOUT << "Updating running seasons..." << endl;
				inserted = 0;
				for (const auto& seasonTeam : activeSeasonTeams)
				{
					auto playerPerformancesSet = ScrapePlayerPerformances(seasonTeam.seasonId, seasonTeam.teamId, true);
					InsertOr(common::OnConflict::Replace, db, playerPerformancesSet);
					inserted += playerPerformancesSet.size();
					TCOUT << "Total player performances updated: " << inserted << std::endl;
				}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("PlayerPerformances");
				return false;
			}
			return true;
		}
		bool Players(SQLite::Database& db)
		{
			TCOUT << "\nScraping players..." << endl;
			try
			{
				std::set<int64_t> playerIdsToImport = QueryWhereNotIn(db, "player_performances", "sm_player_id", "players", "sm_id");
				if (playerIdsToImport.empty())
				{
					TCOUT << "Players are up to date." << endl;
					return true;
				}

				TCOUT << "Found " << playerIdsToImport.size() << " players to import." << endl;
				std::set<UpPlayer, PlayerCmp> playerSet;
				size_t count = 0;
				const int64_t lastPlayerId = *playerIdsToImport.crbegin();
				for (const int64_t playerId : playerIdsToImport)
				{
					playerSet.insert(ScrapePlayer(playerId));
					if (0 == playerSet.size() % 100 || lastPlayerId == playerId)
					{
						count += playerSet.size();
						TCOUT << "Got " << count << " player data." << endl;
						InsertOr(common::OnConflict::Ignore, db, playerSet);
						playerSet.clear();
					}
				}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Players");
				return false;
			}
			return true;
		}
		bool TopScorers(SQLite::Database& db)
		{
			TCOUT << "\nScraping top scorers..." << endl;
			const auto seasonsToImport = QueryWhereNotIn(db, "seasons", "sm_id", "top_goal_scorers", "sm_season_id", "is_current_season = 0");
			const auto activeSeasonsToUpdate = QueryWhereNotIn(db, "seasons", "sm_id", "top_goal_scorers", "sm_season_id", "is_current_season = 1");

			try
			{
				int count = 0;
				TCOUT << seasonsToImport.size() << " seasons found to scrape top scorers for..." << endl;
				for (const auto seasonId : seasonsToImport)
				{
					const auto topScorers = ScrapeTopScorers(seasonId, false);
					InsertOr(common::OnConflict::Ignore, db, topScorers.topGoalScorers);
					InsertOr(common::OnConflict::Ignore, db, topScorers.topAssistScorers);
					InsertOr(common::OnConflict::Ignore, db, topScorers.topCardScorers);

					const auto aggregatedTopScorers = ScrapeAggregatedTopScorers(seasonId, false);
					InsertOr(common::OnConflict::Ignore, db, aggregatedTopScorers.topGoalScorers);
					InsertOr(common::OnConflict::Ignore, db, aggregatedTopScorers.topAssistScorers);
					InsertOr(common::OnConflict::Ignore, db, aggregatedTopScorers.topCardScorers);

					if (0 == ++count % 10 || seasonsToImport.size() == count)
					{
						TCOUT << "Top scorers inserted for " << count << "/" << seasonsToImport.size() << " seasons." << endl;
					}
				}
				count = 0;
				TCOUT << activeSeasonsToUpdate.size() << " active seasons found to update top scorers for..." << endl;
				for (const auto seasonId : activeSeasonsToUpdate)
				{
					const auto topScorers = ScrapeTopScorers(seasonId, true);
					InsertOr(common::OnConflict::Replace, db, topScorers.topGoalScorers);
					InsertOr(common::OnConflict::Replace, db, topScorers.topAssistScorers);
					InsertOr(common::OnConflict::Replace, db, topScorers.topCardScorers);

					const auto aggregatedTopScorers = ScrapeAggregatedTopScorers(seasonId, true);
					InsertOr(common::OnConflict::Replace, db, aggregatedTopScorers.topGoalScorers);
					InsertOr(common::OnConflict::Replace, db, aggregatedTopScorers.topAssistScorers);
					InsertOr(common::OnConflict::Replace, db, aggregatedTopScorers.topCardScorers);

					TCOUT << "Top scorers updated for " << ++count << "/" << activeSeasonsToUpdate.size() << " active seasons." << endl;
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("TopScorers");
				return false;
			}
			return true;
		}
		bool Standings(SQLite::Database& db)
		{
			TCOUT << "\nScraping standings..." << endl;
			const auto oldSeasonsToScrape = QueryWhereNotIn(db, "seasons", "sm_id", "standings", "sm_season_id", "is_current_season = 0");
			const auto activeSeasonsToUpdate = QuerySingleColumnFromTable(db, "seasons", "sm_id", "is_current_season = 1");

			try
			{
				int count = 0;
				for (const auto seasonId : oldSeasonsToScrape)
				{
					std::set<UpStanding, StandingCmp> standings = ScrapeStandings(seasonId, false);
					InsertOr(common::OnConflict::Ignore, db, standings, false, false);
					if (0 == ++count % 10 || oldSeasonsToScrape.size() == count)
					{
						TCOUT << "Standings inserted for " << count << "/" << oldSeasonsToScrape.size() << " seasons." << endl;
					}
				}
				count = 0;
				for (const auto seasonId : activeSeasonsToUpdate)
				{
					std::set<UpStanding, StandingCmp> standings = ScrapeStandings(seasonId, true);
					InsertOr(common::OnConflict::Replace, db, standings, false, false);
					if (0 == ++count % 10 || activeSeasonsToUpdate.size() == count)
					{
						TCOUT << "Standings inserted for " << count << "/" << activeSeasonsToUpdate.size() << " seasons." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Standings");
				return false;
			}
			return true;
		}
		bool Venues(SQLite::Database& db)
		{
			TCOUT << "\nScraping venues..." << endl;
			const auto allSeasonIdsVec = QuerySingleColumnFromTable(db, "seasons", "sm_id");
			const std::set<int64_t> allSeasonIds(allSeasonIdsVec.cbegin(), allSeasonIdsVec.cend());

			try
			{
				int count = 0;
				vector<vector<int64_t>> seasonVenueIds;
				std::set<UpVenue, VenueCmp> allVenues;
				for (const auto seasonId : allSeasonIds)
				{
					std::set<UpVenue, VenueCmp> venues = ScrapeVenues(seasonId, true);
					while (!venues.empty())
					{
						const auto itPair = allVenues.insert(std::move(venues.extract(venues.cbegin()).value()));
						seasonVenueIds.push_back({ seasonId, itPair.first->operator->()->sm_id });
					}

					if (0 == ++count % 10 || allSeasonIds.size() == count)
					{
						TCOUT << "Venues scraped for " << count << "/" << allSeasonIds.size() << " seasons." << endl;
						TCOUT << allVenues.size() << " venues collected." << endl;
					}
				}
				TCOUT << "Upserting " << allVenues.size() << " venues." << endl;
				InsertIntoSwitchTable(db, "seasons_venues", seasonVenueIds);
				InsertOr(common::OnConflict::Replace, db, allVenues);
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Venues");
				return false;
			}
			return true;
		}
		bool Rounds(SQLite::Database& db)
		{
			TCOUT << "\nScraping rounds..." << endl;
			const auto oldMissingSeasons = QueryWhereNotIn(db, "seasons", "sm_id", "rounds", "sm_season_id", "is_current_season = 0");
			const auto activeSeasons = QueryWhereNotIn(db, "seasons", "sm_id", "rounds", "sm_season_id", "is_current_season = 1");

			try
			{
				int count = 0;
				TCOUT << oldMissingSeasons.size() << " old missing seasons found." << endl;
				for (const auto seasonId : oldMissingSeasons)
				{
					std::set<UpRound, RoundCmp> rounds = ScrapeRounds(seasonId, false);
					InsertOr(common::OnConflict::Ignore, db, rounds);
					if (0 == ++count % 10 || oldMissingSeasons.size() == count)
					{
						TCOUT << "Rounds inserted for " << count << "/" << oldMissingSeasons.size() << " old seasons." << endl;
					}
				}
				count = 0;
				TCOUT << activeSeasons.size() << " active seasons will be updated." << endl;
				for (const auto seasonId : activeSeasons)
				{
					std::set<UpRound, RoundCmp> rounds = ScrapeRounds(seasonId, true);
					InsertOr(common::OnConflict::Replace, db, rounds);
					if (0 == ++count % 10 || activeSeasons.size() == count)
					{
						TCOUT << "Rounds updated for " << count << "/" << activeSeasons.size() << " active seasons." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Rounds");
				return false;
			}
			return true;
		}
		bool Bookmakers(SQLite::Database& db)
		{
			TCOUT << "Scraping bookmakers..." << endl;
			std::set<UpBookmaker, BookmakerCmp> bookmakers = ScrapeAllBookmakers();
			InsertOr(common::OnConflict::Ignore, db, bookmakers);
			TCOUT << "Inserted " << bookmakers.size() << " bookmakers." << endl;
			return true;
		}
		bool Markets(SQLite::Database& db)
		{
			TCOUT << "Scraping markets..." << endl;
			std::set<UpMarket, MarketCmp> markets = ScrapeAllMarkets();
			InsertOr(common::OnConflict::Ignore, db, markets);
			TCOUT << "Inserted " << markets.size() << " markets." << endl;
			return true;
		}
		bool Odds(SQLite::Database& db, bool scrapeOldFixtures)
		{
			TCOUT << "\nScraping odds..." << endl;
			const auto bookmakers = Query<Bookmaker>(db, "name = 'Pncl' OR name = 'bet365' OR name = 'Sbo'");
			std::set<int64_t> bookmakerIds;
			for (const auto& upBook : bookmakers)
			{
				bookmakerIds.insert(upBook->sm_id);
			}

			try
			{
				size_t count = 0;
				if (scrapeOldFixtures)
				{
					const auto fixtureIdsWithoutOddsOldSeasons = QueryFixtureIdsWithoutOdds(db, false);
					TCOUT << fixtureIdsWithoutOddsOldSeasons.size() << " fixtures found without odds in old seasons." << endl;
					for (const auto fixtureId : fixtureIdsWithoutOddsOldSeasons)
					{
						std::set<UpOdds, OddsCmp> odds = ScrapeOdds(fixtureId, bookmakerIds, true);
						TCOUT << "Fixture (old season) " << ++count << "/" << fixtureIdsWithoutOddsOldSeasons.size() <<
							": odds.size(): " << odds.size() << endl;
						InsertOr(common::OnConflict::Ignore, db, odds);
						if (0 == count % 100 || fixtureIdsWithoutOddsOldSeasons.size() == count)
						{
							TCOUT << "Inserted odds for " << count << "/" << fixtureIdsWithoutOddsOldSeasons.size() << " fixtures." << endl;
						}
					}
				}
				const auto fixtureIdsWithoutOddsActiveSeasons = QueryFixtureIdsWithoutOdds(db, true);
				TCOUT << fixtureIdsWithoutOddsActiveSeasons.size() << " fixtures found without odds in active seasons." << endl;
				count = 0;
				for (const auto fixtureId : fixtureIdsWithoutOddsActiveSeasons)
				{
					std::set<UpOdds, OddsCmp> odds = ScrapeOdds(fixtureId, bookmakerIds, true);
					TCOUT << "Fixture (active season) " << ++count << "/" << fixtureIdsWithoutOddsActiveSeasons.size() <<
						": odds.size(): " << odds.size() << endl;
					InsertOr(common::OnConflict::Ignore, db, odds);
					if (0 == count % 100 || fixtureIdsWithoutOddsActiveSeasons.size() == count)
					{
						TCOUT << "Inserted odds for " << count << "/" << fixtureIdsWithoutOddsActiveSeasons.size() << " fixtures." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Odds");
				return false;
			}
			return true;
		}
		bool Coaches(SQLite::Database& db)
		{
			TCOUT << "\nScraping coaches..." << endl;
			std::set<int64_t> notImportedCoachIds = v2::QueryNotImportedCoachIds(db);
			if (notImportedCoachIds.empty())
			{
				TCOUT << "Coaches are up to date." << endl;
				return true;
			}

			try
			{
				std::set<UpCoach, CoachCmp> coachSet;
				for (const int64_t coachId : notImportedCoachIds)
				{
					coachSet.insert(ScrapeCoach(coachId, true));
				}
				if (!coachSet.empty())
				{
					InsertOr(common::OnConflict::Ignore, db, coachSet);
					TCOUT << "Inserted " << coachSet.size() << " coaches." << endl;
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Coaches");
				return false;
			}
			return true;
		}
		bool Stages(SQLite::Database& db)
		{
			TCOUT << "\nScraping stages..." << endl;
			try
			{
				{
					const auto oldSeasonIdsWithoutStages =
						QueryWhereNotIn(db, "seasons", "sm_id", "stages", "sm_season_id", "is_current_season = 0");
					TCOUT << "Found " << oldSeasonIdsWithoutStages.size() << " old seasons without stages." << endl;
					int count = 0;
					for (const auto seasonId : oldSeasonIdsWithoutStages)
					{
						std::set<UpStage, StageCmp> stages = ScrapeStages(seasonId, true);
						InsertOr(common::OnConflict::Ignore, db, stages);
						TCOUT << "Scraped " << ++count << "/" << oldSeasonIdsWithoutStages.size() << " old seasons." << endl;
						TCOUT << "Inserted " << stages.size() << " stages." << endl;
					}
				}
				{
					const auto activeSeasonIdsWithoutStages =
						QueryWhereNotIn(db, "seasons", "sm_id", "stages", "sm_season_id", "is_current_season = 1");
					TCOUT << "Found " << activeSeasonIdsWithoutStages.size() << " active seasons without stages." << endl;
					int count = 0;
					for (const auto seasonId : activeSeasonIdsWithoutStages)
					{
						std::set<UpStage, StageCmp> stages = ScrapeStages(seasonId, true);
						InsertOr(common::OnConflict::Ignore, db, stages);
						TCOUT << "Scraped " << ++count << "/" << activeSeasonIdsWithoutStages.size() << " active seasons." << endl;
						TCOUT << "Inserted " << stages.size() << " stages." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Stages");
				return false;
			}
			return true;
		}
		bool SeasonStatistics(SQLite::Database& db)
		{
			TCOUT << "\nScraping season statistics..." << endl;

			try
			{
				{
					const auto oldSeasonIdsWithoutStats =
						QueryWhereNotIn(db, "seasons", "sm_id", "season_statistics", "sm_season_id", "is_current_season = 0");
					TCOUT << oldSeasonIdsWithoutStats.size() << " old seasons were found without statistics." << endl;
					std::set<UpSeasonStatistics, SeasonStatisticsCmp> seasonStatisticsSet;
					int count = 0;
					for (const auto& seasonId : oldSeasonIdsWithoutStats)
					{
						seasonStatisticsSet.insert(std::move(ScrapeSeasonStatistics(seasonId, true)));
						if (0 == ++count % 10 || oldSeasonIdsWithoutStats.size() == count)
						{
							InsertOr(common::OnConflict::Replace, db, seasonStatisticsSet);
							TCOUT << "Scraped " << count << "/" << oldSeasonIdsWithoutStats.size() << " old season statistics." << endl;
							seasonStatisticsSet.clear();
						}
					}
				}
				{
					const auto activeSeasonIdsWithoutStatsVec = QuerySingleColumnFromTable(db, "seasons", "sm_id", "is_current_season = 1");
					const std::set<int64_t> activeSeasonIds(
						activeSeasonIdsWithoutStatsVec.cbegin(),
						activeSeasonIdsWithoutStatsVec.cend());
					TCOUT << activeSeasonIds.size() << " active seasons were found. Updating statistics." << endl;
					std::set<UpSeasonStatistics, SeasonStatisticsCmp> seasonStatisticsSet;
					int count = 0;
					for (const auto& seasonId : activeSeasonIds)
					{
						seasonStatisticsSet.insert(std::move(ScrapeSeasonStatistics(seasonId, true)));
						if (0 == ++count % 10 || activeSeasonIds.size() == count)
						{
							InsertOr(common::OnConflict::Replace, db, seasonStatisticsSet);
							TCOUT << "Scraped " << count << "/" << activeSeasonIds.size() << " active season statistics." << endl;
							seasonStatisticsSet.clear();
						}
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("SeasonStatistics");
				return false;
			}
			return true;
		}

		//functions to fix things
		bool LatestStandingsOfAllSeasons(SQLite::Database& db)
		{
			TCOUT << "\nScraping latest standings of all seasons..." << endl;
			const auto allSeasonIds = QuerySingleColumnFromTable(db, "seasons", "sm_id");

			try
			{
				int count = 0;
				for (const auto seasonId : allSeasonIds)
				{
					std::set<UpStanding, StandingCmp> standings = ScrapeStandings(seasonId, true);
					InsertOr(common::OnConflict::Replace, db, standings, false, false);
					if (0 == ++count % 10 || allSeasonIds.size() == count)
					{
						TCOUT << "Standings inserted for " << count << "/" << allSeasonIds.size() << " seasons." << endl;
					}
				}
			}
			catch (const TooManyAttemptsException& e)
			{
				LogTooManyAttempts("Standings");
				return false;
			}
			return true;
		}

		bool AllTeamsForAllSeasons(SQLite::Database& db, bool forceDownload)
		{
			TCOUT << "\nScraping teams..." << endl;
			const auto allSeasonIds = QuerySingleColumnFromTable(db, "seasons", "sm_id");
			//std::vector<int64_t> allSeasonIds{17138};


			TCOUT << "All seasons: " << allSeasonIds.size() << endl;

			try
			{
				vector<vector<int64_t>> seasonTeamIds;
				std::set<UpTeam, TeamCmp> allTeamsSet;
				int count = 0;
				for (const auto seasonId : allSeasonIds)
				{
					TCOUT << "Scraping teams of season " << ++count << "/" << allSeasonIds.size() << endl;
					auto teamSet = ScrapeTeamsBySeason(seasonId, forceDownload);
					for (const auto& upTeam : teamSet)
					{
						seasonTeamIds.emplace_back(vector<int64_t>{ seasonId, upTeam->sm_id });
					}
					while (!teamSet.empty())
					{
						allTeamsSet.insert(std::move(teamSet.extract(teamSet.begin())));
					}
					TCOUT << "Scraped " << allTeamsSet.size() << " teams in total." << endl;
				}
				InsertIntoSwitchTable(db, "seasons_teams", seasonTeamIds);
				InsertOr(common::OnConflict::Ignore, db, allTeamsSet, true);

				//repeat query, because the missing teams could have been inserted by the previos loop
				//allTeamsSet.clear();
				//for (const auto teamId : QueryWhereNotIn(db, "seasons_teams", "teams_sm_id", "teams", "sm_id"))
				//{
				//	allTeamsSet.insert(ScrapeTeam(teamId));
				//}
				//if (!allTeamsSet.empty())
				//{
				//	TCOUT << "Found " << allTeamsSet.size() << " missing teams. Inserting them to the db..." << endl;
				//	InsertOr(common::OnConflict::Ignore, db, allTeamsSet, true);
				//}
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Teams");
				return false;
			}
			return true;
		}

		
		bool RemoveDeletedMatches(SQLite::Database& db)
		{
			TCOUT << "Looking for deleted matches..." << endl;
			const std::set<int64_t> potentiallyDeletedMatchIds = QueryPotentiallyDeletedMatches(db);

			std::set<int64_t> deletedIds;
			TCOUT << "Checking " << potentiallyDeletedMatchIds.size() << " matches for deleted status." << endl;
			try
			{
				deletedIds = ScrapeDeletedStatus(potentiallyDeletedMatchIds);
			}
			catch (const TooManyAttemptsException&)
			{
				LogTooManyAttempts("Fixtures");
				return false;
			}
			TCOUT << "Found " << deletedIds.size() << " deleted matches. They will be removed from the db." << endl;
			DeleteFromTable(db, "fixtures", "sm_id", deletedIds);
			return true;
		}
	}

	void FullScrape()
	{
		const bool forceDownload = true;
		bool allDone = true;
		utils::date_and_time::StopWatch timer;
		SQLite::Database& db = GetDb(DbType::V2);
		PragmaForeignKeys(db, false);	//easier to fill up db without foreign keys

		do
		{
			allDone = true;
			try
			{
				const int roundTimeSec = 300;
				if (const int elapsedSec = timer.Elapsed<std::chrono::seconds>(); timer.Enabled() && elapsedSec < roundTimeSec)
				{
					const int sleepSec = roundTimeSec - elapsedSec;	//one hour plus 30 seconds just in case
					TCOUT << "Previous round took " << elapsedSec << " seconds. Sleeping for " << sleepSec << " seconds." << endl;
					const int notificationPeriodSec = 60;
					int nextNotification = 0 != sleepSec % notificationPeriodSec ?
						sleepSec % notificationPeriodSec :
						notificationPeriodSec;
					int sleepRemaining = roundTimeSec - timer.Elapsed<std::chrono::seconds>();
					while (0 < sleepRemaining)
					{
						std::this_thread::sleep_for(std::chrono::seconds(nextNotification));
						sleepRemaining = roundTimeSec - timer.Elapsed<std::chrono::seconds>();
						TCOUT << sleepRemaining / 60 << " more mins to sleep." << endl;
						nextNotification = notificationPeriodSec;
					}
				}
				timer.Restart();

				Continents(db);
				Countries(db, forceDownload);
				Leagues(db, forceDownload);
				Seasons(db);
				allDone &= RemoveDeletedMatches(db);
				allDone &= Fixtures(db, false, true);
				allDone &= Commentaries(db);
				allDone &= Teams(db, forceDownload);
				allDone &= PlayerPerformances(db);
				allDone &= Players(db);
				allDone &= TopScorers(db);
				allDone &= Standings(db);
				allDone &= Venues(db);
				allDone &= Rounds(db);
				Bookmakers(db);
				Markets(db);
				allDone &= Odds(db, false);
				allDone &= Coaches(db);
				allDone &= Stages(db);
				allDone &= SeasonStatistics(db);
			}
			catch (const SQLite::Exception& e)
			{
				TCOUT << e.getErrorStr() << ": " << e.what() << endl;
			}
		} while (!allDone);
		TCOUT << "================================================== Full scrape finished. ==================================================" << endl;
	}

	void QuickScrape()
	{
		const bool forceDownload = true;
		bool allDone = true;
		utils::date_and_time::StopWatch timer;
		SQLite::Database& db = GetDb(DbType::V2);
		PragmaForeignKeys(db, false);	//easier to fill up db without foreign keys

		do
		{
			allDone = true;
			try
			{
				const int roundTimeSec = 300;
				if (const int elapsedSec = timer.Elapsed<std::chrono::seconds>(); timer.Enabled() && elapsedSec < roundTimeSec)
				{
					const int sleepSec = roundTimeSec - elapsedSec;	//one hour plus 30 seconds just in case
					TCOUT << "Previous round took " << elapsedSec << " seconds. Sleeping for " << sleepSec << " seconds." << endl;
					const int notificationPeriodSec = 60;
					int nextNotification = 0 != sleepSec % notificationPeriodSec ?
						sleepSec % notificationPeriodSec :
						notificationPeriodSec;
					int sleepRemaining = roundTimeSec - timer.Elapsed<std::chrono::seconds>();
					while (0 < sleepRemaining)
					{
						std::this_thread::sleep_for(std::chrono::seconds(nextNotification));
						sleepRemaining = roundTimeSec - timer.Elapsed<std::chrono::seconds>();
						TCOUT << sleepRemaining / 60 << " more mins to sleep." << endl;
						nextNotification = notificationPeriodSec;
					}
				}
				timer.Restart();

				//Continents(db);
				//Countries(db, forceDownload);
				//Leagues(db, forceDownload);
				//Seasons(db);
				allDone &= RemoveDeletedMatches(db);
				allDone &= Fixtures(db, false, true);
				//allDone &= Commentaries(db);
				allDone &= Teams(db, forceDownload);	//Updates seasons_teams switch table. Not updating it can lead to crashes
				//allDone &= PlayerPerformances(db);
				//allDone &= Players(db);
				//allDone &= TopScorers(db);
				//allDone &= Standings(db);
				//allDone &= Venues(db);
				allDone &= Rounds(db);
				//Bookmakers(db);
				//Markets(db);
				//allDone &= Odds(db, false);
				//allDone &= Coaches(db);
				//allDone &= Stages(db);
				//allDone &= SeasonStatistics(db);
			}
			catch (const SQLite::Exception& e)
			{
				TCOUT << e.getErrorStr() << ": " << e.what() << endl;
			}
		} while (!allDone);
		TCOUT << "================================================== Quick scrape finished. ==================================================" << endl;
	}

	void FixMissingData()
	{
		const bool forceDownload = true;
		SQLite::Database& db = GetDb(DbType::V2);

		//LatestStandingsOfAllSeasons(db);
		//AllTeamsForAllSeasons(db, forceDownload);
		//ScrapeTeamsById(db, {});
		//RemoveDeletedMatches(db);
	}

	bool ScrapeTeamsById(SQLite::Database& db, const std::set<int64_t>& teamIds)
	{
		TCOUT << "\nScraping " << teamIds.size() << " teams by id..." << endl;
		try
		{
			std::set<UpTeam, TeamCmp> allTeamsSet;
			for (const auto teamId : teamIds)
			{
				allTeamsSet.insert(ScrapeTeam(teamId, true /*forceDownload*/));
			}
			if (!allTeamsSet.empty())
			{
				TCOUT << "Found " << allTeamsSet.size() << " missing teams: ";
				for (const UpTeam& upTeam : allTeamsSet)
				{
					TCOUT << upTeam->name << ", ";
				}
				TCOUT << "\nInserting them to the db..." << endl;
				InsertOr(common::OnConflict::Ignore, db, allTeamsSet, true);
			}
		}
		catch (const TooManyAttemptsException&)
		{
			LogTooManyAttempts("Teams");
			return false;
		}
		return true;
	}

}