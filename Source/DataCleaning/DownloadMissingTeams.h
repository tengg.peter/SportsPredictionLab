#pragma once
//lib
#include "DatabaseHandler/DatabaseHandler.h"
//std
#include <set>
namespace sports_prediction_lab::data_cleaning
{
	/*Wrapper around database_handler::v2::QueryMatchResults(). Scrapes if teams are missing from the db*/
	std::set<database_handler::v2::MatchResult> QueryMatchResults(
		SQLite::Database& db,
		const std::string& countryName,
		const std::string& leagueName,
		const std::string& beforeThisDate);
}