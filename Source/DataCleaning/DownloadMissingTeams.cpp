#include "SportsPredictionLab/Source/DataCleaning/DownloadMissingTeams.h"
#include "SportsPredictionLab/Source/Scraping/Scraping.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"
#include "DownloadMissingTeams.h"
//std
#include <algorithm>
#include <map>
#include <sstream>

namespace sports_prediction_lab::data_cleaning
{
	using namespace database_handler::common;
	using namespace database_handler::v2;

	std::set<MatchResult> QueryMatchResults(
		SQLite::Database& db,
		const std::string& countryName,
		const std::string& leagueName,
		const std::string& beforeThisDate)
	{
		std::set<MatchResult> matchResults = database_handler::v2::QueryMatchResults(db, countryName, leagueName, beforeThisDate);

		std::map<int64_t, const MatchResult*> fixtureIdsToPointers;
		for (const MatchResult& result : matchResults)
		{
			if (result.fixtureInfo.homeTeam.empty() || result.fixtureInfo.awayTeam.empty())
			{
				fixtureIdsToPointers[result.fixtureInfo.fixtureId] = &result;
			}
		}

		if (fixtureIdsToPointers.empty())
		{
			return matchResults;
		}

		utils::Logger::LogInfo("Found " + std::to_string(fixtureIdsToPointers.size()) + " match results with missing team names.", true);

		std::stringstream whereExpr;
		whereExpr << "sm_id IN (";
		bool first = true;
		for (const auto& [fixtureId, _] : fixtureIdsToPointers)
		{
			if (!first)
			{
				whereExpr << ", ";
			}
			whereExpr << fixtureId;
			first = false;
		}
		whereExpr << ")";

		const std::vector<UpFixture> fixturesWithMissingTeams = Query<Fixture>(db, whereExpr.str());
		std::set<int64_t> missingTeamIds;
		for (const UpFixture& upFixture : fixturesWithMissingTeams)
		{
			const auto pFixture = fixtureIdsToPointers.at(upFixture->sm_id);
			int64_t missingTeamId = 0;
			if (pFixture->fixtureInfo.homeTeam.empty())
			{
				missingTeamId = upFixture->sm_localteam_id;
			}
			if (pFixture->fixtureInfo.awayTeam.empty())
			{
				missingTeamId = upFixture->sm_visitorteam_id;
			}
			if (0 == missingTeamIds.count(missingTeamId))
			{
				utils::Logger::LogInfo("Missing team id: " + std::to_string(missingTeamId), true /*toConsoleToo*/);
				missingTeamIds.insert(missingTeamId);
			}
		}

		utils::Logger::LogInfo("Scraping missing teams...", true /*toConsoleToo*/);
		sport_monks_scraping::ScrapeTeamsById(db, missingTeamIds);
		return QueryMatchResults(db, countryName, leagueName, beforeThisDate);
	}
}