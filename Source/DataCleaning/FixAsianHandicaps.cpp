#include "SportsPredictionLab/Source/DataCleaning/FixAsianHandicaps.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"
//std
#include <vector>
namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;
	using namespace utils::string_operations;	//trim
	using std::endl;

	namespace
	{
		const std::vector<utils::String> illFormedHandicaps
		{
			STR("+0")		,
			STR("-0")		,
			STR("-0.25")		,
			STR("+0.25")		,
			STR("-0.5")		,
			STR("+0.5")		,
			STR("-0.75")		,
			STR("+0.75")		,
			STR("-1")		,
			STR("+1")		,
			STR("-1.5")		,
			STR("+1.5")		,
			STR("-1.25")		,
			STR("+1.25")		,
			STR("-1.75")		,
			STR("+1.75")		,
			STR("0")			,
			STR("-2")		,
			STR("+2")		,
			STR("-2.25")		,
			STR("+2.25")		,
			STR("-2.5")		,
			STR("+2.5")		,
			STR("-3")		,
			STR("+3")		,
			STR("-2.75")		,
			STR("+2.75")		,
			STR("-3.25")		,
			STR("+3.25")		,
			STR("-3.75")		,
			STR("+3.75")		,
			STR("-3.5")		,
			STR("+3.5")		,
			STR("-1.0")		,
			STR("+1.0")		,
			STR("-4")		,
			STR("+4")		,
			STR("-4.25")		,
			STR("+4.25")		,
			STR("-4.5")		,
			STR("+4.5")		,
			STR("+2.0")		,
			STR("-2.0")		,
			STR("0.0")		,
			STR("1.50")		,
			STR("0.00")		,
			STR("-0.5, -1.0"),
			STR("0.0, +0.5"),
			STR("+0.5, +1.0"),
			STR("-3.0")		,
			STR("-4.75")		,
			STR("-0.5,-1.0")	,
			STR("-0.50")		,
			STR("0.0, -0.5")	,
			STR("0.25")		,
			STR("+3.0")		,
			STR("+1.0,+1.5")	,
			STR("-1.5, -2.0"),
			STR("0.0,-0.5")	,
			STR("-5")		,
			STR("-5.25")		,
			STR("-5.5")		,
			STR("-1.0,-1.5")	,
			STR("-2.50")		,
			STR("0.50")	,
			STR("-2.00")		,
			STR("-1.0, -1.5"),
			STR("1.00")		,
			STR("-5.75")		,
			STR("+4.75")		,
			STR("0.0,+0.5")	,
			STR("+5")		,
			STR("+5.25")		,
			STR("+5.5")		,
			STR("1.0")		,
			STR("0.5")		,
			STR("0.5-1")		,
			STR("1")			,
			STR("1-1.5")		,
			STR("1.5-2")		,
			STR("0-0.5")		,
			STR("-0.0")		,
			STR("3-3.5")		,
			STR("+0.5,+1.0")	,
			STR("2.0")		,
			STR("0.75")		,
			STR("-1.50")		,
			STR("-4.0")		,
			STR("1.25")		,
			STR("+5.75")		,
			STR("1.5")	,
			STR("+4.0")		,
			STR("2")		,
			STR("-2.0,-2.5")	,
			STR("-3.00")		,
			STR("3")			,
			STR("-1.00")		,
			STR("1.75")		,
			STR("2.25")		,
			STR("-6")	,
			STR("+6")	,
			STR("2.5")		,
			STR("+6.5")		,
			STR("-6.5")		,
			STR("+9.5")		,
			STR("-9.5")		,
			STR("2.75")	,
			STR("-6.25")		,
			STR("+6.25")
		};

		const std::map<utils::String, utils::String> illFormedToWellFormed
		{
			{STR("-0.5, -1.0"), STR("-0.75")},
			{STR("0.0, +0.5"),	STR("+0.25")},
			{STR("+0.5, +1.0"),	STR("+0.75")},
			{STR("-0.5,-1.0"),	STR("-0.75")},
			{STR("0.0, -0.5"),	STR("-0.25")},
			{STR("+1.0,+1.5"),	STR("+1.25")},
			{STR("-1.5, -2.0"),	STR("-1.75")},
			{STR("0.0,-0.5"),	STR("-0.25")},
			{STR("-1.0,-1.5"),	STR("-1.25")},
			{STR("-1.0, -1.5"),	STR("-1.25")},
			{STR("0.0,+0.5"),	STR("+0.25")},
			{STR("0.5-1"),		STR("+0.75")},
			{STR("1-1.5"),		STR("+1.25")},
			{STR("1.5-2"),		STR("+1.75")},
			{STR("0-0.5"),		STR("+0.25")},
			{STR("-0.0"),		STR("+0.0")},
			{STR("3-3.5"),		STR("+3.25")},
			{STR("+0.5,+1.0"),	STR("+0.75")},
			{STR("-2.0,-2.5"),	STR("-2.25")},
			{STR("+1.0, +1.5"),	STR("+1.25")}
		};

		bool FixPipeInLabelFormat(UpOdds& upOdds)
		{
			if (size_t pipePos = upOdds->label.find(STR("|")); utils::String::npos != pipePos)
			{
				utils::String handicap = TrimCopy(upOdds->label.substr(0, pipePos));
				utils::String oneOrTwo = TrimCopy(upOdds->label.substr(pipePos + 1));
				if (STR("1") != oneOrTwo && STR("2") != oneOrTwo)
				{
					return false;
				}
				auto formatIt = illFormedToWellFormed.find(handicap);
				if (illFormedToWellFormed.cend() != formatIt)
				{
					upOdds->handicap_opt = formatIt->second;
				}
				else
				{
					upOdds->handicap_opt = handicap;	//well formedhndicap. Can be written directly.
				}
				upOdds->label = oneOrTwo;
				return true;
			}
			return false;
		}
	}

	void FixAsianHandicaps()
	{
		auto& db = GetDb(DbType::V2);
		TCOUT << "FixAsianHandicaps(): loading odds from db. This can take several minutes..." << endl;
		std::vector<UpOdds> odds = Query<Odds>(db, "sm_market_id = 28");	//28: Asian Handicap
		TCOUT << "FixAsianHandicaps(): loaded " << odds.size() << " odds from db." << endl;

		std::set<UpOdds, OddsCmp> oddsSet;


		auto newEndIt = std::remove_if(odds.begin(), odds.end(), [&odds](UpOdds& upOdds)
			{
				if (!upOdds->handicap_opt.has_value())
				{
					return !FixPipeInLabelFormat(upOdds);	//if it fixes it, then we don't delete. It has to be written back to the db
				}

				bool ret = false;
				if (auto formatIt = illFormedToWellFormed.find(upOdds->handicap_opt.value()); illFormedToWellFormed.cend() != formatIt)
				{
					upOdds->handicap_opt = formatIt->second;
				}
				else
				{
					ret = true;	//correct format, we do not want to write it back. Remove from the vector
				}

				return ret;
			});

		odds.erase(newEndIt, odds.end());
		TCOUT << "Writing back " << odds.size() << " odds to db..." << endl;
		InsertOr(OnConflict::Replace, db, odds);
	}
}