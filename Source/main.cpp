//own
#include "DatabaseHandler/Source/Common/Constants.h"
#include "DatabaseHandler/DatabaseHandler.h"
#include "Scraper/Scraper.h"
#include "SportsPredictionLab/Source/FileIO/Config/ConfigIo.h"
#include "SportsPredictionLab/Source/FileIO/Ratings/ForUpcomingMatches.h"
#include "SportsPredictionLab/Source/Ratings/GA/GaForEloRatingParams.h"
#include "SportsPredictionLab/Source/Scraping/Scraping.h"
#include "Utils/Utils.h"

#include "Source/NeuralNet/Training.h"

//testing
#include "SportsPredictionLab/Source/Ratings/RatingCalculation.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
#include "SportsPredictionLab/Source/BettingStrategyTester/TestBettingStrategy.h"
#include "SportsPredictionLab/Source/DataCleaning/FixAsianHandicaps.h"


//std
#include <fcntl.h>
#ifdef _WIN32
#include <io.h>	//_setmode()
#endif

struct Init
{
	utils::date_and_time::StopWatch m_timer;

	Init()
	{
#ifdef _WIN32
		_setmode(_fileno(stdout), _O_U16TEXT);
#endif
		utils::Logger::FilePath(STR("Output/Log"));
		utils::Logger::LogInfo("Program started", true);
		m_timer.Start();
	}
	~Init()
	{
		//TODO: this doesnt work, because the logger (the ofstream in it more specifically) is cleaned up before
		//m_timer.Stop();
		//utils::Logger::LogInfo(STR("Program exited after ") + 
		//	utils::date_and_time::SecsToTime(m_timer.Elapsed<std::chrono::seconds>()), true);
	}
} g_init;

int main(int argc, char* argv[])
{
	using namespace sports_prediction_lab;
	using std::endl;

	//FixAsianHandicaps();
	//TestBettingStrategy();
	//ContinueGaFromJsonFile(STR("Output/GA/generation 109 - 2021.01.18 11.50.42.json"));
	//TestRatingAlgorithms();
	//RunGa();
	//sport_monks_scraping::FixMissingData();
	//sport_monks_scraping::FullScrape();
	//sport_monks_scraping::QuickScrape();
	file_io::GenerateRatingFiles();
	return 0;

	//#if _WIN32
	//	TCOUT << "Program finished. Press Enter to exit." << std::endl;
	//	TCIN.get();
	//#endif

	utils::Logger::LogInfo("Exited normally.", true);
	return 0;
}