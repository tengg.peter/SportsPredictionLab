#include "SportsPredictionLab/Source/Prediction/Naive.h"

namespace sports_prediction_lab
{
	std::optional<double> ExpectedHomeValue(const database_handler::v2::PreMatchStats& stats)
	{
		if (stats.homeTeamHomeForOpt.has_value() && stats.awayTeamAwayAgainstOpt.has_value())
		{
			return (stats.homeTeamHomeForOpt.value() + stats.awayTeamAwayAgainstOpt.value()) / 2.0;
		}
		return std::nullopt;
	}

	std::optional<double> ExpectedAwayValue(const database_handler::v2::PreMatchStats& stats)
	{
		if (stats.homeTeamHomeAgainstOpt.has_value() && stats.awayTeamAwayForOpt.has_value())
		{
			return (stats.homeTeamHomeAgainstOpt.value() + stats.awayTeamAwayForOpt.value()) / 2.0;
		}
		return std::nullopt;
	}

	std::optional<double> ExpectedTotalValue(const database_handler::v2::PreMatchStats& stats)
	{
		if (stats.homeTeamHomeForOpt.has_value() && stats.awayTeamAwayAgainstOpt.has_value() &&
			stats.homeTeamHomeAgainstOpt.has_value() && stats.awayTeamAwayForOpt.has_value())
		{
			return (stats.homeTeamHomeForOpt.value() + stats.awayTeamAwayAgainstOpt.value() + 
				stats.homeTeamHomeAgainstOpt.value() + stats.awayTeamAwayForOpt.value()) / 2.0;
		}
		return std::nullopt;
	}

	std::optional<double> ExpectedAverageValue(const database_handler::v2::PreMatchStats& stats)
	{
		if (auto expTotal = ExpectedTotalValue(stats); expTotal.has_value())
		{
			return expTotal.value() / 2.0;
		}
		return std::nullopt;
	}
}