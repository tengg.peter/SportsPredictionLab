#include "DatabaseHandler/DatabaseHandler.h"

namespace sports_prediction_lab
{
	std::optional<double> ExpectedHomeValue(const database_handler::v2::PreMatchStats& stats);
	std::optional<double> ExpectedAwayValue(const database_handler::v2::PreMatchStats& stats);
	std::optional<double> ExpectedTotalValue(const database_handler::v2::PreMatchStats& stats);
	std::optional<double> ExpectedAverageValue(const database_handler::v2::PreMatchStats& stats);
}