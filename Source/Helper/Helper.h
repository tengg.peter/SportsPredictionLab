#pragma once
//own
#include "SportsPredictionLab/Source/Ratings/Algorithms/RatingPair.h"
//lib
#include "Utils/Utils.h"
//std


namespace sports_prediction_lab::helper
{
	utils::String GetStatFileName();
	utils::String GetOutputFileName();
	utils::String BuildCardsFileName(const utils::String& fileName = STR(""));
	void PrintTeamsSortedByRating(std::map<utils::String, RatingPair>& teamRatings, const utils::String& description = {});
}