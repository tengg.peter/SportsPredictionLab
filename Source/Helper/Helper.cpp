//own
#include "SportsPredictionLab/Source/Helper/Helper.h"
//std
#include <algorithm>

using namespace utils;

using std::endl;
using std::map;
using std::vector;

namespace sports_prediction_lab::helper
{
	utils::String GetStatFileName()
	{
		return STR("Stats - ") + date_and_time::DateTime::Now().ToDateTimeString() + STR(".csv");
	}

	utils::String GetOutputFileName()
	{
		return STR("Output - ") + date_and_time::DateTime::Now().ToDateTimeString() + STR(".csv");
	}

	utils::String BuildCardsFileName(const utils::String& fileName)
	{
		return STR("Cards ") + fileName + STR(" - ") + date_and_time::DateTime::Now().ToDateTimeString() + STR(".csv");
	}

	void PrintTeamsSortedByRating(std::map<utils::String, RatingPair>& teamRatings, const utils::String& description)
	{
		if (teamRatings.empty())
		{
			return;
		}

		vector<const std::pair<const utils::String, RatingPair>*> orderedPointers;
		for (const auto& pair : teamRatings)
		{
			orderedPointers.push_back(&pair);
		}

		std::sort(orderedPointers.begin(), orderedPointers.end(),
			[](const auto& a, const auto& b) -> bool
			{
				return a->second > b->second;
			});

		TCOUT << description << "\n";
		bool counterRatings = orderedPointers.front()->second.counterRating.has_value();
		for (size_t i = 0; i < orderedPointers.size(); i++)
		{
			TCOUT << i + 1 << ". " <<
				orderedPointers[i]->first << " (" <<
				orderedPointers[i]->second.rating;
			if (counterRatings)
			{
				TCOUT << ", " << orderedPointers[i]->second.counterRating.value();
			}
			TCOUT << ") (" << orderedPointers[i]->second.numMatches;
			if (counterRatings)
			{
				TCOUT << ", " << orderedPointers[i]->second.numCounterMatches.value();
			}
			TCOUT << ")\n";
		}
		TCOUT << endl;
	}

}
