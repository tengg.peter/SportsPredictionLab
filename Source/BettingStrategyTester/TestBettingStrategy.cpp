#include "SportsPredictionLab/Source/BettingStrategyTester/GoalDiffRatingStrategy.h"
#include "SportsPredictionLab/Source/BettingStrategyTester/MarketNameConsts.h"
#include "SportsPredictionLab/Source/BettingStrategyTester/TestBettingStrategy.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"

namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;
	using std::endl;

	void TestBettingStrategy()
	{
		auto& db = GetDb(DbType::V2);
		const OddsMap oddsMap = QueryOdds(db);
		const auto matchResults = QueryAllMatchResults(db);

		std::unique_ptr<ABettingStrategy> upBettingStrategy = std::make_unique<GoalDiffRatingStrategy>(matchResults);

		size_t numBets = 0;
		size_t numWins = 0;
		size_t numHalfWins = 0;
		size_t numPushes = 0;
		size_t numHalfLosses = 0;
		size_t numLosses = 0;
		double sumOdds = 0.0;
		double profit = 0.0;

		for (const auto& [leagueInfo, leagueResults] : matchResults)
		{
			for (const auto& matchResult : leagueResults)
			{
				const auto oddsIt = oddsMap.find(matchResult.fixtureInfo.fixtureId);
				if (oddsMap.cend() == oddsIt)
				{
					continue;
				}
				PlacedBetsMap marketsToBookmakersToLabelsToOdds =
					upBettingStrategy->PlaceBets(leagueInfo, matchResult.fixtureInfo.fixtureId, oddsIt->second);

				for (const auto& [market, bookmakersToLabelsToOdds] : marketsToBookmakersToLabelsToOdds)
				{
					for (const auto& [bookmaker, labelsToOdds] : bookmakersToLabelsToOdds)
					{
						for (const auto& [label, odds] : labelsToOdds)
						{
							++numBets;
							sumOdds += odds.value;
							if (ASIAN_HANDICAP_MARKET_NAME == market)
							{
								double handicappedResult;
								if (STR("1") == label)
								{
									handicappedResult = matchResult.home_goals - matchResult.away_goals + std::stod(odds.handicap);
								}
								else if (STR("2") == label)
								{
									handicappedResult = matchResult.away_goals - matchResult.home_goals + std::stod(odds.handicap);
								}
								else
								{
									throw std::runtime_error("Invalid label");
								}

								const double delta = 0.1;
								if (0.5 <= handicappedResult)
								{
									++numWins;
									profit += odds.value - 1;
								}
								else if(std::abs(handicappedResult - 0.25) < delta)	// == 0.25
								{
									++numHalfWins;
									profit += (odds.value - 1) / 2;
								}
								else if (std::abs(handicappedResult) < delta)	// == 0.0
								{
									++numPushes;
								}
								else if (std::abs(handicappedResult + 0.25) < delta)	// == -0.25
								{
									++numHalfLosses;
									profit -= 0.5;
								}
								else if(-0.5 >= handicappedResult)
								{
									++numLosses;
									profit -= 1;
								}
								else
								{
									throw std::runtime_error("Something is wrong with the result evaluation.");
								}
							}
							else if (THREE_WAY_MARKET_NAME == market)
							{
								const int goalDiff = matchResult.home_goals - matchResult.away_goals;
								if ((STR("1") == label && 0 < goalDiff) ||
									(STR("X") == label || STR("x") == label) && 0 == goalDiff ||
									STR("2") == label && 0 > goalDiff)
								{
									//three way bet won
									++numWins;
									profit += odds.value - 1;
								}
								else
								{
									++numLosses;
									profit -= 1;
								}
							}
						}
					}
				}
			}
		}
		/*	size_t numBets = 0;
			size_t numWins = 0;
			size_t numLosses = 0;
			size_t numPushes = 0;
			double sumOdds = 0.0;
			double profit = 0.0;*/

		TCOUT << "\nprofit: " << profit << endl;
		TCOUT << "wins: " << numWins << endl;
		TCOUT << "half wins: " << numHalfWins << endl;
		TCOUT << "pushes: " << numPushes << endl;
		TCOUT << "half losses: " << numHalfLosses << endl;
		TCOUT << "losses: " << numLosses << endl;
		TCOUT << "total bets: " << numBets << endl;
		TCOUT << "avg. odds: " << sumOdds / numBets << endl;
		TCOUT << "ROI: " << profit / numBets << endl;
	}
}

