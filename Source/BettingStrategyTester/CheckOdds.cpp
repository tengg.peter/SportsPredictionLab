#include "SportsPredictionLab/Source/BettingStrategyTester/CheckOdds.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"
//std
#include <numeric>

namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;

	namespace
	{
		bool CheckAhOddsPair(const OddsMarket& a, const OddsMarket& b)
		{
			const double sumProbability = (1 / a.value) + (1 / b.value);
			if (1 >= sumProbability)	//it should be greater than one because of the bookmakers' margin
			{
				return false;
			}
			return true;
		}
	}

	bool CheckAhOdds(const std::map<utils::String /*label*/, OddsMarket>& labelsToOdds)
	{
		if (0 != labelsToOdds.size() % 2)
		{
			return false;
		}

		for (const auto& [label, odds] : labelsToOdds)
		{
			if (STR("1") != label && STR("2") != label)
			{
				return false;
			}
			if (odds.handicap.empty() || 0 >= odds.value)
			{
				return false;
			}
		}

		std::map<utils::String, OddsMarket> oddsCopy = labelsToOdds;
		while (!oddsCopy.empty())
		{
			auto itA = oddsCopy.begin();
			const utils::String otherLabel = STR("1") == itA->first ? STR("2") : STR("1");
			utils::String foundSign, lookForSign;
			if (0 == itA->second.handicap.find(STR('+')))
			{
				foundSign = STR('+');
				lookForSign = STR('-');
			}
			else if (0 == itA->second.handicap.find(STR('-')))
			{
				foundSign = STR('-');
				lookForSign = STR('+');
			}
			else
			{
				//unrecognised handicap format
				return false;
			}
			const utils::String otherHandicap = utils::string_operations::Replace(itA->second.handicap, foundSign.c_str(), lookForSign.c_str());
			auto itB = std::find_if(oddsCopy.cbegin(), oddsCopy.cend(), [&otherLabel, &otherHandicap](const auto& labelOdds)
				{
					return labelOdds.first == otherLabel && labelOdds.second.handicap == otherHandicap;
				});
			if (oddsCopy.cend() == itB)
			{
				//the counter market is not found
				return false;
			}

			if (!CheckAhOddsPair(itA->second, itB->second))
			{
				return false;
			}
			oddsCopy.erase(itA);
			oddsCopy.erase(itB);
		}
		return true;
	}
	bool CheckThreeWayOdds(const std::map<utils::String, database_handler::v2::OddsMarket>& labelsToOdds)
	{
		if (3 != labelsToOdds.size())
		{
			return false;
		}

		double sumProb = 0.0;
		for (const auto& [label, odds] : labelsToOdds)
		{
			if (STR("1") != label && STR("X") != label && STR("x") != label && STR("2") != label)
			{
				return false;
			}
			if (0 >= odds.value)
			{
				return false;
			}
			sumProb += 1 / odds.value;
		}
		if (1 >= sumProb)
		{
			return false;	//it should be greater than one because of the bookmakers' margin
		}

		return true;
	}
}