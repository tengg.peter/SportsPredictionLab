#pragma once
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"
//std
#include <map>
namespace sports_prediction_lab
{
	using PlacedBetsMap = std::map<utils::String /*market*/,
		std::map<utils::String /*bookmaker*/, 
		std::map<utils::String /*label*/, database_handler::v2::OddsMarket>>>;

	class ABettingStrategy
	{
	public:
		ABettingStrategy(const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& matchResults)
			: m_matchResults(matchResults)
		{

		}

		virtual PlacedBetsMap PlaceBets(
			const database_handler::v2::LeagueInfo& leagueInfo, 
			int64_t fixtureId,
			const database_handler::v2::FixtureOddsMap& odds) = 0;

	protected:
		const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& m_matchResults;
	};
}