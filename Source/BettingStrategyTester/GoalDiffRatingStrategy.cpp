#include "SportsPredictionLab/Source/BettingStrategyTester/CheckOdds.h"
#include "SportsPredictionLab/Source/BettingStrategyTester/GoalDiffRatingStrategy.h"
#include "SportsPredictionLab/Source/BettingStrategyTester/MarketNameConsts.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/BestAlgorithms.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"

namespace sports_prediction_lab
{
	using namespace database_handler::v2;
	using std::endl;

	GoalDiffRatingStrategy::GoalDiffRatingStrategy(const std::map<LeagueInfo, std::set<MatchResult>>& matchResults)
		: ABettingStrategy(matchResults)
	{
		RatingCalculationTask task
		(
			GetBestAlgorithm(point_calc_names::GOAL_DIFF),
			[](const database_handler::v2::MatchResult& result)
			{
				return std::make_pair(result.home_goals, result.away_goals);
			}
		);
		m_taskVec.emplace_back(task);

		m_ratingStats = TestRatingAlgorithms(
			m_taskVec,
			false /*writeStatsToFile*/,
			true /*printLeagues*/,
			false /*printMatches*/,
			m_matchResults);
	}

	PlacedBetsMap GoalDiffRatingStrategy::PlaceBets(
		const database_handler::v2::LeagueInfo& leagueInfo,
		int64_t fixtureId,
		const database_handler::v2::FixtureOddsMap& marketToBookmakerToLabelToOdds)
	{
		PlacedBetsMap ret;
		const std::set<MatchResult>& leagueResults = m_matchResults.at(leagueInfo);
		const int numMatchesInASeason = leagueInfo.numTeams * (leagueInfo.numTeams - 1);
		if (nullptr == m_upRatingCalculator || m_upRatingCalculator->CurrentMatch()->fixtureInfo.leagueInfo != leagueInfo)	//TODO: may need end check
		{
			m_upRatingCalculator = std::make_unique<RatingCalculator<std::set<database_handler::v2::MatchResult>::const_iterator>>(
				leagueResults.cbegin(), leagueResults.cend(), m_taskVec, false);
		}

		m_upRatingCalculator->RunUntil(fixtureId);
		auto& task = m_taskVec.front();
		if (m_upRatingCalculator->MatchCount() < numMatchesInASeason * task.spRatingAlgo->warmUp)
		{
			//match found during warmup so we don't have enough prior statistics to bet
			return {};
		}
		else
		{
			const auto* pNextMatch = m_upRatingCalculator->NextMatch();
			m_upRatingCalculator->UpdateTeams(pNextMatch->fixtureInfo.seasonId);	//if a new season starts, loads the current teams.
			const RatingPair& homeTeamRating = task.Rating(pNextMatch->fixtureInfo.homeTeam);
			const RatingPair& awayTeamRating = task.Rating(pNextMatch->fixtureInfo.awayTeam);
			const auto& relevantRatings = task.spRatingAlgo->GetRelevantRatings(homeTeamRating, awayTeamRating);
			const RatingPair ratingDiff = relevantRatings.first - relevantRatings.second;

			RatingStatEntry aggregate = m_ratingStats.AggregateEntries(
				*task.spRatingAlgo, RatingStats::OVERALL_STATS, ratingDiff, 1000 /*minSampleSize*/);
			const auto& calculatedHandicaps = aggregate.GetHandicapLines();

			utils::String bestValueMarket;
			utils::String bestValueBookmaker;
			utils::String bestValueLabel;
			OddsMarket bestValueOdds;
			double bestValue = 1.0;

			//check asian handicap odds values
			if (0 < marketToBookmakerToLabelToOdds.count(ASIAN_HANDICAP_MARKET_NAME))
			{
				for (const auto& [bookmaker, labelsToOdds] : marketToBookmakerToLabelToOdds.at(ASIAN_HANDICAP_MARKET_NAME))
				{
					if (!CheckAhOdds(labelsToOdds))
					{
						continue;
					}

					for (const auto& [label, odds] : labelsToOdds)
					{
						const double bookieHandicap = std::stod(odds.handicap);
						auto handicapIt = calculatedHandicaps.find(bookieHandicap);
						if (calculatedHandicaps.cend() == handicapIt)
						{
							continue;
						}
						const double calculatedProb = label == STR("1") ? handicapIt->second.underProb :
							label == STR("2") ? handicapIt->second.overProb : /*should not happen*/ 0.0;

						const double value = calculatedProb * odds.value;
						if (value > bestValue)
						{
							bestValue = value;
							bestValueMarket = ASIAN_HANDICAP_MARKET_NAME;
							bestValueBookmaker = bookmaker;
							bestValueLabel = label;
							bestValueOdds = odds;
						}
					}
				}
			}

			//check three way odds values
			if (0 < marketToBookmakerToLabelToOdds.count(THREE_WAY_MARKET_NAME))
			{
				for (const auto& [bookmaker, labelsToOdds] : marketToBookmakerToLabelToOdds.at(THREE_WAY_MARKET_NAME))
				{
					if (!CheckThreeWayOdds(labelsToOdds))
					{
						continue;
					}

					for (const auto& [label, odds] : labelsToOdds)
					{
						const double bookieHandicap = label == STR("1") ? -0.5 :
							label == STR("2") ? +0.5 : /*X: it will trigger continue*/ -1000.0;

						auto handicapIt = calculatedHandicaps.find(bookieHandicap);
						if (calculatedHandicaps.cend() == handicapIt)
						{
							continue;
						}
						const double calculatedProb = label == STR("1") ? handicapIt->second.underProb :
							label == STR("2") ? handicapIt->second.overProb : /*should not happen*/ 0.0;

						const double value = calculatedProb * odds.value;
						if (value > bestValue)
						{
							bestValue = value;
							bestValueMarket = THREE_WAY_MARKET_NAME;
							bestValueBookmaker = bookmaker;
							bestValueLabel = label;
							bestValueOdds = odds;
						}
					}
				}
			}

			if (1.1 > bestValue)
			{
				return {};
			}

			ret[bestValueMarket][bestValueBookmaker][bestValueLabel] = std::move(bestValueOdds);
			return ret;
		}
		return ret;
	}

}