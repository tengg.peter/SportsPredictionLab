#pragma once
#include "SportsPredictionLab/Source/BettingStrategyTester/ABettingStrategy.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"

namespace sports_prediction_lab
{
	class GoalDiffRatingStrategy : public ABettingStrategy
	{
	public:
		GoalDiffRatingStrategy(const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& matchResults);
		
		virtual PlacedBetsMap PlaceBets(
			const database_handler::v2::LeagueInfo& leagueInfo,
			int64_t fixtureId,
			const database_handler::v2::FixtureOddsMap& marketToBookmakerToLabelToOdds) override;
		
	protected:
		RatingStats m_ratingStats;
		std::vector<RatingCalculationTask> m_taskVec;
		std::unique_ptr<RatingCalculator<std::set<database_handler::v2::MatchResult>::const_iterator>> m_upRatingCalculator;
	};
}