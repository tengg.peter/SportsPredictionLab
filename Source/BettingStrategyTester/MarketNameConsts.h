#include "Utils/Utils.h"

namespace sports_prediction_lab
{
	const utils::String THREE_WAY_MARKET_NAME = STR("3Way Result");
	const utils::String ASIAN_HANDICAP_MARKET_NAME = STR("Asian Handicap");
}