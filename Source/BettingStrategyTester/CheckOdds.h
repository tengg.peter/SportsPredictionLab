#pragma once
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"

namespace sports_prediction_lab
{
	bool CheckAhOdds(const std::map<utils::String /*label*/, database_handler::v2::OddsMarket>& labelsToOdds);
	bool CheckThreeWayOdds(const std::map<utils::String /*label*/, database_handler::v2::OddsMarket>& labelsToOdds);
}