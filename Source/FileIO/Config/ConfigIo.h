#pragma once
//own
#include "AiLab/NeuralNet.h"
#include "Utils/Utils.h"
#include "SportsPredictionLab/Source/FileIO/Config/Config.h"

//3rd party
#include "yaml-cpp/yaml.h"

//std
#include <array>
#include <vector>

namespace sports_prediction_lab::file_io::config
{
	const std::string TRAIN_NETWORKS_VAL{ "trainNetworks" };
	const std::string MAKE_CARD_FILES_VAL{ "makeCardFiles" };
	const std::string NONE_VAL{ "none" };
	const std::string XAVIER_VAL{ "Xavier" };

	const Config& GetConfig();
}