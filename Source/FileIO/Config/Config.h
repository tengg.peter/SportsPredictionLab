#pragma once
//own
//#include "SportsPredictionLab/Source/FileIO/Config/Config.h"

//std
#include <optional>
#include <string>
#include <vector>

namespace sports_prediction_lab::file_io::config
{
	struct Competition
	{
		utils::String country;
		utils::String competition;
		int startYear;
		int endYear;
	};

	struct Layer
	{
		utils::String activationFunction;
		std::optional<utils::String> outputFunction;
		int numNeurons;
	};

	struct Mlp
	{
		std::vector<Layer> layers;
	};

	struct StopConditions
	{
		int minNumIterations;
		int maxNumIterations;
		double generalisationError;
		double stopIfNotImprovedEpochRatio;
	};

	struct TrainingTask
	{
		Mlp mlp;
		double learningRate;
		double momentum;
		double testSamplingFrequency;
		int miniBatchSize;
		bool setUpInputLayerScaling;
		utils::String weightInitialisation;
		utils::String lossFunction;
		StopConditions stopConditions;
		utils::String neuralNetJsonFilePath;
		utils::String trainingErrorsFilePath;
		utils::String errorDistributionFilePath;
		utils::String country;
		utils::String competition;
		int times;
	};

	struct Config
	{
		std::string functionToRun;
		std::vector<Competition> makeCardFiles;
		std::vector<TrainingTask> trainingTasks;
	};

}