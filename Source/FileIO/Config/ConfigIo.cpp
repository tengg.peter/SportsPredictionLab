//own
#include "Scraper/Source/Constants.h"
#include "SportsPredictionLab/Source/FileIO/Config/ConfigIo.h"
#include "Utils/Utils.h"

//std
#include <filesystem>

namespace sports_prediction_lab::file_io::config
{
	const std::string CONFIG_YAML_FILE_NAME = "Config.yaml";

	//functions
	const std::string FUNCTION_TO_RUN_KEY = "1_functionToRun";


	//card files config
	const std::string COMPETITION_KEY = "competition";
	const std::string COMPETITIONS_KEY = "competitions";
	const std::string COUNTRY_KEY = "country";
	const std::string END_YEAR_KEY = "endYear";
	const std::string MAKE_CARD_FILES_KEY = "2_makeCardFiles";
	const std::string START_YEAR_KEY = "startYear";

	//training config
	const std::string ACTIVATION_FUNCTION_KEY = "activationFunction";
	const std::string ERROR_DISTRIBUTION_FILE_PATH_KEY = "errorDistributionFilePath";
	const std::string GENERALISATION_ERROR_KEY = "generalisationError";
	const std::string LEARNING_RATE_KEY = "learningRate";
	const std::string LOSS_FUNCTION_KEY = "lossFunction";
	const std::string MAX_NUM_ITERATIONS_KEY = "maxNumIterations";
	const std::string MIN_NUM_ITERATIONS_KEY = "minNumIterations";
	const std::string MINI_BATCH_SIZE_KEY = "miniBatchSize";
	const std::string MLP_KEY = "mlp";
	const std::string MOMENTUM_KEY = "momentum";
	const std::string NEURAL_NET_JSON_FILE_PATH_KEY = "neuralNetJsonFilePath";
	const std::string NUM_NEURONS_KEY = "numNeurons";
	const std::string OUTPUT_FUNCTION_KEY = "outputFunction";
	const std::string SET_UP_INPUT_LAYER_SCALING_KEY = "setUpInputLayerScaling";
	const std::string STOP_CONDITIONS_KEY = "stopConditions";
	const std::string STOP_IF_NOT_IMPROVED_EPOCH_RATIO_KEY = "stopIfNotImprovedEpochRatio";
	const std::string TEST_SAMPLING_FREQUENCY_KEY = "testSamplingFrequency";
	const std::string TIMES_KEY = "times";
	const std::string TRAINING_ERRORS_FILE_PATH_KEY = "trainingErrorsFilePath";
	const std::string TRAINING_TASKS_KEY = "3_trainingTasks";
	const std::string WEIGHT_INITIALISATION_KEY = "weightInitialisation";

	//generating the config file
	static void AddFunctionToRunSection(YAML::Emitter& emitter)
	{
		emitter << YAML::Key << FUNCTION_TO_RUN_KEY;
		emitter << YAML::Value << MAKE_CARD_FILES_VAL;
		emitter << YAML::Comment(
			"Possible values are: " + MAKE_CARD_FILES_VAL + ", " + TRAIN_NETWORKS_VAL + ", " + NONE_VAL +
			" The last one is used for debugging.");
		emitter << YAML::Newline;
		emitter << YAML::Newline;
	}

	static void AddCardFilesSection(YAML::Emitter& emitter)
	{
		const std::string startYear{ "2016" };
		const std::string endYear{ "2019" };

		emitter << YAML::Key << MAKE_CARD_FILES_KEY;
		emitter << YAML::Value << YAML::BeginMap;	//make card files
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::Key << COMPETITIONS_KEY;
		emitter << YAML::Value << YAML::BeginSeq;

		using namespace scraper;
		using namespace scraper::competitions;
		using utils::string_conversion::UtilsStringToStdString;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::AUSTRALIA);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(australia::A_LEAGUE);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::CHINA);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(china::CSL);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::ENGLAND);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(england::PREMIER_LEAGUE);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::ENGLAND);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(england::CHAMPIONSHIP);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::FRANCE);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(france::LIGUE_1);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::GERMANY);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(germany::BUNDESLIGA);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::ITALY);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(italy::SERIE_A);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::NETHERLANDS);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(netherlands::EREDIVISIE);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::BeginMap;
		emitter << YAML::Key << COUNTRY_KEY;
		emitter << YAML::Value << UtilsStringToStdString(countries::SPAIN);
		emitter << YAML::Key << COMPETITION_KEY;
		emitter << YAML::Value << UtilsStringToStdString(spain::PRIMERA_DIVISION);
		emitter << YAML::Key << START_YEAR_KEY;
		emitter << YAML::Value << startYear;
		emitter << YAML::Key << END_YEAR_KEY;
		emitter << YAML::Value << endYear;
		emitter << YAML::EndMap;
		emitter << YAML::Newline;

		emitter << YAML::EndSeq;
		emitter << YAML::EndMap;	//make card files
		emitter << YAML::Newline;
	}

	static void AddTrainingTasksSection(YAML::Emitter& emitter)
	{
		ai_lab::BackPropParams defaultParams;
		using utils::string_conversion::UtilsStringToStdString;

		emitter << YAML::Key << TRAINING_TASKS_KEY;
		emitter << YAML::Value << YAML::BeginSeq;	//training tasks

		for (size_t i = 0; i < 16; i++)
		{
			emitter << YAML::BeginMap;	//training task
			emitter << YAML::Key << MLP_KEY;
			emitter << YAML::BeginSeq;	//layers
			emitter << YAML::BeginMap;	//layer
			emitter << YAML::Key << ACTIVATION_FUNCTION_KEY
				<< YAML::Value << UtilsStringToStdString(ai_lab::LinearScalingActivationFunction::TYPE_STRING);
			emitter << YAML::Key << NUM_NEURONS_KEY << YAML::Value << 86;
			emitter << YAML::EndMap;	//layer
			emitter << YAML::BeginMap;	//layer
			if (0 == i % 2)
			{
				emitter << YAML::Key << ACTIVATION_FUNCTION_KEY
					<< YAML::Value << UtilsStringToStdString(ai_lab::IdentityActivationFunction::TYPE_STRING);
				emitter << YAML::Key << NUM_NEURONS_KEY << YAML::Value << 1;
			}
			else
			{
				emitter << YAML::Key << ACTIVATION_FUNCTION_KEY
					<< YAML::Value << UtilsStringToStdString(ai_lab::IdentityActivationFunction::TYPE_STRING);
				emitter << YAML::Key << OUTPUT_FUNCTION_KEY
					<< YAML::Value << UtilsStringToStdString(ai_lab::SoftmaxOutputFunction::TYPE_STRING);
				emitter << YAML::Key << NUM_NEURONS_KEY << YAML::Value << 16;
			}
			emitter << YAML::EndMap;	//layer
			emitter << YAML::EndSeq;	//layers
			emitter << YAML::Key << LEARNING_RATE_KEY;
			emitter << YAML::Value << 0.00001;
			emitter << YAML::Key << MOMENTUM_KEY;
			emitter << YAML::Value << 0.9;
			emitter << YAML::Key << TEST_SAMPLING_FREQUENCY_KEY;
			emitter << YAML::Value << 0.25;
			emitter << YAML::Key << MINI_BATCH_SIZE_KEY;
			emitter << YAML::Value << 10;
			emitter << YAML::Key << SET_UP_INPUT_LAYER_SCALING_KEY;
			emitter << YAML::Value << true;
			emitter << YAML::Key << WEIGHT_INITIALISATION_KEY;
			emitter << YAML::Value << XAVIER_VAL;
			emitter << YAML::Key << LOSS_FUNCTION_KEY;
			emitter << YAML::Value << UtilsStringToStdString(ai_lab::SquaredErrorLossFunction::TYPE_STRING);;
			emitter << YAML::Key << STOP_CONDITIONS_KEY;
			emitter << YAML::Value << YAML::BeginMap;	//stop conditions
			emitter << YAML::Key << MIN_NUM_ITERATIONS_KEY;
			emitter << YAML::Value << 10;
			emitter << YAML::Key << MAX_NUM_ITERATIONS_KEY;
			emitter << YAML::Value << 1000;
			emitter << YAML::Key << GENERALISATION_ERROR_KEY;
			emitter << YAML::Value << 0.1;
			emitter << YAML::Key << STOP_IF_NOT_IMPROVED_EPOCH_RATIO_KEY;
			emitter << YAML::Value << 1;
			emitter << YAML::EndMap;	//stop conditions
			emitter << YAML::Key << NEURAL_NET_JSON_FILE_PATH_KEY;
			emitter << YAML::Value << UtilsStringToStdString(defaultParams.neuralNetJsonFilePath);
			emitter << YAML::Key << TRAINING_ERRORS_FILE_PATH_KEY;
			emitter << YAML::Value << UtilsStringToStdString(defaultParams.trainingErrorsFilePath);
			emitter << YAML::Key << ERROR_DISTRIBUTION_FILE_PATH_KEY;
			emitter << YAML::Value << UtilsStringToStdString(defaultParams.errorDistributionFilePath);
			const std::string comment{
				"Leave " + COUNTRY_KEY + " and " + COMPETITION_KEY + " empty to use the whole database." };
			emitter << YAML::Newline << YAML::Comment(comment);

			switch (i)
			{
				using namespace scraper;
				using namespace scraper::competitions;

			case 0:
			case 1:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::AUSTRALIA);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(australia::A_LEAGUE);
				break;
			case 2:
			case 3:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::ENGLAND);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(england::CHAMPIONSHIP);
				break;
			case 4:
			case 5:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::ENGLAND);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(england::PREMIER_LEAGUE);
				break;
			case 6:
			case 7:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::FRANCE);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(france::LIGUE_1);
				break;
			case 8:
			case 9:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::GERMANY);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(germany::BUNDESLIGA);
				break;
			case 10:
			case 11:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::ITALY);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(italy::SERIE_A);
				break;
			case 12:
			case 13:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::NETHERLANDS);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(netherlands::EREDIVISIE);
				break;
			case 14:
			case 15:
				emitter << YAML::Key << COUNTRY_KEY;
				emitter << YAML::Value << UtilsStringToStdString(countries::SPAIN);
				emitter << YAML::Key << COMPETITION_KEY;
				emitter << YAML::Value << UtilsStringToStdString(spain::PRIMERA_DIVISION);
				break;
			}
			emitter << YAML::Key << TIMES_KEY;
			emitter << YAML::Value << 10;
			emitter << YAML::EndMap; //training task
			emitter << YAML::Newline;
		}
		emitter << YAML::EndSeq;	//training tasks
		emitter << YAML::Newline;
	}

	static void GenerateDefaultYaml()
	{
		if (std::filesystem::exists(CONFIG_YAML_FILE_NAME))
		{
			utils::Logger::LogInfo(CONFIG_YAML_FILE_NAME + " already exists. Default yaml is not generated.", true);
			return;
		}

		YAML::Emitter configYaml;
		configYaml << YAML::BeginMap;
		AddFunctionToRunSection(configYaml);
		AddCardFilesSection(configYaml);
		AddTrainingTasksSection(configYaml);
		configYaml << YAML::EndMap;

		{
			std::ofstream yamlFile;
			yamlFile.open(CONFIG_YAML_FILE_NAME, std::ios::out);
			yamlFile << configYaml.c_str();
			yamlFile.close();
		}
		utils::Logger::LogWarning("Could not load " + CONFIG_YAML_FILE_NAME + ". Creating a default one.", true);
	}

	//reading the config file
	static YAML::Node LoadOrCreateInputYaml()
	{
		try
		{
			return YAML::LoadFile(CONFIG_YAML_FILE_NAME);
		}
		catch (const std::exception& e)
		{
			GenerateDefaultYaml();
			return YAML::LoadFile(CONFIG_YAML_FILE_NAME);
		}
	}

	const Config& GetConfig()
	{
		static bool configRead = false;
		static Config config;

		using utils::string_conversion::StdStringToUtilsString;
		if (!configRead)
		{
			YAML::Node configYaml = LoadOrCreateInputYaml();
			config.functionToRun = configYaml[FUNCTION_TO_RUN_KEY].as<std::string>();

			for (const YAML::Node comp : configYaml[MAKE_CARD_FILES_KEY][COMPETITIONS_KEY])
			{
				config.makeCardFiles.emplace_back(
					Competition
					{
						StdStringToUtilsString(comp[COUNTRY_KEY].as<std::string>()),
						StdStringToUtilsString(comp[COMPETITION_KEY].as<std::string>()),
						comp[START_YEAR_KEY].as<int>(),
						comp[END_YEAR_KEY].as<int>(),
					});
			}

			for (const YAML::Node& trainingTaskYaml : configYaml[TRAINING_TASKS_KEY])
			{
				TrainingTask trainingTask;
				trainingTask.learningRate = trainingTaskYaml[config::LEARNING_RATE_KEY].as<double>();
				trainingTask.momentum = trainingTaskYaml[config::MOMENTUM_KEY].as<double>();
				trainingTask.testSamplingFrequency = trainingTaskYaml[config::TEST_SAMPLING_FREQUENCY_KEY].as<double>();
				trainingTask.miniBatchSize = trainingTaskYaml[config::MINI_BATCH_SIZE_KEY].as<int>();
				trainingTask.setUpInputLayerScaling = trainingTaskYaml[config::SET_UP_INPUT_LAYER_SCALING_KEY].as<bool>();
				trainingTask.weightInitialisation = StdStringToUtilsString(trainingTaskYaml[config::WEIGHT_INITIALISATION_KEY].as<std::string>());
				trainingTask.neuralNetJsonFilePath = StdStringToUtilsString(trainingTaskYaml[config::NEURAL_NET_JSON_FILE_PATH_KEY].as<std::string>());
				trainingTask.trainingErrorsFilePath = StdStringToUtilsString(trainingTaskYaml[config::TRAINING_ERRORS_FILE_PATH_KEY].as<std::string>());
				trainingTask.errorDistributionFilePath = StdStringToUtilsString(trainingTaskYaml[config::ERROR_DISTRIBUTION_FILE_PATH_KEY].as<std::string>());

				trainingTask.stopConditions.minNumIterations = trainingTaskYaml[config::STOP_CONDITIONS_KEY][config::MIN_NUM_ITERATIONS_KEY].as<int>();
				trainingTask.stopConditions.maxNumIterations = trainingTaskYaml[config::STOP_CONDITIONS_KEY][config::MAX_NUM_ITERATIONS_KEY].as<int>();
				trainingTask.stopConditions.generalisationError = trainingTaskYaml[config::STOP_CONDITIONS_KEY][config::GENERALISATION_ERROR_KEY].as<double>();;
				trainingTask.stopConditions.stopIfNotImprovedEpochRatio = trainingTaskYaml[config::STOP_CONDITIONS_KEY][config::STOP_IF_NOT_IMPROVED_EPOCH_RATIO_KEY].as<double>();

				trainingTask.country = StdStringToUtilsString(trainingTaskYaml[config::COUNTRY_KEY].as<std::string>());
				trainingTask.competition = StdStringToUtilsString(trainingTaskYaml[config::COMPETITION_KEY].as<std::string>());

				for (const YAML::Node& layerYaml : trainingTaskYaml[config::MLP_KEY])
				{
					Layer layer;
					layer.activationFunction = StdStringToUtilsString(layerYaml[config::ACTIVATION_FUNCTION_KEY].as<std::string>());
					layer.numNeurons = layerYaml[config::NUM_NEURONS_KEY].as<int>();
					if (layerYaml[config::OUTPUT_FUNCTION_KEY])
					{
						layer.outputFunction = StdStringToUtilsString(layerYaml[config::OUTPUT_FUNCTION_KEY].as<std::string>());
					}
					trainingTask.mlp.layers.emplace_back(layer);


					trainingTask.times = trainingTaskYaml[config::TIMES_KEY].as<int>();
				}

			}
			configRead = true;
		}
		return config;
	}
}