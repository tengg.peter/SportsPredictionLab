#pragma once
//own
#include "SportsPredictionLab/Source/FileIO/FileIO.h"
//lib
#include "Utils/Utils.h"
//std

namespace sports_prediction_lab::file_io
{
	const utils::String RATINGS_OUTPUT_PATH = OUTPUT_FOLDER_NAME + STR("Ratings/");

	void GenerateRatingFiles();
}