//own
#include "SportsPredictionLab/Source/DataCleaning/DownloadMissingTeams.h"
#include "SportsPredictionLab/Source/FileIO/FileIO.h"
#include "SportsPredictionLab/Source/FileIO/Ratings/ForUpcomingMatches.h"
#include "SportsPredictionLab/Source/Helper/Helper.h"
#include "SportsPredictionLab/Source/Prediction/Naive.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/BestAlgorithms.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"
#include "SportsPredictionLab/Source/Ratings/RatingCalculation.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"

//lib
#include "DatabaseHandler/DatabaseHandler.h"
//std
#include <filesystem>
#include <numeric>	//std::accumulate


namespace sports_prediction_lab::file_io
{
	using namespace database_handler;
	using namespace database_handler::common;
	using namespace database_handler::v2;
	using namespace utils::string_conversion;

	using utils::file_io::CsvFileWriter;
	using std::endl;

	namespace
	{
		void AddFixtureInfoTable(CsvFileWriter& fileWriter, const FixtureInfo& fixtureInfo)
		{
			using utils::string_operations::Replace;
			fileWriter.AddTable(0, 0,
				{
					{
						StdStringToUtilsString(fixtureInfo.leagueInfo.country),
						StdStringToUtilsString(fixtureInfo.leagueInfo.league),
						StdStringToUtilsString(fixtureInfo.homeTeam),
						StdStringToUtilsString(fixtureInfo.awayTeam),
						fixtureInfo.dateTime.ToDateTimeString()
					}
				});
		}

		void AddThreeWayRatingTable(
			CsvFileWriter& fileWriter,
			size_t row,
			size_t col,
			const FixtureInfo& fixtureInfo,
			const RatingCalculationTask& ratingTask,
			const RatingStats& ratingStats)
		{
			const auto& homeRatings = ratingTask.RunningRatings().at(fixtureInfo.homeTeam);
			const auto& awayRatings = ratingTask.RunningRatings().at(fixtureInfo.awayTeam);
			const std::pair<RatingPair, RatingPair> relevantRatings = ratingTask.spRatingAlgo->GetRelevantRatings(homeRatings, awayRatings);
			const RatingPair ratingDiff = relevantRatings.first - relevantRatings.second;
			const auto aggregatedStat = ratingStats.AggregateEntries(*ratingTask.spRatingAlgo, ratingStats.OVERALL_STATS, ratingDiff, 1000);

			const double homeProb = aggregatedStat.GetProbability(1.0);
			const double drawProb = aggregatedStat.GetProbability(0.5);
			const double awayProb = aggregatedStat.GetProbability(0.0);

			using utils::string_conversion::FloatToUtilsString;
			const utils::String homeOdds{ homeProb != 0 ? FloatToUtilsString(1 / homeProb, 3) : STR(" - ") };
			const utils::String drawOdds{ drawProb != 0 ? FloatToUtilsString(1 / drawProb, 3) : STR(" - ") };
			const utils::String awayOdds{ awayProb != 0 ? FloatToUtilsString(1 / awayProb, 3) : STR(" - ") };
			const utils::String diffString{ (ratingDiff.rating > 0 ? STR("+") : STR("")) + utils::ToString(ratingDiff.rating) };

			fileWriter.AddTable(row, col,
				{
					{
						STR(""),
						StdStringToUtilsString(fixtureInfo.homeTeam),
						STR("Draw"),
						StdStringToUtilsString(fixtureInfo.awayTeam),
						STR("Notes"),
					},
					{
						ratingTask.spRatingAlgo->Description(),
						utils::ToString(relevantRatings.first.rating),
						STR(""),
						utils::ToString(relevantRatings.second.rating),
						diffString,
					},
					{
						STR(""),
						FloatToUtilsString(homeProb, 3),
						FloatToUtilsString(drawProb, 3),
						FloatToUtilsString(awayProb, 3),
						aggregatedStat.RatingRange() + STR(" ") + utils::ToString(aggregatedStat.SampleSize()),
					},
					{
						STR(""),
						homeOdds,
						drawOdds,
						awayOdds,
					}
				});
		}

		void GoalDiffRatingTable(
			CsvFileWriter& fileWriter,
			size_t row,
			size_t col,
			const FixtureInfo& fixtureInfo,
			const RatingCalculationTask& ratingTask,
			const RatingStats& ratingStats)
		{
			const auto& homeRatings = ratingTask.RunningRatings().at(fixtureInfo.homeTeam);
			const auto& awayRatings = ratingTask.RunningRatings().at(fixtureInfo.awayTeam);
			const std::pair<RatingPair, RatingPair> relevantRatings = ratingTask.spRatingAlgo->GetRelevantRatings(homeRatings, awayRatings);
			const RatingPair ratingDiff = relevantRatings.first - relevantRatings.second;
			const auto aggregatedStat = ratingStats.AggregateEntries(*ratingTask.spRatingAlgo, ratingStats.OVERALL_STATS, ratingDiff, 1000);

			using utils::string_conversion::FloatToUtilsString;
			const utils::String diffString{ (ratingDiff.rating > 0 ? STR("+") : STR("")) + utils::ToString(ratingDiff.rating) };

			fileWriter.AddTable(row, col,
				{
					{
						ratingTask.spRatingAlgo->Description(),
						StdStringToUtilsString(fixtureInfo.homeTeam),
						StdStringToUtilsString(fixtureInfo.awayTeam),
						STR("Notes"),
					},
					{
						STR("Ratings"),
						utils::ToString(relevantRatings.first.rating),
						utils::ToString(relevantRatings.second.rating),
						diffString
					},
					{
						STR("Line"),
						StdStringToUtilsString(fixtureInfo.homeTeam) + STR(" ") + FloatToUtilsString(-aggregatedStat.Avg(), 2, true /*forceSign*/),
						StdStringToUtilsString(fixtureInfo.awayTeam) + STR(" ") + FloatToUtilsString(aggregatedStat.Avg(), 2, true /*forceSign*/),
						aggregatedStat.RatingRange() + STR(" ") + utils::ToString(aggregatedStat.SampleSize()),
					},
				});


			const auto handicapLines = aggregatedStat.GetHandicapLines();
			double mainLine = 0.0;
			double minDiff = 100.0;
			for (const auto& [line, probs] : handicapLines)
			{
				if (double probDiff = std::abs(probs.underProb - probs.overProb); probDiff < minDiff)
				{
					minDiff = probDiff;
					mainLine = line;
				}
			}

			std::vector<std::vector<utils::String>> goalLineTable;
			for (const auto& [line, probs] : handicapLines)
			{
				const double threshold = 0.001;
				if (threshold > probs.underProb || (1 - threshold) < probs.underProb ||
					threshold > probs.overProb || (1 - threshold) < probs.overProb)
				{
					continue;	//probabilities that are 0 or 1 will not be shown in the file
				}

				goalLineTable.emplace_back(std::vector<utils::String>
				{
					FloatToUtilsString(line, 2, true /*forceSign*/),
						FloatToUtilsString(1.0 / probs.underProb, 3),
						FloatToUtilsString(1.0 / probs.overProb, 3),
						FloatToUtilsString(probs.underProb, 3),
						FloatToUtilsString(probs.overProb, 3),
						line == mainLine ? STR("'<=") : STR("")
				});
			}

			fileWriter.AddTable(row + 3, col, goalLineTable);
		}

		void AddRatingTable(
			CsvFileWriter& fileWriter,
			const FixtureInfo& fixtureInfo,
			const RatingCalculationTask& ratingTask,
			const RatingStats& ratingStats)
		{
			if (point_calc_names::_1X2 == ratingTask.spRatingAlgo->spPointCalculator->name)
			{
				AddThreeWayRatingTable(fileWriter, 2 /*row*/, 0 /*col*/, fixtureInfo, ratingTask, ratingStats);
			}
			else if (point_calc_names::GOAL_DIFF == ratingTask.spRatingAlgo->spPointCalculator->name)
			{
				GoalDiffRatingTable(fileWriter, 7 /*row*/, 0 /*col*/, fixtureInfo, ratingTask, ratingStats);
			}
		}
	}

	void GenerateRatingFiles()
	{
		auto& db = GetDb(DbType::V2);
		std::set<FixtureInfo> upcomingMatches = QueryUpcomingMatches(db);

		std::map<LeagueInfo, std::vector<RatingCalculationTask>> leagueToRatingTasks;
		RatingStats ratingStats;

		for (const FixtureInfo& fixtureInfo : upcomingMatches)
		{
			if (leagueToRatingTasks.cend() == leagueToRatingTasks.find(fixtureInfo.leagueInfo))
			{
				const auto matchResults = data_cleaning::QueryMatchResults(
					db,
					fixtureInfo.leagueInfo.country,
					fixtureInfo.leagueInfo.league,
					fixtureInfo.dateTime.ToSQLiteDateFormat());

				leagueToRatingTasks[fixtureInfo.leagueInfo].emplace_back(
					RatingCalculationTask(
						GetClassicRatingAlgo(point_calc_names::_1X2),
						[](const MatchResult& result)
						{
							return std::make_pair(result.home_goals, result.away_goals);
						}
					)
				);
				leagueToRatingTasks[fixtureInfo.leagueInfo].emplace_back(
					RatingCalculationTask(
						GetClassicRatingAlgo(point_calc_names::GOAL_DIFF),
						[](const database_handler::v2::MatchResult& result)
						{
							return std::make_pair(result.home_goals, result.away_goals);
						}
					)
				);

				RatingCalculator ratingCalculator(matchResults.cbegin(), matchResults.cend(), leagueToRatingTasks.at(fixtureInfo.leagueInfo), false /*printMatches*/);
				ratingCalculator.Run();
				ratingCalculator.UpdateTeams(fixtureInfo.seasonId);	//if a new season starts, loads the current teams.

				if (ratingStats.ratingAlgoToLeagueToStats.empty())
				{
					auto copyTasks = leagueToRatingTasks.at(fixtureInfo.leagueInfo);
					ratingStats = TestRatingAlgorithms(copyTasks, false /*writeRatingStatsToFile*/, true /*printLeagues*/, false /*printMatches*/);
				}
			}

			std::stringstream ss;
			ss <<
				UtilsStringToStdString(fixtureInfo.dateTime.ToDateTimeString()) << " - " <<
				fixtureInfo.leagueInfo.country << " " <<
				fixtureInfo.leagueInfo.league << " - " <<
				fixtureInfo.homeTeam << " - " <<
				fixtureInfo.awayTeam << " - ratings.csv";
			utils::String fileName{ StdStringToUtilsString(ss.str()) };
			fileName = utils::string_operations::Replace(fileName, STR("/"), STR("_"));
			std::filesystem::create_directories(RATINGS_OUTPUT_PATH);
			utils::String filePath = RATINGS_OUTPUT_PATH + fileName;
			CsvFileWriter fileWriter(filePath, false);

			AddFixtureInfoTable(fileWriter, fixtureInfo);

			try
			{
				for (const auto& ratingTask : leagueToRatingTasks.at(fixtureInfo.leagueInfo))
				{
					AddRatingTable(fileWriter, fixtureInfo, ratingTask, ratingStats);
				}
				fileWriter.PrintTables();
			}
			catch (const std::exception& e)
			{
				utils::Logger::LogError(STR("Failed to create ") + fileName, true);
				utils::Logger::LogError(e, true);
			}
			TCOUT << fileName << " created" << endl;
		}
		TCOUT << "Finished generating stat files." << endl;
	}
}