#pragma once
#include "SportsPredictionLab/Source/FileIO/FileIO.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
//lib
#include "SportsPredictionLab/Source/Ratings/Testing/RatingStatEntry.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"
//std
#include <map>

namespace sports_prediction_lab::file_io
{
	const utils::String RATING_TESTER_OUTPUT_PATH = OUTPUT_FOLDER_NAME + STR("RatingTester/");

	void WriteRatingStats(const ARatingAlgorithm& ratingAlgo, const RatingStats& ratingStats);
}