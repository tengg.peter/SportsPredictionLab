#include "SportsPredictionLab/Source/FileIO/Ratings/RatingTesterOutput.h"
#include "SportsPredictionLab/Source/FileIO/FileIO.h"
//lib
#include "Utils/Utils.h"
//std
#include <filesystem>

namespace sports_prediction_lab::file_io
{
	using utils::string_conversion::FloatToUtilsString;
	using utils::string_conversion::StdStringToUtilsString;

	namespace
	{
		void WriteStatsMap(
			utils::file_io::CsvFileWriter& fileWriter,
			const RatingStats& ratingStats,
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::LeagueInfo& leagueInfo,
			int minSampleSize)
		{
			const utils::String ratingDescription{ ratingAlgo.Description() };
			const utils::String leagueName{ StdStringToUtilsString(leagueInfo.country + " - " + leagueInfo.league) };

			fileWriter.WriteLine(ratingDescription);
			fileWriter.WriteLine(leagueName);
			fileWriter.WriteLine();
			fileWriter.Write("Rating diff", "Avg Points", "Exp Points", "Error", "", "Total");
			const auto& distroKeys = ratingStats.GetAllKeysFromDistributions(ratingAlgo, leagueInfo);
			for (const auto& key : distroKeys)
			{
				fileWriter.Write(FloatToUtilsString(key, 2));
			}
			fileWriter.WriteLine();

			double sumError = 0;
			double weightedSumError = 0;
			int totalResults = 0;

			const auto& ratingStatsMap = ratingStats.ratingAlgoToLeagueToStats.at(ratingAlgo).at(leagueInfo);
			for (const auto& [ratingDiff, resultStat] : ratingStatsMap)
			{
				RatingStatEntry aggregate = ratingStats.AggregateEntries(ratingAlgo, leagueInfo, ratingDiff, minSampleSize);

				fileWriter.Write(aggregate.RatingRange());
				fileWriter.Write(
					aggregate.Avg(),
					aggregate.expectedPoints.points,
					aggregate.AbsError(),
					"",
					aggregate.SampleSize()
				);
				for (const auto& key : distroKeys)
				{
					if (0 == aggregate.distribution.count(key))
					{
						fileWriter.Write(0);
					}
					else
					{
						fileWriter.Write(FloatToUtilsString(aggregate.distribution.at(key)));
					}
				}
				fileWriter.WriteLine();

				sumError += aggregate.AbsError();
				weightedSumError += aggregate.AbsError() * aggregate.SampleSize();
				totalResults += aggregate.SampleSize();
			}

			fileWriter.WriteLine();
			fileWriter.WriteLine(ratingDescription);
			fileWriter.WriteLine(leagueName);
			fileWriter.WriteLine("Avg Error", "Avg Weighted Error");
			fileWriter.WriteLine(sumError / ratingStatsMap.size(), weightedSumError / totalResults);
			fileWriter.WriteLine();
			fileWriter.WriteLine();
		}
	}

	void WriteRatingStats(
		const ARatingAlgorithm& ratingAlgo,
		const RatingStats& ratingStats)
	{
		utils::StringStream ss;
		ss << ratingAlgo.Details() << " - " << utils::date_and_time::DateTime::Now() << ".csv";
		utils::String fileName{ ss.str() };
		std::filesystem::create_directories(RATING_TESTER_OUTPUT_PATH);
		utils::String filePath = RATING_TESTER_OUTPUT_PATH + fileName;
		utils::file_io::CsvFileWriter fileWriter(filePath, false /*append*/);

		for (const auto& [leagueInfo, statsMap] : ratingStats.ratingAlgoToLeagueToStats.at(ratingAlgo))
		{
			if (leagueInfo != ratingStats.OVERALL_STATS)
			{
				WriteStatsMap(fileWriter, ratingStats, ratingAlgo, leagueInfo, 1);
			}
		}
		WriteStatsMap(fileWriter, ratingStats, ratingAlgo, ratingStats.OVERALL_STATS, 1);
		WriteStatsMap(fileWriter, ratingStats, ratingAlgo, ratingStats.OVERALL_STATS, 1000);
	}
}