#pragma once
//lib
#include "Utils/Utils.h"
//std

namespace sports_prediction_lab::file_io
{
	void WriteCornerDistributionsToCsv(const utils::String& fileName, int sampleSize);
	void GenerateStatFiles();
}