//own
#include "DatabaseHandler/DatabaseHandler.h"
#include "ForUpcomingMatches.h"
#include "Source/FileIO/FileIO.h"
#include "Source/Prediction/Naive.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/RatingCalculation.h"
//std
#include <filesystem>
#include <numeric>	//std::accumulate


namespace sports_prediction_lab::file_io
{
	using namespace utils::string_conversion;

	using utils::file_io::CsvFileWriter;
	using database_handler::v2::DiscreteDistribution;
	using database_handler::v2::PreMatchStats;
	using database_handler::v2::FixtureInfo;
	using std::endl;

	void WriteCornerDistributionsToCsv(const utils::String& fileName, int sampleSize)
	{
		if (utils::string_operations::Contains(fileName, STR("/")))
		{
			throw std::runtime_error("The filename cannot contain '/'");
		}

		std::filesystem::create_directories(file_io::OUTPUT_FOLDER_NAME);
		utils::String filePath = file_io::OUTPUT_FOLDER_NAME + fileName;
		CsvFileWriter fileWriter(filePath, false);

		using namespace database_handler;
		using namespace database_handler::common;

		std::vector<v2::DiscreteDistribution> cornerDistributions =
			v2::QueryCornerDistributionsForUpcomingMatches(GetDb(DbType::V2), sampleSize);

		if (0 != cornerDistributions.size() % 3)
		{
			throw std::runtime_error("The number of distributions should be divisible by 3: for, against and total.");
		}

		for (size_t i = 0; i < cornerDistributions.size(); i += 3)
		{
			size_t maxLines = std::max({
				cornerDistributions[i].entries.size(),
				cornerDistributions[i + 1].entries.size(),
				cornerDistributions[i + 2].entries.size() });

			fileWriter.WriteLine(
				cornerDistributions[i + 1].description, "times", "percentage", "",	//corners for
				cornerDistributions[i].description, "times", "percentage", "",		//corners against
				cornerDistributions[i + 2].description, "times", "percentage", "");	//corners total

			for (size_t j = 0; j < maxLines; j++)
			{
				utils::String cornersFor;
				utils::String timesFor;
				utils::String frequencyFor;
				if (j < cornerDistributions[i + 1].entries.size())
				{
					auto it = cornerDistributions[i + 1].entries.cbegin();
					std::advance(it, j);
					cornersFor = STR("'") + utils::ToString(it->value);
					timesFor = STR("'") + utils::ToString(it->occurances);
					frequencyFor = STR("'") + utils::string_conversion::RatioToPercent(it->frequency);
				}

				utils::String cornersAgainst;
				utils::String timesAgainst;
				utils::String frequencyAgainst;
				if (j < cornerDistributions[i].entries.size())
				{
					auto it = cornerDistributions[i].entries.cbegin();
					std::advance(it, j);
					cornersAgainst = STR("'") + utils::ToString(it->value);
					timesAgainst = STR("'") + utils::ToString(it->occurances);
					frequencyAgainst = STR("'") + utils::string_conversion::RatioToPercent(it->frequency);
				}

				utils::String cornersTotal;
				utils::String timesTotal;
				utils::String frequencyTotal;
				if (j < cornerDistributions[i + 2].entries.size())
				{
					auto it = cornerDistributions[i + 2].entries.cbegin();
					std::advance(it, j);
					cornersTotal = STR("'") + utils::ToString(it->value);
					timesTotal = STR("'") + utils::ToString(it->occurances);
					frequencyTotal = STR("'") + utils::string_conversion::RatioToPercent(it->frequency);
				}

				fileWriter.WriteLine(
					cornersFor, timesFor, frequencyFor, "",
					cornersAgainst, timesAgainst, frequencyAgainst, "",
					cornersTotal, timesTotal, frequencyTotal, "");
			}
			fileWriter.WriteLine();
		}
	}

	namespace
	{
		struct Stats
		{
			PreMatchStats cornerStats;
			std::vector<DiscreteDistribution> cornerDistributions;
			PreMatchStats goalStats;
			PreMatchStats attackStats;
			PreMatchStats dangerousAttackStats;
			PreMatchStats shotStats;
			PreMatchStats shotsOnGoalStats;
			PreMatchStats possessionStats;
			PreMatchStats crossStats;
		};

		std::map<FixtureInfo, Stats> QueryPreMatchStats()
		{
			std::map<FixtureInfo, Stats> preMatchStats;
			auto& db = database_handler::common::GetDb(database_handler::common::DbType::V2);
			const int sampleSize = 20;

			auto cornerStats = database_handler::v2::QueryWeightedPreMatchCornerStats(db, sampleSize);
			for (const auto& cs : cornerStats)
			{
				preMatchStats[cs.fixtureInfo].cornerStats = cs;
			}

			auto cornerDistributions = database_handler::v2::QueryCornerDistributionsForUpcomingMatches(db, sampleSize);
			for (const auto& cd : cornerDistributions)
			{
				preMatchStats[cd.fixtureInfo].cornerDistributions.emplace_back(cd);
			}

			auto goalStats = database_handler::v2::QueryWeightedPreMatchGoalStats(db, sampleSize);
			for (const auto& gs : goalStats)
			{
				preMatchStats[gs.fixtureInfo].goalStats = gs;
			}

			auto attackStats = database_handler::v2::QueryWeightedPreMatchAttackStats(db, sampleSize);
			for (const auto& as : attackStats)
			{
				preMatchStats[as.fixtureInfo].attackStats = as;
			}

			auto dangerousAttackStats = database_handler::v2::QueryWeightedPreMatchDangerousAttackStats(db, sampleSize);
			for (const auto& das : dangerousAttackStats)
			{
				preMatchStats[das.fixtureInfo].dangerousAttackStats = das;
			}

			auto shotStats = database_handler::v2::QueryWeightedPreMatchShotStats(db, sampleSize);
			for (const auto& ss : shotStats)
			{
				preMatchStats[ss.fixtureInfo].shotStats = ss;
			}

			auto shotsOnGoalStats = database_handler::v2::QueryWeightedPreMatchShotsOnGoalStats(db, sampleSize);
			for (const auto& sog : shotsOnGoalStats)
			{
				preMatchStats[sog.fixtureInfo].shotsOnGoalStats = sog;
			}

			auto possessionStats = database_handler::v2::QueryWeightedPreMatchPossessionStats(db, sampleSize);
			for (const auto& ps : possessionStats)
			{
				preMatchStats[ps.fixtureInfo].possessionStats = ps;
			}

			auto crossStats = database_handler::v2::QueryWeightedPreMatchCrossStats(db, sampleSize);
			for (const auto& cs : crossStats)
			{
				preMatchStats[cs.fixtureInfo].crossStats = cs;
			}

			return preMatchStats;
		}

		const utils::String noDataStr{ STR("<no data>") };

		utils::String DoubleOptToUtilsString(const std::optional<double>& doubleOpt)
		{
			return doubleOpt.has_value() ? FloatToUtilsString(doubleOpt.value()) : noDataStr;
		}

		void WriteStats(
			CsvFileWriter& csv,
			const PreMatchStats& stats,
			const std::vector<utils::String> columnNames,
			const std::vector<std::pair<utils::String, double>>& expectedValues)
		{
			try
			{
				csv.Write();
			}
			catch (const std::exception& e)
			{
				TCOUT << e.what() << std::endl;
				return;
			}

			const std::set<int> emptyColumnPlaces{ 0, 3 };
			for (size_t i = 0; i < columnNames.size(); i++)
			{
				if (0 < emptyColumnPlaces.count(i))
				{
					csv.Write();
				}
				csv.Write(columnNames[i]);
			}
			csv.Write();
			for (const auto& [colName, _] : expectedValues)
			{
				csv.Write(colName);
			}
			csv.WriteLine();
			csv.Write(StdStringToUtilsString(stats.fixtureInfo.homeTeam));
			csv.Write();
			csv.Write(DoubleOptToUtilsString(stats.homeTeamHomeForOpt));
			csv.Write(DoubleOptToUtilsString(stats.homeTeamHomeAgainstOpt));
			csv.Write(DoubleOptToUtilsString(stats.homeTeamHomeTotalOpt));
			csv.Write();
			csv.Write(DoubleOptToUtilsString(stats.homeTeamAnywhereForOpt));
			csv.Write(DoubleOptToUtilsString(stats.homeTeamAnywhereAgainstOpt));
			csv.Write(DoubleOptToUtilsString(stats.homeTeamAnywhereTotalOpt));
			csv.Write();

			for (const auto& [_, expectedValue] : expectedValues)
			{
				csv.Write(expectedValue);
			}

			csv.WriteLine();
			csv.Write(StdStringToUtilsString(stats.fixtureInfo.awayTeam));
			csv.Write();
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAwayForOpt));
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAwayAgainstOpt));
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAwayTotalOpt));
			csv.Write();
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAnywhereForOpt));
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAnywhereAgainstOpt));
			csv.Write(DoubleOptToUtilsString(stats.awayTeamAnywhereTotalOpt));
		}
	}

	namespace
	{
		void AddFixtureInfoTable(CsvFileWriter& fileWriter, const FixtureInfo& fixtureInfo)
		{
			using utils::string_operations::Replace;
			fileWriter.AddTable(0, 0,
				{
					{
						STR("Country"),
						STR("League"),
						STR("Home team"),
						STR("Away team"),
						STR("Date time"),
					},
					{
						StdStringToUtilsString(fixtureInfo.leagueInfo.country),
						StdStringToUtilsString(fixtureInfo.leagueInfo.league),
						StdStringToUtilsString(fixtureInfo.homeTeam),
						StdStringToUtilsString(fixtureInfo.awayTeam),
						fixtureInfo.dateTime.ToDateTimeString()
					}
				});
		}

		void AddExpValueTable(
			CsvFileWriter& fileWriter,
			size_t row,
			size_t col,
			const utils::String& valueName,
			const PreMatchStats& stats)
		{
			fileWriter.AddTable(row, col,
				{
					{
						STR("Wexp home ") + valueName,
						STR("Wexp away ") + valueName,
						STR("Wexp total ") + valueName
					},
					{
						DoubleOptToUtilsString(ExpectedHomeValue(stats)),
						DoubleOptToUtilsString(ExpectedAwayValue(stats)),
						DoubleOptToUtilsString(ExpectedTotalValue(stats))
					}
				});
		}

		void AddCornerDistributionTables(
			CsvFileWriter& fileWriter,
			const FixtureInfo& fixtureInfo,
			const std::vector<DiscreteDistribution>& cornerDistributions)
		{
			const utils::String homeCornersTotalStr{ StdStringToUtilsString(fixtureInfo.homeTeam) + STR(" home corners total") };
			std::vector<std::vector<utils::String>> homeCornerDistTable
			{
				{
					homeCornersTotalStr,
					STR("Times"),
					STR("Frequency")
				}
			};
			const auto& homeIt = std::find_if(cornerDistributions.cbegin(), cornerDistributions.cend(),
				[&homeCornersTotalStr](const DiscreteDistribution& d)
				{
					return homeCornersTotalStr == d.description;
				});
			if (cornerDistributions.cend() == homeIt)
			{
				homeCornerDistTable.emplace_back(
					std::vector<utils::String>
				{
					STR(""),
						noDataStr
				}
				);
			}
			else
			{
				for (const auto& entry : homeIt->entries)
				{
					homeCornerDistTable.emplace_back(
						std::vector<utils::String>
					{
						utils::ToString(entry.value),
							utils::ToString(entry.occurances),
							utils::string_conversion::FloatToUtilsString(entry.frequency)
					}
					);
				}
			}
			fileWriter.AddTable(0, 9, homeCornerDistTable);

			const utils::String awayCornersTotalStr{ StdStringToUtilsString(fixtureInfo.awayTeam) + STR(" away corners total") };
			std::vector<std::vector<utils::String>> awayCornerDistTable
			{
				{
					awayCornersTotalStr,
					STR("Times"),
					STR("Frequency")
				}
			};
			const auto& awayIt = std::find_if(cornerDistributions.cbegin(), cornerDistributions.cend(),
				[&awayCornersTotalStr](const DiscreteDistribution& d)
				{
					return awayCornersTotalStr == d.description;
				});
			if (cornerDistributions.cend() == awayIt)
			{
				awayCornerDistTable.emplace_back(
					std::vector<utils::String>
				{
					STR(""),
						noDataStr
				}
				);
			}
			else
			{
				for (const auto& entry : awayIt->entries)
				{
					awayCornerDistTable.emplace_back(
						std::vector<utils::String>
					{
						utils::ToString(entry.value),
							utils::ToString(entry.occurances),
							utils::string_conversion::FloatToUtilsString(entry.frequency)
					}
					);
				}
			}
			fileWriter.AddTable(0, 12, awayCornerDistTable);
		}


		void AddRatiosTable(CsvFileWriter& fileWriter, size_t row, size_t col, const Stats& stats)
		{
			const PreMatchStats possessionToAttacks = stats.possessionStats / stats.attackStats;
			const PreMatchStats attacksToDangerousAttacks = stats.attackStats / stats.dangerousAttackStats;
			const PreMatchStats attacksToShots = stats.attackStats / stats.shotStats;
			const PreMatchStats dangerousAttacksToShots = stats.dangerousAttackStats / stats.shotStats;
			const PreMatchStats shotsToShotsOnGoal = stats.shotStats / stats.shotsOnGoalStats;
			const PreMatchStats shotsToGoals = stats.shotStats / stats.goalStats;
			const PreMatchStats shotsOnGoalToGoals = stats.shotsOnGoalStats / stats.goalStats;
			const PreMatchStats crossesToCorners = stats.crossStats / stats.cornerStats;
			const PreMatchStats shotsToCorners = stats.shotStats / stats.cornerStats;
			const PreMatchStats shotsOnGoalToCorners = stats.shotsOnGoalStats / stats.cornerStats;
			const PreMatchStats cornersToGoals = stats.cornerStats / stats.goalStats;

			fileWriter.AddTable(row, col,
				{
					{
						STR(""), STR("Home"), STR("Away"), STR("Average")
					},
					{
						STR("Possession / attacks"),
						DoubleOptToUtilsString(ExpectedHomeValue(possessionToAttacks)),
						DoubleOptToUtilsString(ExpectedAwayValue(possessionToAttacks)),
						DoubleOptToUtilsString(ExpectedAverageValue(possessionToAttacks))
					},
					{
						STR("Attacks / dang. attacks"),
						DoubleOptToUtilsString(ExpectedHomeValue(attacksToDangerousAttacks)),
						DoubleOptToUtilsString(ExpectedAwayValue(attacksToDangerousAttacks)),
						DoubleOptToUtilsString(ExpectedAverageValue(attacksToDangerousAttacks))
					},
					{
						STR("Attacks / shots"),
						DoubleOptToUtilsString(ExpectedHomeValue(attacksToShots)),
						DoubleOptToUtilsString(ExpectedAwayValue(attacksToShots)),
						DoubleOptToUtilsString(ExpectedAverageValue(attacksToShots))
					},
					{
						STR("Dang. attacks / shots"),
						DoubleOptToUtilsString(ExpectedHomeValue(dangerousAttacksToShots)),
						DoubleOptToUtilsString(ExpectedAwayValue(dangerousAttacksToShots)),
						DoubleOptToUtilsString(ExpectedAverageValue(dangerousAttacksToShots))
					},
					{
						STR("Shots / shots on goal"),
						DoubleOptToUtilsString(ExpectedHomeValue(shotsToShotsOnGoal)),
						DoubleOptToUtilsString(ExpectedAwayValue(shotsToShotsOnGoal)),
						DoubleOptToUtilsString(ExpectedAverageValue(shotsToShotsOnGoal))
					},
					{
						STR("Shots / goals"),
						DoubleOptToUtilsString(ExpectedHomeValue(shotsToGoals)),
						DoubleOptToUtilsString(ExpectedAwayValue(shotsToGoals)),
						DoubleOptToUtilsString(ExpectedAverageValue(shotsToGoals))
					},
					{
						STR("Shots on goal / goals"),
						DoubleOptToUtilsString(ExpectedHomeValue(shotsOnGoalToGoals)),
						DoubleOptToUtilsString(ExpectedAwayValue(shotsOnGoalToGoals)),
						DoubleOptToUtilsString(ExpectedAverageValue(shotsOnGoalToGoals))
					},
					{
						STR("Crosses / corners"),
						DoubleOptToUtilsString(ExpectedHomeValue(crossesToCorners)),
						DoubleOptToUtilsString(ExpectedAwayValue(crossesToCorners)),
						DoubleOptToUtilsString(ExpectedAverageValue(crossesToCorners))
					},
					{
						STR("Shots / corners"),
						DoubleOptToUtilsString(ExpectedHomeValue(shotsToCorners)),
						DoubleOptToUtilsString(ExpectedAwayValue(shotsToCorners)),
						DoubleOptToUtilsString(ExpectedAverageValue(shotsToCorners))
					},
					{
						STR("Shots on goal / corners"),
						DoubleOptToUtilsString(ExpectedHomeValue(shotsOnGoalToCorners)),
						DoubleOptToUtilsString(ExpectedAwayValue(shotsOnGoalToCorners)),
						DoubleOptToUtilsString(ExpectedAverageValue(shotsOnGoalToCorners))
					},
					{
						STR("Corners / goals"),
						DoubleOptToUtilsString(ExpectedHomeValue(cornersToGoals)),
						DoubleOptToUtilsString(ExpectedAwayValue(cornersToGoals)),
						DoubleOptToUtilsString(ExpectedAverageValue(cornersToGoals))
					},
				});
		}

		void AddStandings(
			CsvFileWriter& fileWriter,
			size_t row,
			size_t col,
			const database_handler::v2::FixtureInfo& fixtureInfo,
			const std::map<utils::String, RatingPair>& ratings)
		{
			using namespace database_handler::v2;
			const std::map<LeagueInfo, Standing> standings = QueryCurrentStandings(GetDb(database_handler::common::DbType::V2));

			std::vector<std::vector<utils::String>> standingTable
			{
				{
					STR("Pos."),
					STR("Team"),
					STR("Rating"),
					STR("Matches played"),
					STR("Won"),
					STR("Draw"),
					STR("Lost"),
					STR("Goals for"),
					STR("Goals against"),
					STR("Goal diff."),
					STR("Pts"),
					STR("Recent form"),
					STR("Result"),
					STR("")
				}
			};

			auto it = standings.find(fixtureInfo.leagueInfo);
			if (standings.cend() != it)
			{
				for (const auto& pos : it->second.positions)
				{
					const int won = pos.home_won + pos.away_won;
					const int draw = pos.home_draw + pos.away_draw;
					const int lost = pos.home_lost + pos.away_lost;
					const int goalsFor = pos.home_goals_scored + pos.away_goals_scored;
					const int goalsAgainst = pos.home_goals_against + pos.away_goals_against;

					bool relevantTeam = pos.team_name == StdStringToUtilsString(fixtureInfo.homeTeam) ||
						pos.team_name == StdStringToUtilsString(fixtureInfo.awayTeam);

					const utils::String markerBefore{ relevantTeam ? STR("=>") : STR("  ") };
					const utils::String markerAfter{ relevantTeam ? STR("<=") : STR("  ") };

					standingTable.emplace_back
					(
						std::vector<utils::String>
					{
						markerBefore + utils::ToString(pos.position),
							pos.team_name,
							utils::ToString(ratings.at(pos.team_name).rating),
							utils::ToString(pos.home_games_played + pos.away_games_played),
							utils::ToString(won),
							utils::ToString(draw),
							utils::ToString(lost),
							utils::ToString(goalsFor),
							utils::ToString(goalsAgainst),
							utils::ToString(goalsFor - goalsAgainst),
							utils::ToString(3 * won + draw),
							pos.recent_form_opt.value_or(STR("")),
							pos.result_opt.value_or(STR("")),
							markerAfter
					}
					);
				}
			}
			else
			{
				utils::Logger::LogWarning(STR("No standing data found for") +
					StdStringToUtilsString(fixtureInfo.leagueInfo.country + " " + fixtureInfo.leagueInfo.league), true);
			}

			fileWriter.AddTable(row, col, standingTable);
		}
	}

	void GenerateStatFiles()
	{
		std::map<FixtureInfo, Stats> preMatchStats{ QueryPreMatchStats() };

		std::map<database_handler::v2::LeagueInfo, std::map<utils::String, RatingPair>> ratingsByLeagues;

		for (const auto& [fixtureInfo, stats] : preMatchStats)
		{
			if (ratingsByLeagues.cend() == ratingsByLeagues.find(fixtureInfo.leagueInfo))
			{
				using namespace database_handler;
				using namespace database_handler::common;
				using namespace database_handler::v2;
				const auto matchResults = QueryMatchResults(
					GetDb(DbType::V2),
					fixtureInfo.leagueInfo.country,
					fixtureInfo.leagueInfo.league,
					fixtureInfo.dateTime.ToSQLiteDateFormat());

				std::vector<RatingCalculationTask> tasks
				{
					RatingCalculationTask(std::make_shared<EloRating>(),
						[](const MatchResult& result)
						{
							return std::make_pair(result.home_goals, result.away_goals);
						}
					)
				};

				RatingCalculator ratingCalculator(matchResults.cbegin(), matchResults.cend(), tasks, false);
				ratingCalculator.Run();

				auto& destMap = ratingsByLeagues[fixtureInfo.leagueInfo];
				std::transform(
					tasks.front().RunningRatings().cbegin(),
					tasks.front().RunningRatings().cend(),
					std::inserter(destMap, destMap.end()),
					[](const std::pair<std::string, RatingPair>& p)
					{
						return std::make_pair(StdStringToUtilsString(p.first), p.second);
					});
			}

			std::stringstream ss;
			ss <<
				UtilsStringToStdString(fixtureInfo.dateTime.ToDateTimeString()) << " - " <<
				fixtureInfo.leagueInfo.country << " " <<
				fixtureInfo.leagueInfo.league << " - " <<
				fixtureInfo.homeTeam << " - " <<
				fixtureInfo.awayTeam << " - stats.csv";
			utils::String fileName{ StdStringToUtilsString(ss.str()) };
			fileName = utils::string_operations::Replace(fileName, STR("/"), STR("_"));
			std::filesystem::create_directories(file_io::OUTPUT_FOLDER_NAME);
			utils::String filePath = file_io::OUTPUT_FOLDER_NAME + fileName;
			CsvFileWriter fileWriter(filePath, false);

			AddFixtureInfoTable(fileWriter, fixtureInfo);
			AddExpValueTable(fileWriter, 0, 5, STR("corners"), stats.cornerStats);
			AddCornerDistributionTables(fileWriter, fixtureInfo, stats.cornerDistributions);
			AddExpValueTable(fileWriter, 8, 0, STR("possession"), stats.possessionStats);
			AddExpValueTable(fileWriter, 8, 4, STR("attacks"), stats.attackStats);
			AddExpValueTable(fileWriter, 11, 0, STR("dangerous attacks"), stats.dangerousAttackStats);
			AddExpValueTable(fileWriter, 11, 4, STR("crosses"), stats.crossStats);
			AddExpValueTable(fileWriter, 14, 0, STR("shots"), stats.shotStats);
			AddExpValueTable(fileWriter, 14, 4, STR("shots on goal"), stats.shotsOnGoalStats);
			AddExpValueTable(fileWriter, 17, 0, STR("goals"), stats.goalStats);
			AddRatiosTable(fileWriter, 20, 0, stats);
			AddStandings(fileWriter, 20, 4, fixtureInfo, ratingsByLeagues.at(fixtureInfo.leagueInfo));

			try
			{
				fileWriter.PrintTables();
			}
			catch (const std::exception& e)
			{
				utils::Logger::LogError(STR("Failed to create ") + fileName, true);
				utils::Logger::LogError(e, true);
			}
			TCOUT << fileName << " created" << endl;
		}
		TCOUT << "Finished generating stat files." << endl;
	}
}