//own
#include "FileIO.h"
#include "Source/Helper/Helper.h"
//std
#include <filesystem>

//TODO: using can be removed when tm is replaced by Cpp Date class
using utils::date_and_time::operator<<;	//this using has to be here, otherwise wont build

namespace sports_prediction_lab::file_io
{
	using namespace utils;
	using namespace utils::string_operations;

	using std::map;
	using std::vector;
	using utils::String;

	static const utils::String RESULTS = STR("Results");
	static const utils::String FIXTURES = STR("Fixtures");

	static bool TryGetColumnMapping(const vector<vector<utils::String>>& csvFileContent, map<utils::String, int>& columnMapping);
	static bool FileIsEmpty(const utils::String& fileName);

	static bool TryGetColumnMapping(const vector<vector<utils::String>>& csvFileContent, map<utils::String, int>& columnMapping)
	{
		if (STR("Date") != csvFileContent[0][0])
		{
			//no header file.
			columnMapping.clear();
			return false;
		}

		for (size_t i = 0; i < csvFileContent[0].size(); i++)
		{
			columnMapping[csvFileContent[0][i]] = i;
		}
		return true;
	}

	//potentially Utils-worthy
	static bool FileIsEmpty(const utils::String & fileName)
	{
		std::wifstream file(fileName);
		return std::wifstream::traits_type::eof() == file.peek();
	}

	const vector<utils::String> ListFilesInFolder(const utils::String & folderName)
	{
		using namespace std::filesystem;
		vector<utils::String> retVector;
		for (const directory_entry& entry : directory_iterator(folderName))
		{
			if (is_regular_file(entry.status()))
			{
#ifdef _WIN32
				retVector.push_back(entry.path().filename().wstring());
#else
				retVector.push_back(entry.path().filename().string());
#endif
			}
		}
		return retVector;
	}
}