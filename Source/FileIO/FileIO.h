#pragma once
//own
#include "Source/Helper/Helper.h"
#include "Utils/Utils.h"
//std
#include <map>
#include <memory>
#include <vector>

namespace sports_prediction_lab::file_io
{
	const utils::String INPUT_FOLDER_NAME = STR("Input/");
	const utils::String OUTPUT_FOLDER_NAME = STR("Output/");
	const utils::String SAMPLE_FOLDER_PATH = INPUT_FOLDER_NAME + STR("Sample/");
	const utils::String CARD_FILES_FOLDER_PATH = STR("Card Files/");

	static utils::String BuildOutputFilePath(utils::String fileName)	//has to be here, because the templates call this function
	{
		if (STR("") == fileName)
		{
			fileName = helper::GetOutputFileName();
		}
		else
		{
			fileName += STR(" ") + utils::date_and_time::DateTime::Now().ToDateTimeString() + STR(".csv");
		}
		return OUTPUT_FOLDER_NAME + STR("/") + fileName;
	}

	const std::vector<utils::String> ListFilesInFolder(const utils::String& folderName);

	template<typename T>
	static void WriteObjectToFile(T& object, utils::String fileName = STR(""))
	{
		utils::OfStream file;
		file.open(BuildOutputFilePath(fileName));
		file << object;
		file.close();
	}

	template<typename T>
	static void WriteObjectsToFile(std::vector<T>& objects, utils::String fileName = STR(""))
	{
		utils::OfStream file;
		file.open(BuildOutputFilePath(fileName));
		for (T& o : objects)
		{
			file << o << std::endl;
		}	
		file.close();
	}
}

