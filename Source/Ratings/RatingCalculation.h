#pragma once
#include "DatabaseHandler/DatabaseHandler.h"
#include "SportsPredictionLab/Source/Ratings/Ratings.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"
//libraries
#include "Utils/Utils.h"
//std
#include <functional>
#include <map>
namespace sports_prediction_lab
{
	using ScoresGetter = std::function<std::pair<int, int>(const database_handler::v2::MatchResult&)>;

	class RatingCalculationTask
	{
	public:
		RatingCalculationTask(
			std::shared_ptr<ARatingAlgorithm> spRatingAlgo,
			const ScoresGetter& scoresGetter)
			: spRatingAlgo(spRatingAlgo)
			, getScores(scoresGetter)
		{}

		RatingPair& Rating(const std::string& team) { return m_runningRatings.at(team); }
		const RatingPair& Rating(const std::string& team) const { return m_runningRatings.at(team); }

		const std::map<std::string, RatingPair>& RunningRatings() const { return m_runningRatings; }

		void RemoveTeam(const std::string& teamName) { m_runningRatings.erase(teamName); }
		void AddTeam(const std::string& teamName, const RatingPair& ratingPair = {})
		{
			if (teamName.empty())
			{
				throw std::invalid_argument("teamName - missing team name");
			}

			if (!ratingPair.counterRating.has_value() && spRatingAlgo->homeAwaySeparate)
			{
				//asymmtric rating, needs to init the counter fields as well
				RatingPair rp;
				rp.counterRating = rp.rating;
				rp.numCounterMatches = rp.numMatches;
				m_runningRatings[teamName] = rp;
			}
			else
			{
				m_runningRatings[teamName] = ratingPair;
			}
		}
		void ClearRunningRatings() { m_runningRatings.clear(); }

	public:
		const std::shared_ptr<ARatingAlgorithm> spRatingAlgo;
		const ScoresGetter getScores;

	private:
		std::map<std::string, RatingPair> m_runningRatings;
	};

	template<typename TIt>
	class RatingCalculator
	{
	public:
		RatingCalculator(
			TIt begin,
			TIt end,
			std::vector<RatingCalculationTask>& tasks,
			bool printMatches)
			: m_begin(begin)
			, m_end(end)
			, m_it(begin)
			, m_tasks(tasks)
			, m_currentSeasonId(begin->fixtureInfo.seasonId)
			, m_currentLeagueId(begin->league_id)
			, m_printMatches(printMatches)
		{
			for (auto& task : m_tasks)
			{
				task.ClearRunningRatings();
			}
		}

		TIt CurrentMatch() const { return m_it; }

		size_t MatchCount() const { return m_matchCount; }

		void UpdateTeams(int64_t currentSeasonId)
		{
			if (currentSeasonId == m_currentSeasonId)
			{
				return;
			}

			//rating inheritance based on team replacements
			auto teamReplacements = database_handler::v2::QueryTeamReplacements(
				database_handler::common::GetDb(database_handler::common::DbType::V2),
				m_currentSeasonId,
				currentSeasonId);

			m_currentSeasonId = currentSeasonId;

			for (auto& task : m_tasks)
			{
				RatingPair sumPair;
				sumPair.rating = 0;
				sumPair.counterRating = 0;

				bool sumCalculated = false;
				for (const std::string& teamOut : teamReplacements.teamsOut)
				{
					auto it = task.RunningRatings().find(teamOut);
					if (task.RunningRatings().cend() == it)
					{
						continue;
					}
					sumPair.rating += it->second.rating;
					if (it->second.counterRating.has_value())
					{
						sumPair.counterRating.value() += it->second.counterRating.value();
					}
					task.RemoveTeam(teamOut);
					sumCalculated = true;
				}

				RatingPair averagePair;
				if (sumCalculated && !teamReplacements.teamsOut.empty())
				{
					averagePair.rating = sumPair.rating / teamReplacements.teamsOut.size();
					if (sumPair.counterRating.has_value())
					{
						averagePair.counterRating = sumPair.counterRating.value() / teamReplacements.teamsOut.size();
						averagePair.numCounterMatches = 0;
					}
				}
				for (const std::string& teamIn : teamReplacements.teamsIn)
				{
					task.AddTeam(teamIn, averagePair);
				}
			}
		}

		bool UpdateTeams()
		{
			if (Finished())
			{
				return false;
			}

			if (m_it->league_id != m_currentLeagueId)
			{
				for (auto& task : m_tasks)
				{
					task.ClearRunningRatings();
				}
				m_currentLeagueId = m_it->league_id;
				return true;
			}

			UpdateTeams(m_it->fixtureInfo.seasonId);
			return true;
		}

		bool UpdateRatings()
		{
			if (Finished())
			{
				return false;
			}

			for (auto& task : m_tasks)
			{
				const std::string& homeTeam = m_it->fixtureInfo.homeTeam;
				const std::string& awayTeam = m_it->fixtureInfo.awayTeam;

				if (0 == task.RunningRatings().count(homeTeam))
				{
					task.AddTeam(homeTeam);
				}
				if (0 == task.RunningRatings().count(awayTeam))
				{
					task.AddTeam(awayTeam);
				}

				RatingPair& homeTeamRatingRef = task.Rating(homeTeam);
				RatingPair& awayTeamRatingRef = task.Rating(awayTeam);

				PrintOriginalRatings(
					*task.spRatingAlgo,
					m_it->fixtureInfo,
					homeTeamRatingRef,
					awayTeamRatingRef);

				const auto result = task.getScores(*m_it);
				RatingPair originalHomeRatings = homeTeamRatingRef;
				task.spRatingAlgo->UpdateRatings(result.first, result.second, homeTeamRatingRef, awayTeamRatingRef);

				PrintUpdatedRatings(
					*task.spRatingAlgo,
					m_it->fixtureInfo,
					result,
					homeTeamRatingRef,
					awayTeamRatingRef,
					homeTeamRatingRef - originalHomeRatings
				);

			}

			return true;
		}

		bool Step()
		{
			if (Finished())
			{
				return false;
			}
			++m_it;
			++m_matchCount;
			return true;
		}

		bool Finished()
		{
			return m_end == m_it;
		}

		void Run()
		{
			while (!Finished())
			{
				UpdateTeams();
				UpdateRatings();
				Step();
			}
		}

		void RunUntil(int64_t fixtureId)
		{
			while (!Finished() && m_it->fixtureInfo.fixtureId != fixtureId)
			{
				UpdateTeams();
				UpdateRatings();
				Step();
			}
		}

		const std::vector<RatingCalculationTask>& Tasks() const { return m_tasks; }

		const database_handler::v2::MatchResult* NextMatch() const
		{
			if (m_end == m_it)
			{
				return nullptr;
			}
			return &*m_it;
		}

	private:
		void PrintOriginalRatings(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::FixtureInfo& fixtureInfo,
			const RatingPair& homeTeamRatings,
			const RatingPair& awayTeamRatings)
		{
			if (!m_printMatches)
			{
				return;
			}

			const auto relevantRatings = ratingAlgo.GetRelevantRatings(homeTeamRatings, awayTeamRatings);
			TCOUT << utils::string_conversion::StdStringToUtilsString(fixtureInfo.leagueInfo.country) << " - " <<
				utils::string_conversion::StdStringToUtilsString(fixtureInfo.leagueInfo.league) << " - " <<
				fixtureInfo.dateTime << "\n";
			TCOUT << ratingAlgo.Description() << ": " << utils::string_conversion::StdStringToUtilsString(fixtureInfo.homeTeam) << "(" <<
				relevantRatings.first.rating << ") - " <<
				utils::string_conversion::StdStringToUtilsString(fixtureInfo.awayTeam) << "(" << relevantRatings.second.rating << ")\n";
		}

		void PrintUpdatedRatings(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::FixtureInfo& fixtureInfo,
			const std::pair<int, int>& result,
			const RatingPair& homeTeamRatings,
			const RatingPair& awayTeamRatings,
			const RatingPair& homeTeamRatingChange
		)
		{
			if (!m_printMatches)
			{
				return;
			}

			TCOUT << ratingAlgo.Description() << ": " << result.first << " - " << result.second << "\n";

			utils::String homeSign{ homeTeamRatingChange.rating > 0 ? STR("+") : STR("") };
			utils::String awaySign{ -homeTeamRatingChange.rating > 0 ? STR("+") : STR("") };

			const auto relevantRatings = ratingAlgo.GetRelevantRatings(homeTeamRatings, awayTeamRatings);
			TCOUT << ratingAlgo.Description() << ": " << utils::string_conversion::StdStringToUtilsString(fixtureInfo.homeTeam) << "(" <<
				relevantRatings.first.rating << ") (" << homeSign << homeTeamRatingChange.rating << ") - " <<
				utils::string_conversion::StdStringToUtilsString(fixtureInfo.awayTeam) << "(" << relevantRatings.second.rating << ") (" << awaySign <<
				-homeTeamRatingChange.rating << ")\n";
			TCOUT << std::endl;
		}


	private:
		const TIt m_begin;
		const TIt m_end;
		TIt m_it;
		std::vector<RatingCalculationTask>& m_tasks;
		int64_t m_currentSeasonId;
		int64_t m_currentLeagueId;
		const bool m_printMatches;
		size_t m_matchCount = 0;
	};
}