#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/GoalDiffFitness.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
//std
#include <numeric>

namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;
	GoalDiffFitness::GoalDiffFitness() : ARatingFitness()
	{
	}

	bool GoalDiffFitness::InverseFitness() const
	{
		return true;
	}

	void GoalDiffFitness::Calculate(ai_lab::Individual<double>& individual) const
	{
		EloRating goalDiffRating;
		goalDiffRating.spPointCalculator = std::make_shared<GoalDiffPointCalc>();
		goalDiffRating.homeAwaySeparate = true;
		goalDiffRating.base = individual.chromosome[0];
		goalDiffRating.k = 3.2;
		goalDiffRating.magnifier = individual.chromosome[1];
		goalDiffRating.minPoints = -5.0;
		goalDiffRating.maxPoints = 5.0;


		std::vector<RatingCalculationTask> tasks;
		tasks.emplace_back(
			RatingCalculationTask(
				std::make_shared<EloRating>(goalDiffRating),
				[](const database_handler::v2::MatchResult& result)
				{
					return std::make_pair(result.home_goals, result.away_goals);
				}
			)
		);

		RatingStats stats = TestRatingAlgorithms(
			tasks, 
			false /*writeRatingStatsToFile*/, 
			false /*printLeagues*/, 
			false /*printMatches*/, 
			m_testSet);

		individual.fitnessValues.clear();
		individual.fitnessValues.emplace_back(stats.AverageError(goalDiffRating, stats.OVERALL_STATS));
	}
}