#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/ThreeWayFitness.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
//std
#include <numeric>

namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;
	ThreeWayFitness::ThreeWayFitness() : ARatingFitness()
	{
	}

	bool ThreeWayFitness::InverseFitness() const
	{
		return true;
	}

	void ThreeWayFitness::Calculate(ai_lab::Individual<double>& individual) const
	{
		EloRating threeWayRating;
		threeWayRating.spPointCalculator = std::make_shared<ThreeWayPointCalc>();
		threeWayRating.homeAwaySeparate = true;
		threeWayRating.base = individual.chromosome[0];
		threeWayRating.k = individual.chromosome[1];
		threeWayRating.magnifier = individual.chromosome[2];
		threeWayRating.minPoints = 0.0;
		threeWayRating.maxPoints = 1.0;

		if (0 == threeWayRating.magnifier)
		{
			individual.fitnessValues[0] = 0;
			return;
		}

		std::vector<RatingCalculationTask> tasks;
		tasks.emplace_back(
			RatingCalculationTask(
				std::make_shared<EloRating>(threeWayRating),
				[](const database_handler::v2::MatchResult& result)
				{
					return std::make_pair(result.home_goals, result.away_goals);
				}
			)
		);

		RatingStats stats = TestRatingAlgorithms(
			tasks, 
			false /*writeRatingStatsToFile*/, 
			false /*printLeagues*/, 
			false /*printMatches*/, 
			m_testSet);

		individual.fitnessValues.clear();
		individual.fitnessValues.emplace_back(stats.AverageError(threeWayRating, stats.OVERALL_STATS));
	}
}