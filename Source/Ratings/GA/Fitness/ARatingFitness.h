#pragma once
//lib
#include "AiLab/GA.h"
#include "DatabaseHandler/DatabaseHandler.h"

namespace sports_prediction_lab
{
	class ARatingFitness : public ai_lab::IFitnessFunction<double>
	{
	public:
		ARatingFitness();

	protected:
		const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>> m_testSet;
		const size_t m_testSetSize = 0;
	};
}