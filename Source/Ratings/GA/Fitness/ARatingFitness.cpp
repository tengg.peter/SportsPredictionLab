#include "SportsPredictionLab/Source/Ratings/GA/Fitness/ARatingFitness.h"
//std
#include <numeric>

namespace sports_prediction_lab
{
	using namespace database_handler::common;
	using namespace database_handler::v2;
	ARatingFitness::ARatingFitness()
		: m_testSet(QueryAllMatchResults(GetDb(DbType::V2)))
		, m_testSetSize(std::accumulate(m_testSet.cbegin(), m_testSet.cend(), 0,
			[](size_t sum, const auto& kvPair) {return sum + kvPair.second.size(); }))
	{
	}
}