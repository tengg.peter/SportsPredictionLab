#pragma once
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/ARatingFitness.h"
//lib
#include "AiLab/GA.h"
#include "DatabaseHandler/DatabaseHandler.h"

namespace sports_prediction_lab
{
	class ThreeWayFitness : public ARatingFitness
	{
	public:
		ThreeWayFitness();
		virtual void Calculate(ai_lab::Individual<double>& individual) const override;
		virtual bool InverseFitness() const override;
	};
}