#pragma once
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/ARatingFitness.h"
//lib
#include "AiLab/GA.h"
#include "DatabaseHandler/DatabaseHandler.h"

namespace sports_prediction_lab
{
	class GoalDiffFitness : public ARatingFitness
	{
	public:
		GoalDiffFitness();
		virtual void Calculate(ai_lab::Individual<double>& individual) const override;
		virtual bool InverseFitness() const override;
	};
}