#include "SportsPredictionLab/Source/FileIO/FileIO.h"
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/GoalDiffFitness.h"
#include "SportsPredictionLab/Source/Ratings/GA/Fitness/ThreeWayFitness.h"
#include "SportsPredictionLab/Source/Ratings/GA/GaForEloRatingParams.h"
#include "SportsPredictionLab/Source/Ratings/GA/StopCondition.h"
//lib
#include "AiLab/GA.h"

namespace sports_prediction_lab
{
	using namespace ai_lab;

	void RunGa()
	{
		GaParams gaParams;
		gaParams.chromLength = 3;
		gaParams.geneValueRanges = { {0, 1000}, {0, 1000}, {0, 10000} };
		gaParams.crossoverProbability = 0.75;
		gaParams.mutationProbability = 0.15;
		gaParams.elitism = true;
		gaParams.popSize = 10;
		gaParams.selectionGroupSize = 2;

		GaOptions gaOptions;
		gaOptions.printDetails = true;
		gaOptions.outputDir = file_io::OUTPUT_FOLDER_NAME + STR("GA/");
		gaOptions.geneNames = { STR("base"), STR("k"), STR("magnifier") };
		gaOptions.maxFitnessThreads = 2;

		GA<double> ga(gaParams, gaOptions, std::make_shared<ThreeWayFitness>(), std::make_shared<EloRatingGaStopCond>());
		ga.Run();
	}

	void ContinueGaFromJsonFile(const utils::String& filePath)
	{
		GA<double> ga = GA<double>::FromJsonFile(filePath);
		ga.SetFitnessFunction(std::make_shared<ThreeWayFitness>());
		ga.SetStopCondition(std::make_shared<EloRatingGaStopCond>());
		ga.Run();
	}
}