#pragma once
//lib
#include "AiLab/GA.h"

namespace sports_prediction_lab
{
	struct EloRatingGaStopCond : public ai_lab::IGaStopCondition
	{
		// Inherited via IGaStopCondition
		virtual bool operator()(int numGenerations, double bestFitness, double averageFitness) override;
	};
}