#pragma once
//own
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingStatEntry.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
//std
#include <map>

namespace sports_prediction_lab
{
	using RatingStatsMap = std::map<std::reference_wrapper<const ARatingAlgorithm>, std::map<database_handler::v2::LeagueInfo, std::map<RatingPair, RatingStatEntry>>>;
	using RatingTransferredMap = std::map<std::reference_wrapper<const ARatingAlgorithm>, std::map<database_handler::v2::LeagueInfo, int>>;

	class RatingStats
	{
	public:
		static const database_handler::v2::LeagueInfo OVERALL_STATS;

	public:
		RatingStatEntry AggregateEntries(
			const ARatingAlgorithm& ratingAlgo, 
			const database_handler::v2::LeagueInfo& leagueInfo,
			const RatingPair& ratingDiff, 
			int minSampleSize) const;

		std::set<double, RatingStatEntry::DoubleLess> GetAllKeysFromDistributions(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::LeagueInfo& leagueInfo) const;

		double AverageError(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::LeagueInfo& leagueInfo) const;

		double WeightedAvgError(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::LeagueInfo& leagueInfo) const;

		void AddRatingTransfer(
			const ARatingAlgorithm& ratingAlgo,
			const database_handler::v2::LeagueInfo& leagueInfo, 
			int ratingTransferred);

		int GetTotalRatingTransferred(
			const ARatingAlgorithm& ratingAlgo, 
			const database_handler::v2::LeagueInfo& leagueInfo);

	public:
		RatingStatsMap ratingAlgoToLeagueToStats;

	private:
		RatingStatEntry FindNextRatingDiff(const std::map<RatingPair, RatingStatEntry>& statsMap, RatingPair ratingDiff) const;

	private:
		RatingTransferredMap m_totalRatingTransferred;
		mutable RatingStatsMap m_aggregateCache;
		
		mutable std::map<std::reference_wrapper<const ARatingAlgorithm>, 
			std::map<database_handler::v2::LeagueInfo, 
			std::set<double, RatingStatEntry::DoubleLess>>> m_distroKeyCache;
		
		mutable std::map<std::reference_wrapper<const ARatingAlgorithm>,
			std::map<database_handler::v2::LeagueInfo,
			double>> m_avgErrorCache;
		
		mutable std::map<std::reference_wrapper<const ARatingAlgorithm>,
			std::map<database_handler::v2::LeagueInfo,
			double>> m_avgWeightedErrorCache;
	};
}