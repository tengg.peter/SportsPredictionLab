#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"
//lib
#include "Utils/Utils.h"
//std
#include <cmath>
#include <optional>
#include <set>

namespace sports_prediction_lab
{
	struct LineProbabilities
	{
		//under or home team
		double underProb = 0.0;
		//over or away team
		double overProb = 0.0;
	};

	class RatingStatEntry
	{
	public:
		struct DoubleLess
		{
			bool operator()(double a, double b) const
			{
				return std::abs(a - b) > epsilon && a < b;
			}
			double epsilon = 0.01;
		};

	public:
		int SampleSize() const;
		double Avg() const;
		double AbsError() const { return std::abs(Avg() - expectedPoints.points); }
		utils::String RatingRange() const;
		int GetValueOrZero(double key) const;
		double GetProbability(double key) const;
		std::map<double, LineProbabilities, DoubleLess> GetHandicapLines() const;

		void SetRatingRange(int lowerBound, int step);
		void SetExpectedPoints(const ARatingAlgorithm& ratingAlgorithm, const RatingPair& lowerBound, int step);
		void Increment(double val);
		RatingStatEntry operator+(const RatingStatEntry& other) const;
		RatingStatEntry& operator+=(const RatingStatEntry& other);

	public:
		RatingPair lowerBound;
		RatingPair upperBound;

		std::map<double, int, DoubleLess> distribution;

		PointsPair expectedPoints;
	};

	utils::OStream& operator<< (utils::OStream& os, const RatingStatEntry& entry);
}