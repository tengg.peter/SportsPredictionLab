#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
//std
#include <limits>
#include <numeric>
namespace sports_prediction_lab
{
	using database_handler::v2::LeagueInfo;

	const LeagueInfo RatingStats::OVERALL_STATS{ 0, "OVERALL", "STATS", false, 0, 0, 0 };

	RatingStatEntry RatingStats::AggregateEntries(
		const ARatingAlgorithm& ratingAlgo,
		const LeagueInfo& leagueInfo,
		const RatingPair& ratingDiff,
		int minSampleSize) const
	{
		const auto& ratingStatsMap = ratingAlgoToLeagueToStats.at(ratingAlgo).at(leagueInfo);
		RatingStatEntry ret = FindNextRatingDiff(ratingStatsMap, ratingDiff);
		if (ret.SampleSize() >= minSampleSize)
		{
			return ret;
		}

		if (0 != m_aggregateCache.count(ratingAlgo) &&
			0 != m_aggregateCache.at(ratingAlgo).count(leagueInfo) &&
			0 != m_aggregateCache.at(ratingAlgo).at(leagueInfo).count(ratingDiff))
		{
			return m_aggregateCache.at(ratingAlgo).at(leagueInfo).at(ratingDiff);
		}

		std::vector<const std::pair<const RatingPair, RatingStatEntry>*> pointers{ ratingStatsMap.size() };
		size_t i = 0;
		for (const auto& pair : ratingStatsMap)
		{
			pointers[i++] = &pair;
		}

		const auto requestedDiffIt = std::find_if(pointers.cbegin(), pointers.cend(),
			[&ratingDiff](const auto& p) {return p->first.rating == ratingDiff.rating; });

		auto from = requestedDiffIt;
		auto to = from;

		int sampleSize1 = 0, sampleSize2 = 0;
		bool toCanMove = false;
		do
		{
			bool fromCanMove = pointers.cbegin() != from;
			toCanMove = pointers.cend() != to;
			if (!fromCanMove && !toCanMove)
			{
				break;
			}

			if (requestedDiffIt == from ||	//hasn't moved yet, must move at least once
				!toCanMove ||	//the other iterator cannot be moved any more, this one has to be moved
				(sampleSize1 <= sampleSize2 &&
					minSampleSize > ret.SampleSize() + sampleSize1 + sampleSize2))
			{
				if (fromCanMove)
				{
					sampleSize1 += (*(--from))->second.SampleSize();
				}
			}

			if (requestedDiffIt == to ||
				!fromCanMove ||
				(sampleSize2 <= sampleSize1 &&
					minSampleSize > ret.SampleSize() + sampleSize1 + sampleSize2))
			{
				if (toCanMove)
				{
					if (pointers.cend() != ++to)
					{
						sampleSize2 += (*to)->second.SampleSize();
					}
				}
			}
		} while (minSampleSize > ret.SampleSize() + sampleSize1 + sampleSize2);

		if (toCanMove)
		{
			++to;
		}

		for (auto it = from; it != to; ++it)
		{
			if (requestedDiffIt != it)
			{
				ret += (*it)->second;
			}
		}
		m_aggregateCache[ratingAlgo][leagueInfo][ratingDiff] = ret;
		return ret;
	}

	std::set<double, RatingStatEntry::DoubleLess> RatingStats::GetAllKeysFromDistributions(const ARatingAlgorithm& ratingAlgo, const database_handler::v2::LeagueInfo& leagueInfo) const
	{
		std::set<double, RatingStatEntry::DoubleLess>& ret = m_distroKeyCache[ratingAlgo][leagueInfo];
		if (0 != ret.size())
		{
			return ret;
		}

		for (const auto& [_, statsEntry] : ratingAlgoToLeagueToStats.at(ratingAlgo).at(leagueInfo))
		{
			for (const auto& [key, _] : statsEntry.distribution)
			{
				ret.insert(key);
			}
		}
		return ret;
	}

	double RatingStats::AverageError(const ARatingAlgorithm& ratingAlgo, const database_handler::v2::LeagueInfo& leagueInfo) const
	{
		double& avgError = m_avgErrorCache[ratingAlgo][leagueInfo];
		if (0 != avgError)
		{
			return avgError;
		}
		const auto& statsMap = ratingAlgoToLeagueToStats.at(ratingAlgo).at(leagueInfo);
		avgError = std::accumulate(statsMap.cbegin(), statsMap.cend(), 0.0, [](double sum, const auto& kvPair)
			{
				return sum + kvPair.second.AbsError();
			}) / statsMap.size();

		return avgError;
	}

	double RatingStats::WeightedAvgError(const ARatingAlgorithm& ratingAlgo, const database_handler::v2::LeagueInfo& leagueInfo) const
	{
		double& avgError = m_avgWeightedErrorCache[ratingAlgo][leagueInfo];
		if (0 != avgError)
		{
			return avgError;
		}
		const auto& statsMap = ratingAlgoToLeagueToStats.at(ratingAlgo).at(leagueInfo);
		double totalError = 0.0;
		int totalSampleSize = 0;
		for (const auto& [_, stats] : statsMap)
		{
			totalError += stats.AbsError() * stats.SampleSize();
			totalSampleSize += stats.SampleSize();
		}
		avgError = totalError / totalSampleSize;
		return avgError;
	}

	void RatingStats::AddRatingTransfer(
		const ARatingAlgorithm& ratingAlgo, 
		const database_handler::v2::LeagueInfo& leagueInfo, 
		int ratingTransferred)
	{
		if (OVERALL_STATS == leagueInfo)
		{
			throw std::invalid_argument("Overall stats is handled automatically. Don't insert into it manually");
		}
		m_totalRatingTransferred[ratingAlgo][leagueInfo] += std::abs(ratingTransferred);
		m_totalRatingTransferred[ratingAlgo][OVERALL_STATS] += std::abs(ratingTransferred);
	}

	int RatingStats::GetTotalRatingTransferred(const ARatingAlgorithm& ratingAlgo, const database_handler::v2::LeagueInfo& leagueInfo)
	{
		return m_totalRatingTransferred.at(ratingAlgo).at(leagueInfo);
	}

	RatingStatEntry RatingStats::FindNextRatingDiff(const std::map<RatingPair, RatingStatEntry>& statsMap, RatingPair ratingDiff) const
	{
		if (statsMap.empty())
		{
			throw std::invalid_argument("statsMap is empty");
		}

		const int step = ratingDiff.rating < 0 ? 1 : -1;
		for (size_t i = 0; i < statsMap.size(); i++)
		{
			if (0 != statsMap.count(ratingDiff))
			{
				return statsMap.at(ratingDiff);
			}
			ratingDiff.rating += step;
		}
		throw std::logic_error("Should never get here.");
	}
}