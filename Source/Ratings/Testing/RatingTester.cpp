#include "SportsPredictionLab/Source/FileIO/Ratings/RatingTesterOutput.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/BestAlgorithms.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"
#include "SportsPredictionLab/Source/Ratings/RatingCalculation.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingTester.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
//std
#include <sstream>

namespace sports_prediction_lab
{
	using std::endl;
	using namespace database_handler::v2;

	namespace
	{
		void PrintRatingStats(const std::map<int, RatingStatEntry> ratingStats)
		{
			double sumError = 0.0;
			double weightedSumError = 0.0;
			int totalResults = 0;

			TCOUT << endl;
			for (const auto& [diff, resultStat] : ratingStats)
			{
				TCOUT << resultStat.RatingRange() << ": ";
				for (const auto& [key, val] : resultStat.distribution)
				{
					TCOUT << key << ": " << val << ", ";
				}
				TCOUT << "tot: " << resultStat.SampleSize() << ", act: " << resultStat.Avg() << ", pred: " <<
					resultStat.expectedPoints.points << endl;

				sumError += resultStat.AbsError();
				weightedSumError += resultStat.AbsError() * resultStat.SampleSize();
				totalResults += resultStat.SampleSize();
			}
			TCOUT << "\nAvg error: " << sumError / ratingStats.size();
			TCOUT << "\nWeighted avg error: " << weightedSumError / totalResults;

			TCOUT << endl;
		}
	}

	RatingStats TestRatingAlgorithms(
		std::vector<RatingCalculationTask>& ratingTasks,
		bool writeRatingStatsToFile,
		bool printLeagues,
		bool printMatches,
		const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& testSet)
	{
		using namespace database_handler;
		using namespace database_handler::common;
		using namespace database_handler::v2;

		SQLite::Database& db = GetDb(DbType::V2);
		const auto& allMatchResults = testSet.empty() ? QueryAllMatchResults(db) : testSet;

		RatingStats ret;

		if (printLeagues)
		{
			TCOUT << "Collecting rating statistics for..." << endl;
		}
		for (const auto& [leagueInfo, leagueResults] : allMatchResults)
		{
			//if ("Tipico Bundesliga" != leagueInfo.league)
			//{
			//	continue;
			//}

			using utils::string_conversion::StdStringToUtilsString;
			if (printLeagues)
			{
				TCOUT << StdStringToUtilsString(leagueInfo.country) << " - " << StdStringToUtilsString(leagueInfo.league) << endl;
			}
			const int numMatchesInASeason = leagueInfo.numTeams * (leagueInfo.numTeams - 1);

			RatingCalculator ratingCalculator(leagueResults.cbegin(), leagueResults.cend(), ratingTasks, printMatches);

			size_t matchCount = 0;
			do
			{
				if (!ratingCalculator.UpdateTeams())
				{
					break;
				}

				for (auto& task : ratingCalculator.Tasks())
				{
					auto& leagueRatingStats = ret.ratingAlgoToLeagueToStats[*task.spRatingAlgo][leagueInfo];
					if (matchCount >= numMatchesInASeason * task.spRatingAlgo->warmUp)
					{
						const auto it = ratingCalculator.CurrentMatch();
						const RatingPair& homeTeamRating = task.Rating(it->fixtureInfo.homeTeam);
						const RatingPair& awayTeamRating = task.Rating(it->fixtureInfo.awayTeam);
						const auto& relevantRatings = task.spRatingAlgo->GetRelevantRatings(homeTeamRating, awayTeamRating);
						const RatingPair ratingDiff = relevantRatings.first - relevantRatings.second;

						if (0 == leagueRatingStats.count(ratingDiff))
						{
							leagueRatingStats[ratingDiff].expectedPoints = task.spRatingAlgo->GetExpectedPointsA(homeTeamRating, awayTeamRating);
							leagueRatingStats.at(ratingDiff).SetRatingRange(ratingDiff.rating, 1);
						}

						auto& overallTaskRatingStats = ret.ratingAlgoToLeagueToStats[*task.spRatingAlgo][ret.OVERALL_STATS];
						if (0 == overallTaskRatingStats.count(ratingDiff))
						{
							overallTaskRatingStats[ratingDiff].expectedPoints = task.spRatingAlgo->GetExpectedPointsA(homeTeamRating, awayTeamRating);
							overallTaskRatingStats.at(ratingDiff).SetRatingRange(ratingDiff.rating, 1);
						}

						const auto scores = task.getScores(*it);
						const double result = task.spRatingAlgo->spPointCalculator->GetPointsA(scores.first, scores.second);
						ret.AddRatingTransfer(
							*task.spRatingAlgo,
							leagueInfo,
							task.spRatingAlgo->GetRatingsTransferred(PointsPair{ result, 0 }, homeTeamRating, awayTeamRating).rating);
						leagueRatingStats.at(ratingDiff).Increment(result);
						overallTaskRatingStats.at(ratingDiff).Increment(result);
					}
					if (1 == std::distance(ratingCalculator.CurrentMatch(), leagueResults.cend()))
					{
						//PrintRatingStats(leagueRatingStats);
					}
				}
				ratingCalculator.UpdateRatings();
				++matchCount;
			} while (ratingCalculator.Step());
		}
		//PrintRatingStats(overallRatingStatsPerAlgo.cbegin()->second);
		if (writeRatingStatsToFile)
		{
			for (const auto& [ratingAlgo, statsMap] : ret.ratingAlgoToLeagueToStats)
			{
				file_io::WriteRatingStats(ratingAlgo, ret);
			}
		}
		return ret;
	}

	RatingStats TestRatingAlgorithm(
		const RatingCalculationTask& ratingTask,
		bool writeRatingStatsToFile,
		bool printLeagues,
		bool printMatches,
		const std::map<LeagueInfo, std::set<MatchResult>>& testSet)
	{
		std::vector<RatingCalculationTask> taskVec{ ratingTask };
		return TestRatingAlgorithms(taskVec, writeRatingStatsToFile, printLeagues, printMatches, testSet);
	}

	void TestRatingAlgorithms()
	{
		EloRating threeWayRating;
		threeWayRating.homeAwaySeparate = true;
		threeWayRating.spPointCalculator = std::make_shared<ThreeWayPointCalc>();
		threeWayRating.base = 350.093;
		threeWayRating.k = 3.4676;
		threeWayRating.magnifier = 167.424;

		//EloRating goalDiffRating;
		//goalDiffRating.homeAwaySeparate = true;
		//goalDiffRating.spPointCalculator = std::make_shared<GoalDiffPointCalc>();
		//goalDiffRating.minPoints = -5.0;
		//goalDiffRating.maxPoints = 5.0;
		//goalDiffRating.base = 699.022;
		//goalDiffRating.k = 3.2;
		//goalDiffRating.magnifier = 1556.64;

		const auto goalGetter = [](const database_handler::v2::MatchResult& result)
		{
			return std::make_pair(result.home_goals, result.away_goals);
		};

		std::vector<RatingCalculationTask> tasks;
		//tasks.emplace_back(
		//	RatingCalculationTask(
		//		GetBestAlgorithm(point_calc_names::GOAL_DIFF),
		//		goalGetter
		//	)
		//);
		tasks.emplace_back(
			RatingCalculationTask(
				std::make_shared<EloRating>(threeWayRating),
				goalGetter
			)
		);
		const auto stats = TestRatingAlgorithms(
			tasks,
			true /*writeRatingStatsToFile*/,
			true /*printLeagues*/,
			false /*printMatches*/);
		TCOUT << "Avg. Err: " << stats.AverageError(threeWayRating, RatingStats::OVERALL_STATS) <<
			", Avg. Weighted Err: " << stats.WeightedAvgError(threeWayRating, RatingStats::OVERALL_STATS) << endl;
	}
}