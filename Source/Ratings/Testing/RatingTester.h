#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"
#include "SportsPredictionLab/Source/Ratings/RatingCalculation.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingStats.h"
#include "SportsPredictionLab/Source/Ratings/Testing/RatingStatEntry.h"
//lib
#include "DatabaseHandler/DatabaseHandler.h"
#include "Utils/Utils.h"

namespace sports_prediction_lab
{
	RatingStats TestRatingAlgorithms(
		std::vector<RatingCalculationTask>& ratingTasks, 
		bool writeRatingStatsToFile, 
		bool printLeagues,
		bool printMatches,
		const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& testSet = {});

	RatingStats TestRatingAlgorithm(
		const RatingCalculationTask& ratingTask,
		bool writeRatingStatsToFile,
		bool printLeagues,
		bool printMatches,
		const std::map<database_handler::v2::LeagueInfo, std::set<database_handler::v2::MatchResult>>& testSet = {});

	void TestRatingAlgorithms();
}