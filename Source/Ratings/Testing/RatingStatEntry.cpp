#include "SportsPredictionLab/Source/Ratings/Testing/RatingStatEntry.h"
//std
#include <numeric>

namespace sports_prediction_lab
{
	namespace
	{
		std::map<double, int, RatingStatEntry::DoubleLess> MergeDistributions(
			const std::map<double, int, RatingStatEntry::DoubleLess>& a,
			const std::map<double, int, RatingStatEntry::DoubleLess>& b)
		{
			std::map<double, int, RatingStatEntry::DoubleLess> ret = a;
			for (const auto& [key, val] : b)
			{
				if (0 == ret.count(key))
				{
					ret[key] = val;
				}
				else
				{
					ret.at(key) += val;
				}
			}
			return ret;
		}
	}

	int RatingStatEntry::SampleSize() const
	{
		return std::accumulate(distribution.cbegin(), distribution.cend(), 0, [](int sum, const auto& kvPair)
			{ return sum + kvPair.second; });
	}

	double RatingStatEntry::Avg() const
	{
		const double total = std::accumulate(distribution.cbegin(), distribution.cend(), 0, [](double sum, const auto& kvPair)
			{ return sum + (kvPair.first * kvPair.second); });
		return total / SampleSize();
	}

	utils::String RatingStatEntry::RatingRange() const
	{
		if (lowerBound.rating == upperBound.rating)
		{
			return utils::ToString(lowerBound.rating);
		}

		utils::String ret;
		if (lowerBound.rating < 0)
		{
			ret += STR("(");
		}
		ret += utils::ToString(lowerBound.rating);
		if (lowerBound.rating < 0)
		{
			ret += STR(")");
		}
		ret += STR(" - ");
		if (upperBound.rating < 0)
		{
			ret += STR("(");
		}
		ret += utils::ToString(upperBound.rating);
		if (upperBound.rating < 0)
		{
			ret += STR(")");
		}
		return ret;
	}

	int RatingStatEntry::GetValueOrZero(double key) const
	{
		if (0 < distribution.count(key))
		{
			return distribution.at(key);
		}
		return 0;
	}

	double RatingStatEntry::GetProbability(double key) const
	{
		return static_cast<double>(GetValueOrZero(key)) / SampleSize();
	}

	std::map<double, LineProbabilities, RatingStatEntry::DoubleLess> RatingStatEntry::GetHandicapLines() const
	{
		std::map<double, LineProbabilities, DoubleLess> ret;

		//calculates 0.5 lines
		double line = 0.0;
		double underProb = 0.0;
		double overProb = 1.0;
		for (const auto& [key, _] : distribution)
		{
			line = key - 0.5;
			ret[-line] = { overProb, underProb };	//flips the key and the probabilities, because the handicap line is the inverse of the expected gal difference
			const double prob = GetProbability(key);
			underProb += prob;
			overProb -= prob;
			const double threshold = 0.000001;
			if (threshold > std::abs(underProb)) 
			{ 
				underProb = 0; 
			}
			if (threshold > std::abs(overProb))
			{
				overProb = 0;
			}
		}
		ret[-(line + 1)] = { overProb, underProb };

		//calculates 0.0 lines
		const int sampleSize = SampleSize();
		int underSampleSize = 0;
		int overSampleSize = sampleSize;
		for (const auto& [key, occurances] : distribution)
		{
			line = key == 0 ? key : -key;	//this is only for the looks. To avoid -0.0, which doesnt look that pretty
			overSampleSize -= occurances;
			underProb = underSampleSize / (static_cast<double>(sampleSize) - static_cast<double>(occurances));	//casts to avoid overflow
			overProb = overSampleSize / (static_cast<double>(sampleSize) - static_cast<double>(occurances));
			ret[line] = { overProb, underProb };
			underSampleSize += occurances;
		}

		//calculates quarter lines
		std::set<double, DoubleLess> keys;
		std::transform(ret.cbegin(), ret.cend(), std::inserter(keys, keys.begin()),
			[](const auto& kvPair)
			{
				return kvPair.first;
			});

		auto firstIt = keys.cbegin();
		auto secondIt = firstIt++;

		for(;keys.cend() != firstIt; ++firstIt, ++secondIt)
		{
			line = (*firstIt + *secondIt) / 2;
			underProb = (ret.at(*firstIt).underProb + ret.at(*secondIt).underProb) / 2;
			overProb = (ret.at(*firstIt).overProb + ret.at(*secondIt).overProb) / 2;

			ret[line] = { underProb, overProb };
		}

		return ret;
	}

	void RatingStatEntry::SetRatingRange(int lowerBound, int step)
	{
		this->lowerBound.rating = lowerBound;
		this->upperBound.rating = lowerBound + step - 1;
	}

	void RatingStatEntry::SetExpectedPoints(const ARatingAlgorithm& ratingAlgorithm, const RatingPair& lowerBound, int step)
	{
		RatingPair zero;
		zero.rating = 0;
		zero.counterRating = 0;

		for (size_t i = 0; i < step; i++)
		{
			expectedPoints += ratingAlgorithm.GetExpectedPointsA(lowerBound + 1, zero);
		}
		expectedPoints /= step;
	}

	void RatingStatEntry::Increment(double val)
	{
		++distribution[val];
	}

	RatingStatEntry RatingStatEntry::operator+(const RatingStatEntry& other) const
	{
		RatingStatEntry ret;
		ret.lowerBound = std::min(lowerBound, other.lowerBound);
		ret.upperBound = std::max(upperBound, other.upperBound);
		ret.distribution = MergeDistributions(distribution, other.distribution);
		ret.expectedPoints = (expectedPoints * SampleSize() + other.expectedPoints * other.SampleSize()) / (SampleSize() + other.SampleSize());
		return ret;
	}

	RatingStatEntry& RatingStatEntry::operator+=(const RatingStatEntry& other)
	{
		*this = *this + other;
		return *this;
	}

	utils::OStream& operator<<(utils::OStream& os, const RatingStatEntry& entry)
	{
		os << entry.RatingRange() << STR(", ");
		for (const auto [key, val] : entry.distribution)
		{
			os << key << ": " << val << ", ";
		}
		os << entry.SampleSize() << STR(", ") << entry.Avg() << STR(", ") << entry.expectedPoints.points;

		return os;
	}
}