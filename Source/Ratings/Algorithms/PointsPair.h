#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/RatingPair.h"
//std
#include <optional>

namespace sports_prediction_lab
{
	struct PointsPair
	{
		double points = 0.0;
		std::optional<double> counterPoints;

		PointsPair operator-(const PointsPair& other) const;
		PointsPair operator+(const PointsPair& other) const;
		PointsPair& operator+=(const PointsPair& other);
		PointsPair operator/(int i) const;
		PointsPair& operator/=(int i);
		explicit operator RatingPair() const;
	};

	PointsPair operator*(int i, const PointsPair& pp);
	PointsPair operator*(const PointsPair& pp, int i);
	PointsPair Round(const PointsPair& pp);
	PointsPair Abs(const PointsPair& pp);
}