//own
#include "EloRating.h"
#include "Source/Ratings/Ratings.h"
//std
#include <cmath>

namespace sports_prediction_lab
{
	PointsPair EloRating::GetExpectedPointsA(const RatingPair& ratingA, const RatingPair& ratingB) const
	{
		PointsPair ret;

		if (homeAwaySeparate)
		{
			ret.points = EloFormula(ratingA.rating, ratingB.counterRating.value());
		}
		else
		{
			ret.points = EloFormula(ratingA.rating, ratingB.rating);
		}
		return ret;
	}
	RatingPair EloRating::GetRatingsTransferred(const PointsPair& pointsA, const RatingPair& ratingA, const RatingPair& ratingB) const
	{
		return static_cast<RatingPair>(Round(k * (pointsA - GetExpectedPointsA(ratingA, ratingB))));
	}
	double EloRating::EloFormula(int ratingA, int ratingB) const
	{
		if (0 == magnifier)
		{
			throw std::logic_error("Division by 0: magnifier");
		}
		return ::abs(maxPoints - minPoints) / (1 + std::pow(base, static_cast<double>(ratingB - ratingA) / magnifier)) + minPoints;
	}
	utils::String EloRating::Description() const
	{
		utils::StringStream ss;
		ss << name << " " << spPointCalculator->Description() << " homeAwaySeparate=" << homeAwaySeparate;
		return ss.str();
	}
	utils::String EloRating::Details() const
	{
		utils::StringStream ss;
		ss << std::boolalpha;
		ss << Description() << " k=" << k << " magnifier=" << magnifier << " base=" << base << " minPoints=" << minPoints << " maxPoints=" << maxPoints;
		return ss.str();
	}
}