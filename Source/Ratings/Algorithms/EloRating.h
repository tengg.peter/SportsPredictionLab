#pragma once
//own
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"

namespace sports_prediction_lab
{
	class EloRating : public ARatingAlgorithm
	{		
	public:
		EloRating() : ARatingAlgorithm(STR("EloRating")){}

		virtual PointsPair GetExpectedPointsA(const RatingPair& ratingA, const RatingPair& ratingB) const override;
		virtual utils::String Description() const override;
		virtual utils::String Details() const override;

	public:
		//default: 32
		double k = 32;
		//default: 400
		double magnifier = 400;
		//default: 10
		double base = 10;
		//default: 1.0
		double maxPoints = 1;
		//default: 0.0
		double minPoints = 0;

	protected:
		virtual RatingPair GetRatingsTransferred(const PointsPair& pointsA, const RatingPair& ratingA, const RatingPair& ratingB) const override;
	
	private:
		double EloFormula(int ratingA, int ratingB) const;
	};
}