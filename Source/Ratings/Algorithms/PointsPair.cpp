#include "SportsPredictionLab/Source/Ratings/Algorithms/PointsPair.h"
//std
#include <cmath>

namespace sports_prediction_lab
{
	PointsPair PointsPair::operator-(const PointsPair& other) const
	{
		PointsPair ret;
		ret.points = points - other.points;
		if (counterPoints.has_value() && other.counterPoints.has_value())
		{
			ret.counterPoints = counterPoints.value() - other.counterPoints.value();
		}
		return ret;
	}

	PointsPair PointsPair::operator+(const PointsPair& other) const
	{
		PointsPair ret;
		ret.points = points + other.points;
		if (counterPoints.has_value() && other.counterPoints.has_value())
		{
			ret.counterPoints = counterPoints.value() + other.counterPoints.value();
		}
		return ret;
	}

	PointsPair::operator RatingPair() const
	{
		RatingPair ret;
		ret.rating = static_cast<int>(points);
		if (counterPoints.has_value())
		{
			ret.counterRating = static_cast<int>(counterPoints.value());
		}
		return ret;
	}

	PointsPair& PointsPair::operator+=(const PointsPair& other)
	{
		*this = *this + other;
		return *this;
	}

	PointsPair PointsPair::operator/(int i) const
	{
		PointsPair ret;
		ret.points = points / i;
		if (counterPoints.has_value())
		{
			ret.counterPoints = counterPoints.value() / i;
		}
		return ret;
	}

	PointsPair& PointsPair::operator/=(int i)
	{
		*this = *this / i;
		return *this;
	}

	PointsPair operator*(int i, const PointsPair& pp)
	{
		PointsPair ret;
		ret.points = pp.points * i;
		if (pp.counterPoints.has_value())
		{
			ret.counterPoints = pp.counterPoints.value() * i;
		}
		return ret;
	}

	PointsPair operator*(const PointsPair& pp, int i)
	{
		return i * pp;
	}

	PointsPair Round(const PointsPair& pp)
	{
		PointsPair ret;
		ret.points = ::round(pp.points);
		if (pp.counterPoints.has_value())
		{
			ret.counterPoints = ::round(pp.counterPoints.value());
		}
		return ret;
	}

	PointsPair Abs(const PointsPair& pp)
	{
		PointsPair ret;
		ret.points = std::abs(pp.points);
		if (pp.counterPoints.has_value())
		{
			ret.counterPoints = std::abs(pp.counterPoints.value());
		}
		return ret;
	}
}