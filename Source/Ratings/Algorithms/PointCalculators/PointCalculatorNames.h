#pragma once
//lib
#include "Utils/Utils.h"

namespace sports_prediction_lab::point_calc_names
{
	const inline utils::String _1X2{ STR("1x2") };
	const inline utils::String GOAL_DIFF{ STR("GoalDiff") };
}