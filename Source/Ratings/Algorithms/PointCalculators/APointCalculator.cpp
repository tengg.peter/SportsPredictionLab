#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/APointCalculator.h"

namespace sports_prediction_lab
{
	APointCalculator::APointCalculator(const utils::String& name)
		: name(name)
	{}

	double APointCalculator::GetPointsA(int scoreA, int scoreB) const
	{
		double a, b;
		return GetPoints(scoreA, scoreB, a, b);
	}
}