#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"

namespace sports_prediction_lab
{
	GoalDiffPointCalc::GoalDiffPointCalc()
		: APointCalculator(point_calc_names::GOAL_DIFF)
	{}

	double GoalDiffPointCalc::GetPoints(int scoreA, int scoreB, double& pointsA, double& pointsB) const
	{
		pointsA = scoreA - scoreB;
		pointsB = -pointsA;
		return pointsA;
	}
	utils::String GoalDiffPointCalc::Description() const
	{
		return name;
	}
}