#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/APointCalculator.h"
//lib
#include "Utils/Utils.h"

namespace sports_prediction_lab
{
	class ThreeWayPointCalc : public APointCalculator
	{
	public:
		ThreeWayPointCalc();
		virtual double GetPoints(int scoreA, int scoreB, double& pointsA, double& pointsB) const override;
		virtual utils::String Description() const override;
	};
}