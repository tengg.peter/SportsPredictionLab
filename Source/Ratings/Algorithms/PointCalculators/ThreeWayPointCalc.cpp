#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/ThreeWayPointCalc.h"

namespace sports_prediction_lab
{
	ThreeWayPointCalc::ThreeWayPointCalc()
		: APointCalculator(point_calc_names::_1X2)
	{}

	double ThreeWayPointCalc::GetPoints(int scoreA, int scoreB, double& pointsA, double& pointsB) const
	{
		if (scoreA > scoreB)
		{
			pointsA = 1;
			pointsB = 0;
		}
		else if (scoreA < scoreB)
		{
			pointsA = 0;
			pointsB = 1;
		}
		else
		{
			pointsA = 0.5f;
			pointsB = 0.5f;
		}
		return pointsA;
	}
	utils::String ThreeWayPointCalc::Description() const
	{
		return name;
	}
}