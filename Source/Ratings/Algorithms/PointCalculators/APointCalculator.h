#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointsPair.h"
//lib
#include "Utils/Utils.h"

namespace sports_prediction_lab
{
	class APointCalculator
	{
	public:
		APointCalculator() = default;
		APointCalculator(const utils::String& name);

		virtual double GetPointsA(int scoreA, int scoreB) const;
		virtual double GetPoints(int scoreA, int scoreB, double& pointsA, double& pointsB) const = 0;
		virtual utils::String Description() const = 0;

	public:
		utils::String name = STR("APointCalculator");
	};
}