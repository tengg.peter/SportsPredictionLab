#pragma once
//own
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/ThreeWayPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointsPair.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/RatingPair.h"
#include "SportsPredictionLab/Source/Ratings/Ratings.h"

//lib
#include "Utils/Utils.h"
//std
#include <memory>
#include <optional>
#include <vector>

namespace sports_prediction_lab
{
	class ARatingAlgorithm
	{
	public:
		ARatingAlgorithm() = default;
		ARatingAlgorithm(const utils::String& name) : name(name){}

		virtual void UpdateRatings(int scoreA, int scoreB, RatingPair& ratingA, RatingPair& ratingB) const;
		virtual PointsPair GetExpectedPointsA(const RatingPair& ratingA, const RatingPair& ratingB) const = 0;
		virtual utils::String Description() const = 0;
		virtual utils::String Details() const = 0;
		virtual std::pair<RatingPair, RatingPair> GetRelevantRatings(const RatingPair& homeRatings, const RatingPair& awayRatings) const;
		virtual RatingPair GetRatingsTransferred(const PointsPair& pointsA, const RatingPair& ratingA, const RatingPair& ratingB) const = 0;
		virtual ~ARatingAlgorithm() {}

	public:
		//default: ThreeWayPointCalc
		std::shared_ptr<APointCalculator> spPointCalculator = std::make_shared<ThreeWayPointCalc>();
		//default: false
		bool homeAwaySeparate = false;
		//default: 1.0
		double warmUp = 1.0;
		utils::String name = STR("ARatingAlgorithm");
	};

	inline bool operator<(const std::reference_wrapper<const ARatingAlgorithm>& a, const std::reference_wrapper<const ARatingAlgorithm>& b)
	{
		return a.get().Details() < b.get().Details();
	}
}