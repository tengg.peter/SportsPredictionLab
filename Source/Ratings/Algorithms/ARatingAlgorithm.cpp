#include "ARatingAlgorithm.h"

namespace sports_prediction_lab
{
	void ARatingAlgorithm::UpdateRatings(int scoreA, int scoreB, RatingPair& ratingA, RatingPair& ratingB) const
	{
		if (homeAwaySeparate)
		{
			++ratingA.numMatches;
			if (!ratingB.numCounterMatches.has_value())
			{
				ratingB.numCounterMatches = 0;
				ratingB.counterRating = ratingB.rating;	//initialises to default value, 1600 that is
			}
			++ratingB.numCounterMatches.value();
		}
		else
		{
			++ratingA.numMatches;
			++ratingB.numMatches;
		}

		PointsPair ppA;
		double pointsA, pointsB;
		spPointCalculator->GetPoints(scoreA, scoreB, pointsA, pointsB);

		ppA.points = pointsA;
		RatingPair ratingsTransferred = GetRatingsTransferred(ppA, ratingA, ratingB);
		
		if (homeAwaySeparate)
		{
			ratingA.rating += ratingsTransferred.rating;
			ratingB.counterRating.value() -= ratingsTransferred.rating;
		}
		else
		{
			ratingA += ratingsTransferred;
			ratingB -= ratingsTransferred;
		}
	}
	std::pair<RatingPair, RatingPair> ARatingAlgorithm::GetRelevantRatings(const RatingPair& homeRatings, const RatingPair& awayRatings) const
	{
		std::pair<RatingPair, RatingPair> retVal;
		if (homeAwaySeparate)
		{
			retVal.first.rating = homeRatings.rating;
			retVal.second.rating = awayRatings.counterRating.value();
		}
		else
		{
			retVal.first.rating = homeRatings.rating;
			retVal.second.rating = awayRatings.rating;
		}
		return retVal;
	}
}