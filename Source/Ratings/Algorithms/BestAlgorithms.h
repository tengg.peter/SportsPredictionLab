#pragma once
#include "SportsPredictionLab/Source/Ratings/Algorithms/ARatingAlgorithm.h"
//lib
#include "Utils/Utils.h"
//std
#include <memory>

namespace sports_prediction_lab
{
	std::shared_ptr<ARatingAlgorithm> GetBestAlgorithm(const utils::String& pointCalcName);
	std::shared_ptr<ARatingAlgorithm> GetClassicRatingAlgo(const utils::String& pointCalcName);
}