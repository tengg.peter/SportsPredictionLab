#include "SportsPredictionLab/Source/Ratings/Algorithms/BestAlgorithms.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/EloRating.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/GoalDiffPointCalc.h"
#include "SportsPredictionLab/Source/Ratings/Algorithms/PointCalculators/PointCalculatorNames.h"


namespace sports_prediction_lab
{
	std::shared_ptr<ARatingAlgorithm> GetBestAlgorithm(const utils::String& pointCalcName)
	{
		if (point_calc_names::_1X2 == pointCalcName)
		{
			EloRating threeWayRating;
			threeWayRating.homeAwaySeparate = true;
			threeWayRating.spPointCalculator = std::make_shared<ThreeWayPointCalc>();
			threeWayRating.base = 401.549;
			threeWayRating.k = 12.0212;
			threeWayRating.magnifier = 649.009;
			//837.766;6.0726;353.613 err: 0.0326241
			//993.916;3.4676;267.792 err: 0.02936
			//350.006;3.4676;167.424 err: 0.0264012

			return std::make_shared<EloRating>(threeWayRating);
		}
		if (point_calc_names::GOAL_DIFF == pointCalcName)
		{
			EloRating goalDiffRating;
			goalDiffRating.homeAwaySeparate = true;
			goalDiffRating.spPointCalculator = std::make_shared<GoalDiffPointCalc>();
			goalDiffRating.minPoints = -5.0;
			goalDiffRating.maxPoints = 5.0;
			goalDiffRating.base = 699.022;
			goalDiffRating.k = 3.2;
			goalDiffRating.magnifier = 1556.64;

			return std::make_shared<EloRating>(goalDiffRating);
		}
	}
	
	std::shared_ptr<ARatingAlgorithm> GetClassicRatingAlgo(const utils::String& pointCalcName)
	{
		if (point_calc_names::_1X2 == pointCalcName)
		{
			return std::make_shared<EloRating>();	//use default values. (Voobly style)
		}
		if (point_calc_names::GOAL_DIFF == pointCalcName)
		{
			EloRating goalDiffRating;
			goalDiffRating.homeAwaySeparate = true;
			goalDiffRating.spPointCalculator = std::make_shared<GoalDiffPointCalc>();
			goalDiffRating.minPoints = -5.0;
			goalDiffRating.maxPoints = 5.0;
			goalDiffRating.k = 6.4;

			return std::make_shared<EloRating>(goalDiffRating);
		}
	}
}