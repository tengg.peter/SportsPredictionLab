#include "SportsPredictionLab/Source/Ratings/Algorithms/RatingPair.h"

namespace sports_prediction_lab
{
	RatingPair RatingPair::operator-(const RatingPair& other) const
	{
		RatingPair ret;
		ret.rating = rating - other.rating;
		ret.numMatches = numMatches;
		if (counterRating.has_value() && other.counterRating.has_value())
		{
			ret.counterRating = counterRating.value() - other.counterRating.value();
			ret.numCounterMatches = numCounterMatches;
		}
		return ret;
	}
	RatingPair& RatingPair::operator-=(const RatingPair& other)
	{
		*this = *this - other;
		return *this;
	}
	RatingPair RatingPair::operator+(const RatingPair& other) const
	{
		RatingPair ret;
		ret.rating = rating + other.rating;
		ret.numMatches = numMatches;
		if (counterRating.has_value() && other.counterRating.has_value())
		{
			ret.counterRating = counterRating.value() + other.counterRating.value();
			ret.numCounterMatches = numCounterMatches;
		}
		return ret;
	}
	RatingPair& RatingPair::operator+=(const RatingPair& other)
	{
		*this = *this + other;
		return *this;
	}
	RatingPair RatingPair::operator+(int i) const
	{
		RatingPair ret;
		ret.rating = rating + i;
		ret.numMatches = numMatches;
		if (counterRating.has_value())
		{
			ret.counterRating = counterRating.value() + i;
			ret.numCounterMatches = numCounterMatches;
		}
		return ret;
	}

	bool RatingPair::operator<(const RatingPair& other) const
	{
		const int thisSumRating = rating + counterRating.value_or(0);
		const int otherSumRating = other.rating + other.counterRating.value_or(0);
		
		if (thisSumRating != otherSumRating)
		{
			return thisSumRating < otherSumRating;
		}
		if (rating != other.rating)
		{
			return rating < other.rating;
		}
		return false;
	}

	bool RatingPair::operator>(const RatingPair& other) const
	{
		const int thisSumRating = rating + counterRating.value_or(0);
		const int otherSumRating = other.rating + other.counterRating.value_or(0);

		if (thisSumRating != otherSumRating)
		{
			return thisSumRating > otherSumRating;
		}
		if (rating != other.rating)
		{
			return rating > other.rating;
		}
		return false;
	}
}