#pragma once
//std
#include <optional>

namespace sports_prediction_lab
{
	struct RatingPair
	{
		int rating = 1600;
		int numMatches = 0;
		std::optional<int> counterRating;
		std::optional<int> numCounterMatches;

		RatingPair operator-(const RatingPair& other) const;
		RatingPair& operator-=(const RatingPair& other);
		RatingPair operator+(const RatingPair& other) const;
		RatingPair operator+(int i) const;
		RatingPair& operator+=(const RatingPair& other);
		bool operator<(const RatingPair& other) const;
		bool operator>(const RatingPair& other) const;
	};
}