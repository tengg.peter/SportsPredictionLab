#pragma once

#include "DatabaseHandler/DatabaseHandler.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

class InMemoryDbFixture : public ::testing::Test {
public:

    InMemoryDbFixture() : db(":memory:", SQLite::OPEN_CREATE | SQLite::OPEN_READWRITE)
    {
        
    }

    SQLite::Database db;
};