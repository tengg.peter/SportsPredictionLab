#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/SportMonksCache/DataClasses/CacheEntry.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, CacheEntryTests)
{
	using namespace database_handler;
	using namespace database_handler::sm_cache;
	using namespace database_handler::common;
	
	CacheEntry cacheEntry1;
	cacheEntry1.query = STR("query1");
	cacheEntry1.updated = STR("updated2");
	cacheEntry1.json_response = STR("json_response3");
	
	CacheEntry cacheEntry2;
	
	CacheEntry cacheEntry3;
	cacheEntry3.query = STR("query4");
	cacheEntry3.updated = STR("updated5");
	cacheEntry3.json_response = STR("json_response6");
	
	std::vector<UpCacheEntry> oneCacheEntry;
	oneCacheEntry.emplace_back(std::make_unique<CacheEntry>(cacheEntry1));

	std::vector<UpCacheEntry> twocacheEntries;
	twocacheEntries.emplace_back(std::make_unique<CacheEntry>(cacheEntry1));
	twocacheEntries.emplace_back(std::make_unique<CacheEntry>(cacheEntry2));

	std::vector<UpCacheEntry> threecacheEntries;
	threecacheEntries.emplace_back(std::make_unique<CacheEntry>(cacheEntry1));
	threecacheEntries.emplace_back(std::make_unique<CacheEntry>(cacheEntry2));
	threecacheEntries.emplace_back(std::make_unique<CacheEntry>(cacheEntry3));

	EXPECT_EQ(1, oneCacheEntry.size());
	EXPECT_EQ(2, twocacheEntries.size());
	EXPECT_EQ(3, threecacheEntries.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_CACHE_ENTRIES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneCacheEntry));
	ASSERT_EQ(1, QueryCount<CacheEntry>(db));
	{
		auto cacheEntries = QueryAll<CacheEntry>(db);
		EXPECT_EQ(1, cacheEntries.size());

		EXPECT_NE(0, cacheEntries[0]->id);
		EXPECT_EQ(cacheEntry1.query, cacheEntries[0]->query);
		EXPECT_EQ(cacheEntry1.updated, cacheEntries[0]->updated);
		EXPECT_EQ(cacheEntry1.json_response, cacheEntries[0]->json_response);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twocacheEntries));
	ASSERT_EQ(2, QueryCount<CacheEntry>(db));
	{
		auto cacheEntries = QueryAll<CacheEntry>(db);
		EXPECT_EQ(2, cacheEntries.size());

		EXPECT_NE(0, cacheEntries[0]->id);
		EXPECT_EQ(cacheEntry1.query, cacheEntries[0]->query);
		EXPECT_EQ(cacheEntry1.updated, cacheEntries[0]->updated);
		EXPECT_EQ(cacheEntry1.json_response, cacheEntries[0]->json_response);
		

		EXPECT_NE(0, cacheEntries[1]->id);
		EXPECT_EQ(cacheEntry2.query, cacheEntries[1]->query);
		EXPECT_EQ(cacheEntry2.updated, cacheEntries[1]->updated);
		EXPECT_EQ(cacheEntry2.json_response, cacheEntries[1]->json_response);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threecacheEntries));
	ASSERT_EQ(3, QueryCount<CacheEntry>(db));
	{
		auto cacheEntries = QueryAll<CacheEntry>(db);
		EXPECT_EQ(3, cacheEntries.size());

		EXPECT_NE(0, cacheEntries[0]->id);
		EXPECT_EQ(cacheEntry1.query, cacheEntries[0]->query);
		EXPECT_EQ(cacheEntry1.updated, cacheEntries[0]->updated);
		EXPECT_EQ(cacheEntry1.json_response, cacheEntries[0]->json_response);
		

		EXPECT_NE(0, cacheEntries[1]->id);
		EXPECT_EQ(cacheEntry2.query, cacheEntries[1]->query);
		EXPECT_EQ(cacheEntry2.updated, cacheEntries[1]->updated);
		EXPECT_EQ(cacheEntry2.json_response, cacheEntries[1]->json_response);
		

		EXPECT_NE(0, cacheEntries[2]->id);
		EXPECT_EQ(cacheEntry3.query, cacheEntries[2]->query);
		EXPECT_EQ(cacheEntry3.updated, cacheEntries[2]->updated);
		EXPECT_EQ(cacheEntry3.json_response, cacheEntries[2]->json_response);
		
	}
}