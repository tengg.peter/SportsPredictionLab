#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalLine.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TeamGoalLineTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TeamGoalLine teamGoalLine1;
	teamGoalLine1.team_season_statistics_id = 1;
	teamGoalLine1.line_over = STR("line_over2");
	teamGoalLine1.home_percentage = 3.4;
	teamGoalLine1.away_percentage = 5.6;
	
	TeamGoalLine teamGoalLine2;
	
	TeamGoalLine teamGoalLine3;
	teamGoalLine3.team_season_statistics_id = 7;
	teamGoalLine3.line_over = STR("line_over8");
	teamGoalLine3.home_percentage = 9.10;
	teamGoalLine3.away_percentage = 11.12;
	
	std::vector<UpTeamGoalLine> oneTeamGoalLine;
	oneTeamGoalLine.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine1));

	std::vector<UpTeamGoalLine> twoteamGoalLines;
	twoteamGoalLines.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine1));
	twoteamGoalLines.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine2));

	std::vector<UpTeamGoalLine> threeteamGoalLines;
	threeteamGoalLines.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine1));
	threeteamGoalLines.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine2));
	threeteamGoalLines.emplace_back(std::make_unique<TeamGoalLine>(teamGoalLine3));

	EXPECT_EQ(1, oneTeamGoalLine.size());
	EXPECT_EQ(2, twoteamGoalLines.size());
	EXPECT_EQ(3, threeteamGoalLines.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TEAM_GOAL_LINES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTeamGoalLine));
	ASSERT_EQ(1, QueryCount<TeamGoalLine>(db));
	{
		auto teamGoalLines = QueryAll<TeamGoalLine>(db);
		EXPECT_EQ(1, teamGoalLines.size());

		EXPECT_NE(0, teamGoalLines[0]->id);
		EXPECT_EQ(teamGoalLine1.team_season_statistics_id, teamGoalLines[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine1.line_over, teamGoalLines[0]->line_over);
		EXPECT_EQ(teamGoalLine1.home_percentage, teamGoalLines[0]->home_percentage);
		EXPECT_EQ(teamGoalLine1.away_percentage, teamGoalLines[0]->away_percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoteamGoalLines));
	ASSERT_EQ(2, QueryCount<TeamGoalLine>(db));
	{
		auto teamGoalLines = QueryAll<TeamGoalLine>(db);
		EXPECT_EQ(2, teamGoalLines.size());

		EXPECT_NE(0, teamGoalLines[0]->id);
		EXPECT_EQ(teamGoalLine1.team_season_statistics_id, teamGoalLines[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine1.line_over, teamGoalLines[0]->line_over);
		EXPECT_EQ(teamGoalLine1.home_percentage, teamGoalLines[0]->home_percentage);
		EXPECT_EQ(teamGoalLine1.away_percentage, teamGoalLines[0]->away_percentage);
		

		EXPECT_NE(0, teamGoalLines[1]->id);
		EXPECT_EQ(teamGoalLine2.team_season_statistics_id, teamGoalLines[1]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine2.line_over, teamGoalLines[1]->line_over);
		EXPECT_EQ(teamGoalLine2.home_percentage, teamGoalLines[1]->home_percentage);
		EXPECT_EQ(teamGoalLine2.away_percentage, teamGoalLines[1]->away_percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeteamGoalLines));
	ASSERT_EQ(3, QueryCount<TeamGoalLine>(db));
	{
		auto teamGoalLines = QueryAll<TeamGoalLine>(db);
		EXPECT_EQ(3, teamGoalLines.size());

		EXPECT_NE(0, teamGoalLines[0]->id);
		EXPECT_EQ(teamGoalLine1.team_season_statistics_id, teamGoalLines[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine1.line_over, teamGoalLines[0]->line_over);
		EXPECT_EQ(teamGoalLine1.home_percentage, teamGoalLines[0]->home_percentage);
		EXPECT_EQ(teamGoalLine1.away_percentage, teamGoalLines[0]->away_percentage);
		

		EXPECT_NE(0, teamGoalLines[1]->id);
		EXPECT_EQ(teamGoalLine2.team_season_statistics_id, teamGoalLines[1]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine2.line_over, teamGoalLines[1]->line_over);
		EXPECT_EQ(teamGoalLine2.home_percentage, teamGoalLines[1]->home_percentage);
		EXPECT_EQ(teamGoalLine2.away_percentage, teamGoalLines[1]->away_percentage);
		

		EXPECT_NE(0, teamGoalLines[2]->id);
		EXPECT_EQ(teamGoalLine3.team_season_statistics_id, teamGoalLines[2]->team_season_statistics_id);
		EXPECT_EQ(teamGoalLine3.line_over, teamGoalLines[2]->line_over);
		EXPECT_EQ(teamGoalLine3.home_percentage, teamGoalLines[2]->home_percentage);
		EXPECT_EQ(teamGoalLine3.away_percentage, teamGoalLines[2]->away_percentage);
		
	}
}