#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/StandingPosition.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, StandingPositionTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	StandingPosition standingPosition1;
	standingPosition1.sm_standing_id = 1;
	standingPosition1.position = 2;
	standingPosition1.sm_team_id = 3;
	standingPosition1.team_name = STR("team_name4");
	standingPosition1.sm_round_id = 5;
	standingPosition1.round_name = 6;
	standingPosition1.sm_group_id_opt = 7;
	standingPosition1.group_name_opt = STR("group_name_opt8");
	standingPosition1.home_games_played = 9;
	standingPosition1.home_won = 10;
	standingPosition1.home_draw = 11;
	standingPosition1.home_lost = 12;
	standingPosition1.home_goals_scored = 13;
	standingPosition1.home_goals_against = 14;
	standingPosition1.away_games_played = 15;
	standingPosition1.away_won = 16;
	standingPosition1.away_draw = 17;
	standingPosition1.away_lost = 18;
	standingPosition1.away_goals_scored = 19;
	standingPosition1.away_goals_against = 20;
	standingPosition1.result_opt = STR("result_opt21");
	standingPosition1.recent_form_opt = STR("recent_form_opt22");
	standingPosition1.status_opt = STR("status_opt23");
	
	StandingPosition standingPosition2;
	
	StandingPosition standingPosition3;
	standingPosition3.sm_standing_id = 24;
	standingPosition3.position = 25;
	standingPosition3.sm_team_id = 26;
	standingPosition3.team_name = STR("team_name27");
	standingPosition3.sm_round_id = 28;
	standingPosition3.round_name = 29;
	standingPosition3.sm_group_id_opt = 30;
	standingPosition3.group_name_opt = STR("group_name_opt31");
	standingPosition3.home_games_played = 32;
	standingPosition3.home_won = 33;
	standingPosition3.home_draw = 34;
	standingPosition3.home_lost = 35;
	standingPosition3.home_goals_scored = 36;
	standingPosition3.home_goals_against = 37;
	standingPosition3.away_games_played = 38;
	standingPosition3.away_won = 39;
	standingPosition3.away_draw = 40;
	standingPosition3.away_lost = 41;
	standingPosition3.away_goals_scored = 42;
	standingPosition3.away_goals_against = 43;
	standingPosition3.result_opt = STR("result_opt44");
	standingPosition3.recent_form_opt = STR("recent_form_opt45");
	standingPosition3.status_opt = STR("status_opt46");
	
	std::vector<UpStandingPosition> oneStandingPosition;
	oneStandingPosition.emplace_back(std::make_unique<StandingPosition>(standingPosition1));

	std::vector<UpStandingPosition> twostandingPositions;
	twostandingPositions.emplace_back(std::make_unique<StandingPosition>(standingPosition1));
	twostandingPositions.emplace_back(std::make_unique<StandingPosition>(standingPosition2));

	std::vector<UpStandingPosition> threestandingPositions;
	threestandingPositions.emplace_back(std::make_unique<StandingPosition>(standingPosition1));
	threestandingPositions.emplace_back(std::make_unique<StandingPosition>(standingPosition2));
	threestandingPositions.emplace_back(std::make_unique<StandingPosition>(standingPosition3));

	EXPECT_EQ(1, oneStandingPosition.size());
	EXPECT_EQ(2, twostandingPositions.size());
	EXPECT_EQ(3, threestandingPositions.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_STANDING_POSITIONS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneStandingPosition));
	ASSERT_EQ(1, QueryCount<StandingPosition>(db));
	{
		auto standingPositions = QueryAll<StandingPosition>(db);
		EXPECT_EQ(1, standingPositions.size());

		EXPECT_NE(0, standingPositions[0]->id);
		EXPECT_EQ(standingPosition1.sm_standing_id, standingPositions[0]->sm_standing_id);
		EXPECT_EQ(standingPosition1.position, standingPositions[0]->position);
		EXPECT_EQ(standingPosition1.sm_team_id, standingPositions[0]->sm_team_id);
		EXPECT_EQ(standingPosition1.team_name, standingPositions[0]->team_name);
		EXPECT_EQ(standingPosition1.sm_round_id, standingPositions[0]->sm_round_id);
		EXPECT_EQ(standingPosition1.round_name, standingPositions[0]->round_name);
		EXPECT_EQ(standingPosition1.sm_group_id_opt, standingPositions[0]->sm_group_id_opt);
		EXPECT_EQ(standingPosition1.group_name_opt, standingPositions[0]->group_name_opt);
		EXPECT_EQ(standingPosition1.home_games_played, standingPositions[0]->home_games_played);
		EXPECT_EQ(standingPosition1.home_won, standingPositions[0]->home_won);
		EXPECT_EQ(standingPosition1.home_draw, standingPositions[0]->home_draw);
		EXPECT_EQ(standingPosition1.home_lost, standingPositions[0]->home_lost);
		EXPECT_EQ(standingPosition1.home_goals_scored, standingPositions[0]->home_goals_scored);
		EXPECT_EQ(standingPosition1.home_goals_against, standingPositions[0]->home_goals_against);
		EXPECT_EQ(standingPosition1.away_games_played, standingPositions[0]->away_games_played);
		EXPECT_EQ(standingPosition1.away_won, standingPositions[0]->away_won);
		EXPECT_EQ(standingPosition1.away_draw, standingPositions[0]->away_draw);
		EXPECT_EQ(standingPosition1.away_lost, standingPositions[0]->away_lost);
		EXPECT_EQ(standingPosition1.away_goals_scored, standingPositions[0]->away_goals_scored);
		EXPECT_EQ(standingPosition1.away_goals_against, standingPositions[0]->away_goals_against);
		EXPECT_EQ(standingPosition1.result_opt, standingPositions[0]->result_opt);
		EXPECT_EQ(standingPosition1.recent_form_opt, standingPositions[0]->recent_form_opt);
		EXPECT_EQ(standingPosition1.status_opt, standingPositions[0]->status_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twostandingPositions));
	ASSERT_EQ(2, QueryCount<StandingPosition>(db));
	{
		auto standingPositions = QueryAll<StandingPosition>(db);
		EXPECT_EQ(2, standingPositions.size());

		EXPECT_NE(0, standingPositions[0]->id);
		EXPECT_EQ(standingPosition1.sm_standing_id, standingPositions[0]->sm_standing_id);
		EXPECT_EQ(standingPosition1.position, standingPositions[0]->position);
		EXPECT_EQ(standingPosition1.sm_team_id, standingPositions[0]->sm_team_id);
		EXPECT_EQ(standingPosition1.team_name, standingPositions[0]->team_name);
		EXPECT_EQ(standingPosition1.sm_round_id, standingPositions[0]->sm_round_id);
		EXPECT_EQ(standingPosition1.round_name, standingPositions[0]->round_name);
		EXPECT_EQ(standingPosition1.sm_group_id_opt, standingPositions[0]->sm_group_id_opt);
		EXPECT_EQ(standingPosition1.group_name_opt, standingPositions[0]->group_name_opt);
		EXPECT_EQ(standingPosition1.home_games_played, standingPositions[0]->home_games_played);
		EXPECT_EQ(standingPosition1.home_won, standingPositions[0]->home_won);
		EXPECT_EQ(standingPosition1.home_draw, standingPositions[0]->home_draw);
		EXPECT_EQ(standingPosition1.home_lost, standingPositions[0]->home_lost);
		EXPECT_EQ(standingPosition1.home_goals_scored, standingPositions[0]->home_goals_scored);
		EXPECT_EQ(standingPosition1.home_goals_against, standingPositions[0]->home_goals_against);
		EXPECT_EQ(standingPosition1.away_games_played, standingPositions[0]->away_games_played);
		EXPECT_EQ(standingPosition1.away_won, standingPositions[0]->away_won);
		EXPECT_EQ(standingPosition1.away_draw, standingPositions[0]->away_draw);
		EXPECT_EQ(standingPosition1.away_lost, standingPositions[0]->away_lost);
		EXPECT_EQ(standingPosition1.away_goals_scored, standingPositions[0]->away_goals_scored);
		EXPECT_EQ(standingPosition1.away_goals_against, standingPositions[0]->away_goals_against);
		EXPECT_EQ(standingPosition1.result_opt, standingPositions[0]->result_opt);
		EXPECT_EQ(standingPosition1.recent_form_opt, standingPositions[0]->recent_form_opt);
		EXPECT_EQ(standingPosition1.status_opt, standingPositions[0]->status_opt);
		

		EXPECT_NE(0, standingPositions[1]->id);
		EXPECT_EQ(standingPosition2.sm_standing_id, standingPositions[1]->sm_standing_id);
		EXPECT_EQ(standingPosition2.position, standingPositions[1]->position);
		EXPECT_EQ(standingPosition2.sm_team_id, standingPositions[1]->sm_team_id);
		EXPECT_EQ(standingPosition2.team_name, standingPositions[1]->team_name);
		EXPECT_EQ(standingPosition2.sm_round_id, standingPositions[1]->sm_round_id);
		EXPECT_EQ(standingPosition2.round_name, standingPositions[1]->round_name);
		EXPECT_EQ(standingPosition2.sm_group_id_opt, standingPositions[1]->sm_group_id_opt);
		EXPECT_EQ(standingPosition2.group_name_opt, standingPositions[1]->group_name_opt);
		EXPECT_EQ(standingPosition2.home_games_played, standingPositions[1]->home_games_played);
		EXPECT_EQ(standingPosition2.home_won, standingPositions[1]->home_won);
		EXPECT_EQ(standingPosition2.home_draw, standingPositions[1]->home_draw);
		EXPECT_EQ(standingPosition2.home_lost, standingPositions[1]->home_lost);
		EXPECT_EQ(standingPosition2.home_goals_scored, standingPositions[1]->home_goals_scored);
		EXPECT_EQ(standingPosition2.home_goals_against, standingPositions[1]->home_goals_against);
		EXPECT_EQ(standingPosition2.away_games_played, standingPositions[1]->away_games_played);
		EXPECT_EQ(standingPosition2.away_won, standingPositions[1]->away_won);
		EXPECT_EQ(standingPosition2.away_draw, standingPositions[1]->away_draw);
		EXPECT_EQ(standingPosition2.away_lost, standingPositions[1]->away_lost);
		EXPECT_EQ(standingPosition2.away_goals_scored, standingPositions[1]->away_goals_scored);
		EXPECT_EQ(standingPosition2.away_goals_against, standingPositions[1]->away_goals_against);
		EXPECT_EQ(standingPosition2.result_opt, standingPositions[1]->result_opt);
		EXPECT_EQ(standingPosition2.recent_form_opt, standingPositions[1]->recent_form_opt);
		EXPECT_EQ(standingPosition2.status_opt, standingPositions[1]->status_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threestandingPositions));
	ASSERT_EQ(3, QueryCount<StandingPosition>(db));
	{
		auto standingPositions = QueryAll<StandingPosition>(db);
		EXPECT_EQ(3, standingPositions.size());

		EXPECT_NE(0, standingPositions[0]->id);
		EXPECT_EQ(standingPosition1.sm_standing_id, standingPositions[0]->sm_standing_id);
		EXPECT_EQ(standingPosition1.position, standingPositions[0]->position);
		EXPECT_EQ(standingPosition1.sm_team_id, standingPositions[0]->sm_team_id);
		EXPECT_EQ(standingPosition1.team_name, standingPositions[0]->team_name);
		EXPECT_EQ(standingPosition1.sm_round_id, standingPositions[0]->sm_round_id);
		EXPECT_EQ(standingPosition1.round_name, standingPositions[0]->round_name);
		EXPECT_EQ(standingPosition1.sm_group_id_opt, standingPositions[0]->sm_group_id_opt);
		EXPECT_EQ(standingPosition1.group_name_opt, standingPositions[0]->group_name_opt);
		EXPECT_EQ(standingPosition1.home_games_played, standingPositions[0]->home_games_played);
		EXPECT_EQ(standingPosition1.home_won, standingPositions[0]->home_won);
		EXPECT_EQ(standingPosition1.home_draw, standingPositions[0]->home_draw);
		EXPECT_EQ(standingPosition1.home_lost, standingPositions[0]->home_lost);
		EXPECT_EQ(standingPosition1.home_goals_scored, standingPositions[0]->home_goals_scored);
		EXPECT_EQ(standingPosition1.home_goals_against, standingPositions[0]->home_goals_against);
		EXPECT_EQ(standingPosition1.away_games_played, standingPositions[0]->away_games_played);
		EXPECT_EQ(standingPosition1.away_won, standingPositions[0]->away_won);
		EXPECT_EQ(standingPosition1.away_draw, standingPositions[0]->away_draw);
		EXPECT_EQ(standingPosition1.away_lost, standingPositions[0]->away_lost);
		EXPECT_EQ(standingPosition1.away_goals_scored, standingPositions[0]->away_goals_scored);
		EXPECT_EQ(standingPosition1.away_goals_against, standingPositions[0]->away_goals_against);
		EXPECT_EQ(standingPosition1.result_opt, standingPositions[0]->result_opt);
		EXPECT_EQ(standingPosition1.recent_form_opt, standingPositions[0]->recent_form_opt);
		EXPECT_EQ(standingPosition1.status_opt, standingPositions[0]->status_opt);
		

		EXPECT_NE(0, standingPositions[1]->id);
		EXPECT_EQ(standingPosition2.sm_standing_id, standingPositions[1]->sm_standing_id);
		EXPECT_EQ(standingPosition2.position, standingPositions[1]->position);
		EXPECT_EQ(standingPosition2.sm_team_id, standingPositions[1]->sm_team_id);
		EXPECT_EQ(standingPosition2.team_name, standingPositions[1]->team_name);
		EXPECT_EQ(standingPosition2.sm_round_id, standingPositions[1]->sm_round_id);
		EXPECT_EQ(standingPosition2.round_name, standingPositions[1]->round_name);
		EXPECT_EQ(standingPosition2.sm_group_id_opt, standingPositions[1]->sm_group_id_opt);
		EXPECT_EQ(standingPosition2.group_name_opt, standingPositions[1]->group_name_opt);
		EXPECT_EQ(standingPosition2.home_games_played, standingPositions[1]->home_games_played);
		EXPECT_EQ(standingPosition2.home_won, standingPositions[1]->home_won);
		EXPECT_EQ(standingPosition2.home_draw, standingPositions[1]->home_draw);
		EXPECT_EQ(standingPosition2.home_lost, standingPositions[1]->home_lost);
		EXPECT_EQ(standingPosition2.home_goals_scored, standingPositions[1]->home_goals_scored);
		EXPECT_EQ(standingPosition2.home_goals_against, standingPositions[1]->home_goals_against);
		EXPECT_EQ(standingPosition2.away_games_played, standingPositions[1]->away_games_played);
		EXPECT_EQ(standingPosition2.away_won, standingPositions[1]->away_won);
		EXPECT_EQ(standingPosition2.away_draw, standingPositions[1]->away_draw);
		EXPECT_EQ(standingPosition2.away_lost, standingPositions[1]->away_lost);
		EXPECT_EQ(standingPosition2.away_goals_scored, standingPositions[1]->away_goals_scored);
		EXPECT_EQ(standingPosition2.away_goals_against, standingPositions[1]->away_goals_against);
		EXPECT_EQ(standingPosition2.result_opt, standingPositions[1]->result_opt);
		EXPECT_EQ(standingPosition2.recent_form_opt, standingPositions[1]->recent_form_opt);
		EXPECT_EQ(standingPosition2.status_opt, standingPositions[1]->status_opt);
		

		EXPECT_NE(0, standingPositions[2]->id);
		EXPECT_EQ(standingPosition3.sm_standing_id, standingPositions[2]->sm_standing_id);
		EXPECT_EQ(standingPosition3.position, standingPositions[2]->position);
		EXPECT_EQ(standingPosition3.sm_team_id, standingPositions[2]->sm_team_id);
		EXPECT_EQ(standingPosition3.team_name, standingPositions[2]->team_name);
		EXPECT_EQ(standingPosition3.sm_round_id, standingPositions[2]->sm_round_id);
		EXPECT_EQ(standingPosition3.round_name, standingPositions[2]->round_name);
		EXPECT_EQ(standingPosition3.sm_group_id_opt, standingPositions[2]->sm_group_id_opt);
		EXPECT_EQ(standingPosition3.group_name_opt, standingPositions[2]->group_name_opt);
		EXPECT_EQ(standingPosition3.home_games_played, standingPositions[2]->home_games_played);
		EXPECT_EQ(standingPosition3.home_won, standingPositions[2]->home_won);
		EXPECT_EQ(standingPosition3.home_draw, standingPositions[2]->home_draw);
		EXPECT_EQ(standingPosition3.home_lost, standingPositions[2]->home_lost);
		EXPECT_EQ(standingPosition3.home_goals_scored, standingPositions[2]->home_goals_scored);
		EXPECT_EQ(standingPosition3.home_goals_against, standingPositions[2]->home_goals_against);
		EXPECT_EQ(standingPosition3.away_games_played, standingPositions[2]->away_games_played);
		EXPECT_EQ(standingPosition3.away_won, standingPositions[2]->away_won);
		EXPECT_EQ(standingPosition3.away_draw, standingPositions[2]->away_draw);
		EXPECT_EQ(standingPosition3.away_lost, standingPositions[2]->away_lost);
		EXPECT_EQ(standingPosition3.away_goals_scored, standingPositions[2]->away_goals_scored);
		EXPECT_EQ(standingPosition3.away_goals_against, standingPositions[2]->away_goals_against);
		EXPECT_EQ(standingPosition3.result_opt, standingPositions[2]->result_opt);
		EXPECT_EQ(standingPosition3.recent_form_opt, standingPositions[2]->recent_form_opt);
		EXPECT_EQ(standingPosition3.status_opt, standingPositions[2]->status_opt);
		
	}
}