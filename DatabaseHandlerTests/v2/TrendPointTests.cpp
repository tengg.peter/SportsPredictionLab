#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TrendPoint.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TrendPointTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TrendPoint trendPoint1;
	trendPoint1.sm_trend_id = 1;
	trendPoint1.minute = 2.3;
	trendPoint1.amount = 4.5;
	
	TrendPoint trendPoint2;
	
	TrendPoint trendPoint3;
	trendPoint3.sm_trend_id = 6;
	trendPoint3.minute = 7.8;
	trendPoint3.amount = 9.10;
	
	std::vector<UpTrendPoint> oneTrendPoint;
	oneTrendPoint.emplace_back(std::make_unique<TrendPoint>(trendPoint1));

	std::vector<UpTrendPoint> twotrendPoints;
	twotrendPoints.emplace_back(std::make_unique<TrendPoint>(trendPoint1));
	twotrendPoints.emplace_back(std::make_unique<TrendPoint>(trendPoint2));

	std::vector<UpTrendPoint> threetrendPoints;
	threetrendPoints.emplace_back(std::make_unique<TrendPoint>(trendPoint1));
	threetrendPoints.emplace_back(std::make_unique<TrendPoint>(trendPoint2));
	threetrendPoints.emplace_back(std::make_unique<TrendPoint>(trendPoint3));

	EXPECT_EQ(1, oneTrendPoint.size());
	EXPECT_EQ(2, twotrendPoints.size());
	EXPECT_EQ(3, threetrendPoints.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TREND_POINTS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTrendPoint));
	ASSERT_EQ(1, QueryCount<TrendPoint>(db));
	{
		auto trendPoints = QueryAll<TrendPoint>(db);
		EXPECT_EQ(1, trendPoints.size());

		EXPECT_NE(0, trendPoints[0]->id);
		EXPECT_EQ(trendPoint1.sm_trend_id, trendPoints[0]->sm_trend_id);
		EXPECT_EQ(trendPoint1.minute, trendPoints[0]->minute);
		EXPECT_EQ(trendPoint1.amount, trendPoints[0]->amount);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twotrendPoints));
	ASSERT_EQ(2, QueryCount<TrendPoint>(db));
	{
		auto trendPoints = QueryAll<TrendPoint>(db);
		EXPECT_EQ(2, trendPoints.size());

		EXPECT_NE(0, trendPoints[0]->id);
		EXPECT_EQ(trendPoint1.sm_trend_id, trendPoints[0]->sm_trend_id);
		EXPECT_EQ(trendPoint1.minute, trendPoints[0]->minute);
		EXPECT_EQ(trendPoint1.amount, trendPoints[0]->amount);
		

		EXPECT_NE(0, trendPoints[1]->id);
		EXPECT_EQ(trendPoint2.sm_trend_id, trendPoints[1]->sm_trend_id);
		EXPECT_EQ(trendPoint2.minute, trendPoints[1]->minute);
		EXPECT_EQ(trendPoint2.amount, trendPoints[1]->amount);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threetrendPoints));
	ASSERT_EQ(3, QueryCount<TrendPoint>(db));
	{
		auto trendPoints = QueryAll<TrendPoint>(db);
		EXPECT_EQ(3, trendPoints.size());

		EXPECT_NE(0, trendPoints[0]->id);
		EXPECT_EQ(trendPoint1.sm_trend_id, trendPoints[0]->sm_trend_id);
		EXPECT_EQ(trendPoint1.minute, trendPoints[0]->minute);
		EXPECT_EQ(trendPoint1.amount, trendPoints[0]->amount);
		

		EXPECT_NE(0, trendPoints[1]->id);
		EXPECT_EQ(trendPoint2.sm_trend_id, trendPoints[1]->sm_trend_id);
		EXPECT_EQ(trendPoint2.minute, trendPoints[1]->minute);
		EXPECT_EQ(trendPoint2.amount, trendPoints[1]->amount);
		

		EXPECT_NE(0, trendPoints[2]->id);
		EXPECT_EQ(trendPoint3.sm_trend_id, trendPoints[2]->sm_trend_id);
		EXPECT_EQ(trendPoint3.minute, trendPoints[2]->minute);
		EXPECT_EQ(trendPoint3.amount, trendPoints[2]->amount);
		
	}
}