#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Market.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, MarketTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Market market1;
	market1.sm_id = 1;
	market1.name = STR("name2");
	
	Market market2;
	
	Market market3;
	market3.sm_id = 3;
	market3.name = STR("name4");
	
	std::vector<UpMarket> oneMarket;
	oneMarket.emplace_back(std::make_unique<Market>(market1));

	std::vector<UpMarket> twomarkets;
	twomarkets.emplace_back(std::make_unique<Market>(market1));
	twomarkets.emplace_back(std::make_unique<Market>(market2));

	std::vector<UpMarket> threemarkets;
	threemarkets.emplace_back(std::make_unique<Market>(market1));
	threemarkets.emplace_back(std::make_unique<Market>(market2));
	threemarkets.emplace_back(std::make_unique<Market>(market3));

	EXPECT_EQ(1, oneMarket.size());
	EXPECT_EQ(2, twomarkets.size());
	EXPECT_EQ(3, threemarkets.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_MARKETS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneMarket));
	ASSERT_EQ(1, QueryCount<Market>(db));
	{
		auto markets = QueryAll<Market>(db);
		EXPECT_EQ(1, markets.size());

		EXPECT_NE(0, markets[0]->id);
		EXPECT_EQ(market1.sm_id, markets[0]->sm_id);
		EXPECT_EQ(market1.name, markets[0]->name);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twomarkets));
	ASSERT_EQ(2, QueryCount<Market>(db));
	{
		auto markets = QueryAll<Market>(db);
		EXPECT_EQ(2, markets.size());

		EXPECT_NE(0, markets[0]->id);
		EXPECT_EQ(market1.sm_id, markets[0]->sm_id);
		EXPECT_EQ(market1.name, markets[0]->name);
		

		EXPECT_NE(0, markets[1]->id);
		EXPECT_EQ(market2.sm_id, markets[1]->sm_id);
		EXPECT_EQ(market2.name, markets[1]->name);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threemarkets));
	ASSERT_EQ(3, QueryCount<Market>(db));
	{
		auto markets = QueryAll<Market>(db);
		EXPECT_EQ(3, markets.size());

		EXPECT_NE(0, markets[0]->id);
		EXPECT_EQ(market1.sm_id, markets[0]->sm_id);
		EXPECT_EQ(market1.name, markets[0]->name);
		

		EXPECT_NE(0, markets[1]->id);
		EXPECT_EQ(market2.sm_id, markets[1]->sm_id);
		EXPECT_EQ(market2.name, markets[1]->name);
		

		EXPECT_NE(0, markets[2]->id);
		EXPECT_EQ(market3.sm_id, markets[2]->sm_id);
		EXPECT_EQ(market3.name, markets[2]->name);
		
	}
}