#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Odds.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, OddsTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Odds odds1;
	odds1.sm_fixture_id = 1;
	odds1.sm_market_id = 2;
	odds1.sm_bookmaker_id = 3;
	odds1.suspended = false;
	odds1.label = STR("label5");
	odds1.value = 6.7;
	odds1.probability_opt = 8.9;
	odds1.dp3_opt = STR("dp3_opt10");
	odds1.american = 11;
	odds1.fractional_opt = STR("fractional_opt12");
	odds1.winning_opt = true;
	odds1.handicap_opt = STR("handicap_opt14");
	odds1.total_opt = STR("total_opt15");
	odds1.stop = false;
	odds1.bookmaker_event_id_opt = 17;
	odds1.last_update = STR("last_update18");
	odds1.timezone_type_opt = 19;
	odds1.timezone = STR("timezone20");
	
	Odds odds2;
	
	Odds odds3;
	odds3.sm_fixture_id = 21;
	odds3.sm_market_id = 22;
	odds3.sm_bookmaker_id = 23;
	odds3.suspended = false;
	odds3.label = STR("label25");
	odds3.value = 26.27;
	odds3.probability_opt = 28.29;
	odds3.dp3_opt = STR("dp3_opt30");
	odds3.american = 31;
	odds3.fractional_opt = STR("fractional_opt32");
	odds3.winning_opt = true;
	odds3.handicap_opt = STR("handicap_opt34");
	odds3.total_opt = STR("total_opt35");
	odds3.stop = false;
	odds3.bookmaker_event_id_opt = 37;
	odds3.last_update = STR("last_update38");
	odds3.timezone_type_opt = 39;
	odds3.timezone = STR("timezone40");
	
	std::vector<UpOdds> oneOdds;
	oneOdds.emplace_back(std::make_unique<Odds>(odds1));

	std::vector<UpOdds> twoodds;
	twoodds.emplace_back(std::make_unique<Odds>(odds1));
	twoodds.emplace_back(std::make_unique<Odds>(odds2));

	std::vector<UpOdds> threeodds;
	threeodds.emplace_back(std::make_unique<Odds>(odds1));
	threeodds.emplace_back(std::make_unique<Odds>(odds2));
	threeodds.emplace_back(std::make_unique<Odds>(odds3));

	EXPECT_EQ(1, oneOdds.size());
	EXPECT_EQ(2, twoodds.size());
	EXPECT_EQ(3, threeodds.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_ODDS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneOdds));
	ASSERT_EQ(1, QueryCount<Odds>(db));
	{
		auto odds = QueryAll<Odds>(db);
		EXPECT_EQ(1, odds.size());

		EXPECT_NE(0, odds[0]->id);
		EXPECT_EQ(odds1.sm_fixture_id, odds[0]->sm_fixture_id);
		EXPECT_EQ(odds1.sm_market_id, odds[0]->sm_market_id);
		EXPECT_EQ(odds1.sm_bookmaker_id, odds[0]->sm_bookmaker_id);
		EXPECT_EQ(odds1.suspended, odds[0]->suspended);
		EXPECT_EQ(odds1.label, odds[0]->label);
		EXPECT_EQ(odds1.value, odds[0]->value);
		EXPECT_EQ(odds1.probability_opt, odds[0]->probability_opt);
		EXPECT_EQ(odds1.dp3_opt, odds[0]->dp3_opt);
		EXPECT_EQ(odds1.american, odds[0]->american);
		EXPECT_EQ(odds1.fractional_opt, odds[0]->fractional_opt);
		EXPECT_EQ(odds1.winning_opt, odds[0]->winning_opt);
		EXPECT_EQ(odds1.handicap_opt, odds[0]->handicap_opt);
		EXPECT_EQ(odds1.total_opt, odds[0]->total_opt);
		EXPECT_EQ(odds1.stop, odds[0]->stop);
		EXPECT_EQ(odds1.bookmaker_event_id_opt, odds[0]->bookmaker_event_id_opt);
		EXPECT_EQ(odds1.last_update, odds[0]->last_update);
		EXPECT_EQ(odds1.timezone_type_opt, odds[0]->timezone_type_opt);
		EXPECT_EQ(odds1.timezone, odds[0]->timezone);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoodds));
	ASSERT_EQ(2, QueryCount<Odds>(db));
	{
		auto odds = QueryAll<Odds>(db);
		EXPECT_EQ(2, odds.size());

		EXPECT_NE(0, odds[0]->id);
		EXPECT_EQ(odds1.sm_fixture_id, odds[0]->sm_fixture_id);
		EXPECT_EQ(odds1.sm_market_id, odds[0]->sm_market_id);
		EXPECT_EQ(odds1.sm_bookmaker_id, odds[0]->sm_bookmaker_id);
		EXPECT_EQ(odds1.suspended, odds[0]->suspended);
		EXPECT_EQ(odds1.label, odds[0]->label);
		EXPECT_EQ(odds1.value, odds[0]->value);
		EXPECT_EQ(odds1.probability_opt, odds[0]->probability_opt);
		EXPECT_EQ(odds1.dp3_opt, odds[0]->dp3_opt);
		EXPECT_EQ(odds1.american, odds[0]->american);
		EXPECT_EQ(odds1.fractional_opt, odds[0]->fractional_opt);
		EXPECT_EQ(odds1.winning_opt, odds[0]->winning_opt);
		EXPECT_EQ(odds1.handicap_opt, odds[0]->handicap_opt);
		EXPECT_EQ(odds1.total_opt, odds[0]->total_opt);
		EXPECT_EQ(odds1.stop, odds[0]->stop);
		EXPECT_EQ(odds1.bookmaker_event_id_opt, odds[0]->bookmaker_event_id_opt);
		EXPECT_EQ(odds1.last_update, odds[0]->last_update);
		EXPECT_EQ(odds1.timezone_type_opt, odds[0]->timezone_type_opt);
		EXPECT_EQ(odds1.timezone, odds[0]->timezone);
		

		EXPECT_NE(0, odds[1]->id);
		EXPECT_EQ(odds2.sm_fixture_id, odds[1]->sm_fixture_id);
		EXPECT_EQ(odds2.sm_market_id, odds[1]->sm_market_id);
		EXPECT_EQ(odds2.sm_bookmaker_id, odds[1]->sm_bookmaker_id);
		EXPECT_EQ(odds2.suspended, odds[1]->suspended);
		EXPECT_EQ(odds2.label, odds[1]->label);
		EXPECT_EQ(odds2.value, odds[1]->value);
		EXPECT_EQ(odds2.probability_opt, odds[1]->probability_opt);
		EXPECT_EQ(odds2.dp3_opt, odds[1]->dp3_opt);
		EXPECT_EQ(odds2.american, odds[1]->american);
		EXPECT_EQ(odds2.fractional_opt, odds[1]->fractional_opt);
		EXPECT_EQ(odds2.winning_opt, odds[1]->winning_opt);
		EXPECT_EQ(odds2.handicap_opt, odds[1]->handicap_opt);
		EXPECT_EQ(odds2.total_opt, odds[1]->total_opt);
		EXPECT_EQ(odds2.stop, odds[1]->stop);
		EXPECT_EQ(odds2.bookmaker_event_id_opt, odds[1]->bookmaker_event_id_opt);
		EXPECT_EQ(odds2.last_update, odds[1]->last_update);
		EXPECT_EQ(odds2.timezone_type_opt, odds[1]->timezone_type_opt);
		EXPECT_EQ(odds2.timezone, odds[1]->timezone);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeodds));
	ASSERT_EQ(3, QueryCount<Odds>(db));
	{
		auto odds = QueryAll<Odds>(db);
		EXPECT_EQ(3, odds.size());

		EXPECT_NE(0, odds[0]->id);
		EXPECT_EQ(odds1.sm_fixture_id, odds[0]->sm_fixture_id);
		EXPECT_EQ(odds1.sm_market_id, odds[0]->sm_market_id);
		EXPECT_EQ(odds1.sm_bookmaker_id, odds[0]->sm_bookmaker_id);
		EXPECT_EQ(odds1.suspended, odds[0]->suspended);
		EXPECT_EQ(odds1.label, odds[0]->label);
		EXPECT_EQ(odds1.value, odds[0]->value);
		EXPECT_EQ(odds1.probability_opt, odds[0]->probability_opt);
		EXPECT_EQ(odds1.dp3_opt, odds[0]->dp3_opt);
		EXPECT_EQ(odds1.american, odds[0]->american);
		EXPECT_EQ(odds1.fractional_opt, odds[0]->fractional_opt);
		EXPECT_EQ(odds1.winning_opt, odds[0]->winning_opt);
		EXPECT_EQ(odds1.handicap_opt, odds[0]->handicap_opt);
		EXPECT_EQ(odds1.total_opt, odds[0]->total_opt);
		EXPECT_EQ(odds1.stop, odds[0]->stop);
		EXPECT_EQ(odds1.bookmaker_event_id_opt, odds[0]->bookmaker_event_id_opt);
		EXPECT_EQ(odds1.last_update, odds[0]->last_update);
		EXPECT_EQ(odds1.timezone_type_opt, odds[0]->timezone_type_opt);
		EXPECT_EQ(odds1.timezone, odds[0]->timezone);
		

		EXPECT_NE(0, odds[1]->id);
		EXPECT_EQ(odds2.sm_fixture_id, odds[1]->sm_fixture_id);
		EXPECT_EQ(odds2.sm_market_id, odds[1]->sm_market_id);
		EXPECT_EQ(odds2.sm_bookmaker_id, odds[1]->sm_bookmaker_id);
		EXPECT_EQ(odds2.suspended, odds[1]->suspended);
		EXPECT_EQ(odds2.label, odds[1]->label);
		EXPECT_EQ(odds2.value, odds[1]->value);
		EXPECT_EQ(odds2.probability_opt, odds[1]->probability_opt);
		EXPECT_EQ(odds2.dp3_opt, odds[1]->dp3_opt);
		EXPECT_EQ(odds2.american, odds[1]->american);
		EXPECT_EQ(odds2.fractional_opt, odds[1]->fractional_opt);
		EXPECT_EQ(odds2.winning_opt, odds[1]->winning_opt);
		EXPECT_EQ(odds2.handicap_opt, odds[1]->handicap_opt);
		EXPECT_EQ(odds2.total_opt, odds[1]->total_opt);
		EXPECT_EQ(odds2.stop, odds[1]->stop);
		EXPECT_EQ(odds2.bookmaker_event_id_opt, odds[1]->bookmaker_event_id_opt);
		EXPECT_EQ(odds2.last_update, odds[1]->last_update);
		EXPECT_EQ(odds2.timezone_type_opt, odds[1]->timezone_type_opt);
		EXPECT_EQ(odds2.timezone, odds[1]->timezone);
		

		EXPECT_NE(0, odds[2]->id);
		EXPECT_EQ(odds3.sm_fixture_id, odds[2]->sm_fixture_id);
		EXPECT_EQ(odds3.sm_market_id, odds[2]->sm_market_id);
		EXPECT_EQ(odds3.sm_bookmaker_id, odds[2]->sm_bookmaker_id);
		EXPECT_EQ(odds3.suspended, odds[2]->suspended);
		EXPECT_EQ(odds3.label, odds[2]->label);
		EXPECT_EQ(odds3.value, odds[2]->value);
		EXPECT_EQ(odds3.probability_opt, odds[2]->probability_opt);
		EXPECT_EQ(odds3.dp3_opt, odds[2]->dp3_opt);
		EXPECT_EQ(odds3.american, odds[2]->american);
		EXPECT_EQ(odds3.fractional_opt, odds[2]->fractional_opt);
		EXPECT_EQ(odds3.winning_opt, odds[2]->winning_opt);
		EXPECT_EQ(odds3.handicap_opt, odds[2]->handicap_opt);
		EXPECT_EQ(odds3.total_opt, odds[2]->total_opt);
		EXPECT_EQ(odds3.stop, odds[2]->stop);
		EXPECT_EQ(odds3.bookmaker_event_id_opt, odds[2]->bookmaker_event_id_opt);
		EXPECT_EQ(odds3.last_update, odds[2]->last_update);
		EXPECT_EQ(odds3.timezone_type_opt, odds[2]->timezone_type_opt);
		EXPECT_EQ(odds3.timezone, odds[2]->timezone);
		
	}
}