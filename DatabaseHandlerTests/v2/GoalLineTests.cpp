#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/GoalLine.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, GoalLineTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	GoalLine goalLine1;
	goalLine1.sm_season_statistics_id = 1;
	goalLine1.line_over = STR("line_over2");
	goalLine1.percentage = 3.4;
	
	GoalLine goalLine2;
	
	GoalLine goalLine3;
	goalLine3.sm_season_statistics_id = 5;
	goalLine3.line_over = STR("line_over6");
	goalLine3.percentage = 7.8;
	
	std::vector<UpGoalLine> oneGoalLine;
	oneGoalLine.emplace_back(std::make_unique<GoalLine>(goalLine1));

	std::vector<UpGoalLine> twogoalLines;
	twogoalLines.emplace_back(std::make_unique<GoalLine>(goalLine1));
	twogoalLines.emplace_back(std::make_unique<GoalLine>(goalLine2));

	std::vector<UpGoalLine> threegoalLines;
	threegoalLines.emplace_back(std::make_unique<GoalLine>(goalLine1));
	threegoalLines.emplace_back(std::make_unique<GoalLine>(goalLine2));
	threegoalLines.emplace_back(std::make_unique<GoalLine>(goalLine3));

	EXPECT_EQ(1, oneGoalLine.size());
	EXPECT_EQ(2, twogoalLines.size());
	EXPECT_EQ(3, threegoalLines.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_GOAL_LINES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneGoalLine));
	ASSERT_EQ(1, QueryCount<GoalLine>(db));
	{
		auto goalLines = QueryAll<GoalLine>(db);
		EXPECT_EQ(1, goalLines.size());

		EXPECT_NE(0, goalLines[0]->id);
		EXPECT_EQ(goalLine1.sm_season_statistics_id, goalLines[0]->sm_season_statistics_id);
		EXPECT_EQ(goalLine1.line_over, goalLines[0]->line_over);
		EXPECT_EQ(goalLine1.percentage, goalLines[0]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twogoalLines));
	ASSERT_EQ(2, QueryCount<GoalLine>(db));
	{
		auto goalLines = QueryAll<GoalLine>(db);
		EXPECT_EQ(2, goalLines.size());

		EXPECT_NE(0, goalLines[0]->id);
		EXPECT_EQ(goalLine1.sm_season_statistics_id, goalLines[0]->sm_season_statistics_id);
		EXPECT_EQ(goalLine1.line_over, goalLines[0]->line_over);
		EXPECT_EQ(goalLine1.percentage, goalLines[0]->percentage);
		

		EXPECT_NE(0, goalLines[1]->id);
		EXPECT_EQ(goalLine2.sm_season_statistics_id, goalLines[1]->sm_season_statistics_id);
		EXPECT_EQ(goalLine2.line_over, goalLines[1]->line_over);
		EXPECT_EQ(goalLine2.percentage, goalLines[1]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threegoalLines));
	ASSERT_EQ(3, QueryCount<GoalLine>(db));
	{
		auto goalLines = QueryAll<GoalLine>(db);
		EXPECT_EQ(3, goalLines.size());

		EXPECT_NE(0, goalLines[0]->id);
		EXPECT_EQ(goalLine1.sm_season_statistics_id, goalLines[0]->sm_season_statistics_id);
		EXPECT_EQ(goalLine1.line_over, goalLines[0]->line_over);
		EXPECT_EQ(goalLine1.percentage, goalLines[0]->percentage);
		

		EXPECT_NE(0, goalLines[1]->id);
		EXPECT_EQ(goalLine2.sm_season_statistics_id, goalLines[1]->sm_season_statistics_id);
		EXPECT_EQ(goalLine2.line_over, goalLines[1]->line_over);
		EXPECT_EQ(goalLine2.percentage, goalLines[1]->percentage);
		

		EXPECT_NE(0, goalLines[2]->id);
		EXPECT_EQ(goalLine3.sm_season_statistics_id, goalLines[2]->sm_season_statistics_id);
		EXPECT_EQ(goalLine3.line_over, goalLines[2]->line_over);
		EXPECT_EQ(goalLine3.percentage, goalLines[2]->percentage);
		
	}
}