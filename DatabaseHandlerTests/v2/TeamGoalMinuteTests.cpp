#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalMinute.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TeamGoalMinuteTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TeamGoalMinute teamGoalMinute1;
	teamGoalMinute1.team_season_statistics_id = 1;
	teamGoalMinute1.type = STR("type2");
	teamGoalMinute1.period = STR("period3");
	teamGoalMinute1.count = 4;
	teamGoalMinute1.percentage = 5.6;
	
	TeamGoalMinute teamGoalMinute2;
	
	TeamGoalMinute teamGoalMinute3;
	teamGoalMinute3.team_season_statistics_id = 7;
	teamGoalMinute3.type = STR("type8");
	teamGoalMinute3.period = STR("period9");
	teamGoalMinute3.count = 10;
	teamGoalMinute3.percentage = 11.12;
	
	std::vector<UpTeamGoalMinute> oneTeamGoalMinute;
	oneTeamGoalMinute.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute1));

	std::vector<UpTeamGoalMinute> twoteamGoalMinutes;
	twoteamGoalMinutes.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute1));
	twoteamGoalMinutes.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute2));

	std::vector<UpTeamGoalMinute> threeteamGoalMinutes;
	threeteamGoalMinutes.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute1));
	threeteamGoalMinutes.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute2));
	threeteamGoalMinutes.emplace_back(std::make_unique<TeamGoalMinute>(teamGoalMinute3));

	EXPECT_EQ(1, oneTeamGoalMinute.size());
	EXPECT_EQ(2, twoteamGoalMinutes.size());
	EXPECT_EQ(3, threeteamGoalMinutes.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TEAM_GOAL_MINUTES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTeamGoalMinute));
	ASSERT_EQ(1, QueryCount<TeamGoalMinute>(db));
	{
		auto teamGoalMinutes = QueryAll<TeamGoalMinute>(db);
		EXPECT_EQ(1, teamGoalMinutes.size());

		EXPECT_NE(0, teamGoalMinutes[0]->id);
		EXPECT_EQ(teamGoalMinute1.team_season_statistics_id, teamGoalMinutes[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute1.type, teamGoalMinutes[0]->type);
		EXPECT_EQ(teamGoalMinute1.period, teamGoalMinutes[0]->period);
		EXPECT_EQ(teamGoalMinute1.count, teamGoalMinutes[0]->count);
		EXPECT_EQ(teamGoalMinute1.percentage, teamGoalMinutes[0]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoteamGoalMinutes));
	ASSERT_EQ(2, QueryCount<TeamGoalMinute>(db));
	{
		auto teamGoalMinutes = QueryAll<TeamGoalMinute>(db);
		EXPECT_EQ(2, teamGoalMinutes.size());

		EXPECT_NE(0, teamGoalMinutes[0]->id);
		EXPECT_EQ(teamGoalMinute1.team_season_statistics_id, teamGoalMinutes[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute1.type, teamGoalMinutes[0]->type);
		EXPECT_EQ(teamGoalMinute1.period, teamGoalMinutes[0]->period);
		EXPECT_EQ(teamGoalMinute1.count, teamGoalMinutes[0]->count);
		EXPECT_EQ(teamGoalMinute1.percentage, teamGoalMinutes[0]->percentage);
		

		EXPECT_NE(0, teamGoalMinutes[1]->id);
		EXPECT_EQ(teamGoalMinute2.team_season_statistics_id, teamGoalMinutes[1]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute2.type, teamGoalMinutes[1]->type);
		EXPECT_EQ(teamGoalMinute2.period, teamGoalMinutes[1]->period);
		EXPECT_EQ(teamGoalMinute2.count, teamGoalMinutes[1]->count);
		EXPECT_EQ(teamGoalMinute2.percentage, teamGoalMinutes[1]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeteamGoalMinutes));
	ASSERT_EQ(3, QueryCount<TeamGoalMinute>(db));
	{
		auto teamGoalMinutes = QueryAll<TeamGoalMinute>(db);
		EXPECT_EQ(3, teamGoalMinutes.size());

		EXPECT_NE(0, teamGoalMinutes[0]->id);
		EXPECT_EQ(teamGoalMinute1.team_season_statistics_id, teamGoalMinutes[0]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute1.type, teamGoalMinutes[0]->type);
		EXPECT_EQ(teamGoalMinute1.period, teamGoalMinutes[0]->period);
		EXPECT_EQ(teamGoalMinute1.count, teamGoalMinutes[0]->count);
		EXPECT_EQ(teamGoalMinute1.percentage, teamGoalMinutes[0]->percentage);
		

		EXPECT_NE(0, teamGoalMinutes[1]->id);
		EXPECT_EQ(teamGoalMinute2.team_season_statistics_id, teamGoalMinutes[1]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute2.type, teamGoalMinutes[1]->type);
		EXPECT_EQ(teamGoalMinute2.period, teamGoalMinutes[1]->period);
		EXPECT_EQ(teamGoalMinute2.count, teamGoalMinutes[1]->count);
		EXPECT_EQ(teamGoalMinute2.percentage, teamGoalMinutes[1]->percentage);
		

		EXPECT_NE(0, teamGoalMinutes[2]->id);
		EXPECT_EQ(teamGoalMinute3.team_season_statistics_id, teamGoalMinutes[2]->team_season_statistics_id);
		EXPECT_EQ(teamGoalMinute3.type, teamGoalMinutes[2]->type);
		EXPECT_EQ(teamGoalMinute3.period, teamGoalMinutes[2]->period);
		EXPECT_EQ(teamGoalMinute3.count, teamGoalMinutes[2]->count);
		EXPECT_EQ(teamGoalMinute3.percentage, teamGoalMinutes[2]->percentage);
		
	}
}