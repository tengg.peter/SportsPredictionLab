#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Team.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TeamTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Team team1;
	team1.sm_id = 1;
	team1.sm_legacy_id_opt = 2;
	team1.name = STR("name3");
	team1.short_code_opt = STR("short_code_opt4");
	team1.twitter_opt = STR("twitter_opt5");
	team1.sm_country_id = 6;
	team1.national_team = true;
	team1.founded_opt = 8;
	team1.logo_path_opt = STR("logo_path_opt9");
	team1.sm_venue_id_opt = 10;
	team1.sm_current_season_id_opt = 11;
	
	Team team2;
	
	Team team3;
	team3.sm_id = 12;
	team3.sm_legacy_id_opt = 13;
	team3.name = STR("name14");
	team3.short_code_opt = STR("short_code_opt15");
	team3.twitter_opt = STR("twitter_opt16");
	team3.sm_country_id = 17;
	team3.national_team = false;
	team3.founded_opt = 19;
	team3.logo_path_opt = STR("logo_path_opt20");
	team3.sm_venue_id_opt = 21;
	team3.sm_current_season_id_opt = 22;
	
	std::vector<UpTeam> oneTeam;
	oneTeam.emplace_back(std::make_unique<Team>(team1));

	std::vector<UpTeam> twoteams;
	twoteams.emplace_back(std::make_unique<Team>(team1));
	twoteams.emplace_back(std::make_unique<Team>(team2));

	std::vector<UpTeam> threeteams;
	threeteams.emplace_back(std::make_unique<Team>(team1));
	threeteams.emplace_back(std::make_unique<Team>(team2));
	threeteams.emplace_back(std::make_unique<Team>(team3));

	EXPECT_EQ(1, oneTeam.size());
	EXPECT_EQ(2, twoteams.size());
	EXPECT_EQ(3, threeteams.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TEAMS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTeam));
	ASSERT_EQ(1, QueryCount<Team>(db));
	{
		auto teams = QueryAll<Team>(db);
		EXPECT_EQ(1, teams.size());

		EXPECT_NE(0, teams[0]->id);
		EXPECT_EQ(team1.sm_id, teams[0]->sm_id);
		EXPECT_EQ(team1.sm_legacy_id_opt, teams[0]->sm_legacy_id_opt);
		EXPECT_EQ(team1.name, teams[0]->name);
		EXPECT_EQ(team1.short_code_opt, teams[0]->short_code_opt);
		EXPECT_EQ(team1.twitter_opt, teams[0]->twitter_opt);
		EXPECT_EQ(team1.sm_country_id, teams[0]->sm_country_id);
		EXPECT_EQ(team1.national_team, teams[0]->national_team);
		EXPECT_EQ(team1.founded_opt, teams[0]->founded_opt);
		EXPECT_EQ(team1.logo_path_opt, teams[0]->logo_path_opt);
		EXPECT_EQ(team1.sm_venue_id_opt, teams[0]->sm_venue_id_opt);
		EXPECT_EQ(team1.sm_current_season_id_opt, teams[0]->sm_current_season_id_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoteams));
	ASSERT_EQ(2, QueryCount<Team>(db));
	{
		auto teams = QueryAll<Team>(db);
		EXPECT_EQ(2, teams.size());

		EXPECT_NE(0, teams[0]->id);
		EXPECT_EQ(team1.sm_id, teams[0]->sm_id);
		EXPECT_EQ(team1.sm_legacy_id_opt, teams[0]->sm_legacy_id_opt);
		EXPECT_EQ(team1.name, teams[0]->name);
		EXPECT_EQ(team1.short_code_opt, teams[0]->short_code_opt);
		EXPECT_EQ(team1.twitter_opt, teams[0]->twitter_opt);
		EXPECT_EQ(team1.sm_country_id, teams[0]->sm_country_id);
		EXPECT_EQ(team1.national_team, teams[0]->national_team);
		EXPECT_EQ(team1.founded_opt, teams[0]->founded_opt);
		EXPECT_EQ(team1.logo_path_opt, teams[0]->logo_path_opt);
		EXPECT_EQ(team1.sm_venue_id_opt, teams[0]->sm_venue_id_opt);
		EXPECT_EQ(team1.sm_current_season_id_opt, teams[0]->sm_current_season_id_opt);
		

		EXPECT_NE(0, teams[1]->id);
		EXPECT_EQ(team2.sm_id, teams[1]->sm_id);
		EXPECT_EQ(team2.sm_legacy_id_opt, teams[1]->sm_legacy_id_opt);
		EXPECT_EQ(team2.name, teams[1]->name);
		EXPECT_EQ(team2.short_code_opt, teams[1]->short_code_opt);
		EXPECT_EQ(team2.twitter_opt, teams[1]->twitter_opt);
		EXPECT_EQ(team2.sm_country_id, teams[1]->sm_country_id);
		EXPECT_EQ(team2.national_team, teams[1]->national_team);
		EXPECT_EQ(team2.founded_opt, teams[1]->founded_opt);
		EXPECT_EQ(team2.logo_path_opt, teams[1]->logo_path_opt);
		EXPECT_EQ(team2.sm_venue_id_opt, teams[1]->sm_venue_id_opt);
		EXPECT_EQ(team2.sm_current_season_id_opt, teams[1]->sm_current_season_id_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeteams));
	ASSERT_EQ(3, QueryCount<Team>(db));
	{
		auto teams = QueryAll<Team>(db);
		EXPECT_EQ(3, teams.size());

		EXPECT_NE(0, teams[0]->id);
		EXPECT_EQ(team1.sm_id, teams[0]->sm_id);
		EXPECT_EQ(team1.sm_legacy_id_opt, teams[0]->sm_legacy_id_opt);
		EXPECT_EQ(team1.name, teams[0]->name);
		EXPECT_EQ(team1.short_code_opt, teams[0]->short_code_opt);
		EXPECT_EQ(team1.twitter_opt, teams[0]->twitter_opt);
		EXPECT_EQ(team1.sm_country_id, teams[0]->sm_country_id);
		EXPECT_EQ(team1.national_team, teams[0]->national_team);
		EXPECT_EQ(team1.founded_opt, teams[0]->founded_opt);
		EXPECT_EQ(team1.logo_path_opt, teams[0]->logo_path_opt);
		EXPECT_EQ(team1.sm_venue_id_opt, teams[0]->sm_venue_id_opt);
		EXPECT_EQ(team1.sm_current_season_id_opt, teams[0]->sm_current_season_id_opt);
		

		EXPECT_NE(0, teams[1]->id);
		EXPECT_EQ(team2.sm_id, teams[1]->sm_id);
		EXPECT_EQ(team2.sm_legacy_id_opt, teams[1]->sm_legacy_id_opt);
		EXPECT_EQ(team2.name, teams[1]->name);
		EXPECT_EQ(team2.short_code_opt, teams[1]->short_code_opt);
		EXPECT_EQ(team2.twitter_opt, teams[1]->twitter_opt);
		EXPECT_EQ(team2.sm_country_id, teams[1]->sm_country_id);
		EXPECT_EQ(team2.national_team, teams[1]->national_team);
		EXPECT_EQ(team2.founded_opt, teams[1]->founded_opt);
		EXPECT_EQ(team2.logo_path_opt, teams[1]->logo_path_opt);
		EXPECT_EQ(team2.sm_venue_id_opt, teams[1]->sm_venue_id_opt);
		EXPECT_EQ(team2.sm_current_season_id_opt, teams[1]->sm_current_season_id_opt);
		

		EXPECT_NE(0, teams[2]->id);
		EXPECT_EQ(team3.sm_id, teams[2]->sm_id);
		EXPECT_EQ(team3.sm_legacy_id_opt, teams[2]->sm_legacy_id_opt);
		EXPECT_EQ(team3.name, teams[2]->name);
		EXPECT_EQ(team3.short_code_opt, teams[2]->short_code_opt);
		EXPECT_EQ(team3.twitter_opt, teams[2]->twitter_opt);
		EXPECT_EQ(team3.sm_country_id, teams[2]->sm_country_id);
		EXPECT_EQ(team3.national_team, teams[2]->national_team);
		EXPECT_EQ(team3.founded_opt, teams[2]->founded_opt);
		EXPECT_EQ(team3.logo_path_opt, teams[2]->logo_path_opt);
		EXPECT_EQ(team3.sm_venue_id_opt, teams[2]->sm_venue_id_opt);
		EXPECT_EQ(team3.sm_current_season_id_opt, teams[2]->sm_current_season_id_opt);
		
	}
}