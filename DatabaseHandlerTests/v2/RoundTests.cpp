#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Round.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, RoundTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Round round1;
	round1.sm_id = 1;
	round1.name = 2;
	round1.sm_league_id = 3;
	round1.sm_season_id = 4;
	round1.sm_stage_id = 5;
	round1.start = STR("start6");
	round1.end = STR("end7");
	
	Round round2;
	
	Round round3;
	round3.sm_id = 8;
	round3.name = 9;
	round3.sm_league_id = 10;
	round3.sm_season_id = 11;
	round3.sm_stage_id = 12;
	round3.start = STR("start13");
	round3.end = STR("end14");
	
	std::vector<UpRound> oneRound;
	oneRound.emplace_back(std::make_unique<Round>(round1));

	std::vector<UpRound> tworounds;
	tworounds.emplace_back(std::make_unique<Round>(round1));
	tworounds.emplace_back(std::make_unique<Round>(round2));

	std::vector<UpRound> threerounds;
	threerounds.emplace_back(std::make_unique<Round>(round1));
	threerounds.emplace_back(std::make_unique<Round>(round2));
	threerounds.emplace_back(std::make_unique<Round>(round3));

	EXPECT_EQ(1, oneRound.size());
	EXPECT_EQ(2, tworounds.size());
	EXPECT_EQ(3, threerounds.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_ROUNDS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneRound));
	ASSERT_EQ(1, QueryCount<Round>(db));
	{
		auto rounds = QueryAll<Round>(db);
		EXPECT_EQ(1, rounds.size());

		EXPECT_NE(0, rounds[0]->id);
		EXPECT_EQ(round1.sm_id, rounds[0]->sm_id);
		EXPECT_EQ(round1.name, rounds[0]->name);
		EXPECT_EQ(round1.sm_league_id, rounds[0]->sm_league_id);
		EXPECT_EQ(round1.sm_season_id, rounds[0]->sm_season_id);
		EXPECT_EQ(round1.sm_stage_id, rounds[0]->sm_stage_id);
		EXPECT_EQ(round1.start, rounds[0]->start);
		EXPECT_EQ(round1.end, rounds[0]->end);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, tworounds));
	ASSERT_EQ(2, QueryCount<Round>(db));
	{
		auto rounds = QueryAll<Round>(db);
		EXPECT_EQ(2, rounds.size());

		EXPECT_NE(0, rounds[0]->id);
		EXPECT_EQ(round1.sm_id, rounds[0]->sm_id);
		EXPECT_EQ(round1.name, rounds[0]->name);
		EXPECT_EQ(round1.sm_league_id, rounds[0]->sm_league_id);
		EXPECT_EQ(round1.sm_season_id, rounds[0]->sm_season_id);
		EXPECT_EQ(round1.sm_stage_id, rounds[0]->sm_stage_id);
		EXPECT_EQ(round1.start, rounds[0]->start);
		EXPECT_EQ(round1.end, rounds[0]->end);
		

		EXPECT_NE(0, rounds[1]->id);
		EXPECT_EQ(round2.sm_id, rounds[1]->sm_id);
		EXPECT_EQ(round2.name, rounds[1]->name);
		EXPECT_EQ(round2.sm_league_id, rounds[1]->sm_league_id);
		EXPECT_EQ(round2.sm_season_id, rounds[1]->sm_season_id);
		EXPECT_EQ(round2.sm_stage_id, rounds[1]->sm_stage_id);
		EXPECT_EQ(round2.start, rounds[1]->start);
		EXPECT_EQ(round2.end, rounds[1]->end);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threerounds));
	ASSERT_EQ(3, QueryCount<Round>(db));
	{
		auto rounds = QueryAll<Round>(db);
		EXPECT_EQ(3, rounds.size());

		EXPECT_NE(0, rounds[0]->id);
		EXPECT_EQ(round1.sm_id, rounds[0]->sm_id);
		EXPECT_EQ(round1.name, rounds[0]->name);
		EXPECT_EQ(round1.sm_league_id, rounds[0]->sm_league_id);
		EXPECT_EQ(round1.sm_season_id, rounds[0]->sm_season_id);
		EXPECT_EQ(round1.sm_stage_id, rounds[0]->sm_stage_id);
		EXPECT_EQ(round1.start, rounds[0]->start);
		EXPECT_EQ(round1.end, rounds[0]->end);
		

		EXPECT_NE(0, rounds[1]->id);
		EXPECT_EQ(round2.sm_id, rounds[1]->sm_id);
		EXPECT_EQ(round2.name, rounds[1]->name);
		EXPECT_EQ(round2.sm_league_id, rounds[1]->sm_league_id);
		EXPECT_EQ(round2.sm_season_id, rounds[1]->sm_season_id);
		EXPECT_EQ(round2.sm_stage_id, rounds[1]->sm_stage_id);
		EXPECT_EQ(round2.start, rounds[1]->start);
		EXPECT_EQ(round2.end, rounds[1]->end);
		

		EXPECT_NE(0, rounds[2]->id);
		EXPECT_EQ(round3.sm_id, rounds[2]->sm_id);
		EXPECT_EQ(round3.name, rounds[2]->name);
		EXPECT_EQ(round3.sm_league_id, rounds[2]->sm_league_id);
		EXPECT_EQ(round3.sm_season_id, rounds[2]->sm_season_id);
		EXPECT_EQ(round3.sm_stage_id, rounds[2]->sm_stage_id);
		EXPECT_EQ(round3.start, rounds[2]->start);
		EXPECT_EQ(round3.end, rounds[2]->end);
		
	}
}