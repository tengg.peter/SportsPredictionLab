#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Season.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, SeasonTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Season season1;
	season1.sm_id = 1;
	season1.name = STR("name2");
	season1.sm_league_id = 3;
	season1.is_current_season = false;
	season1.sm_current_round_id_opt = 5;
	season1.sm_current_stage_id_opt = 6;
	
	Season season2;
	
	Season season3;
	season3.sm_id = 7;
	season3.name = STR("name8");
	season3.sm_league_id = 9;
	season3.is_current_season = false;
	season3.sm_current_round_id_opt = 11;
	season3.sm_current_stage_id_opt = 12;
	
	std::vector<UpSeason> oneSeason;
	oneSeason.emplace_back(std::make_unique<Season>(season1));

	std::vector<UpSeason> twoseasons;
	twoseasons.emplace_back(std::make_unique<Season>(season1));
	twoseasons.emplace_back(std::make_unique<Season>(season2));

	std::vector<UpSeason> threeseasons;
	threeseasons.emplace_back(std::make_unique<Season>(season1));
	threeseasons.emplace_back(std::make_unique<Season>(season2));
	threeseasons.emplace_back(std::make_unique<Season>(season3));

	EXPECT_EQ(1, oneSeason.size());
	EXPECT_EQ(2, twoseasons.size());
	EXPECT_EQ(3, threeseasons.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_SEASONS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneSeason));
	ASSERT_EQ(1, QueryCount<Season>(db));
	{
		auto seasons = QueryAll<Season>(db);
		EXPECT_EQ(1, seasons.size());

		EXPECT_NE(0, seasons[0]->id);
		EXPECT_EQ(season1.sm_id, seasons[0]->sm_id);
		EXPECT_EQ(season1.name, seasons[0]->name);
		EXPECT_EQ(season1.sm_league_id, seasons[0]->sm_league_id);
		EXPECT_EQ(season1.is_current_season, seasons[0]->is_current_season);
		EXPECT_EQ(season1.sm_current_round_id_opt, seasons[0]->sm_current_round_id_opt);
		EXPECT_EQ(season1.sm_current_stage_id_opt, seasons[0]->sm_current_stage_id_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoseasons));
	ASSERT_EQ(2, QueryCount<Season>(db));
	{
		auto seasons = QueryAll<Season>(db);
		EXPECT_EQ(2, seasons.size());

		EXPECT_NE(0, seasons[0]->id);
		EXPECT_EQ(season1.sm_id, seasons[0]->sm_id);
		EXPECT_EQ(season1.name, seasons[0]->name);
		EXPECT_EQ(season1.sm_league_id, seasons[0]->sm_league_id);
		EXPECT_EQ(season1.is_current_season, seasons[0]->is_current_season);
		EXPECT_EQ(season1.sm_current_round_id_opt, seasons[0]->sm_current_round_id_opt);
		EXPECT_EQ(season1.sm_current_stage_id_opt, seasons[0]->sm_current_stage_id_opt);
		

		EXPECT_NE(0, seasons[1]->id);
		EXPECT_EQ(season2.sm_id, seasons[1]->sm_id);
		EXPECT_EQ(season2.name, seasons[1]->name);
		EXPECT_EQ(season2.sm_league_id, seasons[1]->sm_league_id);
		EXPECT_EQ(season2.is_current_season, seasons[1]->is_current_season);
		EXPECT_EQ(season2.sm_current_round_id_opt, seasons[1]->sm_current_round_id_opt);
		EXPECT_EQ(season2.sm_current_stage_id_opt, seasons[1]->sm_current_stage_id_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeseasons));
	ASSERT_EQ(3, QueryCount<Season>(db));
	{
		auto seasons = QueryAll<Season>(db);
		EXPECT_EQ(3, seasons.size());

		EXPECT_NE(0, seasons[0]->id);
		EXPECT_EQ(season1.sm_id, seasons[0]->sm_id);
		EXPECT_EQ(season1.name, seasons[0]->name);
		EXPECT_EQ(season1.sm_league_id, seasons[0]->sm_league_id);
		EXPECT_EQ(season1.is_current_season, seasons[0]->is_current_season);
		EXPECT_EQ(season1.sm_current_round_id_opt, seasons[0]->sm_current_round_id_opt);
		EXPECT_EQ(season1.sm_current_stage_id_opt, seasons[0]->sm_current_stage_id_opt);
		

		EXPECT_NE(0, seasons[1]->id);
		EXPECT_EQ(season2.sm_id, seasons[1]->sm_id);
		EXPECT_EQ(season2.name, seasons[1]->name);
		EXPECT_EQ(season2.sm_league_id, seasons[1]->sm_league_id);
		EXPECT_EQ(season2.is_current_season, seasons[1]->is_current_season);
		EXPECT_EQ(season2.sm_current_round_id_opt, seasons[1]->sm_current_round_id_opt);
		EXPECT_EQ(season2.sm_current_stage_id_opt, seasons[1]->sm_current_stage_id_opt);
		

		EXPECT_NE(0, seasons[2]->id);
		EXPECT_EQ(season3.sm_id, seasons[2]->sm_id);
		EXPECT_EQ(season3.name, seasons[2]->name);
		EXPECT_EQ(season3.sm_league_id, seasons[2]->sm_league_id);
		EXPECT_EQ(season3.is_current_season, seasons[2]->is_current_season);
		EXPECT_EQ(season3.sm_current_round_id_opt, seasons[2]->sm_current_round_id_opt);
		EXPECT_EQ(season3.sm_current_stage_id_opt, seasons[2]->sm_current_stage_id_opt);
		
	}
}