#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Player.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

TEST(CmpTests, CmpTests)
{
	//using namespace database_handler;
	using namespace database_handler::v2;
	//using namespace database_handler::common;

	UpPlayer null1;
	UpPlayer null2;
	UpPlayer p1{ std::make_unique<Player>() };
	p1->sm_id = 1;
	UpPlayer p2{ std::make_unique<Player>() };
	p2->sm_id = 2;
	PlayerCmp cmp;

	EXPECT_FALSE(cmp(null1, null2));
	EXPECT_TRUE(cmp(null1, p1));
	EXPECT_FALSE(cmp(p1, null2));
	EXPECT_FALSE(cmp(p1, p1));
	EXPECT_FALSE(cmp(p2, p2));

	EXPECT_TRUE(cmp(p1, p2));
	EXPECT_FALSE(cmp(p2, p1));

	std::set<UpPlayer, PlayerCmp> set;
	set.insert(std::move(p2));
	set.insert(std::move(null2));
	set.insert(std::move(p1));
	set.insert(std::move(null1));

	//set will contain { nullptr, 1, 2 }
	EXPECT_EQ(3, set.size());
	size_t i = 0;
	for (auto it = set.cbegin(); it != set.cend(); ++it, ++i)
	{
		if (0 == i)
		{
			EXPECT_EQ(nullptr, *it);
		}
		else
		{
			EXPECT_EQ(i, (*it)->sm_id);
		}
	}
}