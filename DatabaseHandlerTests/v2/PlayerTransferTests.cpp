#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerTransfer.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, PlayerTransferTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	PlayerTransfer playerTransfer1;
	playerTransfer1.sm_player_id = 1;
	playerTransfer1.sm_from_team_id = 2;
	playerTransfer1.sm_to_team_id = 3;
	playerTransfer1.sm_season_id_opt = 4;
	playerTransfer1.transfer = STR("transfer5");
	playerTransfer1.date = utils::date_and_time::DateTime::FromSQLiteDateFormat("2006-08-09 10:11:12");
	playerTransfer1.amount_opt = STR("amount_opt12");
	
	PlayerTransfer playerTransfer2;
	
	PlayerTransfer playerTransfer3;
	playerTransfer3.sm_player_id = 13;
	playerTransfer3.sm_from_team_id = 14;
	playerTransfer3.sm_to_team_id = 15;
	playerTransfer3.sm_season_id_opt = 16;
	playerTransfer3.transfer = STR("transfer17");
	playerTransfer3.date = utils::date_and_time::DateTime::FromSQLiteDateFormat("2018-08-21 22:23:24");
	playerTransfer3.amount_opt = STR("amount_opt24");
	
	std::vector<UpPlayerTransfer> onePlayerTransfer;
	onePlayerTransfer.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer1));

	std::vector<UpPlayerTransfer> twoplayerTransfers;
	twoplayerTransfers.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer1));
	twoplayerTransfers.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer2));

	std::vector<UpPlayerTransfer> threeplayerTransfers;
	threeplayerTransfers.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer1));
	threeplayerTransfers.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer2));
	threeplayerTransfers.emplace_back(std::make_unique<PlayerTransfer>(playerTransfer3));

	EXPECT_EQ(1, onePlayerTransfer.size());
	EXPECT_EQ(2, twoplayerTransfers.size());
	EXPECT_EQ(3, threeplayerTransfers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_PLAYER_TRANSFERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, onePlayerTransfer));
	ASSERT_EQ(1, QueryCount<PlayerTransfer>(db));
	{
		auto playerTransfers = QueryAll<PlayerTransfer>(db);
		EXPECT_EQ(1, playerTransfers.size());

		EXPECT_NE(0, playerTransfers[0]->id);
		EXPECT_EQ(playerTransfer1.sm_player_id, playerTransfers[0]->sm_player_id);
		EXPECT_EQ(playerTransfer1.sm_from_team_id, playerTransfers[0]->sm_from_team_id);
		EXPECT_EQ(playerTransfer1.sm_to_team_id, playerTransfers[0]->sm_to_team_id);
		EXPECT_EQ(playerTransfer1.sm_season_id_opt, playerTransfers[0]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer1.transfer, playerTransfers[0]->transfer);
		EXPECT_EQ(playerTransfer1.date, playerTransfers[0]->date);
		EXPECT_EQ(playerTransfer1.amount_opt, playerTransfers[0]->amount_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoplayerTransfers));
	ASSERT_EQ(2, QueryCount<PlayerTransfer>(db));
	{
		auto playerTransfers = QueryAll<PlayerTransfer>(db);
		EXPECT_EQ(2, playerTransfers.size());

		EXPECT_NE(0, playerTransfers[0]->id);
		EXPECT_EQ(playerTransfer1.sm_player_id, playerTransfers[0]->sm_player_id);
		EXPECT_EQ(playerTransfer1.sm_from_team_id, playerTransfers[0]->sm_from_team_id);
		EXPECT_EQ(playerTransfer1.sm_to_team_id, playerTransfers[0]->sm_to_team_id);
		EXPECT_EQ(playerTransfer1.sm_season_id_opt, playerTransfers[0]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer1.transfer, playerTransfers[0]->transfer);
		EXPECT_EQ(playerTransfer1.date, playerTransfers[0]->date);
		EXPECT_EQ(playerTransfer1.amount_opt, playerTransfers[0]->amount_opt);
		

		EXPECT_NE(0, playerTransfers[1]->id);
		EXPECT_EQ(playerTransfer2.sm_player_id, playerTransfers[1]->sm_player_id);
		EXPECT_EQ(playerTransfer2.sm_from_team_id, playerTransfers[1]->sm_from_team_id);
		EXPECT_EQ(playerTransfer2.sm_to_team_id, playerTransfers[1]->sm_to_team_id);
		EXPECT_EQ(playerTransfer2.sm_season_id_opt, playerTransfers[1]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer2.transfer, playerTransfers[1]->transfer);
		EXPECT_EQ(playerTransfer2.date, playerTransfers[1]->date);
		EXPECT_EQ(playerTransfer2.amount_opt, playerTransfers[1]->amount_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeplayerTransfers));
	ASSERT_EQ(3, QueryCount<PlayerTransfer>(db));
	{
		auto playerTransfers = QueryAll<PlayerTransfer>(db);
		EXPECT_EQ(3, playerTransfers.size());

		EXPECT_NE(0, playerTransfers[0]->id);
		EXPECT_EQ(playerTransfer1.sm_player_id, playerTransfers[0]->sm_player_id);
		EXPECT_EQ(playerTransfer1.sm_from_team_id, playerTransfers[0]->sm_from_team_id);
		EXPECT_EQ(playerTransfer1.sm_to_team_id, playerTransfers[0]->sm_to_team_id);
		EXPECT_EQ(playerTransfer1.sm_season_id_opt, playerTransfers[0]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer1.transfer, playerTransfers[0]->transfer);
		EXPECT_EQ(playerTransfer1.date, playerTransfers[0]->date);
		EXPECT_EQ(playerTransfer1.amount_opt, playerTransfers[0]->amount_opt);
		

		EXPECT_NE(0, playerTransfers[1]->id);
		EXPECT_EQ(playerTransfer2.sm_player_id, playerTransfers[1]->sm_player_id);
		EXPECT_EQ(playerTransfer2.sm_from_team_id, playerTransfers[1]->sm_from_team_id);
		EXPECT_EQ(playerTransfer2.sm_to_team_id, playerTransfers[1]->sm_to_team_id);
		EXPECT_EQ(playerTransfer2.sm_season_id_opt, playerTransfers[1]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer2.transfer, playerTransfers[1]->transfer);
		EXPECT_EQ(playerTransfer2.date, playerTransfers[1]->date);
		EXPECT_EQ(playerTransfer2.amount_opt, playerTransfers[1]->amount_opt);
		

		EXPECT_NE(0, playerTransfers[2]->id);
		EXPECT_EQ(playerTransfer3.sm_player_id, playerTransfers[2]->sm_player_id);
		EXPECT_EQ(playerTransfer3.sm_from_team_id, playerTransfers[2]->sm_from_team_id);
		EXPECT_EQ(playerTransfer3.sm_to_team_id, playerTransfers[2]->sm_to_team_id);
		EXPECT_EQ(playerTransfer3.sm_season_id_opt, playerTransfers[2]->sm_season_id_opt);
		EXPECT_EQ(playerTransfer3.transfer, playerTransfers[2]->transfer);
		EXPECT_EQ(playerTransfer3.date, playerTransfers[2]->date);
		EXPECT_EQ(playerTransfer3.amount_opt, playerTransfers[2]->amount_opt);
		
	}
}