#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Standing.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, StandingTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Standing standing1;
	standing1.sm_id = 1;
	standing1.name = STR("name2");
	standing1.sm_league_id = 3;
	standing1.sm_season_id = 4;
	standing1.sm_round_id = 5;
	standing1.round_name = 6;
	standing1.type = STR("type7");
	standing1.sm_stage_id = 8;
	standing1.stage_name = STR("stage_name9");
	standing1.resource = STR("resource10");
	
	Standing standing2;
	
	Standing standing3;
	standing3.sm_id = 11;
	standing3.name = STR("name12");
	standing3.sm_league_id = 13;
	standing3.sm_season_id = 14;
	standing3.sm_round_id = 15;
	standing3.round_name = 16;
	standing3.type = STR("type17");
	standing3.sm_stage_id = 18;
	standing3.stage_name = STR("stage_name19");
	standing3.resource = STR("resource20");
	
	std::vector<UpStanding> oneStanding;
	oneStanding.emplace_back(std::make_unique<Standing>(standing1));

	std::vector<UpStanding> twostandings;
	twostandings.emplace_back(std::make_unique<Standing>(standing1));
	twostandings.emplace_back(std::make_unique<Standing>(standing2));

	std::vector<UpStanding> threestandings;
	threestandings.emplace_back(std::make_unique<Standing>(standing1));
	threestandings.emplace_back(std::make_unique<Standing>(standing2));
	threestandings.emplace_back(std::make_unique<Standing>(standing3));

	EXPECT_EQ(1, oneStanding.size());
	EXPECT_EQ(2, twostandings.size());
	EXPECT_EQ(3, threestandings.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_STANDINGS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneStanding));
	ASSERT_EQ(1, QueryCount<Standing>(db));
	{
		auto standings = QueryAll<Standing>(db);
		EXPECT_EQ(1, standings.size());

		EXPECT_NE(0, standings[0]->id);
		EXPECT_EQ(standing1.sm_id, standings[0]->sm_id);
		EXPECT_EQ(standing1.name, standings[0]->name);
		EXPECT_EQ(standing1.sm_league_id, standings[0]->sm_league_id);
		EXPECT_EQ(standing1.sm_season_id, standings[0]->sm_season_id);
		EXPECT_EQ(standing1.sm_round_id, standings[0]->sm_round_id);
		EXPECT_EQ(standing1.round_name, standings[0]->round_name);
		EXPECT_EQ(standing1.type, standings[0]->type);
		EXPECT_EQ(standing1.sm_stage_id, standings[0]->sm_stage_id);
		EXPECT_EQ(standing1.stage_name, standings[0]->stage_name);
		EXPECT_EQ(standing1.resource, standings[0]->resource);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twostandings));
	ASSERT_EQ(2, QueryCount<Standing>(db));
	{
		auto standings = QueryAll<Standing>(db);
		EXPECT_EQ(2, standings.size());

		EXPECT_NE(0, standings[0]->id);
		EXPECT_EQ(standing1.sm_id, standings[0]->sm_id);
		EXPECT_EQ(standing1.name, standings[0]->name);
		EXPECT_EQ(standing1.sm_league_id, standings[0]->sm_league_id);
		EXPECT_EQ(standing1.sm_season_id, standings[0]->sm_season_id);
		EXPECT_EQ(standing1.sm_round_id, standings[0]->sm_round_id);
		EXPECT_EQ(standing1.round_name, standings[0]->round_name);
		EXPECT_EQ(standing1.type, standings[0]->type);
		EXPECT_EQ(standing1.sm_stage_id, standings[0]->sm_stage_id);
		EXPECT_EQ(standing1.stage_name, standings[0]->stage_name);
		EXPECT_EQ(standing1.resource, standings[0]->resource);
		

		EXPECT_NE(0, standings[1]->id);
		EXPECT_EQ(standing2.sm_id, standings[1]->sm_id);
		EXPECT_EQ(standing2.name, standings[1]->name);
		EXPECT_EQ(standing2.sm_league_id, standings[1]->sm_league_id);
		EXPECT_EQ(standing2.sm_season_id, standings[1]->sm_season_id);
		EXPECT_EQ(standing2.sm_round_id, standings[1]->sm_round_id);
		EXPECT_EQ(standing2.round_name, standings[1]->round_name);
		EXPECT_EQ(standing2.type, standings[1]->type);
		EXPECT_EQ(standing2.sm_stage_id, standings[1]->sm_stage_id);
		EXPECT_EQ(standing2.stage_name, standings[1]->stage_name);
		EXPECT_EQ(standing2.resource, standings[1]->resource);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threestandings));
	ASSERT_EQ(3, QueryCount<Standing>(db));
	{
		auto standings = QueryAll<Standing>(db);
		EXPECT_EQ(3, standings.size());

		EXPECT_NE(0, standings[0]->id);
		EXPECT_EQ(standing1.sm_id, standings[0]->sm_id);
		EXPECT_EQ(standing1.name, standings[0]->name);
		EXPECT_EQ(standing1.sm_league_id, standings[0]->sm_league_id);
		EXPECT_EQ(standing1.sm_season_id, standings[0]->sm_season_id);
		EXPECT_EQ(standing1.sm_round_id, standings[0]->sm_round_id);
		EXPECT_EQ(standing1.round_name, standings[0]->round_name);
		EXPECT_EQ(standing1.type, standings[0]->type);
		EXPECT_EQ(standing1.sm_stage_id, standings[0]->sm_stage_id);
		EXPECT_EQ(standing1.stage_name, standings[0]->stage_name);
		EXPECT_EQ(standing1.resource, standings[0]->resource);
		

		EXPECT_NE(0, standings[1]->id);
		EXPECT_EQ(standing2.sm_id, standings[1]->sm_id);
		EXPECT_EQ(standing2.name, standings[1]->name);
		EXPECT_EQ(standing2.sm_league_id, standings[1]->sm_league_id);
		EXPECT_EQ(standing2.sm_season_id, standings[1]->sm_season_id);
		EXPECT_EQ(standing2.sm_round_id, standings[1]->sm_round_id);
		EXPECT_EQ(standing2.round_name, standings[1]->round_name);
		EXPECT_EQ(standing2.type, standings[1]->type);
		EXPECT_EQ(standing2.sm_stage_id, standings[1]->sm_stage_id);
		EXPECT_EQ(standing2.stage_name, standings[1]->stage_name);
		EXPECT_EQ(standing2.resource, standings[1]->resource);
		

		EXPECT_NE(0, standings[2]->id);
		EXPECT_EQ(standing3.sm_id, standings[2]->sm_id);
		EXPECT_EQ(standing3.name, standings[2]->name);
		EXPECT_EQ(standing3.sm_league_id, standings[2]->sm_league_id);
		EXPECT_EQ(standing3.sm_season_id, standings[2]->sm_season_id);
		EXPECT_EQ(standing3.sm_round_id, standings[2]->sm_round_id);
		EXPECT_EQ(standing3.round_name, standings[2]->round_name);
		EXPECT_EQ(standing3.type, standings[2]->type);
		EXPECT_EQ(standing3.sm_stage_id, standings[2]->sm_stage_id);
		EXPECT_EQ(standing3.stage_name, standings[2]->stage_name);
		EXPECT_EQ(standing3.resource, standings[2]->resource);
		
	}
}