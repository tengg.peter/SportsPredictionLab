#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Commentary.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, CommentaryTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Commentary commentary1;
	commentary1.sm_fixture_id = 1;
	commentary1.important = false;
	commentary1.order_ = 3;
	commentary1.goal = false;
	commentary1.minute = 5;
	commentary1.extra_minute_opt = 6;
	commentary1.comment = STR("comment7");
	
	Commentary commentary2;
	
	Commentary commentary3;
	commentary3.sm_fixture_id = 8;
	commentary3.important = true;
	commentary3.order_ = 10;
	commentary3.goal = true;
	commentary3.minute = 12;
	commentary3.extra_minute_opt = 13;
	commentary3.comment = STR("comment14");
	
	std::vector<UpCommentary> oneCommentary;
	oneCommentary.emplace_back(std::make_unique<Commentary>(commentary1));

	std::vector<UpCommentary> twocommentaries;
	twocommentaries.emplace_back(std::make_unique<Commentary>(commentary1));
	twocommentaries.emplace_back(std::make_unique<Commentary>(commentary2));

	std::vector<UpCommentary> threecommentaries;
	threecommentaries.emplace_back(std::make_unique<Commentary>(commentary1));
	threecommentaries.emplace_back(std::make_unique<Commentary>(commentary2));
	threecommentaries.emplace_back(std::make_unique<Commentary>(commentary3));

	EXPECT_EQ(1, oneCommentary.size());
	EXPECT_EQ(2, twocommentaries.size());
	EXPECT_EQ(3, threecommentaries.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_COMMENTARIES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneCommentary));
	ASSERT_EQ(1, QueryCount<Commentary>(db));
	{
		auto commentaries = QueryAll<Commentary>(db);
		EXPECT_EQ(1, commentaries.size());

		EXPECT_NE(0, commentaries[0]->id);
		EXPECT_EQ(commentary1.sm_fixture_id, commentaries[0]->sm_fixture_id);
		EXPECT_EQ(commentary1.important, commentaries[0]->important);
		EXPECT_EQ(commentary1.order_, commentaries[0]->order_);
		EXPECT_EQ(commentary1.goal, commentaries[0]->goal);
		EXPECT_EQ(commentary1.minute, commentaries[0]->minute);
		EXPECT_EQ(commentary1.extra_minute_opt, commentaries[0]->extra_minute_opt);
		EXPECT_EQ(commentary1.comment, commentaries[0]->comment);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twocommentaries));
	ASSERT_EQ(2, QueryCount<Commentary>(db));
	{
		auto commentaries = QueryAll<Commentary>(db);
		EXPECT_EQ(2, commentaries.size());

		EXPECT_NE(0, commentaries[0]->id);
		EXPECT_EQ(commentary1.sm_fixture_id, commentaries[0]->sm_fixture_id);
		EXPECT_EQ(commentary1.important, commentaries[0]->important);
		EXPECT_EQ(commentary1.order_, commentaries[0]->order_);
		EXPECT_EQ(commentary1.goal, commentaries[0]->goal);
		EXPECT_EQ(commentary1.minute, commentaries[0]->minute);
		EXPECT_EQ(commentary1.extra_minute_opt, commentaries[0]->extra_minute_opt);
		EXPECT_EQ(commentary1.comment, commentaries[0]->comment);
		

		EXPECT_NE(0, commentaries[1]->id);
		EXPECT_EQ(commentary2.sm_fixture_id, commentaries[1]->sm_fixture_id);
		EXPECT_EQ(commentary2.important, commentaries[1]->important);
		EXPECT_EQ(commentary2.order_, commentaries[1]->order_);
		EXPECT_EQ(commentary2.goal, commentaries[1]->goal);
		EXPECT_EQ(commentary2.minute, commentaries[1]->minute);
		EXPECT_EQ(commentary2.extra_minute_opt, commentaries[1]->extra_minute_opt);
		EXPECT_EQ(commentary2.comment, commentaries[1]->comment);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threecommentaries));
	ASSERT_EQ(3, QueryCount<Commentary>(db));
	{
		auto commentaries = QueryAll<Commentary>(db);
		EXPECT_EQ(3, commentaries.size());

		EXPECT_NE(0, commentaries[0]->id);
		EXPECT_EQ(commentary1.sm_fixture_id, commentaries[0]->sm_fixture_id);
		EXPECT_EQ(commentary1.important, commentaries[0]->important);
		EXPECT_EQ(commentary1.order_, commentaries[0]->order_);
		EXPECT_EQ(commentary1.goal, commentaries[0]->goal);
		EXPECT_EQ(commentary1.minute, commentaries[0]->minute);
		EXPECT_EQ(commentary1.extra_minute_opt, commentaries[0]->extra_minute_opt);
		EXPECT_EQ(commentary1.comment, commentaries[0]->comment);
		

		EXPECT_NE(0, commentaries[1]->id);
		EXPECT_EQ(commentary2.sm_fixture_id, commentaries[1]->sm_fixture_id);
		EXPECT_EQ(commentary2.important, commentaries[1]->important);
		EXPECT_EQ(commentary2.order_, commentaries[1]->order_);
		EXPECT_EQ(commentary2.goal, commentaries[1]->goal);
		EXPECT_EQ(commentary2.minute, commentaries[1]->minute);
		EXPECT_EQ(commentary2.extra_minute_opt, commentaries[1]->extra_minute_opt);
		EXPECT_EQ(commentary2.comment, commentaries[1]->comment);
		

		EXPECT_NE(0, commentaries[2]->id);
		EXPECT_EQ(commentary3.sm_fixture_id, commentaries[2]->sm_fixture_id);
		EXPECT_EQ(commentary3.important, commentaries[2]->important);
		EXPECT_EQ(commentary3.order_, commentaries[2]->order_);
		EXPECT_EQ(commentary3.goal, commentaries[2]->goal);
		EXPECT_EQ(commentary3.minute, commentaries[2]->minute);
		EXPECT_EQ(commentary3.extra_minute_opt, commentaries[2]->extra_minute_opt);
		EXPECT_EQ(commentary3.comment, commentaries[2]->comment);
		
	}
}