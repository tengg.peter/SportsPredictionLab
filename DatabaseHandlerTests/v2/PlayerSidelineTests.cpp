#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerSideline.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, PlayerSidelineTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	PlayerSideline playerSideline1;
	playerSideline1.sm_player_id = 1;
	playerSideline1.sm_season_id_opt = 2;
	playerSideline1.sm_team_id_opt = 3;
	playerSideline1.description = STR("description4");
	playerSideline1.start_date = STR("start_date5");
	playerSideline1.end_date_opt = STR("end_date_opt6");
	
	PlayerSideline playerSideline2;
	
	PlayerSideline playerSideline3;
	playerSideline3.sm_player_id = 7;
	playerSideline3.sm_season_id_opt = 8;
	playerSideline3.sm_team_id_opt = 9;
	playerSideline3.description = STR("description10");
	playerSideline3.start_date = STR("start_date11");
	playerSideline3.end_date_opt = STR("end_date_opt12");
	
	std::vector<UpPlayerSideline> onePlayerSideline;
	onePlayerSideline.emplace_back(std::make_unique<PlayerSideline>(playerSideline1));

	std::vector<UpPlayerSideline> twoplayerSidelines;
	twoplayerSidelines.emplace_back(std::make_unique<PlayerSideline>(playerSideline1));
	twoplayerSidelines.emplace_back(std::make_unique<PlayerSideline>(playerSideline2));

	std::vector<UpPlayerSideline> threeplayerSidelines;
	threeplayerSidelines.emplace_back(std::make_unique<PlayerSideline>(playerSideline1));
	threeplayerSidelines.emplace_back(std::make_unique<PlayerSideline>(playerSideline2));
	threeplayerSidelines.emplace_back(std::make_unique<PlayerSideline>(playerSideline3));

	EXPECT_EQ(1, onePlayerSideline.size());
	EXPECT_EQ(2, twoplayerSidelines.size());
	EXPECT_EQ(3, threeplayerSidelines.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_PLAYER_SIDELINES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, onePlayerSideline));
	ASSERT_EQ(1, QueryCount<PlayerSideline>(db));
	{
		auto playerSidelines = QueryAll<PlayerSideline>(db);
		EXPECT_EQ(1, playerSidelines.size());

		EXPECT_NE(0, playerSidelines[0]->id);
		EXPECT_EQ(playerSideline1.sm_player_id, playerSidelines[0]->sm_player_id);
		EXPECT_EQ(playerSideline1.sm_season_id_opt, playerSidelines[0]->sm_season_id_opt);
		EXPECT_EQ(playerSideline1.sm_team_id_opt, playerSidelines[0]->sm_team_id_opt);
		EXPECT_EQ(playerSideline1.description, playerSidelines[0]->description);
		EXPECT_EQ(playerSideline1.start_date, playerSidelines[0]->start_date);
		EXPECT_EQ(playerSideline1.end_date_opt, playerSidelines[0]->end_date_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoplayerSidelines));
	ASSERT_EQ(2, QueryCount<PlayerSideline>(db));
	{
		auto playerSidelines = QueryAll<PlayerSideline>(db);
		EXPECT_EQ(2, playerSidelines.size());

		EXPECT_NE(0, playerSidelines[0]->id);
		EXPECT_EQ(playerSideline1.sm_player_id, playerSidelines[0]->sm_player_id);
		EXPECT_EQ(playerSideline1.sm_season_id_opt, playerSidelines[0]->sm_season_id_opt);
		EXPECT_EQ(playerSideline1.sm_team_id_opt, playerSidelines[0]->sm_team_id_opt);
		EXPECT_EQ(playerSideline1.description, playerSidelines[0]->description);
		EXPECT_EQ(playerSideline1.start_date, playerSidelines[0]->start_date);
		EXPECT_EQ(playerSideline1.end_date_opt, playerSidelines[0]->end_date_opt);
		

		EXPECT_NE(0, playerSidelines[1]->id);
		EXPECT_EQ(playerSideline2.sm_player_id, playerSidelines[1]->sm_player_id);
		EXPECT_EQ(playerSideline2.sm_season_id_opt, playerSidelines[1]->sm_season_id_opt);
		EXPECT_EQ(playerSideline2.sm_team_id_opt, playerSidelines[1]->sm_team_id_opt);
		EXPECT_EQ(playerSideline2.description, playerSidelines[1]->description);
		EXPECT_EQ(playerSideline2.start_date, playerSidelines[1]->start_date);
		EXPECT_EQ(playerSideline2.end_date_opt, playerSidelines[1]->end_date_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeplayerSidelines));
	ASSERT_EQ(3, QueryCount<PlayerSideline>(db));
	{
		auto playerSidelines = QueryAll<PlayerSideline>(db);
		EXPECT_EQ(3, playerSidelines.size());

		EXPECT_NE(0, playerSidelines[0]->id);
		EXPECT_EQ(playerSideline1.sm_player_id, playerSidelines[0]->sm_player_id);
		EXPECT_EQ(playerSideline1.sm_season_id_opt, playerSidelines[0]->sm_season_id_opt);
		EXPECT_EQ(playerSideline1.sm_team_id_opt, playerSidelines[0]->sm_team_id_opt);
		EXPECT_EQ(playerSideline1.description, playerSidelines[0]->description);
		EXPECT_EQ(playerSideline1.start_date, playerSidelines[0]->start_date);
		EXPECT_EQ(playerSideline1.end_date_opt, playerSidelines[0]->end_date_opt);
		

		EXPECT_NE(0, playerSidelines[1]->id);
		EXPECT_EQ(playerSideline2.sm_player_id, playerSidelines[1]->sm_player_id);
		EXPECT_EQ(playerSideline2.sm_season_id_opt, playerSidelines[1]->sm_season_id_opt);
		EXPECT_EQ(playerSideline2.sm_team_id_opt, playerSidelines[1]->sm_team_id_opt);
		EXPECT_EQ(playerSideline2.description, playerSidelines[1]->description);
		EXPECT_EQ(playerSideline2.start_date, playerSidelines[1]->start_date);
		EXPECT_EQ(playerSideline2.end_date_opt, playerSidelines[1]->end_date_opt);
		

		EXPECT_NE(0, playerSidelines[2]->id);
		EXPECT_EQ(playerSideline3.sm_player_id, playerSidelines[2]->sm_player_id);
		EXPECT_EQ(playerSideline3.sm_season_id_opt, playerSidelines[2]->sm_season_id_opt);
		EXPECT_EQ(playerSideline3.sm_team_id_opt, playerSidelines[2]->sm_team_id_opt);
		EXPECT_EQ(playerSideline3.description, playerSidelines[2]->description);
		EXPECT_EQ(playerSideline3.start_date, playerSidelines[2]->start_date);
		EXPECT_EQ(playerSideline3.end_date_opt, playerSidelines[2]->end_date_opt);
		
	}
}