#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Continent.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, ContinentTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Continent continent1;
	continent1.sm_id = 1;
	continent1.name = STR("name2");
	
	Continent continent2;
	
	Continent continent3;
	continent3.sm_id = 3;
	continent3.name = STR("name4");
	
	std::vector<UpContinent> oneContinent;
	oneContinent.emplace_back(std::make_unique<Continent>(continent1));

	std::vector<UpContinent> twocontinents;
	twocontinents.emplace_back(std::make_unique<Continent>(continent1));
	twocontinents.emplace_back(std::make_unique<Continent>(continent2));

	std::vector<UpContinent> threecontinents;
	threecontinents.emplace_back(std::make_unique<Continent>(continent1));
	threecontinents.emplace_back(std::make_unique<Continent>(continent2));
	threecontinents.emplace_back(std::make_unique<Continent>(continent3));

	EXPECT_EQ(1, oneContinent.size());
	EXPECT_EQ(2, twocontinents.size());
	EXPECT_EQ(3, threecontinents.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_CONTINENTS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneContinent));
	ASSERT_EQ(1, QueryCount<Continent>(db));
	{
		auto continents = QueryAll<Continent>(db);
		EXPECT_EQ(1, continents.size());

		EXPECT_NE(0, continents[0]->id);
		EXPECT_EQ(continent1.sm_id, continents[0]->sm_id);
		EXPECT_EQ(continent1.name, continents[0]->name);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twocontinents));
	ASSERT_EQ(2, QueryCount<Continent>(db));
	{
		auto continents = QueryAll<Continent>(db);
		EXPECT_EQ(2, continents.size());

		EXPECT_NE(0, continents[0]->id);
		EXPECT_EQ(continent1.sm_id, continents[0]->sm_id);
		EXPECT_EQ(continent1.name, continents[0]->name);
		

		EXPECT_NE(0, continents[1]->id);
		EXPECT_EQ(continent2.sm_id, continents[1]->sm_id);
		EXPECT_EQ(continent2.name, continents[1]->name);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threecontinents));
	ASSERT_EQ(3, QueryCount<Continent>(db));
	{
		auto continents = QueryAll<Continent>(db);
		EXPECT_EQ(3, continents.size());

		EXPECT_NE(0, continents[0]->id);
		EXPECT_EQ(continent1.sm_id, continents[0]->sm_id);
		EXPECT_EQ(continent1.name, continents[0]->name);
		

		EXPECT_NE(0, continents[1]->id);
		EXPECT_EQ(continent2.sm_id, continents[1]->sm_id);
		EXPECT_EQ(continent2.name, continents[1]->name);
		

		EXPECT_NE(0, continents[2]->id);
		EXPECT_EQ(continent3.sm_id, continents[2]->sm_id);
		EXPECT_EQ(continent3.name, continents[2]->name);
		
	}
}