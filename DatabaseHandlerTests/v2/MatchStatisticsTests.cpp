#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/MatchStatistics.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, MatchStatisticsTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	MatchStatistics matchStatistics1;
	matchStatistics1.sm_team_id = 1;
	matchStatistics1.sm_fixture_id = 2;
	matchStatistics1.shots_total_opt = 3;
	matchStatistics1.shots_ongoal_opt = 4;
	matchStatistics1.shots_offgoal_opt = 5;
	matchStatistics1.shots_blocked_opt = 6;
	matchStatistics1.shots_insidebox_opt = 7;
	matchStatistics1.shots_outsidebox_opt = 8;
	matchStatistics1.passes_total_opt = 9;
	matchStatistics1.passes_accurate_opt = 10;
	matchStatistics1.passes_percentage_opt = 11.12;
	matchStatistics1.attacks_opt = 13;
	matchStatistics1.dangerous_attacks_opt = 14;
	matchStatistics1.fouls_opt = 15;
	matchStatistics1.corners_opt = 16;
	matchStatistics1.offsides_opt = 17;
	matchStatistics1.possessiontime_opt = 18;
	matchStatistics1.yellowcards_opt = 19;
	matchStatistics1.redcards_opt = 20;
	matchStatistics1.yellowredcards_opt = 21;
	matchStatistics1.saves_opt = 22;
	matchStatistics1.substitutions_opt = 23;
	matchStatistics1.goal_kick_opt = 24;
	matchStatistics1.throw_in_opt = 25;
	matchStatistics1.ball_safe_opt = 26;
	matchStatistics1.goals_opt = 27;
	matchStatistics1.penalties_opt = 28;
	matchStatistics1.injuries_opt = 29;
	
	MatchStatistics matchStatistics2;
	
	MatchStatistics matchStatistics3;
	matchStatistics3.sm_team_id = 30;
	matchStatistics3.sm_fixture_id = 31;
	matchStatistics3.shots_total_opt = 32;
	matchStatistics3.shots_ongoal_opt = 33;
	matchStatistics3.shots_offgoal_opt = 34;
	matchStatistics3.shots_blocked_opt = 35;
	matchStatistics3.shots_insidebox_opt = 36;
	matchStatistics3.shots_outsidebox_opt = 37;
	matchStatistics3.passes_total_opt = 38;
	matchStatistics3.passes_accurate_opt = 39;
	matchStatistics3.passes_percentage_opt = 40.41;
	matchStatistics3.attacks_opt = 42;
	matchStatistics3.dangerous_attacks_opt = 43;
	matchStatistics3.fouls_opt = 44;
	matchStatistics3.corners_opt = 45;
	matchStatistics3.offsides_opt = 46;
	matchStatistics3.possessiontime_opt = 47;
	matchStatistics3.yellowcards_opt = 48;
	matchStatistics3.redcards_opt = 49;
	matchStatistics3.yellowredcards_opt = 50;
	matchStatistics3.saves_opt = 51;
	matchStatistics3.substitutions_opt = 52;
	matchStatistics3.goal_kick_opt = 53;
	matchStatistics3.throw_in_opt = 54;
	matchStatistics3.ball_safe_opt = 55;
	matchStatistics3.goals_opt = 56;
	matchStatistics3.penalties_opt = 57;
	matchStatistics3.injuries_opt = 58;
	
	std::vector<UpMatchStatistics> oneMatchStatistics;
	oneMatchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics1));

	std::vector<UpMatchStatistics> twomatchStatistics;
	twomatchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics1));
	twomatchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics2));

	std::vector<UpMatchStatistics> threematchStatistics;
	threematchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics1));
	threematchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics2));
	threematchStatistics.emplace_back(std::make_unique<MatchStatistics>(matchStatistics3));

	EXPECT_EQ(1, oneMatchStatistics.size());
	EXPECT_EQ(2, twomatchStatistics.size());
	EXPECT_EQ(3, threematchStatistics.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_MATCH_STATISTICS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneMatchStatistics));
	ASSERT_EQ(1, QueryCount<MatchStatistics>(db));
	{
		auto matchStatistics = QueryAll<MatchStatistics>(db);
		EXPECT_EQ(1, matchStatistics.size());

		EXPECT_NE(0, matchStatistics[0]->id);
		EXPECT_EQ(matchStatistics1.sm_team_id, matchStatistics[0]->sm_team_id);
		EXPECT_EQ(matchStatistics1.sm_fixture_id, matchStatistics[0]->sm_fixture_id);
		EXPECT_EQ(matchStatistics1.shots_total_opt, matchStatistics[0]->shots_total_opt);
		EXPECT_EQ(matchStatistics1.shots_ongoal_opt, matchStatistics[0]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics1.shots_offgoal_opt, matchStatistics[0]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics1.shots_blocked_opt, matchStatistics[0]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics1.shots_insidebox_opt, matchStatistics[0]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics1.shots_outsidebox_opt, matchStatistics[0]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics1.passes_total_opt, matchStatistics[0]->passes_total_opt);
		EXPECT_EQ(matchStatistics1.passes_accurate_opt, matchStatistics[0]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics1.passes_percentage_opt, matchStatistics[0]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics1.attacks_opt, matchStatistics[0]->attacks_opt);
		EXPECT_EQ(matchStatistics1.dangerous_attacks_opt, matchStatistics[0]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics1.fouls_opt, matchStatistics[0]->fouls_opt);
		EXPECT_EQ(matchStatistics1.corners_opt, matchStatistics[0]->corners_opt);
		EXPECT_EQ(matchStatistics1.offsides_opt, matchStatistics[0]->offsides_opt);
		EXPECT_EQ(matchStatistics1.possessiontime_opt, matchStatistics[0]->possessiontime_opt);
		EXPECT_EQ(matchStatistics1.yellowcards_opt, matchStatistics[0]->yellowcards_opt);
		EXPECT_EQ(matchStatistics1.redcards_opt, matchStatistics[0]->redcards_opt);
		EXPECT_EQ(matchStatistics1.yellowredcards_opt, matchStatistics[0]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics1.saves_opt, matchStatistics[0]->saves_opt);
		EXPECT_EQ(matchStatistics1.substitutions_opt, matchStatistics[0]->substitutions_opt);
		EXPECT_EQ(matchStatistics1.goal_kick_opt, matchStatistics[0]->goal_kick_opt);
		EXPECT_EQ(matchStatistics1.throw_in_opt, matchStatistics[0]->throw_in_opt);
		EXPECT_EQ(matchStatistics1.ball_safe_opt, matchStatistics[0]->ball_safe_opt);
		EXPECT_EQ(matchStatistics1.goals_opt, matchStatistics[0]->goals_opt);
		EXPECT_EQ(matchStatistics1.penalties_opt, matchStatistics[0]->penalties_opt);
		EXPECT_EQ(matchStatistics1.injuries_opt, matchStatistics[0]->injuries_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twomatchStatistics));
	ASSERT_EQ(2, QueryCount<MatchStatistics>(db));
	{
		auto matchStatistics = QueryAll<MatchStatistics>(db);
		EXPECT_EQ(2, matchStatistics.size());

		EXPECT_NE(0, matchStatistics[0]->id);
		EXPECT_EQ(matchStatistics1.sm_team_id, matchStatistics[0]->sm_team_id);
		EXPECT_EQ(matchStatistics1.sm_fixture_id, matchStatistics[0]->sm_fixture_id);
		EXPECT_EQ(matchStatistics1.shots_total_opt, matchStatistics[0]->shots_total_opt);
		EXPECT_EQ(matchStatistics1.shots_ongoal_opt, matchStatistics[0]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics1.shots_offgoal_opt, matchStatistics[0]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics1.shots_blocked_opt, matchStatistics[0]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics1.shots_insidebox_opt, matchStatistics[0]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics1.shots_outsidebox_opt, matchStatistics[0]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics1.passes_total_opt, matchStatistics[0]->passes_total_opt);
		EXPECT_EQ(matchStatistics1.passes_accurate_opt, matchStatistics[0]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics1.passes_percentage_opt, matchStatistics[0]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics1.attacks_opt, matchStatistics[0]->attacks_opt);
		EXPECT_EQ(matchStatistics1.dangerous_attacks_opt, matchStatistics[0]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics1.fouls_opt, matchStatistics[0]->fouls_opt);
		EXPECT_EQ(matchStatistics1.corners_opt, matchStatistics[0]->corners_opt);
		EXPECT_EQ(matchStatistics1.offsides_opt, matchStatistics[0]->offsides_opt);
		EXPECT_EQ(matchStatistics1.possessiontime_opt, matchStatistics[0]->possessiontime_opt);
		EXPECT_EQ(matchStatistics1.yellowcards_opt, matchStatistics[0]->yellowcards_opt);
		EXPECT_EQ(matchStatistics1.redcards_opt, matchStatistics[0]->redcards_opt);
		EXPECT_EQ(matchStatistics1.yellowredcards_opt, matchStatistics[0]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics1.saves_opt, matchStatistics[0]->saves_opt);
		EXPECT_EQ(matchStatistics1.substitutions_opt, matchStatistics[0]->substitutions_opt);
		EXPECT_EQ(matchStatistics1.goal_kick_opt, matchStatistics[0]->goal_kick_opt);
		EXPECT_EQ(matchStatistics1.throw_in_opt, matchStatistics[0]->throw_in_opt);
		EXPECT_EQ(matchStatistics1.ball_safe_opt, matchStatistics[0]->ball_safe_opt);
		EXPECT_EQ(matchStatistics1.goals_opt, matchStatistics[0]->goals_opt);
		EXPECT_EQ(matchStatistics1.penalties_opt, matchStatistics[0]->penalties_opt);
		EXPECT_EQ(matchStatistics1.injuries_opt, matchStatistics[0]->injuries_opt);
		

		EXPECT_NE(0, matchStatistics[1]->id);
		EXPECT_EQ(matchStatistics2.sm_team_id, matchStatistics[1]->sm_team_id);
		EXPECT_EQ(matchStatistics2.sm_fixture_id, matchStatistics[1]->sm_fixture_id);
		EXPECT_EQ(matchStatistics2.shots_total_opt, matchStatistics[1]->shots_total_opt);
		EXPECT_EQ(matchStatistics2.shots_ongoal_opt, matchStatistics[1]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics2.shots_offgoal_opt, matchStatistics[1]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics2.shots_blocked_opt, matchStatistics[1]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics2.shots_insidebox_opt, matchStatistics[1]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics2.shots_outsidebox_opt, matchStatistics[1]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics2.passes_total_opt, matchStatistics[1]->passes_total_opt);
		EXPECT_EQ(matchStatistics2.passes_accurate_opt, matchStatistics[1]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics2.passes_percentage_opt, matchStatistics[1]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics2.attacks_opt, matchStatistics[1]->attacks_opt);
		EXPECT_EQ(matchStatistics2.dangerous_attacks_opt, matchStatistics[1]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics2.fouls_opt, matchStatistics[1]->fouls_opt);
		EXPECT_EQ(matchStatistics2.corners_opt, matchStatistics[1]->corners_opt);
		EXPECT_EQ(matchStatistics2.offsides_opt, matchStatistics[1]->offsides_opt);
		EXPECT_EQ(matchStatistics2.possessiontime_opt, matchStatistics[1]->possessiontime_opt);
		EXPECT_EQ(matchStatistics2.yellowcards_opt, matchStatistics[1]->yellowcards_opt);
		EXPECT_EQ(matchStatistics2.redcards_opt, matchStatistics[1]->redcards_opt);
		EXPECT_EQ(matchStatistics2.yellowredcards_opt, matchStatistics[1]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics2.saves_opt, matchStatistics[1]->saves_opt);
		EXPECT_EQ(matchStatistics2.substitutions_opt, matchStatistics[1]->substitutions_opt);
		EXPECT_EQ(matchStatistics2.goal_kick_opt, matchStatistics[1]->goal_kick_opt);
		EXPECT_EQ(matchStatistics2.throw_in_opt, matchStatistics[1]->throw_in_opt);
		EXPECT_EQ(matchStatistics2.ball_safe_opt, matchStatistics[1]->ball_safe_opt);
		EXPECT_EQ(matchStatistics2.goals_opt, matchStatistics[1]->goals_opt);
		EXPECT_EQ(matchStatistics2.penalties_opt, matchStatistics[1]->penalties_opt);
		EXPECT_EQ(matchStatistics2.injuries_opt, matchStatistics[1]->injuries_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threematchStatistics));
	ASSERT_EQ(3, QueryCount<MatchStatistics>(db));
	{
		auto matchStatistics = QueryAll<MatchStatistics>(db);
		EXPECT_EQ(3, matchStatistics.size());

		EXPECT_NE(0, matchStatistics[0]->id);
		EXPECT_EQ(matchStatistics1.sm_team_id, matchStatistics[0]->sm_team_id);
		EXPECT_EQ(matchStatistics1.sm_fixture_id, matchStatistics[0]->sm_fixture_id);
		EXPECT_EQ(matchStatistics1.shots_total_opt, matchStatistics[0]->shots_total_opt);
		EXPECT_EQ(matchStatistics1.shots_ongoal_opt, matchStatistics[0]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics1.shots_offgoal_opt, matchStatistics[0]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics1.shots_blocked_opt, matchStatistics[0]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics1.shots_insidebox_opt, matchStatistics[0]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics1.shots_outsidebox_opt, matchStatistics[0]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics1.passes_total_opt, matchStatistics[0]->passes_total_opt);
		EXPECT_EQ(matchStatistics1.passes_accurate_opt, matchStatistics[0]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics1.passes_percentage_opt, matchStatistics[0]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics1.attacks_opt, matchStatistics[0]->attacks_opt);
		EXPECT_EQ(matchStatistics1.dangerous_attacks_opt, matchStatistics[0]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics1.fouls_opt, matchStatistics[0]->fouls_opt);
		EXPECT_EQ(matchStatistics1.corners_opt, matchStatistics[0]->corners_opt);
		EXPECT_EQ(matchStatistics1.offsides_opt, matchStatistics[0]->offsides_opt);
		EXPECT_EQ(matchStatistics1.possessiontime_opt, matchStatistics[0]->possessiontime_opt);
		EXPECT_EQ(matchStatistics1.yellowcards_opt, matchStatistics[0]->yellowcards_opt);
		EXPECT_EQ(matchStatistics1.redcards_opt, matchStatistics[0]->redcards_opt);
		EXPECT_EQ(matchStatistics1.yellowredcards_opt, matchStatistics[0]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics1.saves_opt, matchStatistics[0]->saves_opt);
		EXPECT_EQ(matchStatistics1.substitutions_opt, matchStatistics[0]->substitutions_opt);
		EXPECT_EQ(matchStatistics1.goal_kick_opt, matchStatistics[0]->goal_kick_opt);
		EXPECT_EQ(matchStatistics1.throw_in_opt, matchStatistics[0]->throw_in_opt);
		EXPECT_EQ(matchStatistics1.ball_safe_opt, matchStatistics[0]->ball_safe_opt);
		EXPECT_EQ(matchStatistics1.goals_opt, matchStatistics[0]->goals_opt);
		EXPECT_EQ(matchStatistics1.penalties_opt, matchStatistics[0]->penalties_opt);
		EXPECT_EQ(matchStatistics1.injuries_opt, matchStatistics[0]->injuries_opt);
		

		EXPECT_NE(0, matchStatistics[1]->id);
		EXPECT_EQ(matchStatistics2.sm_team_id, matchStatistics[1]->sm_team_id);
		EXPECT_EQ(matchStatistics2.sm_fixture_id, matchStatistics[1]->sm_fixture_id);
		EXPECT_EQ(matchStatistics2.shots_total_opt, matchStatistics[1]->shots_total_opt);
		EXPECT_EQ(matchStatistics2.shots_ongoal_opt, matchStatistics[1]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics2.shots_offgoal_opt, matchStatistics[1]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics2.shots_blocked_opt, matchStatistics[1]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics2.shots_insidebox_opt, matchStatistics[1]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics2.shots_outsidebox_opt, matchStatistics[1]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics2.passes_total_opt, matchStatistics[1]->passes_total_opt);
		EXPECT_EQ(matchStatistics2.passes_accurate_opt, matchStatistics[1]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics2.passes_percentage_opt, matchStatistics[1]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics2.attacks_opt, matchStatistics[1]->attacks_opt);
		EXPECT_EQ(matchStatistics2.dangerous_attacks_opt, matchStatistics[1]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics2.fouls_opt, matchStatistics[1]->fouls_opt);
		EXPECT_EQ(matchStatistics2.corners_opt, matchStatistics[1]->corners_opt);
		EXPECT_EQ(matchStatistics2.offsides_opt, matchStatistics[1]->offsides_opt);
		EXPECT_EQ(matchStatistics2.possessiontime_opt, matchStatistics[1]->possessiontime_opt);
		EXPECT_EQ(matchStatistics2.yellowcards_opt, matchStatistics[1]->yellowcards_opt);
		EXPECT_EQ(matchStatistics2.redcards_opt, matchStatistics[1]->redcards_opt);
		EXPECT_EQ(matchStatistics2.yellowredcards_opt, matchStatistics[1]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics2.saves_opt, matchStatistics[1]->saves_opt);
		EXPECT_EQ(matchStatistics2.substitutions_opt, matchStatistics[1]->substitutions_opt);
		EXPECT_EQ(matchStatistics2.goal_kick_opt, matchStatistics[1]->goal_kick_opt);
		EXPECT_EQ(matchStatistics2.throw_in_opt, matchStatistics[1]->throw_in_opt);
		EXPECT_EQ(matchStatistics2.ball_safe_opt, matchStatistics[1]->ball_safe_opt);
		EXPECT_EQ(matchStatistics2.goals_opt, matchStatistics[1]->goals_opt);
		EXPECT_EQ(matchStatistics2.penalties_opt, matchStatistics[1]->penalties_opt);
		EXPECT_EQ(matchStatistics2.injuries_opt, matchStatistics[1]->injuries_opt);
		

		EXPECT_NE(0, matchStatistics[2]->id);
		EXPECT_EQ(matchStatistics3.sm_team_id, matchStatistics[2]->sm_team_id);
		EXPECT_EQ(matchStatistics3.sm_fixture_id, matchStatistics[2]->sm_fixture_id);
		EXPECT_EQ(matchStatistics3.shots_total_opt, matchStatistics[2]->shots_total_opt);
		EXPECT_EQ(matchStatistics3.shots_ongoal_opt, matchStatistics[2]->shots_ongoal_opt);
		EXPECT_EQ(matchStatistics3.shots_offgoal_opt, matchStatistics[2]->shots_offgoal_opt);
		EXPECT_EQ(matchStatistics3.shots_blocked_opt, matchStatistics[2]->shots_blocked_opt);
		EXPECT_EQ(matchStatistics3.shots_insidebox_opt, matchStatistics[2]->shots_insidebox_opt);
		EXPECT_EQ(matchStatistics3.shots_outsidebox_opt, matchStatistics[2]->shots_outsidebox_opt);
		EXPECT_EQ(matchStatistics3.passes_total_opt, matchStatistics[2]->passes_total_opt);
		EXPECT_EQ(matchStatistics3.passes_accurate_opt, matchStatistics[2]->passes_accurate_opt);
		EXPECT_EQ(matchStatistics3.passes_percentage_opt, matchStatistics[2]->passes_percentage_opt);
		EXPECT_EQ(matchStatistics3.attacks_opt, matchStatistics[2]->attacks_opt);
		EXPECT_EQ(matchStatistics3.dangerous_attacks_opt, matchStatistics[2]->dangerous_attacks_opt);
		EXPECT_EQ(matchStatistics3.fouls_opt, matchStatistics[2]->fouls_opt);
		EXPECT_EQ(matchStatistics3.corners_opt, matchStatistics[2]->corners_opt);
		EXPECT_EQ(matchStatistics3.offsides_opt, matchStatistics[2]->offsides_opt);
		EXPECT_EQ(matchStatistics3.possessiontime_opt, matchStatistics[2]->possessiontime_opt);
		EXPECT_EQ(matchStatistics3.yellowcards_opt, matchStatistics[2]->yellowcards_opt);
		EXPECT_EQ(matchStatistics3.redcards_opt, matchStatistics[2]->redcards_opt);
		EXPECT_EQ(matchStatistics3.yellowredcards_opt, matchStatistics[2]->yellowredcards_opt);
		EXPECT_EQ(matchStatistics3.saves_opt, matchStatistics[2]->saves_opt);
		EXPECT_EQ(matchStatistics3.substitutions_opt, matchStatistics[2]->substitutions_opt);
		EXPECT_EQ(matchStatistics3.goal_kick_opt, matchStatistics[2]->goal_kick_opt);
		EXPECT_EQ(matchStatistics3.throw_in_opt, matchStatistics[2]->throw_in_opt);
		EXPECT_EQ(matchStatistics3.ball_safe_opt, matchStatistics[2]->ball_safe_opt);
		EXPECT_EQ(matchStatistics3.goals_opt, matchStatistics[2]->goals_opt);
		EXPECT_EQ(matchStatistics3.penalties_opt, matchStatistics[2]->penalties_opt);
		EXPECT_EQ(matchStatistics3.injuries_opt, matchStatistics[2]->injuries_opt);
		
	}
}