#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/League.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, LeagueTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	League league1;
	league1.sm_id = 1;
	league1.active = false;
	league1.type_opt = STR("type_opt3");
	league1.sm_legacy_id_opt = 4;
	league1.sm_country_id = 5;
	league1.logo_path_opt = STR("logo_path_opt6");
	league1.name = STR("name7");
	league1.is_cup = false;
	league1.sm_current_season_id = 9;
	league1.sm_current_round_id_opt = 10;
	league1.sm_current_stage_id_opt = 11;
	league1.live_standings = false;
	league1.coverage_predictions = true;
	league1.coverage_topscorer_goals = false;
	league1.coverage_topscorer_assists = true;
	league1.coverage_topscorer_cards = false;
	
	League league2;
	
	League league3;
	league3.sm_id = 17;
	league3.active = false;
	league3.type_opt = STR("type_opt19");
	league3.sm_legacy_id_opt = 20;
	league3.sm_country_id = 21;
	league3.logo_path_opt = STR("logo_path_opt22");
	league3.name = STR("name23");
	league3.is_cup = false;
	league3.sm_current_season_id = 25;
	league3.sm_current_round_id_opt = 26;
	league3.sm_current_stage_id_opt = 27;
	league3.live_standings = false;
	league3.coverage_predictions = true;
	league3.coverage_topscorer_goals = false;
	league3.coverage_topscorer_assists = true;
	league3.coverage_topscorer_cards = false;
	
	std::vector<UpLeague> oneLeague;
	oneLeague.emplace_back(std::make_unique<League>(league1));

	std::vector<UpLeague> twoleagues;
	twoleagues.emplace_back(std::make_unique<League>(league1));
	twoleagues.emplace_back(std::make_unique<League>(league2));

	std::vector<UpLeague> threeleagues;
	threeleagues.emplace_back(std::make_unique<League>(league1));
	threeleagues.emplace_back(std::make_unique<League>(league2));
	threeleagues.emplace_back(std::make_unique<League>(league3));

	EXPECT_EQ(1, oneLeague.size());
	EXPECT_EQ(2, twoleagues.size());
	EXPECT_EQ(3, threeleagues.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_LEAGUES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneLeague));
	ASSERT_EQ(1, QueryCount<League>(db));
	{
		auto leagues = QueryAll<League>(db);
		EXPECT_EQ(1, leagues.size());

		EXPECT_NE(0, leagues[0]->id);
		EXPECT_EQ(league1.sm_id, leagues[0]->sm_id);
		EXPECT_EQ(league1.active, leagues[0]->active);
		EXPECT_EQ(league1.type_opt, leagues[0]->type_opt);
		EXPECT_EQ(league1.sm_legacy_id_opt, leagues[0]->sm_legacy_id_opt);
		EXPECT_EQ(league1.sm_country_id, leagues[0]->sm_country_id);
		EXPECT_EQ(league1.logo_path_opt, leagues[0]->logo_path_opt);
		EXPECT_EQ(league1.name, leagues[0]->name);
		EXPECT_EQ(league1.is_cup, leagues[0]->is_cup);
		EXPECT_EQ(league1.sm_current_season_id, leagues[0]->sm_current_season_id);
		EXPECT_EQ(league1.sm_current_round_id_opt, leagues[0]->sm_current_round_id_opt);
		EXPECT_EQ(league1.sm_current_stage_id_opt, leagues[0]->sm_current_stage_id_opt);
		EXPECT_EQ(league1.live_standings, leagues[0]->live_standings);
		EXPECT_EQ(league1.coverage_predictions, leagues[0]->coverage_predictions);
		EXPECT_EQ(league1.coverage_topscorer_goals, leagues[0]->coverage_topscorer_goals);
		EXPECT_EQ(league1.coverage_topscorer_assists, leagues[0]->coverage_topscorer_assists);
		EXPECT_EQ(league1.coverage_topscorer_cards, leagues[0]->coverage_topscorer_cards);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoleagues));
	ASSERT_EQ(2, QueryCount<League>(db));
	{
		auto leagues = QueryAll<League>(db);
		EXPECT_EQ(2, leagues.size());

		EXPECT_NE(0, leagues[0]->id);
		EXPECT_EQ(league1.sm_id, leagues[0]->sm_id);
		EXPECT_EQ(league1.active, leagues[0]->active);
		EXPECT_EQ(league1.type_opt, leagues[0]->type_opt);
		EXPECT_EQ(league1.sm_legacy_id_opt, leagues[0]->sm_legacy_id_opt);
		EXPECT_EQ(league1.sm_country_id, leagues[0]->sm_country_id);
		EXPECT_EQ(league1.logo_path_opt, leagues[0]->logo_path_opt);
		EXPECT_EQ(league1.name, leagues[0]->name);
		EXPECT_EQ(league1.is_cup, leagues[0]->is_cup);
		EXPECT_EQ(league1.sm_current_season_id, leagues[0]->sm_current_season_id);
		EXPECT_EQ(league1.sm_current_round_id_opt, leagues[0]->sm_current_round_id_opt);
		EXPECT_EQ(league1.sm_current_stage_id_opt, leagues[0]->sm_current_stage_id_opt);
		EXPECT_EQ(league1.live_standings, leagues[0]->live_standings);
		EXPECT_EQ(league1.coverage_predictions, leagues[0]->coverage_predictions);
		EXPECT_EQ(league1.coverage_topscorer_goals, leagues[0]->coverage_topscorer_goals);
		EXPECT_EQ(league1.coverage_topscorer_assists, leagues[0]->coverage_topscorer_assists);
		EXPECT_EQ(league1.coverage_topscorer_cards, leagues[0]->coverage_topscorer_cards);
		

		EXPECT_NE(0, leagues[1]->id);
		EXPECT_EQ(league2.sm_id, leagues[1]->sm_id);
		EXPECT_EQ(league2.active, leagues[1]->active);
		EXPECT_EQ(league2.type_opt, leagues[1]->type_opt);
		EXPECT_EQ(league2.sm_legacy_id_opt, leagues[1]->sm_legacy_id_opt);
		EXPECT_EQ(league2.sm_country_id, leagues[1]->sm_country_id);
		EXPECT_EQ(league2.logo_path_opt, leagues[1]->logo_path_opt);
		EXPECT_EQ(league2.name, leagues[1]->name);
		EXPECT_EQ(league2.is_cup, leagues[1]->is_cup);
		EXPECT_EQ(league2.sm_current_season_id, leagues[1]->sm_current_season_id);
		EXPECT_EQ(league2.sm_current_round_id_opt, leagues[1]->sm_current_round_id_opt);
		EXPECT_EQ(league2.sm_current_stage_id_opt, leagues[1]->sm_current_stage_id_opt);
		EXPECT_EQ(league2.live_standings, leagues[1]->live_standings);
		EXPECT_EQ(league2.coverage_predictions, leagues[1]->coverage_predictions);
		EXPECT_EQ(league2.coverage_topscorer_goals, leagues[1]->coverage_topscorer_goals);
		EXPECT_EQ(league2.coverage_topscorer_assists, leagues[1]->coverage_topscorer_assists);
		EXPECT_EQ(league2.coverage_topscorer_cards, leagues[1]->coverage_topscorer_cards);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeleagues));
	ASSERT_EQ(3, QueryCount<League>(db));
	{
		auto leagues = QueryAll<League>(db);
		EXPECT_EQ(3, leagues.size());

		EXPECT_NE(0, leagues[0]->id);
		EXPECT_EQ(league1.sm_id, leagues[0]->sm_id);
		EXPECT_EQ(league1.active, leagues[0]->active);
		EXPECT_EQ(league1.type_opt, leagues[0]->type_opt);
		EXPECT_EQ(league1.sm_legacy_id_opt, leagues[0]->sm_legacy_id_opt);
		EXPECT_EQ(league1.sm_country_id, leagues[0]->sm_country_id);
		EXPECT_EQ(league1.logo_path_opt, leagues[0]->logo_path_opt);
		EXPECT_EQ(league1.name, leagues[0]->name);
		EXPECT_EQ(league1.is_cup, leagues[0]->is_cup);
		EXPECT_EQ(league1.sm_current_season_id, leagues[0]->sm_current_season_id);
		EXPECT_EQ(league1.sm_current_round_id_opt, leagues[0]->sm_current_round_id_opt);
		EXPECT_EQ(league1.sm_current_stage_id_opt, leagues[0]->sm_current_stage_id_opt);
		EXPECT_EQ(league1.live_standings, leagues[0]->live_standings);
		EXPECT_EQ(league1.coverage_predictions, leagues[0]->coverage_predictions);
		EXPECT_EQ(league1.coverage_topscorer_goals, leagues[0]->coverage_topscorer_goals);
		EXPECT_EQ(league1.coverage_topscorer_assists, leagues[0]->coverage_topscorer_assists);
		EXPECT_EQ(league1.coverage_topscorer_cards, leagues[0]->coverage_topscorer_cards);
		

		EXPECT_NE(0, leagues[1]->id);
		EXPECT_EQ(league2.sm_id, leagues[1]->sm_id);
		EXPECT_EQ(league2.active, leagues[1]->active);
		EXPECT_EQ(league2.type_opt, leagues[1]->type_opt);
		EXPECT_EQ(league2.sm_legacy_id_opt, leagues[1]->sm_legacy_id_opt);
		EXPECT_EQ(league2.sm_country_id, leagues[1]->sm_country_id);
		EXPECT_EQ(league2.logo_path_opt, leagues[1]->logo_path_opt);
		EXPECT_EQ(league2.name, leagues[1]->name);
		EXPECT_EQ(league2.is_cup, leagues[1]->is_cup);
		EXPECT_EQ(league2.sm_current_season_id, leagues[1]->sm_current_season_id);
		EXPECT_EQ(league2.sm_current_round_id_opt, leagues[1]->sm_current_round_id_opt);
		EXPECT_EQ(league2.sm_current_stage_id_opt, leagues[1]->sm_current_stage_id_opt);
		EXPECT_EQ(league2.live_standings, leagues[1]->live_standings);
		EXPECT_EQ(league2.coverage_predictions, leagues[1]->coverage_predictions);
		EXPECT_EQ(league2.coverage_topscorer_goals, leagues[1]->coverage_topscorer_goals);
		EXPECT_EQ(league2.coverage_topscorer_assists, leagues[1]->coverage_topscorer_assists);
		EXPECT_EQ(league2.coverage_topscorer_cards, leagues[1]->coverage_topscorer_cards);
		

		EXPECT_NE(0, leagues[2]->id);
		EXPECT_EQ(league3.sm_id, leagues[2]->sm_id);
		EXPECT_EQ(league3.active, leagues[2]->active);
		EXPECT_EQ(league3.type_opt, leagues[2]->type_opt);
		EXPECT_EQ(league3.sm_legacy_id_opt, leagues[2]->sm_legacy_id_opt);
		EXPECT_EQ(league3.sm_country_id, leagues[2]->sm_country_id);
		EXPECT_EQ(league3.logo_path_opt, leagues[2]->logo_path_opt);
		EXPECT_EQ(league3.name, leagues[2]->name);
		EXPECT_EQ(league3.is_cup, leagues[2]->is_cup);
		EXPECT_EQ(league3.sm_current_season_id, leagues[2]->sm_current_season_id);
		EXPECT_EQ(league3.sm_current_round_id_opt, leagues[2]->sm_current_round_id_opt);
		EXPECT_EQ(league3.sm_current_stage_id_opt, leagues[2]->sm_current_stage_id_opt);
		EXPECT_EQ(league3.live_standings, leagues[2]->live_standings);
		EXPECT_EQ(league3.coverage_predictions, leagues[2]->coverage_predictions);
		EXPECT_EQ(league3.coverage_topscorer_goals, leagues[2]->coverage_topscorer_goals);
		EXPECT_EQ(league3.coverage_topscorer_assists, leagues[2]->coverage_topscorer_assists);
		EXPECT_EQ(league3.coverage_topscorer_cards, leagues[2]->coverage_topscorer_cards);
		
	}
}