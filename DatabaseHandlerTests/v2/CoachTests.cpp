#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Coach.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, CoachTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Coach coach1;
	coach1.sm_id = 1;
	coach1.sm_team_id = 2;
	coach1.sm_country_id = 3;
	coach1.common_name = STR("common_name4");
	coach1.fullname = STR("fullname5");
	coach1.firstname_opt = STR("firstname_opt6");
	coach1.lastname_opt = STR("lastname_opt7");
	coach1.nationality = STR("nationality8");
	coach1.birthdate_opt = STR("birthdate_opt9");
	coach1.birthcountry_opt = STR("birthcountry_opt10");
	coach1.birthplace_opt = STR("birthplace_opt11");
	coach1.image_path_opt = STR("image_path_opt12");
	
	Coach coach2;
	
	Coach coach3;
	coach3.sm_id = 13;
	coach3.sm_team_id = 14;
	coach3.sm_country_id = 15;
	coach3.common_name = STR("common_name16");
	coach3.fullname = STR("fullname17");
	coach3.firstname_opt = STR("firstname_opt18");
	coach3.lastname_opt = STR("lastname_opt19");
	coach3.nationality = STR("nationality20");
	coach3.birthdate_opt = STR("birthdate_opt21");
	coach3.birthcountry_opt = STR("birthcountry_opt22");
	coach3.birthplace_opt = STR("birthplace_opt23");
	coach3.image_path_opt = STR("image_path_opt24");
	
	std::vector<UpCoach> oneCoach;
	oneCoach.emplace_back(std::make_unique<Coach>(coach1));

	std::vector<UpCoach> twocoaches;
	twocoaches.emplace_back(std::make_unique<Coach>(coach1));
	twocoaches.emplace_back(std::make_unique<Coach>(coach2));

	std::vector<UpCoach> threecoaches;
	threecoaches.emplace_back(std::make_unique<Coach>(coach1));
	threecoaches.emplace_back(std::make_unique<Coach>(coach2));
	threecoaches.emplace_back(std::make_unique<Coach>(coach3));

	EXPECT_EQ(1, oneCoach.size());
	EXPECT_EQ(2, twocoaches.size());
	EXPECT_EQ(3, threecoaches.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_COACHES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneCoach));
	ASSERT_EQ(1, QueryCount<Coach>(db));
	{
		auto coaches = QueryAll<Coach>(db);
		EXPECT_EQ(1, coaches.size());

		EXPECT_NE(0, coaches[0]->id);
		EXPECT_EQ(coach1.sm_id, coaches[0]->sm_id);
		EXPECT_EQ(coach1.sm_team_id, coaches[0]->sm_team_id);
		EXPECT_EQ(coach1.sm_country_id, coaches[0]->sm_country_id);
		EXPECT_EQ(coach1.common_name, coaches[0]->common_name);
		EXPECT_EQ(coach1.fullname, coaches[0]->fullname);
		EXPECT_EQ(coach1.firstname_opt, coaches[0]->firstname_opt);
		EXPECT_EQ(coach1.lastname_opt, coaches[0]->lastname_opt);
		EXPECT_EQ(coach1.nationality, coaches[0]->nationality);
		EXPECT_EQ(coach1.birthdate_opt, coaches[0]->birthdate_opt);
		EXPECT_EQ(coach1.birthcountry_opt, coaches[0]->birthcountry_opt);
		EXPECT_EQ(coach1.birthplace_opt, coaches[0]->birthplace_opt);
		EXPECT_EQ(coach1.image_path_opt, coaches[0]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twocoaches));
	ASSERT_EQ(2, QueryCount<Coach>(db));
	{
		auto coaches = QueryAll<Coach>(db);
		EXPECT_EQ(2, coaches.size());

		EXPECT_NE(0, coaches[0]->id);
		EXPECT_EQ(coach1.sm_id, coaches[0]->sm_id);
		EXPECT_EQ(coach1.sm_team_id, coaches[0]->sm_team_id);
		EXPECT_EQ(coach1.sm_country_id, coaches[0]->sm_country_id);
		EXPECT_EQ(coach1.common_name, coaches[0]->common_name);
		EXPECT_EQ(coach1.fullname, coaches[0]->fullname);
		EXPECT_EQ(coach1.firstname_opt, coaches[0]->firstname_opt);
		EXPECT_EQ(coach1.lastname_opt, coaches[0]->lastname_opt);
		EXPECT_EQ(coach1.nationality, coaches[0]->nationality);
		EXPECT_EQ(coach1.birthdate_opt, coaches[0]->birthdate_opt);
		EXPECT_EQ(coach1.birthcountry_opt, coaches[0]->birthcountry_opt);
		EXPECT_EQ(coach1.birthplace_opt, coaches[0]->birthplace_opt);
		EXPECT_EQ(coach1.image_path_opt, coaches[0]->image_path_opt);
		

		EXPECT_NE(0, coaches[1]->id);
		EXPECT_EQ(coach2.sm_id, coaches[1]->sm_id);
		EXPECT_EQ(coach2.sm_team_id, coaches[1]->sm_team_id);
		EXPECT_EQ(coach2.sm_country_id, coaches[1]->sm_country_id);
		EXPECT_EQ(coach2.common_name, coaches[1]->common_name);
		EXPECT_EQ(coach2.fullname, coaches[1]->fullname);
		EXPECT_EQ(coach2.firstname_opt, coaches[1]->firstname_opt);
		EXPECT_EQ(coach2.lastname_opt, coaches[1]->lastname_opt);
		EXPECT_EQ(coach2.nationality, coaches[1]->nationality);
		EXPECT_EQ(coach2.birthdate_opt, coaches[1]->birthdate_opt);
		EXPECT_EQ(coach2.birthcountry_opt, coaches[1]->birthcountry_opt);
		EXPECT_EQ(coach2.birthplace_opt, coaches[1]->birthplace_opt);
		EXPECT_EQ(coach2.image_path_opt, coaches[1]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threecoaches));
	ASSERT_EQ(3, QueryCount<Coach>(db));
	{
		auto coaches = QueryAll<Coach>(db);
		EXPECT_EQ(3, coaches.size());

		EXPECT_NE(0, coaches[0]->id);
		EXPECT_EQ(coach1.sm_id, coaches[0]->sm_id);
		EXPECT_EQ(coach1.sm_team_id, coaches[0]->sm_team_id);
		EXPECT_EQ(coach1.sm_country_id, coaches[0]->sm_country_id);
		EXPECT_EQ(coach1.common_name, coaches[0]->common_name);
		EXPECT_EQ(coach1.fullname, coaches[0]->fullname);
		EXPECT_EQ(coach1.firstname_opt, coaches[0]->firstname_opt);
		EXPECT_EQ(coach1.lastname_opt, coaches[0]->lastname_opt);
		EXPECT_EQ(coach1.nationality, coaches[0]->nationality);
		EXPECT_EQ(coach1.birthdate_opt, coaches[0]->birthdate_opt);
		EXPECT_EQ(coach1.birthcountry_opt, coaches[0]->birthcountry_opt);
		EXPECT_EQ(coach1.birthplace_opt, coaches[0]->birthplace_opt);
		EXPECT_EQ(coach1.image_path_opt, coaches[0]->image_path_opt);
		

		EXPECT_NE(0, coaches[1]->id);
		EXPECT_EQ(coach2.sm_id, coaches[1]->sm_id);
		EXPECT_EQ(coach2.sm_team_id, coaches[1]->sm_team_id);
		EXPECT_EQ(coach2.sm_country_id, coaches[1]->sm_country_id);
		EXPECT_EQ(coach2.common_name, coaches[1]->common_name);
		EXPECT_EQ(coach2.fullname, coaches[1]->fullname);
		EXPECT_EQ(coach2.firstname_opt, coaches[1]->firstname_opt);
		EXPECT_EQ(coach2.lastname_opt, coaches[1]->lastname_opt);
		EXPECT_EQ(coach2.nationality, coaches[1]->nationality);
		EXPECT_EQ(coach2.birthdate_opt, coaches[1]->birthdate_opt);
		EXPECT_EQ(coach2.birthcountry_opt, coaches[1]->birthcountry_opt);
		EXPECT_EQ(coach2.birthplace_opt, coaches[1]->birthplace_opt);
		EXPECT_EQ(coach2.image_path_opt, coaches[1]->image_path_opt);
		

		EXPECT_NE(0, coaches[2]->id);
		EXPECT_EQ(coach3.sm_id, coaches[2]->sm_id);
		EXPECT_EQ(coach3.sm_team_id, coaches[2]->sm_team_id);
		EXPECT_EQ(coach3.sm_country_id, coaches[2]->sm_country_id);
		EXPECT_EQ(coach3.common_name, coaches[2]->common_name);
		EXPECT_EQ(coach3.fullname, coaches[2]->fullname);
		EXPECT_EQ(coach3.firstname_opt, coaches[2]->firstname_opt);
		EXPECT_EQ(coach3.lastname_opt, coaches[2]->lastname_opt);
		EXPECT_EQ(coach3.nationality, coaches[2]->nationality);
		EXPECT_EQ(coach3.birthdate_opt, coaches[2]->birthdate_opt);
		EXPECT_EQ(coach3.birthcountry_opt, coaches[2]->birthcountry_opt);
		EXPECT_EQ(coach3.birthplace_opt, coaches[2]->birthplace_opt);
		EXPECT_EQ(coach3.image_path_opt, coaches[2]->image_path_opt);
		
	}
}