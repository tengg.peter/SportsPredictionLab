#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Bookmaker.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, BookmakerTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Bookmaker bookmaker1;
	bookmaker1.sm_id = 1;
	bookmaker1.name = STR("name2");
	bookmaker1.logo_opt = STR("logo_opt3");
	
	Bookmaker bookmaker2;
	
	Bookmaker bookmaker3;
	bookmaker3.sm_id = 4;
	bookmaker3.name = STR("name5");
	bookmaker3.logo_opt = STR("logo_opt6");
	
	std::vector<UpBookmaker> oneBookmaker;
	oneBookmaker.emplace_back(std::make_unique<Bookmaker>(bookmaker1));

	std::vector<UpBookmaker> twobookmakers;
	twobookmakers.emplace_back(std::make_unique<Bookmaker>(bookmaker1));
	twobookmakers.emplace_back(std::make_unique<Bookmaker>(bookmaker2));

	std::vector<UpBookmaker> threebookmakers;
	threebookmakers.emplace_back(std::make_unique<Bookmaker>(bookmaker1));
	threebookmakers.emplace_back(std::make_unique<Bookmaker>(bookmaker2));
	threebookmakers.emplace_back(std::make_unique<Bookmaker>(bookmaker3));

	EXPECT_EQ(1, oneBookmaker.size());
	EXPECT_EQ(2, twobookmakers.size());
	EXPECT_EQ(3, threebookmakers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_BOOKMAKERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneBookmaker));
	ASSERT_EQ(1, QueryCount<Bookmaker>(db));
	{
		auto bookmakers = QueryAll<Bookmaker>(db);
		EXPECT_EQ(1, bookmakers.size());

		EXPECT_NE(0, bookmakers[0]->id);
		EXPECT_EQ(bookmaker1.sm_id, bookmakers[0]->sm_id);
		EXPECT_EQ(bookmaker1.name, bookmakers[0]->name);
		EXPECT_EQ(bookmaker1.logo_opt, bookmakers[0]->logo_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twobookmakers));
	ASSERT_EQ(2, QueryCount<Bookmaker>(db));
	{
		auto bookmakers = QueryAll<Bookmaker>(db);
		EXPECT_EQ(2, bookmakers.size());

		EXPECT_NE(0, bookmakers[0]->id);
		EXPECT_EQ(bookmaker1.sm_id, bookmakers[0]->sm_id);
		EXPECT_EQ(bookmaker1.name, bookmakers[0]->name);
		EXPECT_EQ(bookmaker1.logo_opt, bookmakers[0]->logo_opt);
		

		EXPECT_NE(0, bookmakers[1]->id);
		EXPECT_EQ(bookmaker2.sm_id, bookmakers[1]->sm_id);
		EXPECT_EQ(bookmaker2.name, bookmakers[1]->name);
		EXPECT_EQ(bookmaker2.logo_opt, bookmakers[1]->logo_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threebookmakers));
	ASSERT_EQ(3, QueryCount<Bookmaker>(db));
	{
		auto bookmakers = QueryAll<Bookmaker>(db);
		EXPECT_EQ(3, bookmakers.size());

		EXPECT_NE(0, bookmakers[0]->id);
		EXPECT_EQ(bookmaker1.sm_id, bookmakers[0]->sm_id);
		EXPECT_EQ(bookmaker1.name, bookmakers[0]->name);
		EXPECT_EQ(bookmaker1.logo_opt, bookmakers[0]->logo_opt);
		

		EXPECT_NE(0, bookmakers[1]->id);
		EXPECT_EQ(bookmaker2.sm_id, bookmakers[1]->sm_id);
		EXPECT_EQ(bookmaker2.name, bookmakers[1]->name);
		EXPECT_EQ(bookmaker2.logo_opt, bookmakers[1]->logo_opt);
		

		EXPECT_NE(0, bookmakers[2]->id);
		EXPECT_EQ(bookmaker3.sm_id, bookmakers[2]->sm_id);
		EXPECT_EQ(bookmaker3.name, bookmakers[2]->name);
		EXPECT_EQ(bookmaker3.logo_opt, bookmakers[2]->logo_opt);
		
	}
}