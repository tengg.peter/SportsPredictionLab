#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Stage.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, StageTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Stage stage1;
	stage1.sm_id = 1;
	stage1.name = STR("name2");
	stage1.type = STR("type3");
	stage1.sm_league_id = 4;
	stage1.sm_season_id = 5;
	stage1.sort_order_opt = 6;
	stage1.has_standings = true;
	
	Stage stage2;
	
	Stage stage3;
	stage3.sm_id = 8;
	stage3.name = STR("name9");
	stage3.type = STR("type10");
	stage3.sm_league_id = 11;
	stage3.sm_season_id = 12;
	stage3.sort_order_opt = 13;
	stage3.has_standings = false;
	
	std::vector<UpStage> oneStage;
	oneStage.emplace_back(std::make_unique<Stage>(stage1));

	std::vector<UpStage> twostages;
	twostages.emplace_back(std::make_unique<Stage>(stage1));
	twostages.emplace_back(std::make_unique<Stage>(stage2));

	std::vector<UpStage> threestages;
	threestages.emplace_back(std::make_unique<Stage>(stage1));
	threestages.emplace_back(std::make_unique<Stage>(stage2));
	threestages.emplace_back(std::make_unique<Stage>(stage3));

	EXPECT_EQ(1, oneStage.size());
	EXPECT_EQ(2, twostages.size());
	EXPECT_EQ(3, threestages.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_STAGES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneStage));
	ASSERT_EQ(1, QueryCount<Stage>(db));
	{
		auto stages = QueryAll<Stage>(db);
		EXPECT_EQ(1, stages.size());

		EXPECT_NE(0, stages[0]->id);
		EXPECT_EQ(stage1.sm_id, stages[0]->sm_id);
		EXPECT_EQ(stage1.name, stages[0]->name);
		EXPECT_EQ(stage1.type, stages[0]->type);
		EXPECT_EQ(stage1.sm_league_id, stages[0]->sm_league_id);
		EXPECT_EQ(stage1.sm_season_id, stages[0]->sm_season_id);
		EXPECT_EQ(stage1.sort_order_opt, stages[0]->sort_order_opt);
		EXPECT_EQ(stage1.has_standings, stages[0]->has_standings);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twostages));
	ASSERT_EQ(2, QueryCount<Stage>(db));
	{
		auto stages = QueryAll<Stage>(db);
		EXPECT_EQ(2, stages.size());

		EXPECT_NE(0, stages[0]->id);
		EXPECT_EQ(stage1.sm_id, stages[0]->sm_id);
		EXPECT_EQ(stage1.name, stages[0]->name);
		EXPECT_EQ(stage1.type, stages[0]->type);
		EXPECT_EQ(stage1.sm_league_id, stages[0]->sm_league_id);
		EXPECT_EQ(stage1.sm_season_id, stages[0]->sm_season_id);
		EXPECT_EQ(stage1.sort_order_opt, stages[0]->sort_order_opt);
		EXPECT_EQ(stage1.has_standings, stages[0]->has_standings);
		

		EXPECT_NE(0, stages[1]->id);
		EXPECT_EQ(stage2.sm_id, stages[1]->sm_id);
		EXPECT_EQ(stage2.name, stages[1]->name);
		EXPECT_EQ(stage2.type, stages[1]->type);
		EXPECT_EQ(stage2.sm_league_id, stages[1]->sm_league_id);
		EXPECT_EQ(stage2.sm_season_id, stages[1]->sm_season_id);
		EXPECT_EQ(stage2.sort_order_opt, stages[1]->sort_order_opt);
		EXPECT_EQ(stage2.has_standings, stages[1]->has_standings);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threestages));
	ASSERT_EQ(3, QueryCount<Stage>(db));
	{
		auto stages = QueryAll<Stage>(db);
		EXPECT_EQ(3, stages.size());

		EXPECT_NE(0, stages[0]->id);
		EXPECT_EQ(stage1.sm_id, stages[0]->sm_id);
		EXPECT_EQ(stage1.name, stages[0]->name);
		EXPECT_EQ(stage1.type, stages[0]->type);
		EXPECT_EQ(stage1.sm_league_id, stages[0]->sm_league_id);
		EXPECT_EQ(stage1.sm_season_id, stages[0]->sm_season_id);
		EXPECT_EQ(stage1.sort_order_opt, stages[0]->sort_order_opt);
		EXPECT_EQ(stage1.has_standings, stages[0]->has_standings);
		

		EXPECT_NE(0, stages[1]->id);
		EXPECT_EQ(stage2.sm_id, stages[1]->sm_id);
		EXPECT_EQ(stage2.name, stages[1]->name);
		EXPECT_EQ(stage2.type, stages[1]->type);
		EXPECT_EQ(stage2.sm_league_id, stages[1]->sm_league_id);
		EXPECT_EQ(stage2.sm_season_id, stages[1]->sm_season_id);
		EXPECT_EQ(stage2.sort_order_opt, stages[1]->sort_order_opt);
		EXPECT_EQ(stage2.has_standings, stages[1]->has_standings);
		

		EXPECT_NE(0, stages[2]->id);
		EXPECT_EQ(stage3.sm_id, stages[2]->sm_id);
		EXPECT_EQ(stage3.name, stages[2]->name);
		EXPECT_EQ(stage3.type, stages[2]->type);
		EXPECT_EQ(stage3.sm_league_id, stages[2]->sm_league_id);
		EXPECT_EQ(stage3.sm_season_id, stages[2]->sm_season_id);
		EXPECT_EQ(stage3.sort_order_opt, stages[2]->sort_order_opt);
		EXPECT_EQ(stage3.has_standings, stages[2]->has_standings);
		
	}
}