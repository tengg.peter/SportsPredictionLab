#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopAssistScorer.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TopAssistScorerTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TopAssistScorer topAssistScorer1;
	topAssistScorer1.position = 1;
	topAssistScorer1.sm_season_id = 2;
	topAssistScorer1.sm_player_id = 3;
	topAssistScorer1.sm_team_id = 4;
	topAssistScorer1.sm_stage_id_opt = 5;
	topAssistScorer1.assists = 6;
	topAssistScorer1.type = STR("type7");
	
	TopAssistScorer topAssistScorer2;
	
	TopAssistScorer topAssistScorer3;
	topAssistScorer3.position = 8;
	topAssistScorer3.sm_season_id = 9;
	topAssistScorer3.sm_player_id = 10;
	topAssistScorer3.sm_team_id = 11;
	topAssistScorer3.sm_stage_id_opt = 12;
	topAssistScorer3.assists = 13;
	topAssistScorer3.type = STR("type14");
	
	std::vector<UpTopAssistScorer> oneTopAssistScorer;
	oneTopAssistScorer.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer1));

	std::vector<UpTopAssistScorer> twotopAssistScorers;
	twotopAssistScorers.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer1));
	twotopAssistScorers.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer2));

	std::vector<UpTopAssistScorer> threetopAssistScorers;
	threetopAssistScorers.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer1));
	threetopAssistScorers.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer2));
	threetopAssistScorers.emplace_back(std::make_unique<TopAssistScorer>(topAssistScorer3));

	EXPECT_EQ(1, oneTopAssistScorer.size());
	EXPECT_EQ(2, twotopAssistScorers.size());
	EXPECT_EQ(3, threetopAssistScorers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TOP_ASSIST_SCORERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTopAssistScorer));
	ASSERT_EQ(1, QueryCount<TopAssistScorer>(db));
	{
		auto topAssistScorers = QueryAll<TopAssistScorer>(db);
		EXPECT_EQ(1, topAssistScorers.size());

		EXPECT_NE(0, topAssistScorers[0]->id);
		EXPECT_EQ(topAssistScorer1.position, topAssistScorers[0]->position);
		EXPECT_EQ(topAssistScorer1.sm_season_id, topAssistScorers[0]->sm_season_id);
		EXPECT_EQ(topAssistScorer1.sm_player_id, topAssistScorers[0]->sm_player_id);
		EXPECT_EQ(topAssistScorer1.sm_team_id, topAssistScorers[0]->sm_team_id);
		EXPECT_EQ(topAssistScorer1.sm_stage_id_opt, topAssistScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer1.assists, topAssistScorers[0]->assists);
		EXPECT_EQ(topAssistScorer1.type, topAssistScorers[0]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twotopAssistScorers));
	ASSERT_EQ(2, QueryCount<TopAssistScorer>(db));
	{
		auto topAssistScorers = QueryAll<TopAssistScorer>(db);
		EXPECT_EQ(2, topAssistScorers.size());

		EXPECT_NE(0, topAssistScorers[0]->id);
		EXPECT_EQ(topAssistScorer1.position, topAssistScorers[0]->position);
		EXPECT_EQ(topAssistScorer1.sm_season_id, topAssistScorers[0]->sm_season_id);
		EXPECT_EQ(topAssistScorer1.sm_player_id, topAssistScorers[0]->sm_player_id);
		EXPECT_EQ(topAssistScorer1.sm_team_id, topAssistScorers[0]->sm_team_id);
		EXPECT_EQ(topAssistScorer1.sm_stage_id_opt, topAssistScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer1.assists, topAssistScorers[0]->assists);
		EXPECT_EQ(topAssistScorer1.type, topAssistScorers[0]->type);
		

		EXPECT_NE(0, topAssistScorers[1]->id);
		EXPECT_EQ(topAssistScorer2.position, topAssistScorers[1]->position);
		EXPECT_EQ(topAssistScorer2.sm_season_id, topAssistScorers[1]->sm_season_id);
		EXPECT_EQ(topAssistScorer2.sm_player_id, topAssistScorers[1]->sm_player_id);
		EXPECT_EQ(topAssistScorer2.sm_team_id, topAssistScorers[1]->sm_team_id);
		EXPECT_EQ(topAssistScorer2.sm_stage_id_opt, topAssistScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer2.assists, topAssistScorers[1]->assists);
		EXPECT_EQ(topAssistScorer2.type, topAssistScorers[1]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threetopAssistScorers));
	ASSERT_EQ(3, QueryCount<TopAssistScorer>(db));
	{
		auto topAssistScorers = QueryAll<TopAssistScorer>(db);
		EXPECT_EQ(3, topAssistScorers.size());

		EXPECT_NE(0, topAssistScorers[0]->id);
		EXPECT_EQ(topAssistScorer1.position, topAssistScorers[0]->position);
		EXPECT_EQ(topAssistScorer1.sm_season_id, topAssistScorers[0]->sm_season_id);
		EXPECT_EQ(topAssistScorer1.sm_player_id, topAssistScorers[0]->sm_player_id);
		EXPECT_EQ(topAssistScorer1.sm_team_id, topAssistScorers[0]->sm_team_id);
		EXPECT_EQ(topAssistScorer1.sm_stage_id_opt, topAssistScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer1.assists, topAssistScorers[0]->assists);
		EXPECT_EQ(topAssistScorer1.type, topAssistScorers[0]->type);
		

		EXPECT_NE(0, topAssistScorers[1]->id);
		EXPECT_EQ(topAssistScorer2.position, topAssistScorers[1]->position);
		EXPECT_EQ(topAssistScorer2.sm_season_id, topAssistScorers[1]->sm_season_id);
		EXPECT_EQ(topAssistScorer2.sm_player_id, topAssistScorers[1]->sm_player_id);
		EXPECT_EQ(topAssistScorer2.sm_team_id, topAssistScorers[1]->sm_team_id);
		EXPECT_EQ(topAssistScorer2.sm_stage_id_opt, topAssistScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer2.assists, topAssistScorers[1]->assists);
		EXPECT_EQ(topAssistScorer2.type, topAssistScorers[1]->type);
		

		EXPECT_NE(0, topAssistScorers[2]->id);
		EXPECT_EQ(topAssistScorer3.position, topAssistScorers[2]->position);
		EXPECT_EQ(topAssistScorer3.sm_season_id, topAssistScorers[2]->sm_season_id);
		EXPECT_EQ(topAssistScorer3.sm_player_id, topAssistScorers[2]->sm_player_id);
		EXPECT_EQ(topAssistScorer3.sm_team_id, topAssistScorers[2]->sm_team_id);
		EXPECT_EQ(topAssistScorer3.sm_stage_id_opt, topAssistScorers[2]->sm_stage_id_opt);
		EXPECT_EQ(topAssistScorer3.assists, topAssistScorers[2]->assists);
		EXPECT_EQ(topAssistScorer3.type, topAssistScorers[2]->type);
		
	}
}