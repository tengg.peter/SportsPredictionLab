#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Trend.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TrendTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Trend trend1;
	trend1.sm_id = 1;
	trend1.sm_fixture_id = 2;
	trend1.sm_team_id = 3;
	trend1.type = STR("type4");
	trend1.updated_at = STR("updated_at5");
	trend1.timezone_type = 6;
	trend1.timezone = STR("timezone7");
	
	Trend trend2;
	
	Trend trend3;
	trend3.sm_id = 8;
	trend3.sm_fixture_id = 9;
	trend3.sm_team_id = 10;
	trend3.type = STR("type11");
	trend3.updated_at = STR("updated_at12");
	trend3.timezone_type = 13;
	trend3.timezone = STR("timezone14");
	
	std::vector<UpTrend> oneTrend;
	oneTrend.emplace_back(std::make_unique<Trend>(trend1));

	std::vector<UpTrend> twotrends;
	twotrends.emplace_back(std::make_unique<Trend>(trend1));
	twotrends.emplace_back(std::make_unique<Trend>(trend2));

	std::vector<UpTrend> threetrends;
	threetrends.emplace_back(std::make_unique<Trend>(trend1));
	threetrends.emplace_back(std::make_unique<Trend>(trend2));
	threetrends.emplace_back(std::make_unique<Trend>(trend3));

	EXPECT_EQ(1, oneTrend.size());
	EXPECT_EQ(2, twotrends.size());
	EXPECT_EQ(3, threetrends.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TRENDS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTrend));
	ASSERT_EQ(1, QueryCount<Trend>(db));
	{
		auto trends = QueryAll<Trend>(db);
		EXPECT_EQ(1, trends.size());

		EXPECT_NE(0, trends[0]->id);
		EXPECT_EQ(trend1.sm_id, trends[0]->sm_id);
		EXPECT_EQ(trend1.sm_fixture_id, trends[0]->sm_fixture_id);
		EXPECT_EQ(trend1.sm_team_id, trends[0]->sm_team_id);
		EXPECT_EQ(trend1.type, trends[0]->type);
		EXPECT_EQ(trend1.updated_at, trends[0]->updated_at);
		EXPECT_EQ(trend1.timezone_type, trends[0]->timezone_type);
		EXPECT_EQ(trend1.timezone, trends[0]->timezone);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twotrends));
	ASSERT_EQ(2, QueryCount<Trend>(db));
	{
		auto trends = QueryAll<Trend>(db);
		EXPECT_EQ(2, trends.size());

		EXPECT_NE(0, trends[0]->id);
		EXPECT_EQ(trend1.sm_id, trends[0]->sm_id);
		EXPECT_EQ(trend1.sm_fixture_id, trends[0]->sm_fixture_id);
		EXPECT_EQ(trend1.sm_team_id, trends[0]->sm_team_id);
		EXPECT_EQ(trend1.type, trends[0]->type);
		EXPECT_EQ(trend1.updated_at, trends[0]->updated_at);
		EXPECT_EQ(trend1.timezone_type, trends[0]->timezone_type);
		EXPECT_EQ(trend1.timezone, trends[0]->timezone);
		

		EXPECT_NE(0, trends[1]->id);
		EXPECT_EQ(trend2.sm_id, trends[1]->sm_id);
		EXPECT_EQ(trend2.sm_fixture_id, trends[1]->sm_fixture_id);
		EXPECT_EQ(trend2.sm_team_id, trends[1]->sm_team_id);
		EXPECT_EQ(trend2.type, trends[1]->type);
		EXPECT_EQ(trend2.updated_at, trends[1]->updated_at);
		EXPECT_EQ(trend2.timezone_type, trends[1]->timezone_type);
		EXPECT_EQ(trend2.timezone, trends[1]->timezone);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threetrends));
	ASSERT_EQ(3, QueryCount<Trend>(db));
	{
		auto trends = QueryAll<Trend>(db);
		EXPECT_EQ(3, trends.size());

		EXPECT_NE(0, trends[0]->id);
		EXPECT_EQ(trend1.sm_id, trends[0]->sm_id);
		EXPECT_EQ(trend1.sm_fixture_id, trends[0]->sm_fixture_id);
		EXPECT_EQ(trend1.sm_team_id, trends[0]->sm_team_id);
		EXPECT_EQ(trend1.type, trends[0]->type);
		EXPECT_EQ(trend1.updated_at, trends[0]->updated_at);
		EXPECT_EQ(trend1.timezone_type, trends[0]->timezone_type);
		EXPECT_EQ(trend1.timezone, trends[0]->timezone);
		

		EXPECT_NE(0, trends[1]->id);
		EXPECT_EQ(trend2.sm_id, trends[1]->sm_id);
		EXPECT_EQ(trend2.sm_fixture_id, trends[1]->sm_fixture_id);
		EXPECT_EQ(trend2.sm_team_id, trends[1]->sm_team_id);
		EXPECT_EQ(trend2.type, trends[1]->type);
		EXPECT_EQ(trend2.updated_at, trends[1]->updated_at);
		EXPECT_EQ(trend2.timezone_type, trends[1]->timezone_type);
		EXPECT_EQ(trend2.timezone, trends[1]->timezone);
		

		EXPECT_NE(0, trends[2]->id);
		EXPECT_EQ(trend3.sm_id, trends[2]->sm_id);
		EXPECT_EQ(trend3.sm_fixture_id, trends[2]->sm_fixture_id);
		EXPECT_EQ(trend3.sm_team_id, trends[2]->sm_team_id);
		EXPECT_EQ(trend3.type, trends[2]->type);
		EXPECT_EQ(trend3.updated_at, trends[2]->updated_at);
		EXPECT_EQ(trend3.timezone_type, trends[2]->timezone_type);
		EXPECT_EQ(trend3.timezone, trends[2]->timezone);
		
	}
}