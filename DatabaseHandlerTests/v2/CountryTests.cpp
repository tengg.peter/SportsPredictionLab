#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Country.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, CountryTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Country country1;
	country1.sm_id = 1;
	country1.sm_continent_id_opt = 2;
	country1.name = STR("name3");
	country1.image_path_opt = STR("image_path_opt4");
	
	Country country2;
	
	Country country3;
	country3.sm_id = 5;
	country3.sm_continent_id_opt = 6;
	country3.name = STR("name7");
	country3.image_path_opt = STR("image_path_opt8");
	
	std::vector<UpCountry> oneCountry;
	oneCountry.emplace_back(std::make_unique<Country>(country1));

	std::vector<UpCountry> twocountries;
	twocountries.emplace_back(std::make_unique<Country>(country1));
	twocountries.emplace_back(std::make_unique<Country>(country2));

	std::vector<UpCountry> threecountries;
	threecountries.emplace_back(std::make_unique<Country>(country1));
	threecountries.emplace_back(std::make_unique<Country>(country2));
	threecountries.emplace_back(std::make_unique<Country>(country3));

	EXPECT_EQ(1, oneCountry.size());
	EXPECT_EQ(2, twocountries.size());
	EXPECT_EQ(3, threecountries.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_COUNTRIES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneCountry));
	ASSERT_EQ(1, QueryCount<Country>(db));
	{
		auto countries = QueryAll<Country>(db);
		EXPECT_EQ(1, countries.size());

		EXPECT_NE(0, countries[0]->id);
		EXPECT_EQ(country1.sm_id, countries[0]->sm_id);
		EXPECT_EQ(country1.sm_continent_id_opt, countries[0]->sm_continent_id_opt);
		EXPECT_EQ(country1.name, countries[0]->name);
		EXPECT_EQ(country1.image_path_opt, countries[0]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twocountries));
	ASSERT_EQ(2, QueryCount<Country>(db));
	{
		auto countries = QueryAll<Country>(db);
		EXPECT_EQ(2, countries.size());

		EXPECT_NE(0, countries[0]->id);
		EXPECT_EQ(country1.sm_id, countries[0]->sm_id);
		EXPECT_EQ(country1.sm_continent_id_opt, countries[0]->sm_continent_id_opt);
		EXPECT_EQ(country1.name, countries[0]->name);
		EXPECT_EQ(country1.image_path_opt, countries[0]->image_path_opt);
		

		EXPECT_NE(0, countries[1]->id);
		EXPECT_EQ(country2.sm_id, countries[1]->sm_id);
		EXPECT_EQ(country2.sm_continent_id_opt, countries[1]->sm_continent_id_opt);
		EXPECT_EQ(country2.name, countries[1]->name);
		EXPECT_EQ(country2.image_path_opt, countries[1]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threecountries));
	ASSERT_EQ(3, QueryCount<Country>(db));
	{
		auto countries = QueryAll<Country>(db);
		EXPECT_EQ(3, countries.size());

		EXPECT_NE(0, countries[0]->id);
		EXPECT_EQ(country1.sm_id, countries[0]->sm_id);
		EXPECT_EQ(country1.sm_continent_id_opt, countries[0]->sm_continent_id_opt);
		EXPECT_EQ(country1.name, countries[0]->name);
		EXPECT_EQ(country1.image_path_opt, countries[0]->image_path_opt);
		

		EXPECT_NE(0, countries[1]->id);
		EXPECT_EQ(country2.sm_id, countries[1]->sm_id);
		EXPECT_EQ(country2.sm_continent_id_opt, countries[1]->sm_continent_id_opt);
		EXPECT_EQ(country2.name, countries[1]->name);
		EXPECT_EQ(country2.image_path_opt, countries[1]->image_path_opt);
		

		EXPECT_NE(0, countries[2]->id);
		EXPECT_EQ(country3.sm_id, countries[2]->sm_id);
		EXPECT_EQ(country3.sm_continent_id_opt, countries[2]->sm_continent_id_opt);
		EXPECT_EQ(country3.name, countries[2]->name);
		EXPECT_EQ(country3.image_path_opt, countries[2]->image_path_opt);
		
	}
}