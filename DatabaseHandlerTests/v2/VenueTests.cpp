#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Venue.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, VenueTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Venue venue1;
	venue1.sm_id = 1;
	venue1.name = STR("name2");
	venue1.surface_opt = STR("surface_opt3");
	venue1.address_opt = STR("address_opt4");
	venue1.city = STR("city5");
	venue1.capacity_opt = 6;
	venue1.image_path_opt = STR("image_path_opt7");
	venue1.coordinates_opt = STR("coordinates_opt8");
	
	Venue venue2;
	
	Venue venue3;
	venue3.sm_id = 9;
	venue3.name = STR("name10");
	venue3.surface_opt = STR("surface_opt11");
	venue3.address_opt = STR("address_opt12");
	venue3.city = STR("city13");
	venue3.capacity_opt = 14;
	venue3.image_path_opt = STR("image_path_opt15");
	venue3.coordinates_opt = STR("coordinates_opt16");
	
	std::vector<UpVenue> oneVenue;
	oneVenue.emplace_back(std::make_unique<Venue>(venue1));

	std::vector<UpVenue> twovenues;
	twovenues.emplace_back(std::make_unique<Venue>(venue1));
	twovenues.emplace_back(std::make_unique<Venue>(venue2));

	std::vector<UpVenue> threevenues;
	threevenues.emplace_back(std::make_unique<Venue>(venue1));
	threevenues.emplace_back(std::make_unique<Venue>(venue2));
	threevenues.emplace_back(std::make_unique<Venue>(venue3));

	EXPECT_EQ(1, oneVenue.size());
	EXPECT_EQ(2, twovenues.size());
	EXPECT_EQ(3, threevenues.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_VENUES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneVenue));
	ASSERT_EQ(1, QueryCount<Venue>(db));
	{
		auto venues = QueryAll<Venue>(db);
		EXPECT_EQ(1, venues.size());

		EXPECT_NE(0, venues[0]->id);
		EXPECT_EQ(venue1.sm_id, venues[0]->sm_id);
		EXPECT_EQ(venue1.name, venues[0]->name);
		EXPECT_EQ(venue1.surface_opt, venues[0]->surface_opt);
		EXPECT_EQ(venue1.address_opt, venues[0]->address_opt);
		EXPECT_EQ(venue1.city, venues[0]->city);
		EXPECT_EQ(venue1.capacity_opt, venues[0]->capacity_opt);
		EXPECT_EQ(venue1.image_path_opt, venues[0]->image_path_opt);
		EXPECT_EQ(venue1.coordinates_opt, venues[0]->coordinates_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twovenues));
	ASSERT_EQ(2, QueryCount<Venue>(db));
	{
		auto venues = QueryAll<Venue>(db);
		EXPECT_EQ(2, venues.size());

		EXPECT_NE(0, venues[0]->id);
		EXPECT_EQ(venue1.sm_id, venues[0]->sm_id);
		EXPECT_EQ(venue1.name, venues[0]->name);
		EXPECT_EQ(venue1.surface_opt, venues[0]->surface_opt);
		EXPECT_EQ(venue1.address_opt, venues[0]->address_opt);
		EXPECT_EQ(venue1.city, venues[0]->city);
		EXPECT_EQ(venue1.capacity_opt, venues[0]->capacity_opt);
		EXPECT_EQ(venue1.image_path_opt, venues[0]->image_path_opt);
		EXPECT_EQ(venue1.coordinates_opt, venues[0]->coordinates_opt);
		

		EXPECT_NE(0, venues[1]->id);
		EXPECT_EQ(venue2.sm_id, venues[1]->sm_id);
		EXPECT_EQ(venue2.name, venues[1]->name);
		EXPECT_EQ(venue2.surface_opt, venues[1]->surface_opt);
		EXPECT_EQ(venue2.address_opt, venues[1]->address_opt);
		EXPECT_EQ(venue2.city, venues[1]->city);
		EXPECT_EQ(venue2.capacity_opt, venues[1]->capacity_opt);
		EXPECT_EQ(venue2.image_path_opt, venues[1]->image_path_opt);
		EXPECT_EQ(venue2.coordinates_opt, venues[1]->coordinates_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threevenues));
	ASSERT_EQ(3, QueryCount<Venue>(db));
	{
		auto venues = QueryAll<Venue>(db);
		EXPECT_EQ(3, venues.size());

		EXPECT_NE(0, venues[0]->id);
		EXPECT_EQ(venue1.sm_id, venues[0]->sm_id);
		EXPECT_EQ(venue1.name, venues[0]->name);
		EXPECT_EQ(venue1.surface_opt, venues[0]->surface_opt);
		EXPECT_EQ(venue1.address_opt, venues[0]->address_opt);
		EXPECT_EQ(venue1.city, venues[0]->city);
		EXPECT_EQ(venue1.capacity_opt, venues[0]->capacity_opt);
		EXPECT_EQ(venue1.image_path_opt, venues[0]->image_path_opt);
		EXPECT_EQ(venue1.coordinates_opt, venues[0]->coordinates_opt);
		

		EXPECT_NE(0, venues[1]->id);
		EXPECT_EQ(venue2.sm_id, venues[1]->sm_id);
		EXPECT_EQ(venue2.name, venues[1]->name);
		EXPECT_EQ(venue2.surface_opt, venues[1]->surface_opt);
		EXPECT_EQ(venue2.address_opt, venues[1]->address_opt);
		EXPECT_EQ(venue2.city, venues[1]->city);
		EXPECT_EQ(venue2.capacity_opt, venues[1]->capacity_opt);
		EXPECT_EQ(venue2.image_path_opt, venues[1]->image_path_opt);
		EXPECT_EQ(venue2.coordinates_opt, venues[1]->coordinates_opt);
		

		EXPECT_NE(0, venues[2]->id);
		EXPECT_EQ(venue3.sm_id, venues[2]->sm_id);
		EXPECT_EQ(venue3.name, venues[2]->name);
		EXPECT_EQ(venue3.surface_opt, venues[2]->surface_opt);
		EXPECT_EQ(venue3.address_opt, venues[2]->address_opt);
		EXPECT_EQ(venue3.city, venues[2]->city);
		EXPECT_EQ(venue3.capacity_opt, venues[2]->capacity_opt);
		EXPECT_EQ(venue3.image_path_opt, venues[2]->image_path_opt);
		EXPECT_EQ(venue3.coordinates_opt, venues[2]->coordinates_opt);
		
	}
}