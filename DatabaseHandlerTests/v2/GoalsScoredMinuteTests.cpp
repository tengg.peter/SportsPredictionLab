#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/GoalsScoredMinute.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, GoalsScoredMinuteTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	GoalsScoredMinute goalsScoredMinute1;
	goalsScoredMinute1.sm_season_statistics_id = 1;
	goalsScoredMinute1.interval = STR("interval2");
	goalsScoredMinute1.percentage = 3.4;
	
	GoalsScoredMinute goalsScoredMinute2;
	
	GoalsScoredMinute goalsScoredMinute3;
	goalsScoredMinute3.sm_season_statistics_id = 5;
	goalsScoredMinute3.interval = STR("interval6");
	goalsScoredMinute3.percentage = 7.8;
	
	std::vector<UpGoalsScoredMinute> oneGoalsScoredMinute;
	oneGoalsScoredMinute.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute1));

	std::vector<UpGoalsScoredMinute> twogoalsScoredMinutes;
	twogoalsScoredMinutes.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute1));
	twogoalsScoredMinutes.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute2));

	std::vector<UpGoalsScoredMinute> threegoalsScoredMinutes;
	threegoalsScoredMinutes.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute1));
	threegoalsScoredMinutes.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute2));
	threegoalsScoredMinutes.emplace_back(std::make_unique<GoalsScoredMinute>(goalsScoredMinute3));

	EXPECT_EQ(1, oneGoalsScoredMinute.size());
	EXPECT_EQ(2, twogoalsScoredMinutes.size());
	EXPECT_EQ(3, threegoalsScoredMinutes.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_GOALS_SCORED_MINUTES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneGoalsScoredMinute));
	ASSERT_EQ(1, QueryCount<GoalsScoredMinute>(db));
	{
		auto goalsScoredMinutes = QueryAll<GoalsScoredMinute>(db);
		EXPECT_EQ(1, goalsScoredMinutes.size());

		EXPECT_NE(0, goalsScoredMinutes[0]->id);
		EXPECT_EQ(goalsScoredMinute1.sm_season_statistics_id, goalsScoredMinutes[0]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute1.interval, goalsScoredMinutes[0]->interval);
		EXPECT_EQ(goalsScoredMinute1.percentage, goalsScoredMinutes[0]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twogoalsScoredMinutes));
	ASSERT_EQ(2, QueryCount<GoalsScoredMinute>(db));
	{
		auto goalsScoredMinutes = QueryAll<GoalsScoredMinute>(db);
		EXPECT_EQ(2, goalsScoredMinutes.size());

		EXPECT_NE(0, goalsScoredMinutes[0]->id);
		EXPECT_EQ(goalsScoredMinute1.sm_season_statistics_id, goalsScoredMinutes[0]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute1.interval, goalsScoredMinutes[0]->interval);
		EXPECT_EQ(goalsScoredMinute1.percentage, goalsScoredMinutes[0]->percentage);
		

		EXPECT_NE(0, goalsScoredMinutes[1]->id);
		EXPECT_EQ(goalsScoredMinute2.sm_season_statistics_id, goalsScoredMinutes[1]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute2.interval, goalsScoredMinutes[1]->interval);
		EXPECT_EQ(goalsScoredMinute2.percentage, goalsScoredMinutes[1]->percentage);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threegoalsScoredMinutes));
	ASSERT_EQ(3, QueryCount<GoalsScoredMinute>(db));
	{
		auto goalsScoredMinutes = QueryAll<GoalsScoredMinute>(db);
		EXPECT_EQ(3, goalsScoredMinutes.size());

		EXPECT_NE(0, goalsScoredMinutes[0]->id);
		EXPECT_EQ(goalsScoredMinute1.sm_season_statistics_id, goalsScoredMinutes[0]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute1.interval, goalsScoredMinutes[0]->interval);
		EXPECT_EQ(goalsScoredMinute1.percentage, goalsScoredMinutes[0]->percentage);
		

		EXPECT_NE(0, goalsScoredMinutes[1]->id);
		EXPECT_EQ(goalsScoredMinute2.sm_season_statistics_id, goalsScoredMinutes[1]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute2.interval, goalsScoredMinutes[1]->interval);
		EXPECT_EQ(goalsScoredMinute2.percentage, goalsScoredMinutes[1]->percentage);
		

		EXPECT_NE(0, goalsScoredMinutes[2]->id);
		EXPECT_EQ(goalsScoredMinute3.sm_season_statistics_id, goalsScoredMinutes[2]->sm_season_statistics_id);
		EXPECT_EQ(goalsScoredMinute3.interval, goalsScoredMinutes[2]->interval);
		EXPECT_EQ(goalsScoredMinute3.percentage, goalsScoredMinutes[2]->percentage);
		
	}
}