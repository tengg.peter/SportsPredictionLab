#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopCardScorer.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TopCardScorerTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TopCardScorer topCardScorer1;
	topCardScorer1.position = 1;
	topCardScorer1.sm_season_id = 2;
	topCardScorer1.sm_player_id = 3;
	topCardScorer1.sm_team_id = 4;
	topCardScorer1.sm_stage_id_opt = 5;
	topCardScorer1.yellowcards = 6;
	topCardScorer1.redcards = 7;
	topCardScorer1.type = STR("type8");
	
	TopCardScorer topCardScorer2;
	
	TopCardScorer topCardScorer3;
	topCardScorer3.position = 9;
	topCardScorer3.sm_season_id = 10;
	topCardScorer3.sm_player_id = 11;
	topCardScorer3.sm_team_id = 12;
	topCardScorer3.sm_stage_id_opt = 13;
	topCardScorer3.yellowcards = 14;
	topCardScorer3.redcards = 15;
	topCardScorer3.type = STR("type16");
	
	std::vector<UpTopCardScorer> oneTopCardScorer;
	oneTopCardScorer.emplace_back(std::make_unique<TopCardScorer>(topCardScorer1));

	std::vector<UpTopCardScorer> twotopCardScorers;
	twotopCardScorers.emplace_back(std::make_unique<TopCardScorer>(topCardScorer1));
	twotopCardScorers.emplace_back(std::make_unique<TopCardScorer>(topCardScorer2));

	std::vector<UpTopCardScorer> threetopCardScorers;
	threetopCardScorers.emplace_back(std::make_unique<TopCardScorer>(topCardScorer1));
	threetopCardScorers.emplace_back(std::make_unique<TopCardScorer>(topCardScorer2));
	threetopCardScorers.emplace_back(std::make_unique<TopCardScorer>(topCardScorer3));

	EXPECT_EQ(1, oneTopCardScorer.size());
	EXPECT_EQ(2, twotopCardScorers.size());
	EXPECT_EQ(3, threetopCardScorers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TOP_CARD_SCORERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTopCardScorer));
	ASSERT_EQ(1, QueryCount<TopCardScorer>(db));
	{
		auto topCardScorers = QueryAll<TopCardScorer>(db);
		EXPECT_EQ(1, topCardScorers.size());

		EXPECT_NE(0, topCardScorers[0]->id);
		EXPECT_EQ(topCardScorer1.position, topCardScorers[0]->position);
		EXPECT_EQ(topCardScorer1.sm_season_id, topCardScorers[0]->sm_season_id);
		EXPECT_EQ(topCardScorer1.sm_player_id, topCardScorers[0]->sm_player_id);
		EXPECT_EQ(topCardScorer1.sm_team_id, topCardScorers[0]->sm_team_id);
		EXPECT_EQ(topCardScorer1.sm_stage_id_opt, topCardScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer1.yellowcards, topCardScorers[0]->yellowcards);
		EXPECT_EQ(topCardScorer1.redcards, topCardScorers[0]->redcards);
		EXPECT_EQ(topCardScorer1.type, topCardScorers[0]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twotopCardScorers));
	ASSERT_EQ(2, QueryCount<TopCardScorer>(db));
	{
		auto topCardScorers = QueryAll<TopCardScorer>(db);
		EXPECT_EQ(2, topCardScorers.size());

		EXPECT_NE(0, topCardScorers[0]->id);
		EXPECT_EQ(topCardScorer1.position, topCardScorers[0]->position);
		EXPECT_EQ(topCardScorer1.sm_season_id, topCardScorers[0]->sm_season_id);
		EXPECT_EQ(topCardScorer1.sm_player_id, topCardScorers[0]->sm_player_id);
		EXPECT_EQ(topCardScorer1.sm_team_id, topCardScorers[0]->sm_team_id);
		EXPECT_EQ(topCardScorer1.sm_stage_id_opt, topCardScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer1.yellowcards, topCardScorers[0]->yellowcards);
		EXPECT_EQ(topCardScorer1.redcards, topCardScorers[0]->redcards);
		EXPECT_EQ(topCardScorer1.type, topCardScorers[0]->type);
		

		EXPECT_NE(0, topCardScorers[1]->id);
		EXPECT_EQ(topCardScorer2.position, topCardScorers[1]->position);
		EXPECT_EQ(topCardScorer2.sm_season_id, topCardScorers[1]->sm_season_id);
		EXPECT_EQ(topCardScorer2.sm_player_id, topCardScorers[1]->sm_player_id);
		EXPECT_EQ(topCardScorer2.sm_team_id, topCardScorers[1]->sm_team_id);
		EXPECT_EQ(topCardScorer2.sm_stage_id_opt, topCardScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer2.yellowcards, topCardScorers[1]->yellowcards);
		EXPECT_EQ(topCardScorer2.redcards, topCardScorers[1]->redcards);
		EXPECT_EQ(topCardScorer2.type, topCardScorers[1]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threetopCardScorers));
	ASSERT_EQ(3, QueryCount<TopCardScorer>(db));
	{
		auto topCardScorers = QueryAll<TopCardScorer>(db);
		EXPECT_EQ(3, topCardScorers.size());

		EXPECT_NE(0, topCardScorers[0]->id);
		EXPECT_EQ(topCardScorer1.position, topCardScorers[0]->position);
		EXPECT_EQ(topCardScorer1.sm_season_id, topCardScorers[0]->sm_season_id);
		EXPECT_EQ(topCardScorer1.sm_player_id, topCardScorers[0]->sm_player_id);
		EXPECT_EQ(topCardScorer1.sm_team_id, topCardScorers[0]->sm_team_id);
		EXPECT_EQ(topCardScorer1.sm_stage_id_opt, topCardScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer1.yellowcards, topCardScorers[0]->yellowcards);
		EXPECT_EQ(topCardScorer1.redcards, topCardScorers[0]->redcards);
		EXPECT_EQ(topCardScorer1.type, topCardScorers[0]->type);
		

		EXPECT_NE(0, topCardScorers[1]->id);
		EXPECT_EQ(topCardScorer2.position, topCardScorers[1]->position);
		EXPECT_EQ(topCardScorer2.sm_season_id, topCardScorers[1]->sm_season_id);
		EXPECT_EQ(topCardScorer2.sm_player_id, topCardScorers[1]->sm_player_id);
		EXPECT_EQ(topCardScorer2.sm_team_id, topCardScorers[1]->sm_team_id);
		EXPECT_EQ(topCardScorer2.sm_stage_id_opt, topCardScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer2.yellowcards, topCardScorers[1]->yellowcards);
		EXPECT_EQ(topCardScorer2.redcards, topCardScorers[1]->redcards);
		EXPECT_EQ(topCardScorer2.type, topCardScorers[1]->type);
		

		EXPECT_NE(0, topCardScorers[2]->id);
		EXPECT_EQ(topCardScorer3.position, topCardScorers[2]->position);
		EXPECT_EQ(topCardScorer3.sm_season_id, topCardScorers[2]->sm_season_id);
		EXPECT_EQ(topCardScorer3.sm_player_id, topCardScorers[2]->sm_player_id);
		EXPECT_EQ(topCardScorer3.sm_team_id, topCardScorers[2]->sm_team_id);
		EXPECT_EQ(topCardScorer3.sm_stage_id_opt, topCardScorers[2]->sm_stage_id_opt);
		EXPECT_EQ(topCardScorer3.yellowcards, topCardScorers[2]->yellowcards);
		EXPECT_EQ(topCardScorer3.redcards, topCardScorers[2]->redcards);
		EXPECT_EQ(topCardScorer3.type, topCardScorers[2]->type);
		
	}
}