#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopGoalScorer.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, TopGoalScorerTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	TopGoalScorer topGoalScorer1;
	topGoalScorer1.position = 1;
	topGoalScorer1.sm_season_id = 2;
	topGoalScorer1.sm_player_id = 3;
	topGoalScorer1.sm_team_id = 4;
	topGoalScorer1.sm_stage_id_opt = 5;
	topGoalScorer1.goals = 6;
	topGoalScorer1.penalty_goals = 7;
	topGoalScorer1.type = STR("type8");
	
	TopGoalScorer topGoalScorer2;
	
	TopGoalScorer topGoalScorer3;
	topGoalScorer3.position = 9;
	topGoalScorer3.sm_season_id = 10;
	topGoalScorer3.sm_player_id = 11;
	topGoalScorer3.sm_team_id = 12;
	topGoalScorer3.sm_stage_id_opt = 13;
	topGoalScorer3.goals = 14;
	topGoalScorer3.penalty_goals = 15;
	topGoalScorer3.type = STR("type16");
	
	std::vector<UpTopGoalScorer> oneTopGoalScorer;
	oneTopGoalScorer.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer1));

	std::vector<UpTopGoalScorer> twotopGoalScorers;
	twotopGoalScorers.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer1));
	twotopGoalScorers.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer2));

	std::vector<UpTopGoalScorer> threetopGoalScorers;
	threetopGoalScorers.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer1));
	threetopGoalScorers.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer2));
	threetopGoalScorers.emplace_back(std::make_unique<TopGoalScorer>(topGoalScorer3));

	EXPECT_EQ(1, oneTopGoalScorer.size());
	EXPECT_EQ(2, twotopGoalScorers.size());
	EXPECT_EQ(3, threetopGoalScorers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_TOP_GOAL_SCORERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneTopGoalScorer));
	ASSERT_EQ(1, QueryCount<TopGoalScorer>(db));
	{
		auto topGoalScorers = QueryAll<TopGoalScorer>(db);
		EXPECT_EQ(1, topGoalScorers.size());

		EXPECT_NE(0, topGoalScorers[0]->id);
		EXPECT_EQ(topGoalScorer1.position, topGoalScorers[0]->position);
		EXPECT_EQ(topGoalScorer1.sm_season_id, topGoalScorers[0]->sm_season_id);
		EXPECT_EQ(topGoalScorer1.sm_player_id, topGoalScorers[0]->sm_player_id);
		EXPECT_EQ(topGoalScorer1.sm_team_id, topGoalScorers[0]->sm_team_id);
		EXPECT_EQ(topGoalScorer1.sm_stage_id_opt, topGoalScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer1.goals, topGoalScorers[0]->goals);
		EXPECT_EQ(topGoalScorer1.penalty_goals, topGoalScorers[0]->penalty_goals);
		EXPECT_EQ(topGoalScorer1.type, topGoalScorers[0]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twotopGoalScorers));
	ASSERT_EQ(2, QueryCount<TopGoalScorer>(db));
	{
		auto topGoalScorers = QueryAll<TopGoalScorer>(db);
		EXPECT_EQ(2, topGoalScorers.size());

		EXPECT_NE(0, topGoalScorers[0]->id);
		EXPECT_EQ(topGoalScorer1.position, topGoalScorers[0]->position);
		EXPECT_EQ(topGoalScorer1.sm_season_id, topGoalScorers[0]->sm_season_id);
		EXPECT_EQ(topGoalScorer1.sm_player_id, topGoalScorers[0]->sm_player_id);
		EXPECT_EQ(topGoalScorer1.sm_team_id, topGoalScorers[0]->sm_team_id);
		EXPECT_EQ(topGoalScorer1.sm_stage_id_opt, topGoalScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer1.goals, topGoalScorers[0]->goals);
		EXPECT_EQ(topGoalScorer1.penalty_goals, topGoalScorers[0]->penalty_goals);
		EXPECT_EQ(topGoalScorer1.type, topGoalScorers[0]->type);
		

		EXPECT_NE(0, topGoalScorers[1]->id);
		EXPECT_EQ(topGoalScorer2.position, topGoalScorers[1]->position);
		EXPECT_EQ(topGoalScorer2.sm_season_id, topGoalScorers[1]->sm_season_id);
		EXPECT_EQ(topGoalScorer2.sm_player_id, topGoalScorers[1]->sm_player_id);
		EXPECT_EQ(topGoalScorer2.sm_team_id, topGoalScorers[1]->sm_team_id);
		EXPECT_EQ(topGoalScorer2.sm_stage_id_opt, topGoalScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer2.goals, topGoalScorers[1]->goals);
		EXPECT_EQ(topGoalScorer2.penalty_goals, topGoalScorers[1]->penalty_goals);
		EXPECT_EQ(topGoalScorer2.type, topGoalScorers[1]->type);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threetopGoalScorers));
	ASSERT_EQ(3, QueryCount<TopGoalScorer>(db));
	{
		auto topGoalScorers = QueryAll<TopGoalScorer>(db);
		EXPECT_EQ(3, topGoalScorers.size());

		EXPECT_NE(0, topGoalScorers[0]->id);
		EXPECT_EQ(topGoalScorer1.position, topGoalScorers[0]->position);
		EXPECT_EQ(topGoalScorer1.sm_season_id, topGoalScorers[0]->sm_season_id);
		EXPECT_EQ(topGoalScorer1.sm_player_id, topGoalScorers[0]->sm_player_id);
		EXPECT_EQ(topGoalScorer1.sm_team_id, topGoalScorers[0]->sm_team_id);
		EXPECT_EQ(topGoalScorer1.sm_stage_id_opt, topGoalScorers[0]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer1.goals, topGoalScorers[0]->goals);
		EXPECT_EQ(topGoalScorer1.penalty_goals, topGoalScorers[0]->penalty_goals);
		EXPECT_EQ(topGoalScorer1.type, topGoalScorers[0]->type);
		

		EXPECT_NE(0, topGoalScorers[1]->id);
		EXPECT_EQ(topGoalScorer2.position, topGoalScorers[1]->position);
		EXPECT_EQ(topGoalScorer2.sm_season_id, topGoalScorers[1]->sm_season_id);
		EXPECT_EQ(topGoalScorer2.sm_player_id, topGoalScorers[1]->sm_player_id);
		EXPECT_EQ(topGoalScorer2.sm_team_id, topGoalScorers[1]->sm_team_id);
		EXPECT_EQ(topGoalScorer2.sm_stage_id_opt, topGoalScorers[1]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer2.goals, topGoalScorers[1]->goals);
		EXPECT_EQ(topGoalScorer2.penalty_goals, topGoalScorers[1]->penalty_goals);
		EXPECT_EQ(topGoalScorer2.type, topGoalScorers[1]->type);
		

		EXPECT_NE(0, topGoalScorers[2]->id);
		EXPECT_EQ(topGoalScorer3.position, topGoalScorers[2]->position);
		EXPECT_EQ(topGoalScorer3.sm_season_id, topGoalScorers[2]->sm_season_id);
		EXPECT_EQ(topGoalScorer3.sm_player_id, topGoalScorers[2]->sm_player_id);
		EXPECT_EQ(topGoalScorer3.sm_team_id, topGoalScorers[2]->sm_team_id);
		EXPECT_EQ(topGoalScorer3.sm_stage_id_opt, topGoalScorers[2]->sm_stage_id_opt);
		EXPECT_EQ(topGoalScorer3.goals, topGoalScorers[2]->goals);
		EXPECT_EQ(topGoalScorer3.penalty_goals, topGoalScorers[2]->penalty_goals);
		EXPECT_EQ(topGoalScorer3.type, topGoalScorers[2]->type);
		
	}
}