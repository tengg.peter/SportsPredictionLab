#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/FixtureSideline.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, FixtureSidelineTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	FixtureSideline fixtureSideline1;
	fixtureSideline1.sm_team_id = 1;
	fixtureSideline1.sm_fixture_id = 2;
	fixtureSideline1.sm_player_id = 3;
	fixtureSideline1.player_name = STR("player_name4");
	fixtureSideline1.reason = STR("reason5");
	
	FixtureSideline fixtureSideline2;
	
	FixtureSideline fixtureSideline3;
	fixtureSideline3.sm_team_id = 6;
	fixtureSideline3.sm_fixture_id = 7;
	fixtureSideline3.sm_player_id = 8;
	fixtureSideline3.player_name = STR("player_name9");
	fixtureSideline3.reason = STR("reason10");
	
	std::vector<UpFixtureSideline> oneFixtureSideline;
	oneFixtureSideline.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline1));

	std::vector<UpFixtureSideline> twofixtureSidelines;
	twofixtureSidelines.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline1));
	twofixtureSidelines.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline2));

	std::vector<UpFixtureSideline> threefixtureSidelines;
	threefixtureSidelines.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline1));
	threefixtureSidelines.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline2));
	threefixtureSidelines.emplace_back(std::make_unique<FixtureSideline>(fixtureSideline3));

	EXPECT_EQ(1, oneFixtureSideline.size());
	EXPECT_EQ(2, twofixtureSidelines.size());
	EXPECT_EQ(3, threefixtureSidelines.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_FIXTURE_SIDELINES));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, oneFixtureSideline));
	ASSERT_EQ(1, QueryCount<FixtureSideline>(db));
	{
		auto fixtureSidelines = QueryAll<FixtureSideline>(db);
		EXPECT_EQ(1, fixtureSidelines.size());

		EXPECT_NE(0, fixtureSidelines[0]->id);
		EXPECT_EQ(fixtureSideline1.sm_team_id, fixtureSidelines[0]->sm_team_id);
		EXPECT_EQ(fixtureSideline1.sm_fixture_id, fixtureSidelines[0]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline1.sm_player_id, fixtureSidelines[0]->sm_player_id);
		EXPECT_EQ(fixtureSideline1.player_name, fixtureSidelines[0]->player_name);
		EXPECT_EQ(fixtureSideline1.reason, fixtureSidelines[0]->reason);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twofixtureSidelines));
	ASSERT_EQ(2, QueryCount<FixtureSideline>(db));
	{
		auto fixtureSidelines = QueryAll<FixtureSideline>(db);
		EXPECT_EQ(2, fixtureSidelines.size());

		EXPECT_NE(0, fixtureSidelines[0]->id);
		EXPECT_EQ(fixtureSideline1.sm_team_id, fixtureSidelines[0]->sm_team_id);
		EXPECT_EQ(fixtureSideline1.sm_fixture_id, fixtureSidelines[0]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline1.sm_player_id, fixtureSidelines[0]->sm_player_id);
		EXPECT_EQ(fixtureSideline1.player_name, fixtureSidelines[0]->player_name);
		EXPECT_EQ(fixtureSideline1.reason, fixtureSidelines[0]->reason);
		

		EXPECT_NE(0, fixtureSidelines[1]->id);
		EXPECT_EQ(fixtureSideline2.sm_team_id, fixtureSidelines[1]->sm_team_id);
		EXPECT_EQ(fixtureSideline2.sm_fixture_id, fixtureSidelines[1]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline2.sm_player_id, fixtureSidelines[1]->sm_player_id);
		EXPECT_EQ(fixtureSideline2.player_name, fixtureSidelines[1]->player_name);
		EXPECT_EQ(fixtureSideline2.reason, fixtureSidelines[1]->reason);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threefixtureSidelines));
	ASSERT_EQ(3, QueryCount<FixtureSideline>(db));
	{
		auto fixtureSidelines = QueryAll<FixtureSideline>(db);
		EXPECT_EQ(3, fixtureSidelines.size());

		EXPECT_NE(0, fixtureSidelines[0]->id);
		EXPECT_EQ(fixtureSideline1.sm_team_id, fixtureSidelines[0]->sm_team_id);
		EXPECT_EQ(fixtureSideline1.sm_fixture_id, fixtureSidelines[0]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline1.sm_player_id, fixtureSidelines[0]->sm_player_id);
		EXPECT_EQ(fixtureSideline1.player_name, fixtureSidelines[0]->player_name);
		EXPECT_EQ(fixtureSideline1.reason, fixtureSidelines[0]->reason);
		

		EXPECT_NE(0, fixtureSidelines[1]->id);
		EXPECT_EQ(fixtureSideline2.sm_team_id, fixtureSidelines[1]->sm_team_id);
		EXPECT_EQ(fixtureSideline2.sm_fixture_id, fixtureSidelines[1]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline2.sm_player_id, fixtureSidelines[1]->sm_player_id);
		EXPECT_EQ(fixtureSideline2.player_name, fixtureSidelines[1]->player_name);
		EXPECT_EQ(fixtureSideline2.reason, fixtureSidelines[1]->reason);
		

		EXPECT_NE(0, fixtureSidelines[2]->id);
		EXPECT_EQ(fixtureSideline3.sm_team_id, fixtureSidelines[2]->sm_team_id);
		EXPECT_EQ(fixtureSideline3.sm_fixture_id, fixtureSidelines[2]->sm_fixture_id);
		EXPECT_EQ(fixtureSideline3.sm_player_id, fixtureSidelines[2]->sm_player_id);
		EXPECT_EQ(fixtureSideline3.player_name, fixtureSidelines[2]->player_name);
		EXPECT_EQ(fixtureSideline3.reason, fixtureSidelines[2]->reason);
		
	}
}