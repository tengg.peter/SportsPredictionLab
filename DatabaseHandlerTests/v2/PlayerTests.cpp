#include "DatabaseHandler/DatabaseHandler.h"
#include "DatabaseHandler/Source/v2/DataClasses/Player.h"
#include "DatabaseHandlerTests/InMemoryDbFixture.h"

TEST_F(InMemoryDbFixture, PlayerTests)
{
	using namespace database_handler;
	using namespace database_handler::v2;
	using namespace database_handler::common;
	
	Player player1;
	player1.sm_id = 1;
	player1.sm_team_id = 2;
	player1.sm_country_id_opt = 3;
	player1.sm_position_id = 4;
	player1.common_name = STR("common_name5");
	player1.display_name_opt = STR("display_name_opt6");
	player1.fullname_opt = STR("fullname_opt7");
	player1.firstname_opt = STR("firstname_opt8");
	player1.lastname_opt = STR("lastname_opt9");
	player1.nationality = STR("nationality10");
	player1.birthdate = STR("birthdate11");
	player1.birthcountry_opt = STR("birthcountry_opt12");
	player1.birthplace_opt = STR("birthplace_opt13");
	player1.height_opt = STR("height_opt14");
	player1.weight_opt = STR("weight_opt15");
	player1.image_path_opt = STR("image_path_opt16");
	
	Player player2;
	
	Player player3;
	player3.sm_id = 17;
	player3.sm_team_id = 18;
	player3.sm_country_id_opt = 19;
	player3.sm_position_id = 20;
	player3.common_name = STR("common_name21");
	player3.display_name_opt = STR("display_name_opt22");
	player3.fullname_opt = STR("fullname_opt23");
	player3.firstname_opt = STR("firstname_opt24");
	player3.lastname_opt = STR("lastname_opt25");
	player3.nationality = STR("nationality26");
	player3.birthdate = STR("birthdate27");
	player3.birthcountry_opt = STR("birthcountry_opt28");
	player3.birthplace_opt = STR("birthplace_opt29");
	player3.height_opt = STR("height_opt30");
	player3.weight_opt = STR("weight_opt31");
	player3.image_path_opt = STR("image_path_opt32");
	
	std::vector<UpPlayer> onePlayer;
	onePlayer.emplace_back(std::make_unique<Player>(player1));

	std::vector<UpPlayer> twoplayers;
	twoplayers.emplace_back(std::make_unique<Player>(player1));
	twoplayers.emplace_back(std::make_unique<Player>(player2));

	std::vector<UpPlayer> threeplayers;
	threeplayers.emplace_back(std::make_unique<Player>(player1));
	threeplayers.emplace_back(std::make_unique<Player>(player2));
	threeplayers.emplace_back(std::make_unique<Player>(player3));

	EXPECT_EQ(1, onePlayer.size());
	EXPECT_EQ(2, twoplayers.size());
	EXPECT_EQ(3, threeplayers.size());

	ASSERT_NO_THROW(db.exec(CREATE_TABLE_PLAYERS));
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, onePlayer));
	ASSERT_EQ(1, QueryCount<Player>(db));
	{
		auto players = QueryAll<Player>(db);
		EXPECT_EQ(1, players.size());

		EXPECT_NE(0, players[0]->id);
		EXPECT_EQ(player1.sm_id, players[0]->sm_id);
		EXPECT_EQ(player1.sm_team_id, players[0]->sm_team_id);
		EXPECT_EQ(player1.sm_country_id_opt, players[0]->sm_country_id_opt);
		EXPECT_EQ(player1.sm_position_id, players[0]->sm_position_id);
		EXPECT_EQ(player1.common_name, players[0]->common_name);
		EXPECT_EQ(player1.display_name_opt, players[0]->display_name_opt);
		EXPECT_EQ(player1.fullname_opt, players[0]->fullname_opt);
		EXPECT_EQ(player1.firstname_opt, players[0]->firstname_opt);
		EXPECT_EQ(player1.lastname_opt, players[0]->lastname_opt);
		EXPECT_EQ(player1.nationality, players[0]->nationality);
		EXPECT_EQ(player1.birthdate, players[0]->birthdate);
		EXPECT_EQ(player1.birthcountry_opt, players[0]->birthcountry_opt);
		EXPECT_EQ(player1.birthplace_opt, players[0]->birthplace_opt);
		EXPECT_EQ(player1.height_opt, players[0]->height_opt);
		EXPECT_EQ(player1.weight_opt, players[0]->weight_opt);
		EXPECT_EQ(player1.image_path_opt, players[0]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, twoplayers));
	ASSERT_EQ(2, QueryCount<Player>(db));
	{
		auto players = QueryAll<Player>(db);
		EXPECT_EQ(2, players.size());

		EXPECT_NE(0, players[0]->id);
		EXPECT_EQ(player1.sm_id, players[0]->sm_id);
		EXPECT_EQ(player1.sm_team_id, players[0]->sm_team_id);
		EXPECT_EQ(player1.sm_country_id_opt, players[0]->sm_country_id_opt);
		EXPECT_EQ(player1.sm_position_id, players[0]->sm_position_id);
		EXPECT_EQ(player1.common_name, players[0]->common_name);
		EXPECT_EQ(player1.display_name_opt, players[0]->display_name_opt);
		EXPECT_EQ(player1.fullname_opt, players[0]->fullname_opt);
		EXPECT_EQ(player1.firstname_opt, players[0]->firstname_opt);
		EXPECT_EQ(player1.lastname_opt, players[0]->lastname_opt);
		EXPECT_EQ(player1.nationality, players[0]->nationality);
		EXPECT_EQ(player1.birthdate, players[0]->birthdate);
		EXPECT_EQ(player1.birthcountry_opt, players[0]->birthcountry_opt);
		EXPECT_EQ(player1.birthplace_opt, players[0]->birthplace_opt);
		EXPECT_EQ(player1.height_opt, players[0]->height_opt);
		EXPECT_EQ(player1.weight_opt, players[0]->weight_opt);
		EXPECT_EQ(player1.image_path_opt, players[0]->image_path_opt);
		

		EXPECT_NE(0, players[1]->id);
		EXPECT_EQ(player2.sm_id, players[1]->sm_id);
		EXPECT_EQ(player2.sm_team_id, players[1]->sm_team_id);
		EXPECT_EQ(player2.sm_country_id_opt, players[1]->sm_country_id_opt);
		EXPECT_EQ(player2.sm_position_id, players[1]->sm_position_id);
		EXPECT_EQ(player2.common_name, players[1]->common_name);
		EXPECT_EQ(player2.display_name_opt, players[1]->display_name_opt);
		EXPECT_EQ(player2.fullname_opt, players[1]->fullname_opt);
		EXPECT_EQ(player2.firstname_opt, players[1]->firstname_opt);
		EXPECT_EQ(player2.lastname_opt, players[1]->lastname_opt);
		EXPECT_EQ(player2.nationality, players[1]->nationality);
		EXPECT_EQ(player2.birthdate, players[1]->birthdate);
		EXPECT_EQ(player2.birthcountry_opt, players[1]->birthcountry_opt);
		EXPECT_EQ(player2.birthplace_opt, players[1]->birthplace_opt);
		EXPECT_EQ(player2.height_opt, players[1]->height_opt);
		EXPECT_EQ(player2.weight_opt, players[1]->weight_opt);
		EXPECT_EQ(player2.image_path_opt, players[1]->image_path_opt);
		
	}
	ASSERT_NO_THROW(InsertOr(OnConflict::Ignore, db, threeplayers));
	ASSERT_EQ(3, QueryCount<Player>(db));
	{
		auto players = QueryAll<Player>(db);
		EXPECT_EQ(3, players.size());

		EXPECT_NE(0, players[0]->id);
		EXPECT_EQ(player1.sm_id, players[0]->sm_id);
		EXPECT_EQ(player1.sm_team_id, players[0]->sm_team_id);
		EXPECT_EQ(player1.sm_country_id_opt, players[0]->sm_country_id_opt);
		EXPECT_EQ(player1.sm_position_id, players[0]->sm_position_id);
		EXPECT_EQ(player1.common_name, players[0]->common_name);
		EXPECT_EQ(player1.display_name_opt, players[0]->display_name_opt);
		EXPECT_EQ(player1.fullname_opt, players[0]->fullname_opt);
		EXPECT_EQ(player1.firstname_opt, players[0]->firstname_opt);
		EXPECT_EQ(player1.lastname_opt, players[0]->lastname_opt);
		EXPECT_EQ(player1.nationality, players[0]->nationality);
		EXPECT_EQ(player1.birthdate, players[0]->birthdate);
		EXPECT_EQ(player1.birthcountry_opt, players[0]->birthcountry_opt);
		EXPECT_EQ(player1.birthplace_opt, players[0]->birthplace_opt);
		EXPECT_EQ(player1.height_opt, players[0]->height_opt);
		EXPECT_EQ(player1.weight_opt, players[0]->weight_opt);
		EXPECT_EQ(player1.image_path_opt, players[0]->image_path_opt);
		

		EXPECT_NE(0, players[1]->id);
		EXPECT_EQ(player2.sm_id, players[1]->sm_id);
		EXPECT_EQ(player2.sm_team_id, players[1]->sm_team_id);
		EXPECT_EQ(player2.sm_country_id_opt, players[1]->sm_country_id_opt);
		EXPECT_EQ(player2.sm_position_id, players[1]->sm_position_id);
		EXPECT_EQ(player2.common_name, players[1]->common_name);
		EXPECT_EQ(player2.display_name_opt, players[1]->display_name_opt);
		EXPECT_EQ(player2.fullname_opt, players[1]->fullname_opt);
		EXPECT_EQ(player2.firstname_opt, players[1]->firstname_opt);
		EXPECT_EQ(player2.lastname_opt, players[1]->lastname_opt);
		EXPECT_EQ(player2.nationality, players[1]->nationality);
		EXPECT_EQ(player2.birthdate, players[1]->birthdate);
		EXPECT_EQ(player2.birthcountry_opt, players[1]->birthcountry_opt);
		EXPECT_EQ(player2.birthplace_opt, players[1]->birthplace_opt);
		EXPECT_EQ(player2.height_opt, players[1]->height_opt);
		EXPECT_EQ(player2.weight_opt, players[1]->weight_opt);
		EXPECT_EQ(player2.image_path_opt, players[1]->image_path_opt);
		

		EXPECT_NE(0, players[2]->id);
		EXPECT_EQ(player3.sm_id, players[2]->sm_id);
		EXPECT_EQ(player3.sm_team_id, players[2]->sm_team_id);
		EXPECT_EQ(player3.sm_country_id_opt, players[2]->sm_country_id_opt);
		EXPECT_EQ(player3.sm_position_id, players[2]->sm_position_id);
		EXPECT_EQ(player3.common_name, players[2]->common_name);
		EXPECT_EQ(player3.display_name_opt, players[2]->display_name_opt);
		EXPECT_EQ(player3.fullname_opt, players[2]->fullname_opt);
		EXPECT_EQ(player3.firstname_opt, players[2]->firstname_opt);
		EXPECT_EQ(player3.lastname_opt, players[2]->lastname_opt);
		EXPECT_EQ(player3.nationality, players[2]->nationality);
		EXPECT_EQ(player3.birthdate, players[2]->birthdate);
		EXPECT_EQ(player3.birthcountry_opt, players[2]->birthcountry_opt);
		EXPECT_EQ(player3.birthplace_opt, players[2]->birthplace_opt);
		EXPECT_EQ(player3.height_opt, players[2]->height_opt);
		EXPECT_EQ(player3.weight_opt, players[2]->weight_opt);
		EXPECT_EQ(player3.image_path_opt, players[2]->image_path_opt);
		
	}
}