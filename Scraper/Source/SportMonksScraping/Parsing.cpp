#include "DatabaseHandler/DatabaseHandler.h"
#include "Scraper/Source/Constants.h"
#include "Scraper/Source/SportMonksScraping/JsonKeys.h"
#include "Scraper/Source/SportMonksScraping/Parsing.h"

#include "Utils/Utils.h"
//std

namespace scraper::sport_monks::parsing
{
	using namespace database_handler::v2;
	using namespace json_keys;

	namespace
	{
		std::optional<bool> AsBoolOpt(const Json::Value& node)
		{
			try
			{
				if (node.isNull())
				{
					return std::nullopt;
				}
				if (node.isString())
				{
					return static_cast<bool>(std::stoi(node.asString()));
				}
				return node.asBool();
			}
			catch (const std::invalid_argument&)
			{
				return std::nullopt;
			}
		}

		bool AsBool(const Json::Value& node)
		{
			return AsBoolOpt(node).value_or(false);
		}


		std::optional<int> AsIntOpt(const Json::Value& node)
		{
			try
			{
				if (node.isNull())
				{
					return std::nullopt;
				}
				if (node.isString())
				{
					return std::stoi(node.asString());
				}
				return node.asInt();
			}
			catch (const std::invalid_argument&)
			{
				return std::nullopt;
			}
		}

		int AsInt(const Json::Value& node)
		{
			return AsIntOpt(node).value_or(0);
		}

		std::optional<int64_t> AsInt64Opt(const Json::Value& node)
		{
			try
			{
				if (node.isNull())
				{
					return std::nullopt;
				}
				if (node.isString())
				{
					return std::stoll(node.asString());
				}
				return node.asInt64();
			}
			catch (const std::invalid_argument&)
			{
				return std::nullopt;
			}
		}

		int64_t AsInt64(const Json::Value& node)
		{
			return AsInt64Opt(node).value_or(0ll);
		}

		std::optional<double> AsDoubleOpt(const Json::Value& node)
		{
			try
			{
				if (node.isNull())
				{
					return std::nullopt;
				}
				if (node.isString())
				{
					return std::stod(node.asString());
				}
				return node.asDouble();
			}
			catch (const std::invalid_argument&)
			{
				return std::nullopt;
			}
		}

		double AsDouble(const Json::Value& node)
		{
			return AsDoubleOpt(node).value_or(0.0);
		}
	}

	void ParseCountry(
		const Json::Value& countryNode,
		database_handler::v2::Country& country,
		const std::vector<database_handler::v2::UpContinent>& continents)
	{
		country.sm_id = countryNode[ID].asInt64();
		country.name = StdStringToUtilsString(countryNode[NAME].asString());
		country.image_path_opt = StdStringToUtilsString(countryNode[IMAGE_PATH].asString());
		//setting the correct values for sm_continent_id foreign key
		utils::String searchString;
		if (STR("Mexico") == country.name)
		{
			//Mexico is a special case. Sub ragion says Central America, which is not a valid continent. In their db Mexico belongs to North America
			searchString = STR("North America");
		}
		else if (STR("Wales") == country.name)
		{
			//Wales has no Extra section
			searchString = STR("Europe");
		}
		else if (countryNode[EXTRA].isMember(CONTINENT))
		{
			const std::string continent = countryNode[EXTRA][CONTINENT].asString();
			const std::string subRegion = countryNode[EXTRA][SUB_REGION].asString();
			searchString = StdStringToUtilsString("Americas" == continent ? subRegion : continent);
		}
		else
		{
			//there are "countries", like "Europe" which don't have the extra data. They will be connected to the continent with the same name
			searchString = country.name;
		}
		auto it = std::find_if(continents.cbegin(), continents.cend(), [&searchString](const auto& con) {return con->name == searchString; });
		if (continents.cend() != it)
		{
			country.sm_continent_id_opt = (*it)->id;
		}
	}

	void ParseLeague(const Json::Value& leagueNode, database_handler::v2::League& league)
	{
		league.sm_id = leagueNode[ID].asInt64();
		league.active = leagueNode[ACTIVE].asBool();
		if (!leagueNode[TYPE].isNull())
		{
			league.type_opt = StdStringToUtilsString(leagueNode[TYPE].asString());
		}
		if (!leagueNode[LEGACY_ID].isNull())
		{
			league.sm_legacy_id_opt = leagueNode[LEGACY_ID].asInt64();
		}
		league.sm_country_id = leagueNode[COUNTRY_ID].asInt64();
		if (!leagueNode[LOGO_PATH].isNull())
		{
			league.logo_path_opt = StdStringToUtilsString(leagueNode[LOGO_PATH].asString());
		}
		league.name = StdStringToUtilsString(leagueNode[NAME].asString());
		league.is_cup = leagueNode[IS_CUP].asBool();
		league.sm_current_season_id = leagueNode[CURRENT_SEASON_ID].asInt64();
		if (!leagueNode[CURRENT_ROUND_ID].isNull())
		{
			league.sm_current_round_id_opt = leagueNode[CURRENT_ROUND_ID].asInt64();
		}
		if (!leagueNode[CURRENT_STAGE_ID].isNull())
		{
			league.sm_current_stage_id_opt = leagueNode[CURRENT_STAGE_ID].asInt64();
		}
		league.live_standings = leagueNode[LIVE_STANDINGS].asBool();
		league.coverage_predictions = leagueNode[COVERAGE][PREDICTIONS].asBool();
		league.coverage_topscorer_goals = leagueNode[COVERAGE][TOPSCORER_GOALS].asBool();
		league.coverage_topscorer_assists = leagueNode[COVERAGE][TOPSCORER_ASSISTS].asBool();
		league.coverage_topscorer_cards = leagueNode[COVERAGE][TOPSCORER_CARDS].asBool();
	}

	void ParseFixture(const Json::Value& fixtureNode, Fixture& fixture)
	{
		fixture.sm_id = fixtureNode[ID].asInt64();
		fixture.sm_league_id = fixtureNode[LEAGUE_ID].asInt64();
		fixture.sm_season_id = fixtureNode[SEASON_ID].asInt64();
		fixture.sm_stage_id = fixtureNode[STAGE_ID].asInt64();
		fixture.sm_round_id = fixtureNode[ROUND_ID].asInt64();
		if (!fixtureNode[GROUP_ID].isNull())
		{
			fixture.sm_group_id_opt = fixtureNode[GROUP_ID].asInt64();
		}
		if (!fixtureNode[AGGREGATE_ID].isNull())
		{
			fixture.sm_aggregate_id_opt = fixtureNode[AGGREGATE_ID].asInt64();
		}
		if (!fixtureNode[VENUE_ID].isNull())
		{
			fixture.sm_venue_id_opt = fixtureNode[VENUE_ID].asInt64();
		}
		if (!fixtureNode[REFEREE_ID].isNull())
		{
			fixture.sm_referee_id_opt = fixtureNode[REFEREE_ID].asInt64();
		}
		fixture.sm_localteam_id = fixtureNode[LOCALTEAM_ID].asInt64();
		fixture.sm_visitorteam_id = fixtureNode[VISITORTEAM_ID].asInt64();
		if (!fixtureNode[WINNER_TEAM_ID].isNull())
		{
			fixture.sm_winner_team_id_opt = fixtureNode[WINNER_TEAM_ID].asInt64();
		}
		if (!fixtureNode[WEATHER_REPORT].isNull())
		{
			if (!fixtureNode[WEATHER_REPORT][CODE].isNull())
			{
				fixture.weather_code_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][CODE].asString());
			}
			if (!fixtureNode[WEATHER_REPORT][TYPE].isNull())
			{
				fixture.weather_type_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][TYPE].asString());
			}
			if (!fixtureNode[WEATHER_REPORT][ICON].isNull())
			{
				fixture.weather_icon_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][ICON].asString());
			}
			if (!fixtureNode[TEMPERATURE].isNull())
			{
				if (!fixtureNode[TEMPERATURE][TEMP].isNull())
				{
					fixture.weather_temp_fah_opt = fixtureNode[WEATHER_REPORT][TEMPERATURE][TEMP].asDouble();
				}
			}
			if (!fixtureNode[TEMPERATURE_CELCIUS].isNull())
			{
				if (!fixtureNode[TEMPERATURE_CELCIUS][TEMP].isNull())
				{
					fixture.weather_temp_cel_opt = fixtureNode[WEATHER_REPORT][TEMPERATURE_CELCIUS][TEMP].asDouble();
				}
			}
			if (!fixtureNode[WEATHER_REPORT][CLOUDS].isNull())
			{
				fixture.weather_clouds_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][CLOUDS].asString());
			}
			if (!fixtureNode[WEATHER_REPORT][HUMIDITY].isNull())
			{
				fixture.weather_humidity_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][HUMIDITY].asString());
			}
			if (!fixtureNode[WEATHER_REPORT][PRESSURE].isNull())
			{
				fixture.weather_pressure_opt = fixtureNode[WEATHER_REPORT][PRESSURE].asInt64();
			}
			if (!fixtureNode[WEATHER_REPORT][WIND].isNull())
			{
				if (!fixtureNode[WEATHER_REPORT][WIND][SPEED].isNull())
				{
					fixture.weather_wind_speed_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][WIND][SPEED].asString());
				}
				if (!fixtureNode[WEATHER_REPORT][WIND][DEGREE].isNull())
				{
					fixture.weather_wind_degree_opt = fixtureNode[WEATHER_REPORT][WIND][DEGREE].asInt64();
				}
			}
			if (!fixtureNode[WEATHER_REPORT][COORDINATES].isNull())
			{
				if (!fixtureNode[WEATHER_REPORT][COORDINATES][LAT].isNull())
				{
					fixture.weather_coordinates_lat_opt = fixtureNode[WEATHER_REPORT][COORDINATES][LAT].asDouble();
				}
				if (!fixtureNode[WEATHER_REPORT][COORDINATES][LON].isNull())
				{
					fixture.weather_coordinates_lon_opt = fixtureNode[WEATHER_REPORT][COORDINATES][LON].asDouble();
				}
			}
			if (!fixtureNode[WEATHER_REPORT][UPDATED_AT].isNull())
			{
				fixture.weather_updated_at_opt = StdStringToUtilsString(fixtureNode[WEATHER_REPORT][UPDATED_AT].asString());
			}
		}
		fixture.commentaries = fixtureNode[COMMENTARIES].asBool();
		if (!fixtureNode[ATTENDANCE].isNull())
		{
			fixture.attendance_opt = fixtureNode[ATTENDANCE].asInt64();
		}
		if (!fixtureNode[PITCH].isNull())
		{
			fixture.pitch_opt = StdStringToUtilsString(fixtureNode[PITCH].asString());
		}
		if (!fixtureNode[DETAILS].isNull())
		{
			fixture.details_opt = StdStringToUtilsString(fixtureNode[DETAILS].asString());
		}
		fixture.neutral_venue = fixtureNode[NEUTRAL_VENUE].asBool();
		fixture.winning_odds_calculated = fixtureNode[WINNING_ODDS_CALCULATED].asBool();
		if (!fixtureNode[FORMATIONS].isNull())
		{
			if (!fixtureNode[FORMATIONS][LOCALTEAM_FORMATION].isNull())
			{
				fixture.localteam_formation_opt = StdStringToUtilsString(fixtureNode[FORMATIONS][LOCALTEAM_FORMATION].asString());
			}
			if (!fixtureNode[FORMATIONS][VISITORTEAM_FORMATION].isNull())
			{
				fixture.visitorteam_formation_opt = StdStringToUtilsString(fixtureNode[FORMATIONS][VISITORTEAM_FORMATION].asString());
			}
		}
		fixture.localteam_score = fixtureNode[SCORES][LOCALTEAM_SCORE].asInt64();
		fixture.visitorteam_score = fixtureNode[SCORES][VISITORTEAM_SCORE].asInt64();
		if (!fixtureNode[SCORES][LOCALTEAM_PEN_SCORE].isNull())
		{
			fixture.localteam_pen_score_opt = fixtureNode[SCORES][LOCALTEAM_PEN_SCORE].asInt64();
		}
		if (!fixtureNode[SCORES][VISITORTEAM_PEN_SCORE].isNull())
		{
			fixture.visitorteam_pen_score_opt = fixtureNode[SCORES][VISITORTEAM_PEN_SCORE].asInt64();
		}
		if (!fixtureNode[SCORES][HT_SCORE].isNull())
		{
			fixture.ht_score_opt = StdStringToUtilsString(fixtureNode[SCORES][HT_SCORE].asString());
		}
		fixture.ft_score = StdStringToUtilsString(fixtureNode[SCORES][FT_SCORE].asString());
		if (!fixtureNode[SCORES][ET_SCORE].isNull())
		{
			fixture.et_score_opt = StdStringToUtilsString(fixtureNode[ET_SCORE].asString());
		}
		if (!fixtureNode[SCORES][PS_SCORE].isNull())
		{
			fixture.ps_score_opt = StdStringToUtilsString(fixtureNode[PS_SCORE].asString());
		}
		fixture.status = StdStringToUtilsString(fixtureNode[TIME][STATUS].asString());
		fixture.date_time = utils::date_and_time::DateTime::FromSQLiteDateFormat(fixtureNode[TIME][STARTING_AT][DATE_TIME].asString());
		fixture.date = StdStringToUtilsString(fixtureNode[TIME][STARTING_AT][DATE].asString());
		fixture.time = StdStringToUtilsString(fixtureNode[TIME][STARTING_AT][TIME].asString());
		fixture.timestamp = fixtureNode[TIME][STARTING_AT][TIMESTAMP].asInt64();
		fixture.timezone = StdStringToUtilsString(fixtureNode[TIME][STARTING_AT][TIMEZONE].asString());
		fixture.minute = fixtureNode[TIME][MINUTE].asInt64();
		if (!fixtureNode[TIME][SECOND].isNull())
		{
			fixture.second_opt = AsIntOpt(fixtureNode[TIME][SECOND]);
		}
		if (!fixtureNode[TIME][ADDED_TIME].isNull())
		{
			fixture.added_time_opt = AsIntOpt(fixtureNode[TIME][ADDED_TIME]);
		}
		if (!fixtureNode[TIME][EXTRA_MINUTE].isNull())
		{
			fixture.extra_minute_opt = AsIntOpt(fixtureNode[TIME][EXTRA_MINUTE]);
		}
		if (!fixtureNode[TIME][INJURY_TIME].isNull())
		{
			fixture.injury_time_opt = AsIntOpt(fixtureNode[TIME][INJURY_TIME]);
		}
		if (!fixtureNode[COACHES].isNull())
		{
			if (!fixtureNode[COACHES][LOCALTEAM_COACH_ID].isNull())
			{
				fixture.sm_localteam_coach_id_opt = fixtureNode[COACHES][LOCALTEAM_COACH_ID].asInt64();
			}
			if (!fixtureNode[COACHES][VISITORTEAM_COACH_ID].isNull())
			{
				fixture.sm_visitorteam_coach_id_opt = fixtureNode[COACHES][VISITORTEAM_COACH_ID].asInt64();
			}
		}
		if (!fixtureNode[STANDINGS][LOCALTEAM_POSITION].isNull())
		{
			fixture.localteam_position_opt = fixtureNode[STANDINGS][LOCALTEAM_POSITION].asInt64();
		}
		if (!fixtureNode[STANDINGS][VISITORTEAM_POSITION].isNull())
		{
			fixture.visitorteam_position_opt = fixtureNode[STANDINGS][VISITORTEAM_POSITION].asInt64();
		}
		if (!fixtureNode[ASSISTANTS].isNull())
		{
			if (!fixtureNode[ASSISTANTS][FIRST_ASSISTANT_ID].isNull())
			{
				fixture.sm_first_assistant_id_opt = fixtureNode[ASSISTANTS][FIRST_ASSISTANT_ID].asInt64();
			}
			if (!fixtureNode[ASSISTANTS][SECOND_ASSISTANT_ID].isNull())
			{
				fixture.sm_second_assistant_id_opt = fixtureNode[ASSISTANTS][SECOND_ASSISTANT_ID].asInt64();
			}
			if (!fixtureNode[ASSISTANTS][FOURTH_OFFICIAL_ID].isNull())
			{
				fixture.sm_fourth_official_id_opt = fixtureNode[ASSISTANTS][FOURTH_OFFICIAL_ID].asInt64();
			}
		}
		fixture.leg = StdStringToUtilsString(fixtureNode[LEG].asString());
		if (fixtureNode[COLORS].isArray())
		{
			TCOUT << fixture.sm_id << std::endl;
		}
		if (!fixtureNode[COLORS].isNull() && !fixtureNode[COLORS].isArray())
		{
			if (!fixtureNode[COLORS][LOCALTEAM].isNull())
			{
				if (!fixtureNode[COLORS][LOCALTEAM][COLOR].isNull())
				{
					fixture.localteam_color_opt = StdStringToUtilsString(fixtureNode[COLORS][LOCALTEAM][COLOR].asString());
				}
				if (!fixtureNode[COLORS][LOCALTEAM][KIT_COLORS].isNull())
				{
					fixture.localteam_kit_color_opt = StdStringToUtilsString(fixtureNode[COLORS][LOCALTEAM][KIT_COLORS].asString());
				}
			}
			if (!fixtureNode[COLORS][VISITORTEAM].isNull())
			{
				if (!fixtureNode[COLORS][VISITORTEAM][COLOR].isNull())
				{
					fixture.visitorteam_color_opt = StdStringToUtilsString(fixtureNode[COLORS][VISITORTEAM][COLOR].asString());
				}
				if (!fixtureNode[COLORS][VISITORTEAM][KIT_COLORS].isNull())
				{
					fixture.visitorteam_kit_color_opt = StdStringToUtilsString(fixtureNode[COLORS][VISITORTEAM][KIT_COLORS].asString());
				}
			}
		}
		fixture.deleted = fixtureNode[DELETED].asBool();
	}

	namespace
	{
		void ParseTeamGoalMinutes(const Json::Value& statsNode, TeamSeasonStatistics& stats)
		{
			for (unsigned int i = 0; i < statsNode[SCORING_MINUTES].size(); i++)
			{
				for (unsigned int j = 0; j < statsNode[SCORING_MINUTES][i][PERIOD].size(); j++)
				{
					TeamGoalMinute teamGoalMinute;
					const auto& minutesNode = statsNode[SCORING_MINUTES][i][PERIOD][j];

					teamGoalMinute.type = scraper::SCORED;
					teamGoalMinute.period = StdStringToUtilsString(minutesNode[MINUTE].asString());
					teamGoalMinute.count = AsInt(minutesNode[COUNT]);
					teamGoalMinute.percentage = AsDouble(minutesNode[PERCENTAGE]);

					stats.goalMinutes.insert(std::move(teamGoalMinute));
				}
			}
			for (unsigned int i = 0; i < statsNode[GOALS_CONCEDED_MINUTES].size(); i++)
			{
				for (unsigned int j = 0; j < statsNode[GOALS_CONCEDED_MINUTES][i][PERIOD].size(); j++)
				{
					TeamGoalMinute teamGoalMinute;
					const auto& minutesNode = statsNode[GOALS_CONCEDED_MINUTES][i][PERIOD][j];

					teamGoalMinute.type = scraper::CONCEDED;
					teamGoalMinute.period = StdStringToUtilsString(minutesNode[MINUTE].asString());
					teamGoalMinute.count = minutesNode[COUNT].asInt();
					teamGoalMinute.percentage = minutesNode[PERCENTAGE].asDouble();

					stats.goalMinutes.insert(std::move(teamGoalMinute));
				}
			}
		}

		void ParseTeamGoalLines(const Json::Value& statsNode, TeamSeasonStatistics& stats)
		{
			for (const std::string& goalLine : GOAL_LINES)
			{
				TeamGoalLine teamGoalLine;
				const auto& lineNode = statsNode[GOAL_LINE][OVER][goalLine];

				teamGoalLine.line_over = StdStringToUtilsString(goalLine);
				teamGoalLine.home_percentage = lineNode[HOME].asDouble();
				teamGoalLine.away_percentage = lineNode[AWAY].asDouble();

				stats.goalLines.insert(std::move(teamGoalLine));
			}
		}

		void ParseTeamSeasonStats(const Json::Value& statsNodes, Team& team)
		{
			for (unsigned int i = 0; i < statsNodes.size(); i++)
			{
				TeamSeasonStatistics stats;
				const auto& statsNode = statsNodes[i];

				stats.sm_team_id = statsNode[TEAM_ID].asInt64();
				utils::AssertEquals(team.sm_id, stats.sm_team_id, "The team id must be the same.");
				stats.sm_season_id = statsNode[SEASON_ID].asInt64();
				if (!statsNode[STAGE_ID].isNull())	//can be null in the json response, but we want it to always have 0 as default value, because it is used as part of a unique constraint
				{
					stats.sm_stage_id = statsNode[STAGE_ID].asInt64();
				}
				stats.home_wins = AsInt(statsNode[WIN][HOME]);
				stats.away_wins = AsInt(statsNode[WIN][AWAY]);
				stats.home_draws = AsInt(statsNode[DRAW][HOME]);
				stats.away_draws = AsInt(statsNode[DRAW][AWAY]);
				stats.home_losses = AsInt(statsNode[LOST][HOME]);
				stats.away_losses = AsInt(statsNode[LOST][AWAY]);
				stats.home_goals_for = AsInt(statsNode[GOALS_FOR][HOME]);
				stats.away_goals_for = AsInt(statsNode[GOALS_FOR][AWAY]);
				stats.home_goals_against = AsInt(statsNode[GOALS_AGAINST][HOME]);
				stats.away_goals_against = AsInt(statsNode[GOALS_AGAINST][AWAY]);
				stats.home_clean_sheet = AsInt(statsNode[CLEAN_SHEET][HOME]);
				stats.away_clean_sheet = AsInt(statsNode[CLEAN_SHEET][AWAY]);
				stats.home_failed_to_score = AsInt(statsNode[FAILED_TO_SCORE][HOME]);
				stats.away_failed_to_score = AsInt(statsNode[FAILED_TO_SCORE][AWAY]);
				if (!statsNode[AVG_GOALS_PER_GAME_SCORED].isNull())
				{
					stats.home_avg_goals_per_game_scored_opt = AsDoubleOpt(statsNode[AVG_GOALS_PER_GAME_SCORED][HOME]);
					stats.away_avg_goals_per_game_scored_opt = AsDoubleOpt(statsNode[AVG_GOALS_PER_GAME_SCORED][AWAY]);
				}
				if (!statsNode[AVG_GOALS_PER_GAME_SCORED].isNull())
				{
					stats.home_avg_goals_per_game_conceded_opt = AsDoubleOpt(statsNode[AVG_GOALS_PER_GAME_CONCEDED][HOME]);
					stats.away_avg_goals_per_game_conceded_opt = AsDoubleOpt(statsNode[AVG_GOALS_PER_GAME_CONCEDED][AWAY]);
				}
				stats.home_avg_first_goal_scored = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_SCORED][HOME].asString());
				stats.away_avg_first_goal_scored = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_SCORED][AWAY].asString());
				stats.overall_avg_first_goal_scored = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_SCORED][TOTAL].asString());
				stats.home_avg_first_goal_conceded = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_CONCEDED][HOME].asString());
				stats.away_avg_first_goal_conceded = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_CONCEDED][AWAY].asString());
				stats.overall_avg_first_goal_conceded = StdStringToUtilsString(statsNode[AVG_FIRST_GOAL_CONCEDED][TOTAL].asString());
				if (!statsNode[ATTACKS].isNull())
				{
					stats.attacks_opt = statsNode[ATTACKS].asInt();
				}
				if (!statsNode[DANGEROUS_ATTACKS].isNull())
				{
					stats.dangerous_attacks_opt = statsNode[DANGEROUS_ATTACKS].asInt();
				}
				if (!statsNode[AVG_BALL_POSSESSION_PERCENTAGE].isNull())
				{
					stats.avg_ball_possession_percentage_opt = std::stod(statsNode[AVG_BALL_POSSESSION_PERCENTAGE].asString());
				}
				if (!statsNode[FOULS].isNull())
				{
					stats.fouls_opt = statsNode[FOULS].asInt();
				}
				if (!statsNode[AVG_FOULS_PER_GAME].isNull())
				{
					stats.avg_fouls_per_game_opt = std::stod(statsNode[AVG_FOULS_PER_GAME].asString());
				}
				if (!statsNode[OFFSIDES].isNull())
				{
					stats.offsides_opt = statsNode[OFFSIDES].asInt();
				}
				if (!statsNode[REDCARDS].isNull())
				{
					stats.redcards_opt = statsNode[REDCARDS].asInt();
				}
				if (!statsNode[YELLOWCARDS].isNull())
				{
					stats.yellowcards_opt = statsNode[YELLOWCARDS].asInt();
				}
				if (!statsNode[SHOTS_BLOCKED].isNull())
				{
					stats.shots_blocked_opt = statsNode[SHOTS_BLOCKED].asInt();
				}
				if (!statsNode[SHOTS_OFF_TARGET].isNull())
				{
					stats.shots_off_target_opt = statsNode[SHOTS_OFF_TARGET].asInt();
				}
				if (!statsNode[AVG_SHOTS_OFF_TARGET_PER_GAME].isNull())
				{
					stats.avg_shots_off_target_per_game_opt = std::stod(statsNode[AVG_SHOTS_OFF_TARGET_PER_GAME].asString());
				}
				if (!statsNode[SHOTS_ON_TARGET].isNull())
				{
					stats.shots_on_target_opt = statsNode[SHOTS_ON_TARGET].asInt();
				}
				if (!statsNode[AVG_SHOTS_ON_TARGET_PER_GAME].isNull())
				{
					stats.avg_shots_on_target_per_game_opt = std::stod(statsNode[AVG_SHOTS_ON_TARGET_PER_GAME].asString());
				}
				if (!statsNode[AVG_CORNERS].isNull())
				{
					stats.avg_corners_opt = std::stod(statsNode[AVG_CORNERS].asString());
				}
				if (!statsNode[TOTAL_CORNERS].isNull())
				{
					stats.total_corners_opt = statsNode[TOTAL_CORNERS].asInt();
				}
				if (!statsNode[BTTS].isNull())
				{
					stats.btts_opt = statsNode[BTTS].asDouble();
				}

				ParseTeamGoalMinutes(statsNode, stats);
				ParseTeamGoalLines(statsNode, stats);

				team.seasonStatistics.insert(std::move(stats));
			}

		}
	}

	void ParseTeam(const Json::Value& teamNode, Team& team)
	{
		team.sm_id = teamNode[ID].asInt64();
		if (!teamNode[LEGACY_ID].isNull())
		{
			team.sm_legacy_id_opt = teamNode[LEGACY_ID].asInt64();
		}
		team.name = StdStringToUtilsString(teamNode[NAME].asString());
		if (!teamNode[SHORT_CODE].isNull())
		{
			team.short_code_opt = StdStringToUtilsString(teamNode[SHORT_CODE].asString());
		}
		if (!teamNode[TWITTER].isNull())
		{
			team.twitter_opt = StdStringToUtilsString(teamNode[TWITTER].asString());
		}
		team.sm_country_id = teamNode[COUNTRY_ID].asInt64();
		team.national_team = teamNode[NATIONAL_TEAM].asBool();
		if (!teamNode[FOUNDED].isNull())
		{
			team.founded_opt = teamNode[FOUNDED].asInt64();
		}
		if (!teamNode[LOGO_PATH].isNull())
		{
			team.logo_path_opt = StdStringToUtilsString(teamNode[LOGO_PATH].asString());
		}
		if (!teamNode[VENUE_ID].isNull())
		{
			team.sm_venue_id_opt = teamNode[VENUE_ID].asInt64();
		}
		if (!teamNode[CURRENT_SEASON_ID].isNull())
		{
			team.sm_current_season_id_opt = teamNode[CURRENT_SEASON_ID].asInt64();
		}
		ParseTeamSeasonStats(teamNode[STATS][DATA], team);
	}

	void ParseMatchStatistics(const Json::Value& statsNode, Fixture& fixture)
	{
		for (unsigned int i = 0; i < statsNode[DATA].size(); i++)
		{
			const auto& statsElem = statsNode[DATA][i];

			MatchStatistics matchStatistics;
			matchStatistics.sm_team_id = statsElem[TEAM_ID].asInt64();
			matchStatistics.sm_fixture_id = statsElem[FIXTURE_ID].asInt64();
			utils::AssertEquals(fixture.sm_id, matchStatistics.sm_fixture_id, FIXTURE_ID_SHOULD_BE_THE_SAME);

			if (!statsElem[SHOTS][TOTAL].isNull())
			{
				matchStatistics.shots_total_opt = AsIntOpt(statsElem[SHOTS][TOTAL]);
			}
			if (!statsElem[SHOTS][ONGOAL].isNull())
			{
				matchStatistics.shots_ongoal_opt = AsIntOpt(statsElem[SHOTS][ONGOAL]);
			}
			if (!statsElem[SHOTS][OFFGOAL].isNull())
			{
				matchStatistics.shots_offgoal_opt = AsIntOpt(statsElem[SHOTS][OFFGOAL]);
			}
			if (!statsElem[SHOTS][BLOCKED].isNull())
			{
				matchStatistics.shots_blocked_opt = AsIntOpt(statsElem[SHOTS][BLOCKED]);
			}
			if (!statsElem[SHOTS][INSIDEBOX].isNull())
			{
				matchStatistics.shots_insidebox_opt = AsIntOpt(statsElem[SHOTS][INSIDEBOX]);
			}
			if (!statsElem[SHOTS][OUTSIDEBOX].isNull())
			{
				matchStatistics.shots_outsidebox_opt = AsIntOpt(statsElem[SHOTS][OUTSIDEBOX]);
			}
			if (!statsElem[PASSES][TOTAL].isNull())
			{
				matchStatistics.passes_total_opt = AsIntOpt(statsElem[PASSES][TOTAL]);
			}
			if (!statsElem[PASSES][ACCURATE].isNull())
			{
				matchStatistics.passes_accurate_opt = AsIntOpt(statsElem[PASSES][ACCURATE]);
			}
			if (!statsElem[PASSES][PERCENTAGE].isNull())
			{
				matchStatistics.passes_percentage_opt = AsDoubleOpt(statsElem[PASSES][PERCENTAGE]);
			}
			if (!statsElem[ATTACKS][ATTACKS].isNull())
			{
				matchStatistics.attacks_opt = AsInt(statsElem[ATTACKS][ATTACKS]);
			}
			if (!statsElem[ATTACKS][DANGEROUS_ATTACKS].isNull())
			{
				matchStatistics.dangerous_attacks_opt = AsIntOpt(statsElem[ATTACKS][DANGEROUS_ATTACKS]);
			}
			if (!statsElem[FOULS].isNull())
			{
				matchStatistics.fouls_opt = statsElem[FOULS].asInt();
			}
			if (!statsElem[CORNERS].isNull())
			{
				matchStatistics.corners_opt = statsElem[CORNERS].asInt();
			}
			if (!statsElem[OFFSIDES].isNull())
			{
				matchStatistics.offsides_opt = statsElem[OFFSIDES].asInt();
			}
			if (!statsElem[POSSESSIONTIME].isNull())
			{
				matchStatistics.possessiontime_opt = statsElem[POSSESSIONTIME].asInt();
			}
			if (!statsElem[YELLOWCARDS].isNull())
			{
				matchStatistics.yellowcards_opt = statsElem[YELLOWCARDS].asInt();
			}
			if (!statsElem[REDCARDS].isNull())
			{
				matchStatistics.redcards_opt = statsElem[REDCARDS].asInt();
			}
			if (!statsElem[YELLOWREDCARDS].isNull())
			{
				matchStatistics.yellowredcards_opt = statsElem[YELLOWREDCARDS].asInt();
			}
			if (!statsElem[SAVES].isNull())
			{
				matchStatistics.saves_opt = statsElem[SAVES].asInt();
			}
			if (!statsElem[SUBSTITUTIONS].isNull())
			{
				matchStatistics.substitutions_opt = statsElem[SUBSTITUTIONS].asInt();
			}
			if (!statsElem[GOAL_KICK].isNull())
			{
				matchStatistics.goal_kick_opt = statsElem[GOAL_KICK].asInt();
			}
			if (!statsElem[THROW_IN].isNull())
			{
				matchStatistics.throw_in_opt = statsElem[THROW_IN].asInt();
			}
			if (!statsElem[BALL_SAFE].isNull())
			{
				matchStatistics.ball_safe_opt = statsElem[BALL_SAFE].asInt();
			}
			if (!statsElem[GOALS].isNull())
			{
				matchStatistics.goals_opt = statsElem[GOALS].asInt();
			}
			if (!statsElem[PENALTIES].isNull())
			{
				matchStatistics.penalties_opt = statsElem[PENALTIES].asInt();
			}
			if (!statsElem[INJURIES].isNull())
			{
				matchStatistics.injuries_opt = statsElem[INJURIES].asInt();
			}

			fixture.matchStatistics.insert(std::move(matchStatistics));
		}
	}

	void ParseLineups(const Json::Value& lineupsNode, Fixture& fixture)
	{
		for (unsigned int i = 0; i < lineupsNode[DATA].size(); i++)
		{
			const auto& lineupNode = lineupsNode[DATA][i];

			Lineup lineup;
			lineup.sm_team_id = lineupNode[TEAM_ID].asInt64();
			lineup.sm_fixture_id = lineupNode[FIXTURE_ID].asInt64();
			lineup.sm_player_id = lineupNode[PLAYER_ID].asInt64();
			lineup.player_name = StdStringToUtilsString(lineupNode[PLAYER_NAME].asString());
			lineup.number = lineupNode[NUMBER].asInt();
			lineup.position = StdStringToUtilsString(lineupNode[POSITION].asString());
			if (!lineupNode[ADDITIONAL_POSITION].isNull())
			{
				lineup.additional_position_opt = StdStringToUtilsString(lineupNode[ADDITIONAL_POSITION].asString());
			}
			lineup.formation_position_opt = lineupNode[FORMATION_POSITION].asInt();
			lineup.posx_opt = AsIntOpt(lineupNode[POSX]);
			lineup.posy_opt = AsIntOpt(lineupNode[POSY]);
			lineup.captain_opt = AsBoolOpt(lineupNode[CAPTAIN]);
			lineup.type = StdStringToUtilsString(lineupNode[TYPE].asString());
			if (!lineupNode[STATS][SHOTS][SHOTS_TOTAL].isNull())
			{
				lineup.shots_total_opt = lineupNode[STATS][SHOTS][SHOTS_TOTAL].asInt();
			}
			if (!lineupNode[STATS][SHOTS][SHOTS_ON_GOAL].isNull())
			{
				lineup.shots_on_goal_opt = lineupNode[STATS][SHOTS][SHOTS_ON_GOAL].asInt();
			}
			lineup.goals_scored_opt = AsIntOpt(lineupNode[STATS][GOALS][SCORED]);
			lineup.goals_assists_opt = AsIntOpt(lineupNode[STATS][GOALS][ASSISTS]);
			lineup.goals_conceded_opt = AsIntOpt(lineupNode[STATS][GOALS][CONCEDED]);
			lineup.goals_owngoals_opt = AsIntOpt(lineupNode[STATS][GOALS][OWNGOALS]);
			if (!lineupNode[STATS][FOULS][DRAWN].isNull())
			{
				lineup.fouls_drawn_opt = lineupNode[STATS][FOULS][DRAWN].asInt();
			}
			if (!lineupNode[STATS][FOULS][COMMITTED].isNull())
			{
				lineup.fouls_committed_opt = lineupNode[STATS][FOULS][COMMITTED].asInt();
			}
			if (!lineupNode[STATS][CARDS][YELLOWCARDS].isNull())
			{
				lineup.yellowcards_opt = lineupNode[STATS][CARDS][YELLOWCARDS].asInt();
			}
			if (!lineupNode[STATS][CARDS][REDCARDS].isNull())
			{
				lineup.redcards_opt = lineupNode[STATS][CARDS][REDCARDS].asInt();
			}
			if (!lineupNode[STATS][CARDS][YELLOWREDCARDS].isNull())
			{
				lineup.yellowredcards_opt = lineupNode[STATS][CARDS][YELLOWREDCARDS].asInt();
			}
			if (!lineupNode[STATS][PASSING][TOTAL_CROSSES].isNull())
			{
				lineup.passing_total_crosses_opt = lineupNode[STATS][PASSING][TOTAL_CROSSES].asInt();
			}
			if (!lineupNode[STATS][PASSING][CROSSES_ACCURACY].isNull())
			{
				lineup.passing_crosses_accuracy_opt = lineupNode[STATS][PASSING][CROSSES_ACCURACY].asInt();
			}
			if (!lineupNode[STATS][PASSING][PASSES].isNull())
			{
				lineup.passing_passes_opt = lineupNode[STATS][PASSING][PASSES].asInt();
			}
			lineup.passing_accurate_passes_opt = AsIntOpt(lineupNode[STATS][PASSING][ACCURATE_PASSES]);
			if (!lineupNode[STATS][PASSING][PASSES_ACCURACY].isNull())
			{
				lineup.passing_passes_accuracy_opt = lineupNode[STATS][PASSING][PASSES_ACCURACY].asInt();
			}
			if (!lineupNode[STATS][PASSING][KEY_PASSES].isNull())
			{
				lineup.passing_key_passes_opt = lineupNode[STATS][PASSING][KEY_PASSES].asInt();
			}
			if (!lineupNode[STATS][DRIBBLES][ATTEMPTS].isNull())
			{
				lineup.dribbles_attempts_opt = lineupNode[STATS][DRIBBLES][ATTEMPTS].asInt();
			}
			if (!lineupNode[STATS][DRIBBLES][SUCCESS].isNull())
			{
				lineup.dribbles_success_opt = lineupNode[STATS][DRIBBLES][SUCCESS].asInt();
			}
			if (!lineupNode[STATS][DRIBBLES][DRIBBLED_PAST].isNull())
			{
				lineup.dribbles_dribbled_past_opt = lineupNode[STATS][DRIBBLES][DRIBBLED_PAST].asInt();
			}
			if (!lineupNode[STATS][DUELS][TOTAL].isNull())
			{
				lineup.duels_total_opt = lineupNode[STATS][DUELS][TOTAL].asInt();
			}
			if (!lineupNode[STATS][DUELS][WON].isNull())
			{
				lineup.duels_won_opt = lineupNode[STATS][DUELS][WON].asInt();
			}
			lineup.aerials_won_opt = AsIntOpt(lineupNode[STATS][OTHER][AERIALS_WON]);
			lineup.punches_opt = AsIntOpt(lineupNode[STATS][OTHER][PUNCHES]);
			lineup.offsides_opt = AsIntOpt(lineupNode[STATS][OTHER][OFFSIDES]);
			lineup.saves_opt = AsIntOpt(lineupNode[STATS][OTHER][SAVES]);
			lineup.inside_box_saves_opt = AsIntOpt(lineupNode[STATS][OTHER][INSIDE_BOX_SAVES]);
			lineup.penalty_scored_opt = AsIntOpt(lineupNode[STATS][OTHER][PENALTY_SCORED]);
			lineup.penalty_missed_opt = AsIntOpt(lineupNode[STATS][OTHER][PENALTY_MISSED]);
			lineup.penalty_saved_opt = AsIntOpt(lineupNode[STATS][OTHER][PENALTY_SAVED]);
			lineup.penalty_committed_opt = AsIntOpt(lineupNode[STATS][OTHER][PENALTY_COMMITTED]);
			lineup.penalty_won_opt = AsIntOpt(lineupNode[STATS][OTHER][PENALTY_WON]);
			lineup.hit_woodwork_opt = AsIntOpt(lineupNode[STATS][OTHER][HIT_WOODWORK]);
			lineup.tackles_opt = AsIntOpt(lineupNode[STATS][OTHER][TACKLES]);
			lineup.blocks_opt = AsIntOpt(lineupNode[STATS][OTHER][BLOCKS]);
			lineup.interceptions_opt = AsIntOpt(lineupNode[STATS][OTHER][INTERCEPTIONS]);
			lineup.clearances_opt = AsIntOpt(lineupNode[STATS][OTHER][CLEARANCES]);
			lineup.dispossesed_opt = AsIntOpt(lineupNode[STATS][OTHER][DISPOSSESED]);
			lineup.minutes_played_opt = AsIntOpt(lineupNode[STATS][OTHER][MINUTES_PLAYED]);
			lineup.rating_opt = AsDoubleOpt(lineupNode[STATS][RATING]);


			fixture.lineups.insert(std::move(lineup));
		}
	}

	void ParseSidelines(const Json::Value& sidelinesNode, Fixture& fixture)
	{
		for (unsigned int i = 0; i < sidelinesNode[DATA].size(); i++)
		{
			FixtureSideline fixtureSideline;
			const auto& jsonNode = sidelinesNode[DATA][i];

			fixtureSideline.sm_team_id = jsonNode[TEAM_ID].asInt64();
			//TODO: check and throw team_id. It has to be one of the teams involved in the match
			fixtureSideline.sm_fixture_id = jsonNode[FIXTURE_ID].asInt64();
			utils::AssertEquals(fixture.sm_id, fixtureSideline.sm_fixture_id, FIXTURE_ID_SHOULD_BE_THE_SAME);
			fixtureSideline.sm_player_id = jsonNode[PLAYER_ID].asInt64();
			fixtureSideline.player_name = StdStringToUtilsString(jsonNode[PLAYER_NAME].asString());
			fixtureSideline.reason = StdStringToUtilsString(jsonNode[REASON].asString());

			fixture.sidelines.insert(std::move(fixtureSideline));
		}
	}

	namespace
	{
		void ParseTrendPoints(const Json::Value& analysesNode, Trend& trend)
		{
			for (unsigned int i = 0; i < analysesNode.size(); i++)
			{
				TrendPoint trendPoint;
				const auto& trendPointNode = analysesNode[i];

				trendPoint.sm_trend_id = trend.sm_id;
				trendPoint.minute = std::stod(trendPointNode[MINUTE].asString());
				trendPoint.amount = std::stod(trendPointNode[AMOUNT].asString());

				trend.trendPoints.insert(std::move(trendPoint));
			}
		}
	}

	void ParseTrends(const Json::Value& trendsNode, Fixture& fixture)
	{
		for (unsigned int i = 0; i < trendsNode[DATA].size(); i++)
		{
			Trend trend;
			const auto& trendNode = trendsNode[DATA][i];

			trend.sm_id = trendNode[ID].asInt64();
			trend.sm_fixture_id = trendNode[FIXTURE_ID].asInt64();
			utils::AssertEquals(fixture.sm_id, trend.sm_fixture_id, FIXTURE_ID_SHOULD_BE_THE_SAME);
			trend.sm_team_id = trendNode[TEAM_ID].asInt64();
			trend.type = StdStringToUtilsString(trendNode[TYPE].asString());
			trend.updated_at = StdStringToUtilsString(trendNode[UPDATED_AT].asString());
			trend.timezone_type = trendNode[TIMEZONE_TYPE].asInt64();
			trend.timezone = StdStringToUtilsString(trendNode[TIMEZONE].asString());
			ParseTrendPoints(trendNode[ANALYSES], trend);

			fixture.trends.insert(std::move(trend));
		}
	}

	namespace
	{
		void ParsePlayerSideLines(const Json::Value& playerNode, Player& player)
		{
			for (unsigned int i = 0; i < playerNode[DATA].size(); i++)
			{
				PlayerSideline playerSideline;
				const auto& sidelinedNode = playerNode[DATA][i];

				playerSideline.sm_player_id = sidelinedNode[PLAYER_ID].asInt64();
				utils::AssertEquals(player.sm_id, playerSideline.sm_player_id, "Player ID should be the same.");
				if (!sidelinedNode[SEASON_ID].isNull())
				{
					playerSideline.sm_season_id_opt = sidelinedNode[SEASON_ID].asInt64();
				}
				if (!sidelinedNode[TEAM_ID].isNull())
				{
					playerSideline.sm_team_id_opt = sidelinedNode[TEAM_ID].asInt64();
				}
				playerSideline.description = StdStringToUtilsString(sidelinedNode[DESCRIPTION].asString());
				playerSideline.start_date = StdStringToUtilsString(sidelinedNode[START_DATE].asString());
				if (!sidelinedNode[END_DATE].isNull())
				{
					playerSideline.end_date_opt = StdStringToUtilsString(sidelinedNode[END_DATE].asString());
				}

				player.sidelines.insert(std::move(playerSideline));
			}
		}

		void ParsePlayerTransfers(const Json::Value& transfersNode, Player& player)
		{
			for (unsigned int i = 0; i < transfersNode[DATA].size(); i++)
			{
				PlayerTransfer playerTransfer;
				const auto& transferNode = transfersNode[DATA][i];

				playerTransfer.sm_player_id = transferNode[PLAYER_ID].asInt64();
				playerTransfer.sm_from_team_id = transferNode[FROM_TEAM_ID].asInt64();
				playerTransfer.sm_to_team_id = transferNode[TO_TEAM_ID].asInt64();
				if (!transferNode[SEASON_ID].isNull())
				{
					playerTransfer.sm_season_id_opt = transferNode[SEASON_ID].asInt64();
				}
				playerTransfer.transfer = StdStringToUtilsString(transferNode[TRANSFER].asString());
				playerTransfer.date = utils::date_and_time::DateTime::FromSQLiteDateFormat(transferNode[DATE].asString());
				if (!transferNode[AMOUNT].isNull())
				{
					playerTransfer.amount_opt = StdStringToUtilsString(transferNode[AMOUNT].asString());
				}

				player.transfers.insert(std::move(playerTransfer));
			}
		}
	}

	void ParsePlayer(const Json::Value& playerNode, Player& player)
	{
		player.sm_id = playerNode[PLAYER_ID].asInt64();
		player.sm_team_id = playerNode[TEAM_ID].asInt64();
		if (!playerNode[COUNTRY_ID].isNull())
		{
			player.sm_country_id_opt = playerNode[COUNTRY_ID].asInt64();
		}
		player.sm_position_id = playerNode[POSITION_ID].asInt64();
		player.common_name = StdStringToUtilsString(playerNode[COMMON_NAME].asString());
		if (!playerNode[DISPLAY_NAME].isNull())
		{
			player.display_name_opt = StdStringToUtilsString(playerNode[DISPLAY_NAME].asString());
		}
		if (!playerNode[FULLNAME].isNull())
		{
			player.fullname_opt = StdStringToUtilsString(playerNode[FULLNAME].asString());
		}
		if (!playerNode[FIRSTNAME].isNull())
		{
			player.firstname_opt = StdStringToUtilsString(playerNode[FIRSTNAME].asString());
		}
		if (!playerNode[LASTNAME].isNull())
		{
			player.lastname_opt = StdStringToUtilsString(playerNode[LASTNAME].asString());
		}
		player.nationality = StdStringToUtilsString(playerNode[NATIONALITY].asString());
		player.birthdate = StdStringToUtilsString(playerNode[BIRTHDATE].asString());
		if (!playerNode[BIRTHCOUNTRY].isNull())
		{
			player.birthcountry_opt = StdStringToUtilsString(playerNode[BIRTHCOUNTRY].asString());
		}
		if (!playerNode[BIRTHPLACE].isNull())
		{
			player.birthplace_opt = StdStringToUtilsString(playerNode[BIRTHPLACE].asString());
		}
		if (!playerNode[HEIGHT].isNull())
		{
			player.height_opt = StdStringToUtilsString(playerNode[HEIGHT].asString());
		}
		if (!playerNode[WEIGHT].isNull())
		{
			player.weight_opt = StdStringToUtilsString(playerNode[WEIGHT].asString());
		}
		if (!playerNode[IMAGE_PATH].isNull())
		{
			player.image_path_opt = StdStringToUtilsString(playerNode[IMAGE_PATH].asString());
		}
		ParsePlayerSideLines(playerNode[SIDELINED], player);
		ParsePlayerTransfers(playerNode[TRANSFERS], player);
	}

	void ParseSeason(const Json::Value& seasonNode, database_handler::v2::Season& season)
	{
		season.sm_id = seasonNode[ID].asInt64();
		season.name = StdStringToUtilsString(seasonNode[NAME].asString());
		season.sm_league_id = seasonNode[LEAGUE_ID].asInt64();
		season.is_current_season = seasonNode[IS_CURRENT_SEASON].asBool();
		if (!seasonNode[CURRENT_ROUND_ID].isNull())
		{
			season.sm_current_round_id_opt = seasonNode[CURRENT_ROUND_ID].asInt64();
		}
		if (!seasonNode[CURRENT_STAGE_ID].isNull())
		{
			season.sm_current_stage_id_opt = seasonNode[CURRENT_STAGE_ID].asInt64();
		}
	}

	bool ParseCommentaries(const Json::Value& commentaryNode, database_handler::v2::Commentary& commentary)
	{
		const utils::String comment{ StdStringToUtilsString(commentaryNode[COMMENT].asString()) };
		if (utils::string_operations::Contains(comment, STR("bet365")))
		{
			return true;
		}

		commentary.sm_fixture_id = commentaryNode[FIXTURE_ID].asInt64();
		commentary.important = commentaryNode[IMPORTANT].asBool();
		commentary.order_ = commentaryNode[ORDER].asInt64();
		commentary.goal = commentaryNode[GOAL].asBool();
		commentary.minute = commentaryNode[MINUTE].asInt64();
		if (!commentaryNode[EXTRA_MINUTE].isNull())
		{
			commentary.extra_minute_opt = commentaryNode[EXTRA_MINUTE].asInt64();
		}
		commentary.comment = comment;
		return true;
	}

	void ParseOdds(const Json::Value& oddsNode, database_handler::v2::Odds& odds)
	{
		odds.label = StdStringToUtilsString(oddsNode[LABEL].asString());
		odds.value = AsDouble(oddsNode[VALUE]);
		if (!oddsNode[PROBABILITY].isNull())
		{
			odds.probability_opt = utils::string_conversion::PercentToDouble(oddsNode[PROBABILITY].asString());
		}
		if (!oddsNode[DP3].isNull())
		{
			odds.dp3_opt = StdStringToUtilsString(oddsNode[DP3].asString());
		}
		odds.american = AsInt64(oddsNode[AMERICAN]);
		if (!oddsNode[FACTIONAL].isNull())
		{
			odds.fractional_opt = StdStringToUtilsString(oddsNode[FACTIONAL].asString());
		}
		if (!oddsNode[WINNING].isNull())
		{
			odds.winning_opt = oddsNode[WINNING].asBool();
		}
		if (!oddsNode[HANDICAP].isNull())
		{
			odds.handicap_opt = StdStringToUtilsString(oddsNode[HANDICAP].asString());
		}
		if (!oddsNode[TOTAL].isNull())
		{
			odds.total_opt = StdStringToUtilsString(oddsNode[TOTAL].asString());
		}
		odds.stop = oddsNode[STOP].asBool();

		if (oddsNode[LAST_UPDATE].isObject())
		{
			odds.last_update = StdStringToUtilsString(oddsNode[LAST_UPDATE][DATE].asString());
			if (!oddsNode[LAST_UPDATE][TIMEZONE_TYPE].isNull())
			{
				odds.timezone_type_opt = AsInt64Opt(oddsNode[LAST_UPDATE][TIMEZONE_TYPE]);
			}
			odds.timezone = StdStringToUtilsString(oddsNode[LAST_UPDATE][TIMEZONE].asString());
		}
		else if (oddsNode[LAST_UPDATE].isString())
		{
			odds.last_update = StdStringToUtilsString(oddsNode[LAST_UPDATE].asString());
		}
	}

	void ParseFlatOdds(
		const Json::Value& flatOddsNode,
		std::set<database_handler::v2::UpOdds, database_handler::v2::OddsCmp>& oddsSet,
		const std::set<int64_t>& bookmakerIds,
		int64_t fixtureId)
	{
		for (int i = 0; i < flatOddsNode[DATA].size(); i++)
		{
			const auto& flatOdds = flatOddsNode[DATA][i];
			int64_t bookmaker_id = AsInt64(flatOdds[BOOKMAKER_ID]);
			if (0 == bookmakerIds.count(bookmaker_id))
			{
				throw std::runtime_error("Got bookmaker id that I didn't ask for.");
			}
			std::optional<int64_t> bookmaker_event_id_opt;
			if (!flatOdds[BOOKMAKER_EVENT_ID].isNull())
			{
				bookmaker_event_id_opt = std::stoll(flatOdds[BOOKMAKER_EVENT_ID].asString());
			}
			int64_t market_id = AsInt64(flatOdds[MARKET_ID]);
			bool suspended = flatOdds[SUSPENDED].asBool();


			for (int j = 0; j < flatOdds[ODDS].size(); j++)
			{
				UpOdds upOdds{ std::make_unique<Odds>() };
				upOdds->sm_fixture_id = fixtureId;
				upOdds->sm_bookmaker_id = bookmaker_id;
				upOdds->bookmaker_event_id_opt = bookmaker_event_id_opt;
				upOdds->sm_market_id = market_id;
				upOdds->suspended = suspended;
				ParseOdds(flatOdds[ODDS][j], *upOdds);
				oddsSet.insert(std::move(upOdds));
			}
		}

	}

	void ParseSeasonStatistics(const Json::Value& seasonStatisticsNode, database_handler::v2::SeasonStatistics& seasonStatistics)
	{
		using namespace json_keys;
		seasonStatistics.sm_id = seasonStatisticsNode[ID].asInt64();
		seasonStatistics.sm_season_id = seasonStatisticsNode[SEASON_ID].asInt64();
		seasonStatistics.sm_league_id = seasonStatisticsNode[LEAGUE_ID].asInt64();
		seasonStatistics.number_of_clubs = seasonStatisticsNode[NUMBER_OF_CLUBS].asInt();
		seasonStatistics.number_of_matches = seasonStatisticsNode[NUMBER_OF_MATCHES].asInt();
		seasonStatistics.number_of_matches_played = seasonStatisticsNode[NUMBER_OF_MATCHES_PLAYED].asInt();
		seasonStatistics.number_of_goals = seasonStatisticsNode[NUMBER_OF_GOALS].asInt();
		seasonStatistics.matches_both_teams_scored = seasonStatisticsNode[MATCHES_BOTH_TEAMS_SCORED].asInt();
		seasonStatistics.number_of_yellowcards = seasonStatisticsNode[NUMBER_OF_YELLOWCARDS].asInt();
		seasonStatistics.number_of_yellowredcards = seasonStatisticsNode[NUMBER_OF_YELLOWREDCARDS].asInt();
		seasonStatistics.number_of_redcards = seasonStatisticsNode[NUMBER_OF_REDCARDS].asInt();
		seasonStatistics.avg_goals_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_GOALS_PER_MATCH]);
		seasonStatistics.avg_yellowcards_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_YELLOWCARDS_PER_MATCH]);
		seasonStatistics.avg_yellowredcards_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_YELLOWREDCARDS_PER_MATCH]);
		seasonStatistics.avg_redcards_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_REDCARDS_PER_MATCH]);
		seasonStatistics.sm_team_with_most_goals_id_opt = AsInt64Opt(seasonStatisticsNode[TEAM_WITH_MOST_GOALS_ID]);
		seasonStatistics.sm_team_with_most_conceded_goals_id_opt = AsInt64Opt(seasonStatisticsNode[TEAM_WITH_MOST_CONCEDED_GOALS_ID]);
		seasonStatistics.sm_team_with_most_goals_per_match_id_opt = AsInt64Opt(seasonStatisticsNode[TEAM_WITH_MOST_GOALS_PER_MATCH_ID]);
		seasonStatistics.sm_season_topscorer_id_opt = AsInt64Opt(seasonStatisticsNode[SEASON_TOPSCORER_ID]);
		seasonStatistics.season_topscorer_number_opt = AsInt64Opt(seasonStatisticsNode[SEASON_TOPSCORER_ID]);
		seasonStatistics.sm_season_assist_topscorer_id_opt = AsInt64Opt(seasonStatisticsNode[SEASON_ASSIST_TOPSCORER_ID]);
		seasonStatistics.season_assist_topscorer_number_opt = AsInt64Opt(seasonStatisticsNode[SEASON_ASSIST_TOPSCORER_ID]);
		seasonStatistics.sm_team_most_cleansheets_id_opt = AsInt64Opt(seasonStatisticsNode[TEAM_MOST_CLEANSHEETS_ID]);
		seasonStatistics.team_most_cleansheets_number_opt = AsInt64Opt(seasonStatisticsNode[TEAM_MOST_CLEANSHEETS_ID]);
		seasonStatistics.sm_goalkeeper_most_cleansheets_id_opt = AsInt64Opt(seasonStatisticsNode[GOALKEEPER_MOST_CLEANSHEETS_ID]);
		seasonStatistics.goalkeeper_most_cleansheets_number_opt = AsInt64Opt(seasonStatisticsNode[GOALKEEPER_MOST_CLEANSHEETS_ID]);
		seasonStatistics.goal_scored_every_minutes = seasonStatisticsNode[GOAL_SCORED_EVERY_MINUTES].asInt();
		seasonStatistics.btts_opt = AsDoubleOpt(seasonStatisticsNode[BTTS]);
		if (!seasonStatisticsNode[AVG_CORNERS_PER_MATCH].isNull())
		{
			seasonStatistics.avg_corners_per_match_opt = std::stod(seasonStatisticsNode[AVG_CORNERS_PER_MATCH].asString());
		}
		if (!seasonStatisticsNode[TEAM_MOST_CORNERS_COUNT].isNull())
		{
			seasonStatistics.team_most_corners_count_opt = std::stoi(seasonStatisticsNode[TEAM_MOST_CORNERS_COUNT].asString());
		}
		if (!seasonStatisticsNode[TEAM_MOST_CORNERS_ID].isNull())
		{
			seasonStatistics.sm_team_most_corners_id_opt = seasonStatisticsNode[TEAM_MOST_CORNERS_ID].asInt64();
		}
		seasonStatistics.win_percentage_home_opt = AsDoubleOpt(seasonStatisticsNode[WIN_PERCENTAGE][HOME]);
		seasonStatistics.win_percentage_away_opt = AsDoubleOpt(seasonStatisticsNode[WIN_PERCENTAGE][AWAY]);
		seasonStatistics.draw_percentage_opt = AsDoubleOpt(seasonStatisticsNode[DRAW_PERCENTAGE]);
		seasonStatistics.avg_homegoals_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_HOMEGOALS_PER_MATCH]);
		seasonStatistics.avg_awaygoals_per_match_opt = AsDoubleOpt(seasonStatisticsNode[AVG_AWAYGOALS_PER_MATCH]);
		seasonStatistics.avg_player_rating_opt = AsDoubleOpt(seasonStatisticsNode[AVG_PLAYER_RATING]);
		seasonStatistics.updated_date = StdStringToUtilsString(seasonStatisticsNode[UPDATED_AT].asString());

		std::set<GoalsScoredMinute, GoalsScoredMinuteCmp> goalsScoredminutes;
		GoalsScoredMinute gsm;
		gsm.sm_season_statistics_id = seasonStatistics.sm_id;

		using utils::string_conversion::PercentToDouble;
		if (!seasonStatisticsNode[GOALS_SCORED_MINUTES].isNull())
		{
			gsm.interval = StdStringToUtilsString(_0_15);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_0_15].asString());
			goalsScoredminutes.insert(gsm);
			gsm.interval = StdStringToUtilsString(_15_30);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_15_30].asString());
			goalsScoredminutes.insert(gsm);
			gsm.interval = StdStringToUtilsString(_30_45);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_30_45].asString());
			goalsScoredminutes.insert(gsm);
			gsm.interval = StdStringToUtilsString(_45_60);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_45_60].asString());
			goalsScoredminutes.insert(gsm);
			gsm.interval = StdStringToUtilsString(_60_75);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_60_75].asString());
			goalsScoredminutes.insert(gsm);
			gsm.interval = StdStringToUtilsString(_75_90);
			gsm.percentage = PercentToDouble(seasonStatisticsNode[GOALS_SCORED_MINUTES][_75_90].asString());
			goalsScoredminutes.insert(gsm);
			seasonStatistics.goalsScoredMinutes = std::move(goalsScoredminutes);
		}
		if (!seasonStatisticsNode[GOAL_LINE].isNull())
		{
			std::set<GoalLine, GoalLineCmp> goalLines;
			GoalLine gl;
			gl.sm_season_statistics_id = seasonStatistics.sm_id;
			gl.line_over = StdStringToUtilsString(_0_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_0_5].asDouble();
			goalLines.insert(gl);
			gl.line_over = StdStringToUtilsString(_1_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_1_5].asDouble();
			goalLines.insert(gl);
			gl.line_over = StdStringToUtilsString(_2_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_2_5].asDouble();
			goalLines.insert(gl);
			gl.line_over = StdStringToUtilsString(_3_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_3_5].asDouble();
			goalLines.insert(gl);
			gl.line_over = StdStringToUtilsString(_4_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_4_5].asDouble();
			goalLines.insert(gl);
			gl.line_over = StdStringToUtilsString(_5_5);
			gl.percentage = seasonStatisticsNode[GOAL_LINE][OVER][_5_5].asDouble();
			goalLines.insert(gl);
			seasonStatistics.goalLines = std::move(goalLines);
		}
	}


	Pagination& ParsePagination(const Json::Value& paginationNode, Pagination& pagination)
	{
		pagination.totalObjects = paginationNode[TOTAL].asInt();
		pagination.count = paginationNode[COUNT].asInt();
		pagination.perPage = paginationNode[PER_PAGE].asInt();
		pagination.currentPage = paginationNode[CURRENT_PAGE].asInt();
		pagination.totalPages = paginationNode[TOTAL_PAGES].asInt();
		return pagination;
	}
}
