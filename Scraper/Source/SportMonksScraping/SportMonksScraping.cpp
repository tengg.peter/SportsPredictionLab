#include "DatabaseHandler/DatabaseHandler.h"
#include "Scraper/Source/Constants.h"
#include "Scraper/Source/SportMonksScraping/JsonKeys.h"
#include "Scraper/Source/SportMonksScraping/Parsing.h"
#include "Scraper/Source/SportMonksScraping/SportMonksScraping.h"

#include "Utils/Utils.h"

//3rd party
#include <curl/curl.h>
//std
#include <exception>
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace scraper::sport_monks
{
	using namespace database_handler;
	using namespace database_handler::common;
	using namespace database_handler::sm_cache;
	using namespace database_handler::v2;
	using namespace json_keys;

	using std::endl;
	using std::vector;
	using utils::string_conversion::PercentToDouble;
	using utils::string_conversion::StdStringToUtilsString;
	using utils::string_conversion::UtilsStringToStdString;

	namespace
	{
		size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
		{
			size_t realsize = size * nmemb;
			std::string& str{ *(std::string*)userp };

			char* content = static_cast<char*>(contents);
			str.insert(std::end(str), content, content + realsize);

			return realsize;
		}

		//implements RAII for the Curl instance
		class CurlQueryExecutor
		{
		public:
			std::string content;

			CurlQueryExecutor()
			{
				m_pCurl = curl_easy_init();
			}
			~CurlQueryExecutor()
			{
				curl_easy_cleanup(m_pCurl);
			}

			CURLcode ExecuteQuery(const std::string& query)
			{
				//code from: https://curl.haxx.se/libcurl/c/Scrapeinmemory.html
				if (nullptr == m_pCurl)
				{
					throw std::runtime_error("Curl could not be initialised");
				}

				curl_easy_setopt(m_pCurl, CURLOPT_URL, query.c_str());
				/* example.com is redirected, so we tell libcurl to follow redirection */
				curl_easy_setopt(m_pCurl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
				/* we pass our 'chunk' struct to the callback function */
				curl_easy_setopt(m_pCurl, CURLOPT_WRITEDATA, (void*)&content);
				/* some servers don't like requests that are made without a user-agent field, so we provide one */
				curl_easy_setopt(m_pCurl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

				/* Perform the request, res will Scrape the return code */
				return curl_easy_perform(m_pCurl);
			}

		private:
			CURL* m_pCurl = nullptr;
		};

		std::string ExecuteLibCurlQuery(const std::string& url)
		{
			CurlQueryExecutor executor;

			/* Check for errors */
			const CURLcode res = executor.ExecuteQuery(url);
			if (CURLcode::CURLE_OK != res)
			{
				//fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
				throw std::runtime_error(std::string("curl_easy_perform() failed: %s\n") + curl_easy_strerror(res));
			}
			else
			{
				return executor.content;
			}
		}

		std::string CacheLookupOrDownload(const std::string& query, bool forceDownload = false)
		{
			auto& db = GetDb(DbType::SportMonksCache);
			if (!forceDownload)
			{
				std::vector<UpCacheEntry> cache = Query<CacheEntry>(db, "query = '" + query + "'");
				if (!cache.empty())
				{
					return utils::string_conversion::UtilsStringToStdString(cache.front()->json_response);
				}
			}

			std::string jsonString = ExecuteLibCurlQuery(query);
			if (TooManyAttemptsException::TOO_MANY_ATTEMPTS == jsonString)
			{
				throw TooManyAttemptsException();
			}
			std::vector<UpCacheEntry> cacheVec;
			cacheVec.emplace_back(std::make_unique<CacheEntry>());
			cacheVec.back()->query = StdStringToUtilsString(query);
			cacheVec.back()->json_response = StdStringToUtilsString(jsonString);
			cacheVec.back()->updated = StdStringToUtilsString(utils::date_and_time::DateTime::Now().ToSQLiteDateTimeFormat());
			InsertOr(common::OnConflict::Replace, db, cacheVec);

			return jsonString;
		}

		class Pager
		{
		public:
			explicit Pager(const std::string& query, bool forceDownload)
				: m_query(query)
				, m_forceDownload(forceDownload)
				, m_pagination()
				, m_jsonString()
			{
#undef max
				m_pagination.totalPages = std::numeric_limits<int>::max();
				m_pagination.currentPage = 0;
			}

			bool GetNextPage()
			{
				return GetPage(++m_pagination.currentPage);
			}

			bool GetPage(int pageNum)
			{
				const std::string query{ m_query + "&page=" + std::to_string(pageNum) };

				m_jsonString = CacheLookupOrDownload(query, m_forceDownload);

				Json::Value rootJson;
				Json::Reader reader;
				reader.parse(m_jsonString, rootJson);
				parsing::ParsePagination(rootJson[META][PAGINATION], m_pagination);
				bool finished = m_pagination.currentPage > m_pagination.totalPages;
				if (!finished)
				{
					TCOUT << "Got page " << m_pagination.currentPage << "/" << m_pagination.totalPages << endl;
				}
				return !finished;
			}

			std::string JsonString() const
			{
				return m_jsonString;
			}

		private:
			const std::string m_query;
			const bool m_forceDownload;
			parsing::Pagination m_pagination;
			std::string m_jsonString;
		};
	}

	std::vector<UpContinent> ScrapeAllContinents()
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/continents?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::vector<UpContinent> continents;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			continents.emplace_back(std::make_unique<Continent>());
			continents.back()->sm_id = rootJson[DATA][i][ID].asInt64();
			continents.back()->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());
		}

		return continents;
	}

	std::set<UpCountry, CountryCmp> ScrapeAllCountries(bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/countries?api_token=" + SPORT_MONKS_TOKEN };

		const auto continents = database_handler::v2::QueryAll<Continent>(database_handler::common::GetDb(DbType::V2));
		std::set<UpCountry, CountryCmp> countries;

		Pager pager{ query, forceDownload };
		while (pager.GetNextPage())
		{
			std::string jsonString = pager.JsonString();

			Json::Value rootJson;
			Json::Reader reader;
			reader.parse(jsonString, rootJson);


			using namespace json_keys;
			using namespace utils::string_conversion;
			for (int i = 0; i < rootJson[DATA].size(); i++)
			{
				UpCountry upCountry = std::make_unique<Country>();
				parsing::ParseCountry(rootJson[DATA][i], *upCountry, continents);
				countries.insert(std::move(upCountry));
			}
		}

		return countries;
	}

	UpCountry ScrapeCountry(int64_t countryId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/countries/" + std::to_string(countryId) +
			"?api_token=" + SPORT_MONKS_TOKEN };
		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		const auto continents = database_handler::v2::QueryAll<Continent>(database_handler::common::GetDb(DbType::V2));

		using namespace json_keys;
		UpCountry upCountry = std::make_unique<Country>();
		parsing::ParseCountry(rootJson[DATA], *upCountry, continents);

		return upCountry;
	}

	std::set<UpLeague, LeagueCmp> ScrapeAllLeagues(bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/leagues?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpLeague, LeagueCmp> leagues;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpLeague upLeague{ std::make_unique<League>() };
			parsing::ParseLeague(rootJson[DATA][i], *upLeague);
			leagues.insert(std::move(upLeague));
		}

		return leagues;
	}

	std::set<UpSeason, SeasonCmp> ScrapeAllSeasons(bool forceDownload)
	{
		std::set<UpSeason, SeasonCmp> seasons;

		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/seasons?api_token=" + SPORT_MONKS_TOKEN };

		Pager pager(query, forceDownload);
		while (pager.GetNextPage())
		{
			std::string jsonString = pager.JsonString();

			Json::Value rootJson;
			Json::Reader reader;
			reader.parse(jsonString, rootJson);

			using namespace json_keys;
			for (int i = 0; i < rootJson[DATA].size(); i++)
			{
				UpSeason upSeason{ std::make_unique<Season>() };
				parsing::ParseSeason(rootJson[DATA][i], *upSeason);
				seasons.insert(std::move(upSeason));
			}
		}
		return seasons;
	}

	std::set<UpFixture, FixtureCmp> ScrapeFixturesByYear(const std::string& year, bool forceDownload)
	{
		return ScrapeFixturesBetweenDates(year + "-01-01", year + "-12-31");
	}

	std::set<std::unique_ptr<database_handler::v2::Fixture>, database_handler::v2::FixtureCmp> ScrapeFixturesBetweenDates(
		const std::string& startDate,
		const std::string& endDate,
		bool forceDownload)
	{
		std::set<UpFixture, FixtureCmp> fixtureSet;

		const std::string query{
			"https://soccer.sportmonks.com/api/v2.0/fixtures/between/" +
			startDate + "/" + endDate + "?include=stats,lineup,bench,sidelined,trends&api_token=" + SPORT_MONKS_TOKEN };

		Pager pager(query, forceDownload);
		while (pager.GetNextPage())
		{
			std::string jsonString = pager.JsonString();
			Json::Value rootJson;
			Json::Reader reader;
			reader.parse(jsonString, rootJson);
			if (!reader.good())
			{
				TCOUT << reader.getFormattedErrorMessages().c_str() << endl;
			}

			using namespace json_keys;
			for (int i = 0; i < rootJson[DATA].size(); i++)
			{
				UpFixture upFixture{ std::make_unique<Fixture>() };
				parsing::ParseFixture(rootJson[DATA][i], *upFixture);
				parsing::ParseMatchStatistics(rootJson[DATA][i][STATS], *upFixture);
				parsing::ParseLineups(rootJson[DATA][i][LINEUP], *upFixture);
				parsing::ParseLineups(rootJson[DATA][i][BENCH], *upFixture);
				parsing::ParseSidelines(rootJson[DATA][i][SIDELINED], *upFixture);
				parsing::ParseTrends(rootJson[DATA][i][TRENDS], *upFixture);
				fixtureSet.insert(std::move(upFixture));
			}
		}
		return fixtureSet;
	}

	std::set<int64_t> ScrapeDeletedStatus(const std::set<int64_t>& fixtureIds)
	{
		std::set<int64_t> retDeletedIds;

		for (const int64_t fixtureId : fixtureIds)
		{
			//https://soccer.sportmonks.com/api/v2.0/fixtures/16485768?api_token=
			const std::string query{
				"https://soccer.sportmonks.com/api/v2.0/fixtures/" + std::to_string(fixtureId) +
				"?api_token=" + SPORT_MONKS_TOKEN
			};

			std::string jsonString = CacheLookupOrDownload(query, true /*forceDownload*/);
			Json::Value rootJson;
			Json::Reader reader;
			reader.parse(jsonString, rootJson);
			if (!reader.good())
			{
				TCOUT << reader.getFormattedErrorMessages().c_str() << endl;
			}

			using namespace json_keys;
			UpFixture upFixture{ std::make_unique<Fixture>() };
			parsing::ParseFixture(rootJson[DATA], *upFixture);
			if (upFixture->deleted)
			{
				retDeletedIds.insert(upFixture->sm_id);
			}
		}
		return retDeletedIds;
	}

	std::set<UpCommentary, CommentaryCmp> ScrapeCommentaries(int64_t fixtureId, bool forceDownload)
	{
		std::set<UpCommentary, CommentaryCmp> commentarySet;	//set is used to filter out duplicates
		std::string query{ "https://soccer.sportmonks.com/api/v2.0/commentaries/fixture/" +
			std::to_string(fixtureId) + "?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonResponse = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonResponse, rootJson);

		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			auto upCommentary = std::make_unique<Commentary>();
			if (parsing::ParseCommentaries(rootJson[DATA][i], *upCommentary))
			{
				commentarySet.emplace(std::move(upCommentary));
			}
		}

		return commentarySet;
	}

	std::set<UpTeam, TeamCmp> ScrapeTeamsBySeason(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/teams/season/" + std::to_string(seasonId) +
			"?include=stats&api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		std::set<UpTeam, TeamCmp> teamSet;
		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpTeam upTeam{ std::make_unique<Team>() };
			parsing::ParseTeam(rootJson[DATA][i], *upTeam);
			teamSet.insert(std::move(upTeam));
		}
		return teamSet;
	}

	UpTeam ScrapeTeam(int64_t teamId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/teams/" + std::to_string(teamId) +
			"?include=stats&api_token=" + SPORT_MONKS_TOKEN };
		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		UpTeam upTeam{ std::make_unique<Team>() };
		parsing::ParseTeam(rootJson[DATA], *upTeam);
		return upTeam;
	}

	std::set<UpPlayerPerformance, PlayerPerformanceCmp> ScrapePlayerPerformances(int64_t seasonId, int64_t teamId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/squad/season/" + std::to_string(seasonId) +
			"/team/" + std::to_string(teamId) + "?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpPlayerPerformance, PlayerPerformanceCmp> playerPerformanceSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpPlayerPerformance upPlayerPerformance{ std::make_unique<PlayerPerformance>() };

			upPlayerPerformance->sm_player_id = rootJson[DATA][i][PLAYER_ID].asInt64();
			upPlayerPerformance->sm_season_id = seasonId;
			upPlayerPerformance->sm_team_id = teamId;
			upPlayerPerformance->number = rootJson[DATA][i][NUMBER].asInt64();
			upPlayerPerformance->captain = rootJson[DATA][i][CAPTAIN].asInt64();
			upPlayerPerformance->injured = rootJson[DATA][i][INJURED].asBool();
			upPlayerPerformance->minutes = rootJson[DATA][i][MINUTES].asInt64();
			upPlayerPerformance->appearences = rootJson[DATA][i][APPEARENCES].asInt64();
			upPlayerPerformance->lineups = rootJson[DATA][i][LINEUPS].asInt64();
			upPlayerPerformance->substitute_in = rootJson[DATA][i][SUBSTITUTE_IN].asInt64();
			upPlayerPerformance->substitute_out = rootJson[DATA][i][SUBSTITUTE_OUT].asInt64();
			upPlayerPerformance->substitutes_on_bench = rootJson[DATA][i][SUBSTITUTES_ON_BENCH].asInt64();
			upPlayerPerformance->goals = rootJson[DATA][i][GOALS].asInt64();
			upPlayerPerformance->assists = rootJson[DATA][i][ASSISTS].asInt64();
			if (!rootJson[DATA][i][SAVES].isNull())
			{
				upPlayerPerformance->saves_opt = rootJson[DATA][i][SAVES].asInt64();
			}
			if (!rootJson[DATA][i][INSIDE_BOX_SAVES].isNull())
			{
				upPlayerPerformance->inside_box_saves_opt = rootJson[DATA][i][INSIDE_BOX_SAVES].asInt64();
			}
			if (!rootJson[DATA][i][DISPOSSESED].isNull())
			{
				upPlayerPerformance->dispossesed_opt = rootJson[DATA][i][DISPOSSESED].asInt64();
			}
			if (!rootJson[DATA][i][INTERCEPTIONS].isNull())
			{
				upPlayerPerformance->interceptions_opt = rootJson[DATA][i][INTERCEPTIONS].asInt64();
			}
			upPlayerPerformance->yellowcards = rootJson[DATA][i][YELLOWCARDS].asInt64();
			upPlayerPerformance->yellowred = rootJson[DATA][i][YELLOWRED].asInt64();
			upPlayerPerformance->redcards = rootJson[DATA][i][REDCARDS].asInt64();
			if (!rootJson[DATA][i][TACKLES].isNull())
			{
				upPlayerPerformance->tackles_opt = rootJson[DATA][i][TACKLES].asInt64();
			}
			if (!rootJson[DATA][i][BLOCKS].isNull())
			{
				upPlayerPerformance->blocks_opt = rootJson[DATA][i][BLOCKS].asInt64();
			}
			if (!rootJson[DATA][i][HIT_POST].isNull())
			{
				upPlayerPerformance->hit_post_opt = rootJson[DATA][i][HIT_POST].asInt64();
			}
			if (!rootJson[DATA][i][FOULS][COMMITTED].isNull())
			{
				upPlayerPerformance->fouls_committed_opt = rootJson[DATA][i][FOULS][COMMITTED].asInt64();
			}
			if (!rootJson[DATA][i][FOULS][DRAWN].isNull())
			{
				upPlayerPerformance->fouls_drawn_opt = rootJson[DATA][i][FOULS][DRAWN].asInt64();
			}
			if (!rootJson[DATA][i][CROSSES][TOTAL].isNull())
			{
				upPlayerPerformance->crosses_total_opt = rootJson[DATA][i][CROSSES][TOTAL].asInt64();
			}
			if (!rootJson[DATA][i][CROSSES][ACCURATE].isNull())
			{
				upPlayerPerformance->crosses_accurate_opt = rootJson[DATA][i][CROSSES][ACCURATE].asInt64();
			}
			if (!rootJson[DATA][i][DRIBBLES][ATTEMPTS].isNull())
			{
				upPlayerPerformance->dribbles_attempts_opt = rootJson[DATA][i][DRIBBLES][ATTEMPTS].asInt64();
			}
			if (!rootJson[DATA][i][DRIBBLES][SUCCESS].isNull())
			{
				upPlayerPerformance->dribbles_success_opt = rootJson[DATA][i][DRIBBLES][SUCCESS].asInt64();
			}
			if (!rootJson[DATA][i][DRIBBLES][DRIBBLED_PAST].isNull())
			{
				upPlayerPerformance->dribbled_past_opt = rootJson[DATA][i][DRIBBLES][DRIBBLED_PAST].asInt64();
			}
			if (!rootJson[DATA][i][DUELS][TOTAL].isNull())
			{
				upPlayerPerformance->duels_total_opt = rootJson[DATA][i][DUELS][TOTAL].asInt64();
			}
			if (!rootJson[DATA][i][DUELS][WON].isNull())
			{
				upPlayerPerformance->duels_won_opt = rootJson[DATA][i][DUELS][WON].asInt64();
			}
			if (!rootJson[DATA][i][PASSES][TOTAL].isNull())
			{
				upPlayerPerformance->passes_total_opt = rootJson[DATA][i][PASSES][TOTAL].asInt64();
			}
			if (!rootJson[DATA][i][PASSES][ACCURACY].isNull())
			{
				upPlayerPerformance->passes_accuracy_opt = rootJson[DATA][i][PASSES][ACCURACY].asDouble();
			}
			if (!rootJson[DATA][i][KEY_PASSES].isNull())
			{
				upPlayerPerformance->key_passes_opt = rootJson[DATA][i][KEY_PASSES].asInt64();
			}
			if (!rootJson[DATA][i][PENALTIES][WON].isNull())
			{
				upPlayerPerformance->penalties_won_opt = rootJson[DATA][i][PENALTIES][WON].asInt64();
			}
			if (!rootJson[DATA][i][PENALTIES][SCORES].isNull())
			{
				upPlayerPerformance->penalties_scored_opt = rootJson[DATA][i][PENALTIES][SCORES].asInt64();
			}
			if (!rootJson[DATA][i][PENALTIES][MISSED].isNull())
			{
				upPlayerPerformance->penalties_missed_opt = rootJson[DATA][i][PENALTIES][MISSED].asInt64();
			}
			if (!rootJson[DATA][i][PENALTIES][COMMITTED].isNull())
			{
				upPlayerPerformance->penalties_committed_opt = rootJson[DATA][i][PENALTIES][COMMITTED].asInt64();
			}
			if (!rootJson[DATA][i][PENALTIES][SAVES].isNull())
			{
				upPlayerPerformance->penalties_saved_opt = rootJson[DATA][i][PENALTIES][SAVES].asInt64();
			}
			if (!rootJson[DATA][i][SHOTS][SHOTS_TOTAL].isNull())
			{
				upPlayerPerformance->shots_total_opt = rootJson[DATA][i][SHOTS][SHOTS_TOTAL].asInt64();
			}
			if (!rootJson[DATA][i][SHOTS][SHOTS_ON_TARGET].isNull())
			{
				upPlayerPerformance->shots_on_target_opt = rootJson[DATA][i][SHOTS][SHOTS_ON_TARGET].asInt64();
			}
			if (!rootJson[DATA][i][SHOTS][SHOTS_OFF_TARGET].isNull())
			{
				upPlayerPerformance->shots_off_target_opt = rootJson[DATA][i][SHOTS][SHOTS_OFF_TARGET].asInt64();
			}

			playerPerformanceSet.insert(std::move(upPlayerPerformance));
		}

		return playerPerformanceSet;
	}

	UpPlayer ScrapePlayer(int64_t playerId)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/players/" + std::to_string(playerId) +
			"?include=sidelined,transfers&api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		if (rootJson[DATA].isArray())
		{
			throw std::runtime_error(parsing::ONLY_ONE_OBJECT_IS_EXPECTED);
		}

		UpPlayer upPlayer{ std::make_unique<Player>() };
		parsing::ParsePlayer(rootJson[DATA], *upPlayer);
		return upPlayer;
	}

	namespace
	{
		TopScorers ScrapeTopScorers(int64_t seasonId, bool aggregated, bool forceDownload)
		{
			const std::string query{ aggregated ?
				"https://soccer.sportmonks.com/api/v2.0/topscorers/season/" + std::to_string(seasonId) +
				"/aggregated?api_token=" + SPORT_MONKS_TOKEN :
				"https://soccer.sportmonks.com/api/v2.0/topscorers/season/" + std::to_string(seasonId) +
				"?api_token=" + SPORT_MONKS_TOKEN
			};
			const std::string assistsScorersStr = aggregated ? AGGREGATED_ASSIST_SCORERS : ASSIST_SCORERS;
			const std::string cardsScorersStr = aggregated ? AGGREGATED_CARD_SCORERS : CARD_SCORERS;
			const std::string goalScorersStr = aggregated ? AGGREGATED_GOAL_SCORERS : GOAL_SCORERS;

			std::string jsonString = CacheLookupOrDownload(query, forceDownload);

			Json::Value rootJson;
			Json::Reader reader;
			reader.parse(jsonString, rootJson);

			TopScorers topScorers;

			for (int i = 0; i < rootJson[DATA][goalScorersStr][DATA].size(); i++)
			{
				UpTopGoalScorer upTopGoalScorer{ std::make_unique<TopGoalScorer>() };
				upTopGoalScorer->position = rootJson[DATA][goalScorersStr][DATA][i][POSITION].asInt();
				upTopGoalScorer->sm_season_id = rootJson[DATA][goalScorersStr][DATA][i][SEASON_ID].asInt64();
				utils::AssertEquals(upTopGoalScorer->sm_season_id, seasonId, parsing::SEASON_ID_SHOULD_BE_THE_SAME);
				upTopGoalScorer->sm_player_id = rootJson[DATA][goalScorersStr][DATA][i][PLAYER_ID].asInt64();
				upTopGoalScorer->sm_team_id = rootJson[DATA][goalScorersStr][DATA][i][TEAM_ID].asInt64();
				if (!rootJson[DATA][goalScorersStr][DATA][i][STAGE_ID].isNull())
				{
					upTopGoalScorer->sm_stage_id_opt = rootJson[DATA][goalScorersStr][DATA][i][STAGE_ID].asInt64();
				}
				upTopGoalScorer->goals = rootJson[DATA][goalScorersStr][DATA][i][GOALS].asInt();
				upTopGoalScorer->penalty_goals = rootJson[DATA][goalScorersStr][DATA][i][PENALTY_GOALS].asInt();
				upTopGoalScorer->type = StdStringToUtilsString(rootJson[DATA][goalScorersStr][DATA][i][TYPE].asString());
				topScorers.topGoalScorers.insert(std::move(upTopGoalScorer));
			}

			for (int i = 0; i < rootJson[DATA][assistsScorersStr][DATA].size(); i++)
			{
				UpTopAssistScorer upTopAssistScorer{ std::make_unique<TopAssistScorer>() };
				upTopAssistScorer->position = rootJson[DATA][assistsScorersStr][DATA][i][POSITION].asInt();
				upTopAssistScorer->sm_season_id = rootJson[DATA][assistsScorersStr][DATA][i][SEASON_ID].asInt64();
				utils::AssertEquals(upTopAssistScorer->sm_season_id, seasonId, parsing::SEASON_ID_SHOULD_BE_THE_SAME);
				upTopAssistScorer->sm_player_id = rootJson[DATA][assistsScorersStr][DATA][i][PLAYER_ID].asInt64();
				upTopAssistScorer->sm_team_id = rootJson[DATA][assistsScorersStr][DATA][i][TEAM_ID].asInt64();
				if (!rootJson[DATA][assistsScorersStr][DATA][i][STAGE_ID].isNull())
				{
					upTopAssistScorer->sm_stage_id_opt = rootJson[DATA][assistsScorersStr][DATA][i][STAGE_ID].asInt64();
				}
				upTopAssistScorer->assists = rootJson[DATA][assistsScorersStr][DATA][i][ASSISTS].asInt();
				upTopAssistScorer->type = StdStringToUtilsString(rootJson[DATA][assistsScorersStr][DATA][i][TYPE].asString());
				topScorers.topAssistScorers.insert(std::move(upTopAssistScorer));
			}

			for (int i = 0; i < rootJson[DATA][cardsScorersStr][DATA].size(); i++)
			{
				UpTopCardScorer upTopCardScorer{ std::make_unique<TopCardScorer>() };
				upTopCardScorer->position = rootJson[DATA][cardsScorersStr][DATA][i][POSITION].asInt();
				upTopCardScorer->sm_season_id = rootJson[DATA][cardsScorersStr][DATA][i][SEASON_ID].asInt64();
				utils::AssertEquals(upTopCardScorer->sm_season_id, seasonId, parsing::SEASON_ID_SHOULD_BE_THE_SAME);
				upTopCardScorer->sm_player_id = rootJson[DATA][cardsScorersStr][DATA][i][PLAYER_ID].asInt64();
				upTopCardScorer->sm_team_id = rootJson[DATA][cardsScorersStr][DATA][i][TEAM_ID].asInt64();
				if (!rootJson[DATA][cardsScorersStr][DATA][i][STAGE_ID].isNull())
				{
					upTopCardScorer->sm_stage_id_opt = rootJson[DATA][cardsScorersStr][DATA][i][STAGE_ID].asInt64();
				}
				upTopCardScorer->yellowcards = rootJson[DATA][cardsScorersStr][DATA][i][YELLOWCARDS].asInt();
				upTopCardScorer->redcards = rootJson[DATA][cardsScorersStr][DATA][i][REDCARDS].asInt();
				upTopCardScorer->type = StdStringToUtilsString(rootJson[DATA][cardsScorersStr][DATA][i][TYPE].asString());
				topScorers.topCardScorers.insert(std::move(upTopCardScorer));
			}

			return topScorers;
		}
	}

	TopScorers ScrapeTopScorers(int64_t seasonId, bool forceDownload)
	{
		return ScrapeTopScorers(seasonId, false, forceDownload);
	}

	TopScorers ScrapeAggregatedTopScorers(int64_t seasonId, bool forceDownload)
	{
		return ScrapeTopScorers(seasonId, true, forceDownload);
	}

	std::set<UpStanding, StandingCmp> ScrapeStandings(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/standings/season/" + std::to_string(seasonId) +
			"?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpStanding, StandingCmp> standingSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpStanding upStanding{ std::make_unique<Standing>() };

			upStanding->sm_id = rootJson[DATA][i][ID].asInt64();
			upStanding->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());
			upStanding->sm_league_id = rootJson[DATA][i][LEAGUE_ID].asInt64();
			upStanding->sm_season_id = rootJson[DATA][i][SEASON_ID].asInt64();
			utils::AssertEquals(upStanding->sm_season_id, seasonId, parsing::SEASON_ID_SHOULD_BE_THE_SAME);
			upStanding->sm_round_id = rootJson[DATA][i][ROUND_ID].asInt64();
			upStanding->round_name = rootJson[DATA][i][ROUND_NAME].asInt64();
			upStanding->type = StdStringToUtilsString(rootJson[DATA][i][TYPE].asString());
			upStanding->sm_stage_id = rootJson[DATA][i][STAGE_ID].asInt64();
			upStanding->stage_name = StdStringToUtilsString(rootJson[DATA][i][STAGE_NAME].asString());
			upStanding->resource = StdStringToUtilsString(rootJson[DATA][i][RESOURCE].asString());

			for (int j = 0; j < rootJson[DATA][i][STANDINGS][DATA].size(); j++)
			{
				StandingPosition standingPosition;

				standingPosition.sm_standing_id = upStanding->sm_id;
				standingPosition.position = rootJson[DATA][i][STANDINGS][DATA][j][POSITION].asInt64();
				standingPosition.sm_team_id = rootJson[DATA][i][STANDINGS][DATA][j][TEAM_ID].asInt64();
				standingPosition.team_name = StdStringToUtilsString(rootJson[DATA][i][STANDINGS][DATA][j][TEAM_NAME].asString());
				standingPosition.sm_round_id = rootJson[DATA][i][STANDINGS][DATA][j][ROUND_ID].asInt64();
				standingPosition.round_name = rootJson[DATA][i][STANDINGS][DATA][j][ROUND_NAME].asInt64();
				if (!rootJson[DATA][i][STANDINGS][DATA][j][GROUP_ID].isNull())
				{
					standingPosition.sm_group_id_opt = rootJson[DATA][i][STANDINGS][DATA][j][GROUP_ID].asInt64();
				}
#undef GROUP_NAME
				if (!rootJson[DATA][i][STANDINGS][DATA][j][GROUP_NAME].isNull())
				{
					standingPosition.group_name_opt = StdStringToUtilsString(rootJson[DATA][i][STANDINGS][DATA][j][GROUP_NAME].asString());
				}
				standingPosition.home_games_played = rootJson[DATA][i][STANDINGS][DATA][j][HOME][GAMES_PLAYED].asInt64();
				standingPosition.home_won = rootJson[DATA][i][STANDINGS][DATA][j][HOME][WON].asInt64();
				standingPosition.home_draw = rootJson[DATA][i][STANDINGS][DATA][j][HOME][DRAW].asInt64();
				standingPosition.home_lost = rootJson[DATA][i][STANDINGS][DATA][j][HOME][LOST].asInt64();
				standingPosition.home_goals_scored = rootJson[DATA][i][STANDINGS][DATA][j][HOME][GOALS_SCORED].asInt64();
				standingPosition.home_goals_against = rootJson[DATA][i][STANDINGS][DATA][j][HOME][GOALS_AGAINST].asInt64();
				standingPosition.away_games_played = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][GAMES_PLAYED].asInt64();
				standingPosition.away_won = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][WON].asInt64();
				standingPosition.away_draw = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][DRAW].asInt64();
				standingPosition.away_lost = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][LOST].asInt64();
				standingPosition.away_goals_scored = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][GOALS_SCORED].asInt64();
				standingPosition.away_goals_against = rootJson[DATA][i][STANDINGS][DATA][j][AWAY][GOALS_AGAINST].asInt64();
				if (!rootJson[DATA][i][STANDINGS][DATA][j][RESULT].isNull())
				{
					standingPosition.result_opt = StdStringToUtilsString(rootJson[DATA][i][STANDINGS][DATA][j][RESULT].asString());
				}
				if (!rootJson[DATA][i][STANDINGS][DATA][j][RECENT_FORM].isNull())
				{
					standingPosition.recent_form_opt = StdStringToUtilsString(rootJson[DATA][i][STANDINGS][DATA][j][RECENT_FORM].asString());
				}
				if (!rootJson[DATA][i][STANDINGS][DATA][j][STATUS].isNull())
				{
					standingPosition.status_opt = StdStringToUtilsString(rootJson[DATA][i][STANDINGS][DATA][j][STATUS].asString());
				}

				upStanding->positions.insert(std::move(standingPosition));
			}


			standingSet.insert(std::move(upStanding));
		}

		return standingSet;
	}

	std::set<UpVenue, VenueCmp> ScrapeVenues(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/venues/season/" + std::to_string(seasonId) +
			"?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpVenue, VenueCmp> venueSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpVenue upVenue{ std::make_unique<Venue>() };

			upVenue->sm_id = rootJson[DATA][i][ID].asInt64();
			upVenue->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());
			if (!rootJson[DATA][i][SURFACE].isNull())
			{
				upVenue->surface_opt = StdStringToUtilsString(rootJson[DATA][i][SURFACE].asString());
			}
			if (!rootJson[DATA][i][ADDRESS].isNull())
			{
				upVenue->address_opt = StdStringToUtilsString(rootJson[DATA][i][ADDRESS].asString());
			}
			upVenue->city = StdStringToUtilsString(rootJson[DATA][i][CITY].asString());
			if (!rootJson[DATA][i][CAPACITY].isNull())
			{
				upVenue->capacity_opt = rootJson[DATA][i][CAPACITY].asInt64();
			}
			if (!rootJson[DATA][i][IMAGE_PATH].isNull())
			{
				upVenue->image_path_opt = StdStringToUtilsString(rootJson[DATA][i][IMAGE_PATH].asString());
			}
			if (!rootJson[DATA][i][COORDINATES].isNull())
			{
				upVenue->coordinates_opt = StdStringToUtilsString(rootJson[DATA][i][COORDINATES].asString());
			}
			venueSet.insert(std::move(upVenue));
		}

		return venueSet;
	}

	std::set<UpRound, RoundCmp> ScrapeRounds(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/rounds/season/" + std::to_string(seasonId) +
			"?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpRound, RoundCmp> roundSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpRound upRound{ std::make_unique<Round>() };

			upRound->sm_id = rootJson[DATA][i][ID].asInt64();
			upRound->name = rootJson[DATA][i][NAME].asInt64();
			upRound->sm_league_id = rootJson[DATA][i][LEAGUE_ID].asInt64();
			upRound->sm_season_id = rootJson[DATA][i][SEASON_ID].asInt64();
			upRound->sm_stage_id = rootJson[DATA][i][STAGE_ID].asInt64();
			upRound->start = StdStringToUtilsString(rootJson[DATA][i][START].asString());
			upRound->end = StdStringToUtilsString(rootJson[DATA][i][END].asString());

			roundSet.insert(std::move(upRound));
		}

		return roundSet;
	}

	std::set<UpBookmaker, BookmakerCmp> ScrapeAllBookmakers()
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/bookmakers?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpBookmaker, BookmakerCmp> bookmakerSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpBookmaker upBookmaker{ std::make_unique<Bookmaker>() };

			upBookmaker->sm_id = rootJson[DATA][i][ID].asInt64();
			upBookmaker->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());
			if (!rootJson[DATA][i][LOGO].isNull())
			{
				upBookmaker->logo_opt = StdStringToUtilsString(rootJson[DATA][i][LOGO].asString());
			}

			bookmakerSet.insert(std::move(upBookmaker));
		}

		return bookmakerSet;
	}

	std::set<UpMarket, MarketCmp> ScrapeAllMarkets()
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/markets?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpMarket, MarketCmp> marketSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpMarket upMarket{ std::make_unique<Market>() };

			upMarket->sm_id = rootJson[DATA][i][ID].asInt64();
			upMarket->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());

			marketSet.insert(std::move(upMarket));
		}

		return marketSet;
	}

	std::set<UpOdds, OddsCmp> ScrapeOdds(int64_t fixtureId, const std::set<int64_t>& bookmakerIds, bool forceDownload)
	{
		const std::string ids{ utils::string_conversion::UtilsStringToStdString((utils::string_operations::ContainerToCsvString(bookmakerIds, ','))) };
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/fixtures/" + std::to_string(fixtureId) +
			"?include=flatOdds&bookmakers=" + ids + "&api_token=mG4rJvhR8dHGVfRBPail3VcEHyXKFEo9lrrjBvVTfvrfxH8v6GYmo58JfMg9" };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpOdds, OddsCmp> oddsSet;
		using namespace json_keys;
		parsing::ParseFlatOdds(rootJson[DATA][FLATODDS], oddsSet, bookmakerIds, fixtureId);
		return oddsSet;
	}

	UpCoach ScrapeCoach(int64_t coachId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/coaches/" + std::to_string(coachId) +
			"?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);
		TCOUT << jsonString.c_str() << "\n\n" << endl;
		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		using namespace json_keys;
		if (rootJson[DATA].isArray())
		{
			throw std::runtime_error(parsing::ONLY_ONE_OBJECT_IS_EXPECTED);
		}

		UpCoach upCoach{ std::make_unique<Coach>() };

		upCoach->sm_id = rootJson[DATA][COACH_ID].asInt64();
		upCoach->sm_team_id = rootJson[DATA][TEAM_ID].asInt64();
		upCoach->sm_country_id = rootJson[DATA][COUNTRY_ID].asInt64();
		upCoach->common_name = StdStringToUtilsString(rootJson[DATA][COMMON_NAME].asString());
		upCoach->fullname = StdStringToUtilsString(rootJson[DATA][FULLNAME].asString());
		if (!rootJson[DATA][FIRSTNAME].isNull())
		{
			upCoach->firstname_opt = StdStringToUtilsString(rootJson[DATA][FIRSTNAME].asString());
		}
		if (!rootJson[DATA][LASTNAME].isNull())
		{
			upCoach->lastname_opt = StdStringToUtilsString(rootJson[DATA][LASTNAME].asString());
		}
		upCoach->nationality = StdStringToUtilsString(rootJson[DATA][NATIONALITY].asString());
		if (!rootJson[DATA][BIRTHDATE].isNull())
		{
			upCoach->birthdate_opt = StdStringToUtilsString(rootJson[DATA][BIRTHDATE].asString());
		}
		if (!rootJson[DATA][BIRTHCOUNTRY].isNull())
		{
			upCoach->birthcountry_opt = StdStringToUtilsString(rootJson[DATA][BIRTHCOUNTRY].asString());
		}
		if (!rootJson[DATA][BIRTHPLACE].isNull())
		{
			upCoach->birthplace_opt = StdStringToUtilsString(rootJson[DATA][BIRTHPLACE].asString());
		}
		if (!rootJson[DATA][IMAGE_PATH].isNull())
		{
			upCoach->image_path_opt = StdStringToUtilsString(rootJson[DATA][IMAGE_PATH].asString());
		}

		return upCoach;
	}

	std::set<UpStage, StageCmp> ScrapeStages(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/stages/season/" + std::to_string(seasonId) +
			"?api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		std::set<UpStage, StageCmp> stageSet;
		using namespace json_keys;
		for (int i = 0; i < rootJson[DATA].size(); i++)
		{
			UpStage upStage{ std::make_unique<Stage>() };

			upStage->sm_id = rootJson[DATA][i][ID].asInt64();
			upStage->name = StdStringToUtilsString(rootJson[DATA][i][NAME].asString());
			upStage->type = StdStringToUtilsString(rootJson[DATA][i][TYPE].asString());
			upStage->sm_league_id = rootJson[DATA][i][LEAGUE_ID].asInt64();
			upStage->sm_season_id = rootJson[DATA][i][SEASON_ID].asInt64();
			if (!rootJson[DATA][i][SORT_ORDER].isNull())
			{
				upStage->sort_order_opt = rootJson[DATA][i][SORT_ORDER].asInt64();
			}
			upStage->has_standings = rootJson[DATA][i][HAS_STANDINGS].asBool();

			stageSet.insert(std::move(upStage));
		}

		return stageSet;
	}

	UpSeasonStatistics ScrapeSeasonStatistics(int64_t seasonId, bool forceDownload)
	{
		const std::string query{ "https://soccer.sportmonks.com/api/v2.0/seasons/" + std::to_string(seasonId) +
			"?include=stats&api_token=" + SPORT_MONKS_TOKEN };

		std::string jsonString = CacheLookupOrDownload(query, forceDownload);

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		using namespace json_keys;
		if (rootJson[DATA].isArray())
		{
			throw std::runtime_error(parsing::ONLY_ONE_OBJECT_IS_EXPECTED);
		}

		const Json::Value& seasonStatsNode = rootJson[DATA][STATS][DATA];
		if(seasonStatsNode.isNull() || seasonStatsNode.empty())
		{
			return nullptr;
		}

		UpSeasonStatistics upSeasonStatistics{ std::make_unique<SeasonStatistics>() };
		parsing::ParseSeasonStatistics(rootJson[DATA][STATS][DATA], *upSeasonStatistics);
		return upSeasonStatistics;
	}
}