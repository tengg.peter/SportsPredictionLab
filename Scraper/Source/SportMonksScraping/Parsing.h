#pragma once
#include "DatabaseHandler/Source/v2/DataClasses/ForwDecl.h"

#include <string>

namespace Json { class Value; }

namespace scraper::sport_monks::parsing
{
	const std::string SEASON_ID_SHOULD_BE_THE_SAME{ "Season ID should be the same as the query parameter." };
	const std::string FIXTURE_ID_SHOULD_BE_THE_SAME{ "Fixture ID should be the same as the query parameter." };
	const std::string ONLY_ONE_OBJECT_IS_EXPECTED{ "Only one object is expected in the json response." };

	void ParseCountry(
		const Json::Value& countryNode,
		database_handler::v2::Country& country,
		const std::vector<database_handler::v2::UpContinent>& continents);
	void ParseLeague(const Json::Value& leagueNode, database_handler::v2::League& league);
	void ParseFixture(const Json::Value& fixtureNode, database_handler::v2::Fixture& fixture);
	void ParseTeam(const Json::Value& teamNode, database_handler::v2::Team& team);
	void ParseMatchStatistics(const Json::Value& statsNode, database_handler::v2::Fixture& fixture);
	void ParseLineups(const Json::Value& lineupsNode, database_handler::v2::Fixture& fixture);
	void ParseSidelines(const Json::Value& sidelinesNode, database_handler::v2::Fixture& fixture);
	void ParseTrends(const Json::Value& trendsNode, database_handler::v2::Fixture& fixture);
	void ParsePlayer(const Json::Value& playerNode, database_handler::v2::Player& player);
	void ParseSeason(const Json::Value& seasonNode, database_handler::v2::Season& season);
	bool ParseCommentaries(const Json::Value& commentaryNode, database_handler::v2::Commentary& commentary);
	void ParseFlatOdds(
		const Json::Value& flatOddsNode,
		std::set<database_handler::v2::UpOdds, database_handler::v2::OddsCmp>& oddsSet,
		const std::set<int64_t>& bookmakerIds,
		int64_t fixtureId
	);
	void ParseOdds(const Json::Value& oddsNode, database_handler::v2::Odds& odds);
	void ParseSeasonStatistics(const Json::Value& seasonStatisticsNode, database_handler::v2::SeasonStatistics& seasonStatistics);
	struct Pagination
	{
		int totalObjects = 0;
		int count = 0;
		int perPage = 0;
		int currentPage = 0;
		int totalPages = 0;
		std::string nextPage;
	};
	Pagination& ParsePagination(const Json::Value& paginationNode, Pagination& pagination);
}