#pragma once
#include "DatabaseHandler/DatabaseHandler.h"

#include <memory>
#include <string>

namespace scraper::sport_monks
{
	class TooManyAttemptsException : public std::runtime_error
	{
	public:
		static inline const std::string TOO_MANY_ATTEMPTS{ "Too Many Attempts." };

	public:
		TooManyAttemptsException() : std::runtime_error(TOO_MANY_ATTEMPTS) {}
		TooManyAttemptsException(const std::string& message) : std::runtime_error(message) {}
	};

	struct TopScorers
	{
		std::set<std::unique_ptr<database_handler::v2::TopAssistScorer>, database_handler::v2::TopAssistScorerCmp> topAssistScorers;
		std::set<std::unique_ptr<database_handler::v2::TopCardScorer>, database_handler::v2::TopCardScorerCmp> topCardScorers;
		std::set<std::unique_ptr<database_handler::v2::TopGoalScorer>, database_handler::v2::TopGoalScorerCmp> topGoalScorers;
	};

	std::vector<std::unique_ptr<database_handler::v2::Continent>> ScrapeAllContinents();
	std::set<std::unique_ptr<database_handler::v2::Country>, database_handler::v2::CountryCmp> ScrapeAllCountries(bool forceDownload);
	std::unique_ptr<database_handler::v2::Country> ScrapeCountry(int64_t countryId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::League>, database_handler::v2::LeagueCmp> ScrapeAllLeagues(bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Season>, database_handler::v2::SeasonCmp> ScrapeAllSeasons(bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Fixture>, database_handler::v2::FixtureCmp> ScrapeFixturesByYear(
		const std::string& year,
		bool forceDownload = false);	
	std::set<std::unique_ptr<database_handler::v2::Fixture>, database_handler::v2::FixtureCmp> ScrapeFixturesBetweenDates(
		const std::string& startDate,
		const std::string& endDate,
		bool forceDownload = false);
	std::set<int64_t> ScrapeDeletedStatus(const std::set<int64_t>& fixtureIds);
	std::set<std::unique_ptr<database_handler::v2::Commentary>, database_handler::v2::CommentaryCmp> ScrapeCommentaries(
		int64_t fixtureId,
		bool forceDownload = false);
	std::set<std::unique_ptr<database_handler::v2::Team>, database_handler::v2::TeamCmp> ScrapeTeamsBySeason(int64_t seasonId, bool forceDownload);
	std::unique_ptr<database_handler::v2::Team> ScrapeTeam(int64_t teamId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::PlayerPerformance>, database_handler::v2::PlayerPerformanceCmp> ScrapePlayerPerformances(
		int64_t seasonId, int64_t teamId, bool forceDownload);
	std::unique_ptr<database_handler::v2::Player> ScrapePlayer(int64_t playerId);
	TopScorers ScrapeTopScorers(int64_t seasonId, bool forceDownload);
	TopScorers ScrapeAggregatedTopScorers(int64_t seasonId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Standing>, database_handler::v2::StandingCmp> ScrapeStandings(
		int64_t seasonId, 
		bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Venue>, database_handler::v2::VenueCmp> ScrapeVenues(int64_t seasonId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Round>, database_handler::v2::RoundCmp> ScrapeRounds(int64_t seasonId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Bookmaker>, database_handler::v2::BookmakerCmp> ScrapeAllBookmakers();
	std::set<std::unique_ptr<database_handler::v2::Market>, database_handler::v2::MarketCmp> ScrapeAllMarkets();
	std::set<std::unique_ptr<database_handler::v2::Odds>, database_handler::v2::OddsCmp> ScrapeOdds(
		int64_t fixtureId, 
		const std::set<int64_t>& bookmakerIds, 
		bool forceDownload);
	std::unique_ptr<database_handler::v2::Coach> ScrapeCoach(int64_t coachId, bool forceDownload);
	std::set<std::unique_ptr<database_handler::v2::Stage>, database_handler::v2::StageCmp> ScrapeStages(int64_t seasonId, bool forceDownload);
	std::unique_ptr<database_handler::v2::SeasonStatistics>	ScrapeSeasonStatistics(int64_t seasonId, bool forceDownload);
}