﻿#pragma once

#include "Utils/Utils.h"

namespace scraper
{
	const utils::Char* const CHARS_TO_REMOVE_FROM_TEAM_NAMES = STR("åÅæÆñÑøØ");
	const std::string SPORT_MONKS_TOKEN{ "mG4rJvhR8dHGVfRBPail3VcEHyXKFEo9lrrjBvVTfvrfxH8v6GYmo58JfMg9" };
	const utils::String CONCEDED{ STR("conceded") };
	const utils::String SCORED{ STR("scored") };

	namespace continents
	{
		const utils::String AFRICA = STR("Africa");
		const utils::String ASIA = STR("Asia");
		const utils::String EUROPE = STR("Europe");
		const utils::String NC_AMERICA = STR("N/C America");
		const utils::String OCEANIA = STR("Oceania");
		const utils::String SOUTH_AMERICA = STR("South America");
		const utils::String WORLD = STR("World");
	}
	namespace countries
	{
		const utils::String AUSTRALIA = STR("Australia");
		const utils::String CHINA = STR("China PR");
		const utils::String ENGLAND = STR("England");
		const utils::String FRANCE = STR("France");
		const utils::String GERMANY = STR("Germany");
		const utils::String ITALY = STR("Italy");
		const utils::String NETHERLANDS = STR("Netherlands");
		const utils::String NORWAY = STR("Norway");
		const utils::String SCOTLAND = STR("Scotland");
		const utils::String SPAIN = STR("Spain");
		const utils::String USA = STR("USA");
	}

	namespace competitions
	{
		//countries
		namespace africa
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification Africa");
		}
		namespace asia
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification Asia");
		}
		namespace australia
		{
			const utils::String A_LEAGUE = STR("A-League");
		}
		namespace china
		{
			const utils::String CSL = STR("CSL");
		}
		namespace england
		{
			const utils::String PREMIER_LEAGUE = STR("Premier League");
			const utils::String CHAMPIONSHIP = STR("Championship");
		}
		namespace france
		{
			const utils::String LIGUE_1 = STR("Ligue 1");
		}

		namespace germany
		{
			const utils::String BUNDESLIGA = STR("Bundesliga");
		}

		namespace italy
		{
			const utils::String SERIE_A = STR("Serie A");
		}
		namespace netherlands
		{
			const utils::String EREDIVISIE = STR("Eredivisie");
		}
		namespace norway
		{
			const utils::String ELITESERIEN = STR("Eliteserien");
		}
		namespace scotland
		{
			const utils::String PREMIERSHIP = STR("Premiership");
		}
		namespace spain
		{
			const utils::String PRIMERA_DIVISION = STR("Primera División");
		}
		namespace usa
		{
			const utils::String MLS = STR("MLS");
		}

		//continents
		namespace europe
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification Europe");
		}
		namespace nc_america
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification CONCACAF");
		}
		namespace oceania
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification Oceania");
		}
		namespace south_america
		{
			const utils::String WC_QUALIFICATION = STR("WC Qualification South America");
		}

		//for national teams, like England and Italy
		const utils::String NATIONAL = STR("National");
		const utils::String WC_QUALIFICATION_INTERCONT_PLAYOFFS = STR("WC Qualification Intercontinental Play-offs");
	}
}