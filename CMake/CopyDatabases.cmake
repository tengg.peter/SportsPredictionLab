"if (NOT EXISTS \"$<TARGET_FILE_DIR:SportsPredictionLab>/Databases\")
    execute_process(
            COMMAND \"${CMAKE_COMMAND}\" -E make_directory
                \"$<TARGET_FILE_DIR:SportsPredictionLab>/Databases\"
    )
endif()

set(FOOTBALL_V1_DB_SOURCE \"${PROJECT_SOURCE_DIR}/Databases/football_v1.db\")
set(FOOTBALL_V1_DB_TARGET \"$<TARGET_FILE_DIR:SportsPredictionLab>/Databases/football_v1.db\")
if (EXISTS \"${FOOTBALL_V1_DB_SOURCE}\" AND NOT EXISTS \"${FOOTBALL_V1_DB_TARGET}\")
    execute_process(
            COMMAND \"${CMAKE_COMMAND}\" -E copy
                \"${FOOTBALL_V1_DB_SOURCE}\"
                \"${FOOTBALL_V1_DB_TARGET}\"
    )
endif()

set(FOOTBALL_V2_DB_SOURCE \"${PROJECT_SOURCE_DIR}/Databases/football_v2.db\")
set(FOOTBALL_V2_DB_TARGET \"$<TARGET_FILE_DIR:SportsPredictionLab>/Databases/football_v2.db\")
if (EXISTS \"${FOOTBALL_V2_DB_SOURCE}\" AND NOT EXISTS \"${FOOTBALL_V2_DB_TARGET}\")
    execute_process(
            COMMAND \"${CMAKE_COMMAND}\" -E copy
                \"${FOOTBALL_V2_DB_SOURCE}\"
                \"${FOOTBALL_V2_DB_TARGET}\"
    )
endif()"