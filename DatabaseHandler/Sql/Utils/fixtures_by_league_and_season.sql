SELECT * FROM fixtures
WHERE sm_season_id IN
(
	SELECT sm_id FROM seasons
	WHERE sm_league_id IN
	(
		SELECT sm_id FROM leagues
		WHERE name = 'Premier League'
	) AND
		name = '2021/2022'
)
ORDER BY date_time