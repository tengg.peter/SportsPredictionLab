SELECT 
    countries.name AS country, 
    leagues.name AS league, 
    seasons.name AS season, 
    rounds.name AS round, 
    loc_team.name AS local_team, 
    vis_team.name AS visitor_team, 
    fixtures.date_time, 
    stages.name AS stage, 
    stages.type AS stage_type
FROM fixtures
LEFT JOIN seasons ON fixtures.sm_season_id = seasons.sm_id
LEFT JOIN leagues ON seasons.sm_league_id = leagues.sm_id
LEFT JOIN countries ON leagues.sm_country_id = countries.sm_id
LEFT JOIN teams loc_team ON sm_localteam_id = loc_team.sm_id
LEFT JOIN teams vis_team ON sm_visitorteam_id = vis_team.sm_id
LEFT JOIN rounds ON fixtures.sm_round_id = rounds.sm_id
LEFT JOIN stages ON fixtures.sm_stage_id = stages.sm_id
WHERE fixtures.sm_id IN 
(
28288
)