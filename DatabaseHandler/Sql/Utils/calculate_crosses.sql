WITH
total_crosses AS
(
	SELECT sm_fixture_id, sm_team_id, SUM(passing_total_crosses) AS sum_crosses FROM lineups
	WHERE passing_total_crosses IS NOT NULL
	GROUP BY sm_fixture_id, sm_team_id
)
INSERT INTO calculated_match_stats
SELECT 
	NULL,
	f.sm_id AS sm_fixture_id,
	c1.sum_crosses AS home_crosses,
	c2.sum_crosses AS away_crosses
FROM
fixtures f
JOIN (SELECT * FROM total_crosses) c1 ON f.sm_id = c1.sm_fixture_id AND f.sm_localteam_id = c1.sm_team_id
JOIN (SELECT * FROM total_crosses) c2 ON f.sm_id = c2.sm_fixture_id AND f.sm_visitorteam_id = c2.sm_team_id
ORDER BY sm_fixture_id
