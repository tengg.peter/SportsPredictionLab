SELECT sm_id, name, address, t1.coordinates, count_coordinates FROM venues
JOIN
(SELECT coordinates, COUNT(coordinates) AS count_coordinates FROM venues
GROUP BY coordinates
ORDER BY COUNT(coordinates) DESC) t1
ON venues.coordinates = t1.coordinates
WHERE t1.coordinates <> '' AND count_coordinates > 1
ORDER BY count_coordinates DESC, t1.coordinates ASC
