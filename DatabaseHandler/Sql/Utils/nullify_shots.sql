UPDATE match_statistics
SET
    shots_offgoal = shots_total - shots_ongoal,
    shots_blocked = NULL,
    shots_insidebox = NULL,
    shots_outsidebox = NULL
WHERE
    shots_total > 0 AND
    shots_blocked = 0 AND
    shots_insidebox = 0 AND
    shots_outsidebox = 0
