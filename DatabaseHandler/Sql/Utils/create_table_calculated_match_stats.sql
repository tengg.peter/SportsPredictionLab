-- DROP TABLE IF EXISTS calculated_match_stats;
CREATE TABLE IF NOT EXISTS calculated_match_stats(
    id INTEGER PRIMARY KEY,			
    sm_fixture_id INTEGER NOT NULL UNIQUE,
    home_crosses INT,
    away_crosses INT,

    FOREIGN KEY(sm_fixture_id) REFERENCES fixtures(sm_id)
)