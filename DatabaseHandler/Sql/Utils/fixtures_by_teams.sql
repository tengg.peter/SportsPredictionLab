WITH
team_names AS
(
    SELECT sm_id, name FROM teams t
    WHERE 
        t.name = 'Haugesund' OR
        t.name = 'Sandefjord'
),
matches AS
(
    SELECT * FROM fixtures f
    WHERE 
        f.sm_localteam_id IN (SELECT sm_id FROM team_names) AND
        f.sm_visitorteam_id IN (SELECT sm_id FROM team_names)
),
matches_with_names AS
(
	SELECT
		country_name, 
		[name:3] AS league_name, 
		[name:2] AS season_years, 
		'Round ' || [round_name] AS round_name, 
		name AS home_team, 
		[name:1] AS away_team, 
		datetime(date_time, 'localtime') AS date_time, 
		[name:4] AS stage_name, 
		[type:1] AS stage_type,
		[sm_id:1] AS home_team_id,
		[sm_id:2] AS away_team_id,
		sm_id AS fixture_id,
		sm_league_id
	FROM
	(
		SELECT *, r.name AS round_name, c.name AS country_name FROM matches m
		JOIN teams t1 ON m.sm_localteam_id = t1.sm_id
		JOIN teams t2 ON m.sm_visitorteam_id = t2.sm_id
		JOIN seasons s ON m.sm_season_id = s.sm_id
		LEFT JOIN leagues l ON s.sm_league_id = l.sm_id
		LEFT JOIN stages st ON m.sm_stage_id = st.sm_id
		LEFT JOIN rounds r ON m.sm_round_id = r.sm_id
		JOIN countries c ON l.sm_country_id = c.sm_id
	)
)
SELECT * FROM 
matches_with_names