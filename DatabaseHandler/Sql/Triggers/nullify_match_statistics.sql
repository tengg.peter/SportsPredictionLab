CREATE TRIGGER nullify_possession_time_after_insert_match_statistics
    AFTER INSERT ON match_statistics
WHEN NEW.possessiontime = 0
BEGIN
    UPDATE match_statistics
    SET	possessiontime = NULL
    WHERE possessiontime = 0;
END;

CREATE TRIGGER nullify_attacks_after_insert_match_statistics
    AFTER INSERT ON match_statistics
WHEN NEW.attacks = 0 AND NEW.dangerous_attacks = 0
BEGIN
    UPDATE match_statistics
    SET	
        attacks = NULL,
        dangerous_attacks = NULL
    WHERE attacks = 0 AND dangerous_attacks = 0;
END;