-- DROP VIEW IF EXISTS v_potentially_deleted_fixtures;
-- CREATE VIEW v_potentially_deleted_fixtures AS
SELECT sm_id FROM fixtures
WHERE 
    (status = 'NS' OR status = 'TBA') AND
    date_time < DATETIME('NOW')
ORDER BY date_time