-- DROP VIEW IF EXISTS v_current_round_ids;
-- CREATE VIEW v_current_round_ids AS
SELECT sm_round_id FROM
(
	SELECT 
		ROW_NUMBER() OVER
		(
			PARTITION BY sm_season_id
			ORDER BY date ASC
		) rownum
		,* 
	FROM fixtures
	WHERE 
		date >= DATE('now') AND
		sm_round_id <> 0
)
WHERE rownum = 1
