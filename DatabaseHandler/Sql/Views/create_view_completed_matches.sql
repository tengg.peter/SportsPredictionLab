DROP VIEW IF EXISTS v_completed_matches;
CREATE VIEW IF NOT EXISTS v_completed_matches AS
SELECT * FROM fixtures
WHERE status NOT IN ('NS','TBA', 'CANCL', 'POSTP')
ORDER BY date_time