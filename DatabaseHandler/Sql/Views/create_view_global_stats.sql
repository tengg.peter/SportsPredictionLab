DROP VIEW IF EXISTS v_global_stats;
CREATE VIEW IF NOT EXISTS v_global_stats AS
WITH
goal_stats AS
(
    SELECT 
        COUNT(localteam_score) AS goals_sample_size,
        AVG(localteam_score) AS avg_home_goals,
        AVG(visitorteam_score) AS avg_away_goals
    FROM
        v_completed_matches
),
other_stats AS
(
    SELECT 
        COUNT(home_corners) AS corners_sample_size,
        AVG(home_corners) AS avg_home_corners,
        AVG(away_corners) AS avg_away_corners,
        COUNT(home_shots_total) AS shots_total_sample_size,
        AVG(home_shots_total) AS avg_home_shots_total,
        AVG(away_shots_total) AS avg_away_shots_total,
        COUNT(home_shots_ongoal) AS shots_ongoal_sample_size,
        AVG(home_shots_ongoal) AS avg_home_shots_ongoal,
        AVG(away_shots_ongoal) AS avg_away_shots_ongoal,
        COUNT(home_possession) AS possession_sample_size,
        AVG(home_possession) AS avg_home_possession,
        AVG(away_possession) AS avg_away_possession,
        COUNT(home_attacks) AS attacks_sample_size,
        AVG(home_attacks) AS avg_home_attacks,
        AVG(away_attacks) AS avg_away_attacks,
        COUNT(home_dangerous_attacks) AS dangerous_attacks_sample_size,
        AVG(home_dangerous_attacks) AS avg_home_dangerous_attacks,
        AVG(away_dangerous_attacks) AS avg_away_dangerous_attacks
    FROM v_fixtures_with_stats
),
cross_stats AS
(
    SELECT
        COUNT(home_crosses) AS crosses_sample_size,
        AVG(home_crosses) AS avg_home_crosses,
        AVG(away_crosses) AS avg_away_crosses
    FROM
        calculated_match_stats
)
SELECT * FROM
goal_stats, other_stats, cross_stats