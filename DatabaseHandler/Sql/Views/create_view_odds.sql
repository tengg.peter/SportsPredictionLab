-- DROP VIEW IF EXISTS v_odds;
-- CREATE VIEW IF NOT EXISTS v_odds AS
SELECT
    sm_fixture_id, 
    m.name AS market,
    label, 
    value, 
    winning, 
    handicap,
    b.name AS bookmaker
FROM
(
    SELECT * FROM odds
    WHERE sm_market_id IN
    (
        SELECT sm_id FROM markets
        WHERE name IN ('3Way Result', 'Asian Handicap')
    )
    -- LIMIT 60000
) o
JOIN bookmakers b ON b.sm_id = o.sm_bookmaker_id
JOIN markets m ON m.sm_id = o.sm_market_id