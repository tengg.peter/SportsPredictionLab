-- DROP VIEW IF EXISTS v_max_one_upcoming_match_per_team;
-- CREATE VIEW v_max_one_upcoming_match_per_team AS
WITH
team_matches AS
(
    SELECT
        f.sm_localteam_id AS team_id,
        f.sm_id,
        f.date_time
        ,*
    FROM fixtures f
    WHERE status IN ('NS')
    UNION
    SELECT
        f.sm_visitorteam_id AS team_id,
        f.sm_id,
        f.date_time
        ,*
    FROM fixtures f
    WHERE status IN ('NS')
),
team_appearances AS
(
    SELECT
        ROW_NUMBER() OVER
        (
            PARTITION BY team_id, sm_season_id
            ORDER BY date_time
        ) appearance,
        team_id,
        sm_id AS fixture_id,
        date_time,
        sm_season_id
    FROM team_matches
)
SELECT
    f.* 
FROM
fixtures f
JOIN team_appearances ha ON ha.team_id = f.sm_localteam_id AND ha.fixture_id = f.sm_id
JOIN team_appearances aa ON aa.team_id = f.sm_visitorteam_id AND aa.fixture_id = f.sm_id
WHERE ha.appearance = 1 AND aa.appearance = 1
ORDER BY f.date_time