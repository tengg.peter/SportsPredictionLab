DROP VIEW IF EXISTS v_fixtures_with_stats;
CREATE VIEW v_fixtures_with_stats AS
SELECT 
	s1.sm_fixture_id, 
	seasons.sm_league_id AS sm_league_id,
	sm_localteam_id, 
	sm_visitorteam_id, 
	date_time,  
	s1.corners AS home_corners,
	s2.corners AS away_corners,
	s1.shots_total AS home_shots_total,
	s2.shots_total AS away_shots_total,
	s1.shots_ongoal AS home_shots_ongoal,
	s2.shots_ongoal AS away_shots_ongoal,
	s1.shots_offgoal AS home_shots_offgoal,
	s2.shots_offgoal AS away_shots_offgoal,
	s1.shots_blocked AS home_shots_blocked,
	s2.shots_blocked AS away_shots_blocked,
	s1.shots_insidebox AS home_shots_insidebox,
	s2.shots_insidebox AS away_shots_insidebox,
	s1.shots_outsidebox AS home_shots_outsidebox,
	s2.shots_outsidebox AS away_shots_outsidebox,
	s1.possessiontime AS home_possession,
	s2.possessiontime AS away_possession,
	s1.attacks AS home_attacks,
	s2.attacks AS away_attacks,
	s1.dangerous_attacks AS home_dangerous_attacks,
	s2.dangerous_attacks AS away_dangerous_attacks
FROM v_completed_matches c
JOIN match_statistics s1 ON c.sm_id = s1.sm_fixture_id AND c.sm_localteam_id = s1.sm_team_id
JOIN match_statistics s2 ON c.sm_id = s2.sm_fixture_id AND c.sm_visitorteam_id = s2.sm_team_id
JOIN seasons ON seasons.sm_id = c.sm_season_id
WHERE s1.corners IS NOT NULL AND s2.corners IS NOT NULL