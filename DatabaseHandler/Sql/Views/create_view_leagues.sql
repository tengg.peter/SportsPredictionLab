-- DROP VIEW v_leagues;
-- CREATE VIEW v_leagues AS
WITH
num_teams_per_seasons AS
(
    SELECT 
        seasons_sm_id,
        COUNT(seasons_sm_id) AS num_teams
    FROM seasons_teams
    WHERE seasons_sm_id IN
    (
        SELECT sm_current_season_id FROM leagues
        WHERE sm_current_season_id <> 0
    )
    GROUP BY seasons_sm_id
)
SELECT 
    l.sm_id AS league_id, 
    c.name AS country_name, 
    l.name AS league_name,
    l.is_cup,
    s.sm_id AS current_season_id,
    s.sm_current_round_id,
    n.num_teams
FROM leagues l
JOIN countries c ON l.sm_country_id = c.sm_id
JOIN seasons s ON s.sm_league_id = l.sm_id
JOIN num_teams_per_seasons n ON n.seasons_sm_id = s.sm_id
WHERE s.is_current_season = 1
ORDER BY c.name, l.name