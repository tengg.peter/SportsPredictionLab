-- DROP VIEW IF EXISTS v_upcoming_matches;
-- CREATE VIEW v_upcoming_matches AS
WITH
matches_with_team_names AS
(
	SELECT
		country_name, 
		[name:3] AS league_name, 
		[name:2] AS season_years, 
		'Round ' || [round_name] AS round_name, 
		name AS home_team, 
		[name:1] AS away_team, 
		datetime(date_time, 'localtime') AS date_time, 
		[name:4] AS stage_name, 
		[type:1] AS stage_type,
		[sm_id:1] AS home_team_id,
		[sm_id:2] AS away_team_id,
		sm_id AS fixture_id,
		sm_league_id,
		sm_season_id
	FROM
	(
		SELECT *, r.name AS round_name, c.name AS country_name FROM v_max_one_upcoming_match_per_team m
		JOIN teams t1 ON m.sm_localteam_id = t1.sm_id
		JOIN teams t2 ON m.sm_visitorteam_id = t2.sm_id
		JOIN seasons s ON m.sm_season_id = s.sm_id
		LEFT JOIN leagues l ON s.sm_league_id = l.sm_id
		LEFT JOIN stages st ON m.sm_stage_id = st.sm_id
		LEFT JOIN rounds r ON m.sm_round_id = r.sm_id
		JOIN countries c ON l.sm_country_id = c.sm_id
	)
	--WHERE [name:3] NOT IN ('Coppa Italia', "Carabao Cup")
)
SELECT * FROM matches_with_team_names
ORDER BY date_time, home_team ASC