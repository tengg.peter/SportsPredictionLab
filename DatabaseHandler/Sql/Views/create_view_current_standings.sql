-- DROP VIEW IF EXISTS v_current_standings;
-- CREATE VIEW v_current_standings AS
WITH
current_stage_ids AS
(
    SELECT sm_current_stage_id FROM seasons
    WHERE is_current_season = 1
),
current_standings AS
(
    SELECT * FROM standings
    WHERE sm_stage_id IN (SELECT * FROM current_stage_ids)
)
SELECT 
    c.name AS country,
    l.name AS league,
    p.round_name,
    p.position,
    t.name AS team_name,
    p.home_games_played,
    p.home_won,
    p.home_draw,
    p.home_lost,
    p.home_goals_scored,
    p.home_goals_against,
    p.away_games_played,
    p.away_won,
    p.away_draw,
    p.away_lost,
    p.away_goals_scored,
    p.away_goals_against,
    p.result,
    p.recent_form
FROM 
standing_positions p
JOIN current_standings s ON s.sm_id = p.sm_standing_id
JOIN teams t ON p.sm_team_id = t.sm_id
JOIN leagues l ON s.sm_league_id = l.sm_id
JOIN countries c ON l.sm_country_id = c.sm_id