-- DROP VIEW IF EXISTS v_all_match_results;
-- CREATE VIEW IF NOT EXISTS v_all_match_results AS
WITH
relevant_leagues AS --that are not cups and not play-offs.
(
    SELECT * FROM v_leagues
	WHERE 
		is_cup = 0 AND
		league_name NOT LIKE '%offs%'
),
matches AS
(
	SELECT * FROM
	v_completed_matches m
	JOIN seasons s ON s.sm_id = m.sm_season_id
	JOIN relevant_leagues l ON l.league_id = s.sm_league_id
)
SELECT 
	ht.name AS home_team,
	at.name AS away_team,
	m.country_name,
	m.league_name,
	m.sm_league_id,
	m.sm_season_id,
	m.name AS season_name,
	m.sm_id AS fixture_id,
	m.status,
	m.sm_localteam_id AS home_team_id,
	m.sm_visitorteam_id AS away_team_id,
	m.localteam_score AS home_goals,
	m.visitorteam_score AS away_goals,
	m.date_time,
	m.num_teams
FROM
matches m
LEFT JOIN teams ht ON ht.sm_id = m.sm_localteam_id
LEFT JOIN teams at ON at.sm_id = m.sm_visitorteam_id