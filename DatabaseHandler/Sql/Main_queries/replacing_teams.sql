-- oldSeasonId
-- newSeasonId
WITH
teams_out_ids AS
(
    SELECT teams_sm_id FROM seasons_teams
    WHERE 
        seasons_sm_id = oldSeasonId
        AND teams_sm_id NOT IN
        (
            SELECT teams_sm_id FROM seasons_teams
            WHERE seasons_sm_id = newSeasonId
        )
),
teams_out AS
(
    SELECT 
        'out' AS direction,
        name 
    FROM
        teams
    WHERE sm_id IN (SELECT * FROM teams_out_ids)
),
teams_in_ids AS
(
    SELECT teams_sm_id FROM seasons_teams
    WHERE 
        seasons_sm_id = newSeasonId AND 
        teams_sm_id NOT IN
        (
            SELECT teams_sm_id FROM seasons_teams
            WHERE seasons_sm_id = oldSeasonId
        )
),
teams_in AS
(
    SELECT 
        'in' AS direction,
        name 
    FROM
        teams
    WHERE sm_id IN (SELECT * FROM teams_in_ids)
)
SELECT * FROM teams_out
UNION
SELECT * FROM teams_in