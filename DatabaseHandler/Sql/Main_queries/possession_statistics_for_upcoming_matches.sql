WITH
RECURSIVE weight_table(row_num, w) AS
(
	SELECT 1, 20.0
        UNION ALL 
    SELECT row_num + 1, w - 1.0 FROM weight_table WHERE w > 1.0
),
home_team_home_possession_stats AS
(
	SELECT
		sm_localteam_id,
		average_possession_for AS avg_home_team_home_possession_for,
		weighted_average_possession_for AS wavg_home_team_home_possession_for,
		average_possession_against AS avg_home_team_home_possession_against,
		weighted_average_possession_against AS wavg_home_team_home_possession_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_possession) OVER (PARTITION BY sm_localteam_id) AS average_possession_for,
			SUM(home_possession * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_possession_for,
			AVG(away_possession) OVER (PARTITION BY sm_localteam_id) AS average_possession_against,
			SUM(away_possession * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_possession_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_possession IS NOT NULL AND
			t1.away_possession IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
home_team_away_possession_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_possession_for AS avg_home_team_away_possession_for,
		weighted_average_possession_for AS wavg_home_team_away_possession_for,
		average_possession_against AS avg_home_team_away_possession_against,
		weighted_average_possession_against AS wavg_home_team_away_possession_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_possession) OVER (PARTITION BY sm_visitorteam_id) AS average_possession_for,
			SUM(away_possession * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_possession_for,
			AVG(home_possession) OVER (PARTITION BY sm_visitorteam_id) AS average_possession_against,
			SUM(home_possession * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_possession_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table
        ON weight_table.row_num = t1.row_num AND 
			t1.home_possession IS NOT NULL AND
			t1.away_possession IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
away_team_home_possession_stats AS
(
	SELECT
		sm_localteam_id,
		average_possession_for AS avg_away_team_home_possession_for,
		weighted_average_possession_for AS wavg_away_team_home_possession_for,
		average_possession_against AS avg_away_team_home_possession_against,
		weighted_average_possession_against AS wavg_away_team_home_possession_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_possession) OVER (PARTITION BY sm_localteam_id) AS average_possession_for,
			SUM(home_possession * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_possession_for,
			AVG(away_possession) OVER (PARTITION BY sm_localteam_id) AS average_possession_against,
			SUM(away_possession * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_possession_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_possession IS NOT NULL AND
			t1.away_possession IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
away_team_away_possession_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_possession_for AS avg_away_team_away_possession_for,
		weighted_average_possession_for AS wavg_away_team_away_possession_for,
		average_possession_against AS avg_away_team_away_possession_against,
		weighted_average_possession_against AS wavg_away_team_away_possession_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_possession) OVER (PARTITION BY sm_visitorteam_id) AS average_possession_for,
			SUM(away_possession * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_possession_for,
			AVG(home_possession) OVER (PARTITION BY sm_visitorteam_id) AS average_possession_against,
			SUM(home_possession * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_possession_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_possession IS NOT NULL AND
			t1.away_possession IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
all_possession_stats AS
(
	SELECT 
		country_name, 
		league_name,
		home_team,
		away_team,
		date_time,
		hh.avg_home_team_home_possession_for,
		hh.wavg_home_team_home_possession_for,
		hh.avg_home_team_home_possession_against,
		hh.wavg_home_team_home_possession_against,
		ha.avg_home_team_away_possession_for,
		ha.wavg_home_team_away_possession_for,
		ha.avg_home_team_away_possession_against,
		ha.wavg_home_team_away_possession_against,
		ah.avg_away_team_home_possession_for,
		ah.wavg_away_team_home_possession_for,
		ah.avg_away_team_home_possession_against,
		ah.wavg_away_team_home_possession_against,
		aa.avg_away_team_away_possession_for,
		aa.wavg_away_team_away_possession_for,
		aa.avg_away_team_away_possession_against,
		aa.wavg_away_team_away_possession_against,
		hh.avg_home_team_home_possession_for + hh.avg_home_team_home_possession_against AS avg_home_team_home_possession_total,
		hh.wavg_home_team_home_possession_for + hh.wavg_home_team_home_possession_against AS wavg_home_team_home_possession_total,
		ha.avg_home_team_away_possession_for + ha.avg_home_team_away_possession_against AS avg_home_team_away_possession_total,
		ha.wavg_home_team_away_possession_for + ha.wavg_home_team_away_possession_against AS wavg_home_team_away_possession_total,
		ah.avg_away_team_home_possession_for + ah.avg_away_team_home_possession_against AS avg_away_team_home_possession_total,
		ah.wavg_away_team_home_possession_for + ah.wavg_away_team_home_possession_against AS wavg_away_team_home_possession_total,
		aa.avg_away_team_away_possession_for + aa.avg_away_team_away_possession_against AS avg_away_team_away_possession_total,
		aa.wavg_away_team_away_possession_for + aa.wavg_away_team_away_possession_against AS wavg_away_team_away_possession_total,
		
		(hh.avg_home_team_home_possession_for + ha.avg_home_team_away_possession_for) / 2.0 AS avg_home_team_possession_for,
		(hh.wavg_home_team_home_possession_for + ha.wavg_home_team_away_possession_for) / 2.0 AS wavg_home_team_possession_for,
		(hh.avg_home_team_home_possession_against + ha.avg_home_team_away_possession_against) / 2.0 AS avg_home_team_possession_against,
		(hh.wavg_home_team_home_possession_against + ha.wavg_home_team_away_possession_against) / 2.0 AS wavg_home_team_possession_against,
		(ah.avg_away_team_home_possession_for + aa.avg_away_team_away_possession_for) / 2.0 AS avg_away_team_possession_for,
		(ah.wavg_away_team_home_possession_for + aa.wavg_away_team_away_possession_for) / 2.0 AS wavg_away_team_possession_for,
		(ah.avg_away_team_home_possession_against + aa.avg_away_team_away_possession_against) / 2.0 AS avg_away_team_possession_against,
		(ah.wavg_away_team_home_possession_against + aa.wavg_away_team_away_possession_against) / 2.0 AS wavg_away_team_possession_against,

		(hh.avg_home_team_home_possession_for + ha.avg_home_team_away_possession_for + hh.avg_home_team_home_possession_against + ha.avg_home_team_away_possession_against) / 2.0 
			AS avg_home_team_possession_total,
		(hh.wavg_home_team_home_possession_for + ha.wavg_home_team_away_possession_for + hh.wavg_home_team_home_possession_against + ha.wavg_home_team_away_possession_against) / 2.0 
			AS wavg_home_team_possession_total,
		(ah.avg_away_team_home_possession_for + aa.avg_away_team_away_possession_for + ah.avg_away_team_home_possession_against + aa.avg_away_team_away_possession_against) / 2.0
			AS avg_away_team_possession_total,
		(ah.wavg_away_team_home_possession_for + aa.wavg_away_team_away_possession_for + ah.wavg_away_team_home_possession_against + aa.wavg_away_team_away_possession_against) / 2.0
			AS wavg_away_team_possession_total,

		(hh.avg_home_team_home_possession_for + aa.avg_away_team_away_possession_against) / 2.0 AS expected_home_possession,
		(hh.wavg_home_team_home_possession_for + aa.wavg_away_team_away_possession_against) / 2.0 AS wexpected_home_possession,
		(hh.avg_home_team_home_possession_against + aa.avg_away_team_away_possession_for) / 2.0 AS expected_away_possession,
		(hh.wavg_home_team_home_possession_against + aa.wavg_away_team_away_possession_for) / 2.0 AS wexpected_away_possession,

		(hh.avg_home_team_home_possession_for + aa.avg_away_team_away_possession_against + hh.avg_home_team_home_possession_against + aa.avg_away_team_away_possession_for) / 2.0 
			AS expected_possession_total,
		(hh.wavg_home_team_home_possession_for + aa.wavg_away_team_away_possession_against + hh.wavg_home_team_home_possession_against + aa.wavg_away_team_away_possession_for) / 2.0 
			AS wexpected_possession_total

	FROM v_upcoming_matches m
	LEFT JOIN (SELECT * FROM home_team_home_possession_stats) hh ON m.home_team_id = hh.sm_localteam_id
	LEFT JOIN (SELECT * FROM home_team_away_possession_stats) ha ON m.home_team_id = ha.sm_visitorteam_id
	LEFT JOIN (SELECT * FROM away_team_home_possession_stats) ah ON m.away_team_id = ah.sm_localteam_id
	LEFT JOIN (SELECT * FROM away_team_away_possession_stats) aa ON m.away_team_id = aa.sm_visitorteam_id
	ORDER BY date_time ASC
),
relevant_stats AS
(
	SELECT
		country_name,
		league_name,
		home_team,
		away_team,
		datetime(date_time, 'localtime') AS date_time,
		printf("'%.2f", wavg_home_team_home_possession_for) AS wavg_home_team_home_possession_for,
		printf("'%.2f", wavg_home_team_home_possession_against) AS wavg_home_team_home_possession_against,
		printf("'%.2f", wavg_away_team_away_possession_for) AS wavg_away_team_away_possession_for,
		printf("'%.2f", wavg_away_team_away_possession_against) AS wavg_away_team_away_possession_against,
		
		printf("'%.2f", wavg_home_team_possession_for) AS wavg_home_team_possession_for,
		printf("'%.2f", wavg_home_team_possession_against) AS wavg_home_team_possession_against,
		printf("'%.2f", wavg_away_team_possession_for) AS wavg_away_team_possession_for,
		printf("'%.2f", wavg_away_team_possession_against) AS wavg_away_team_possession_against,

		printf("'%.2f", wexpected_home_possession) AS wexpected_home_possession,
		printf("'%.2f", wexpected_away_possession) AS wexpected_away_possession
	FROM all_possession_stats
)

SELECT * FROM 
-- relevant_stats
all_possession_stats