WITH
RECURSIVE weight_table(row_num, w) AS
(
	SELECT 1, 20.0
        UNION ALL 
    SELECT row_num + 1, w - 1.0 FROM weight_table WHERE w > 1.0
),
home_team_home_dangerous_attack_stats AS
(
	SELECT
		sm_localteam_id,
		average_dangerous_attacks_for AS avg_home_team_home_dangerous_attacks_for,
		weighted_average_dangerous_attacks_for AS wavg_home_team_home_dangerous_attacks_for,
		average_dangerous_attacks_against AS avg_home_team_home_dangerous_attacks_against,
		weighted_average_dangerous_attacks_against AS wavg_home_team_home_dangerous_attacks_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_dangerous_attacks) OVER (PARTITION BY sm_localteam_id) AS average_dangerous_attacks_for,
			SUM(home_dangerous_attacks * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_dangerous_attacks_for,
			AVG(away_dangerous_attacks) OVER (PARTITION BY sm_localteam_id) AS average_dangerous_attacks_against,
			SUM(away_dangerous_attacks * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_dangerous_attacks_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_dangerous_attacks IS NOT NULL AND
			t1.away_dangerous_attacks IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
home_team_away_dangerous_attack_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_dangerous_attacks_for AS avg_home_team_away_dangerous_attacks_for,
		weighted_average_dangerous_attacks_for AS wavg_home_team_away_dangerous_attacks_for,
		average_dangerous_attacks_against AS avg_home_team_away_dangerous_attacks_against,
		weighted_average_dangerous_attacks_against AS wavg_home_team_away_dangerous_attacks_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_dangerous_attacks) OVER (PARTITION BY sm_visitorteam_id) AS average_dangerous_attacks_for,
			SUM(away_dangerous_attacks * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_dangerous_attacks_for,
			AVG(home_dangerous_attacks) OVER (PARTITION BY sm_visitorteam_id) AS average_dangerous_attacks_against,
			SUM(home_dangerous_attacks * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_dangerous_attacks_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_dangerous_attacks IS NOT NULL AND
			t1.away_dangerous_attacks IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
away_team_home_dangerous_attack_stats AS
(
	SELECT
		sm_localteam_id,
		average_dangerous_attacks_for AS avg_away_team_home_dangerous_attacks_for,
		weighted_average_dangerous_attacks_for AS wavg_away_team_home_dangerous_attacks_for,
		average_dangerous_attacks_against AS avg_away_team_home_dangerous_attacks_against,
		weighted_average_dangerous_attacks_against AS wavg_away_team_home_dangerous_attacks_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_dangerous_attacks) OVER (PARTITION BY sm_localteam_id) AS average_dangerous_attacks_for,
			SUM(home_dangerous_attacks * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_dangerous_attacks_for,
			AVG(away_dangerous_attacks) OVER (PARTITION BY sm_localteam_id) AS average_dangerous_attacks_against,
			SUM(away_dangerous_attacks * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_dangerous_attacks_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_dangerous_attacks IS NOT NULL AND
			t1.away_dangerous_attacks IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
away_team_away_dangerous_attack_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_dangerous_attacks_for AS avg_away_team_away_dangerous_attacks_for,
		weighted_average_dangerous_attacks_for AS wavg_away_team_away_dangerous_attacks_for,
		average_dangerous_attacks_against AS avg_away_team_away_dangerous_attacks_against,
		weighted_average_dangerous_attacks_against AS wavg_away_team_away_dangerous_attacks_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_dangerous_attacks) OVER (PARTITION BY sm_visitorteam_id) AS average_dangerous_attacks_for,
			SUM(away_dangerous_attacks * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_dangerous_attacks_for,
			AVG(home_dangerous_attacks) OVER (PARTITION BY sm_visitorteam_id) AS average_dangerous_attacks_against,
			SUM(home_dangerous_attacks * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_dangerous_attacks_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_dangerous_attacks IS NOT NULL AND
			t1.away_dangerous_attacks IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
all_dangerous_attack_stats AS
(
	SELECT 
		country_name, 
		league_name,
		home_team,
		away_team,
		date_time,
		hh.avg_home_team_home_dangerous_attacks_for,
		hh.wavg_home_team_home_dangerous_attacks_for,
		hh.avg_home_team_home_dangerous_attacks_against,
		hh.wavg_home_team_home_dangerous_attacks_against,
		ha.avg_home_team_away_dangerous_attacks_for,
		ha.wavg_home_team_away_dangerous_attacks_for,
		ha.avg_home_team_away_dangerous_attacks_against,
		ha.wavg_home_team_away_dangerous_attacks_against,
		ah.avg_away_team_home_dangerous_attacks_for,
		ah.wavg_away_team_home_dangerous_attacks_for,
		ah.avg_away_team_home_dangerous_attacks_against,
		ah.wavg_away_team_home_dangerous_attacks_against,
		aa.avg_away_team_away_dangerous_attacks_for,
		aa.wavg_away_team_away_dangerous_attacks_for,
		aa.avg_away_team_away_dangerous_attacks_against,
		aa.wavg_away_team_away_dangerous_attacks_against,
		hh.avg_home_team_home_dangerous_attacks_for + hh.avg_home_team_home_dangerous_attacks_against AS avg_home_team_home_dangerous_attacks_total,
		hh.wavg_home_team_home_dangerous_attacks_for + hh.wavg_home_team_home_dangerous_attacks_against AS wavg_home_team_home_dangerous_attacks_total,
		ha.avg_home_team_away_dangerous_attacks_for + ha.avg_home_team_away_dangerous_attacks_against AS avg_home_team_away_dangerous_attacks_total,
		ha.wavg_home_team_away_dangerous_attacks_for + ha.wavg_home_team_away_dangerous_attacks_against AS wavg_home_team_away_dangerous_attacks_total,
		ah.avg_away_team_home_dangerous_attacks_for + ah.avg_away_team_home_dangerous_attacks_against AS avg_away_team_home_dangerous_attacks_total,
		ah.wavg_away_team_home_dangerous_attacks_for + ah.wavg_away_team_home_dangerous_attacks_against AS wavg_away_team_home_dangerous_attacks_total,
		aa.avg_away_team_away_dangerous_attacks_for + aa.avg_away_team_away_dangerous_attacks_against AS avg_away_team_away_dangerous_attacks_total,
		aa.wavg_away_team_away_dangerous_attacks_for + aa.wavg_away_team_away_dangerous_attacks_against AS wavg_away_team_away_dangerous_attacks_total,
		
		(hh.avg_home_team_home_dangerous_attacks_for + ha.avg_home_team_away_dangerous_attacks_for) / 2.0 AS avg_home_team_dangerous_attacks_for,
		(hh.wavg_home_team_home_dangerous_attacks_for + ha.wavg_home_team_away_dangerous_attacks_for) / 2.0 AS wavg_home_team_dangerous_attacks_for,
		(hh.avg_home_team_home_dangerous_attacks_against + ha.avg_home_team_away_dangerous_attacks_against) / 2.0 AS avg_home_team_dangerous_attacks_against,
		(hh.wavg_home_team_home_dangerous_attacks_against + ha.wavg_home_team_away_dangerous_attacks_against) / 2.0 AS wavg_home_team_dangerous_attacks_against,
		(ah.avg_away_team_home_dangerous_attacks_for + aa.avg_away_team_away_dangerous_attacks_for) / 2.0 AS avg_away_team_dangerous_attacks_for,
		(ah.wavg_away_team_home_dangerous_attacks_for + aa.wavg_away_team_away_dangerous_attacks_for) / 2.0 AS wavg_away_team_dangerous_attacks_for,
		(ah.avg_away_team_home_dangerous_attacks_against + aa.avg_away_team_away_dangerous_attacks_against) / 2.0 AS avg_away_team_dangerous_attacks_against,
		(ah.wavg_away_team_home_dangerous_attacks_against + aa.wavg_away_team_away_dangerous_attacks_against) / 2.0 AS wavg_away_team_dangerous_attacks_against,

		(hh.avg_home_team_home_dangerous_attacks_for + ha.avg_home_team_away_dangerous_attacks_for + hh.avg_home_team_home_dangerous_attacks_against + ha.avg_home_team_away_dangerous_attacks_against) / 2.0 
			AS avg_home_team_dangerous_attacks_total,
		(hh.wavg_home_team_home_dangerous_attacks_for + ha.wavg_home_team_away_dangerous_attacks_for + hh.wavg_home_team_home_dangerous_attacks_against + ha.wavg_home_team_away_dangerous_attacks_against) / 2.0 
			AS wavg_home_team_dangerous_attacks_total,
		(ah.avg_away_team_home_dangerous_attacks_for + aa.avg_away_team_away_dangerous_attacks_for + ah.avg_away_team_home_dangerous_attacks_against + aa.avg_away_team_away_dangerous_attacks_against) / 2.0
			AS avg_away_team_dangerous_attacks_total,
		(ah.wavg_away_team_home_dangerous_attacks_for + aa.wavg_away_team_away_dangerous_attacks_for + ah.wavg_away_team_home_dangerous_attacks_against + aa.wavg_away_team_away_dangerous_attacks_against) / 2.0
			AS wavg_away_team_dangerous_attacks_total,

		(hh.avg_home_team_home_dangerous_attacks_for + aa.avg_away_team_away_dangerous_attacks_against) / 2.0 AS expected_home_dangerous_attacks_total,
		(hh.wavg_home_team_home_dangerous_attacks_for + aa.wavg_away_team_away_dangerous_attacks_against) / 2.0 AS wexpected_home_dangerous_attacks_total,
		(hh.avg_home_team_home_dangerous_attacks_against + aa.avg_away_team_away_dangerous_attacks_for) / 2.0 AS expected_away_dangerous_attacks_total,
		(hh.wavg_home_team_home_dangerous_attacks_against + aa.wavg_away_team_away_dangerous_attacks_for) / 2.0 AS wexpected_away_dangerous_attacks_total,

		(hh.avg_home_team_home_dangerous_attacks_for + aa.avg_away_team_away_dangerous_attacks_against + hh.avg_home_team_home_dangerous_attacks_against + aa.avg_away_team_away_dangerous_attacks_for) / 2.0 
			AS expected_dangerous_attacks_total,
		(hh.wavg_home_team_home_dangerous_attacks_for + aa.wavg_away_team_away_dangerous_attacks_against + hh.wavg_home_team_home_dangerous_attacks_against + aa.wavg_away_team_away_dangerous_attacks_for) / 2.0 
			AS wexpected_dangerous_attacks_total

	FROM v_upcoming_matches m
	LEFT JOIN (SELECT * FROM home_team_home_dangerous_attack_stats) hh ON m.home_team_id = hh.sm_localteam_id
	LEFT JOIN (SELECT * FROM home_team_away_dangerous_attack_stats) ha ON m.home_team_id = ha.sm_visitorteam_id
	LEFT JOIN (SELECT * FROM away_team_home_dangerous_attack_stats) ah ON m.away_team_id = ah.sm_localteam_id
	LEFT JOIN (SELECT * FROM away_team_away_dangerous_attack_stats) aa ON m.away_team_id = aa.sm_visitorteam_id
	ORDER BY date_time ASC
),
relevant_dangerous_attack_stats AS
(
	SELECT
		country_name,
		league_name,
		home_team,
		away_team,
		datetime(date_time, 'localtime') AS date_time,
		printf("'%.2f", wavg_home_team_home_dangerous_attacks_for) AS wavg_home_team_home_dangerous_attacks_for,
		printf("'%.2f", wavg_home_team_home_dangerous_attacks_against) AS wavg_home_team_home_dangerous_attacks_against,
		printf("'%.2f", wavg_away_team_away_dangerous_attacks_for) AS wavg_away_team_away_dangerous_attacks_for,
		printf("'%.2f", wavg_away_team_away_dangerous_attacks_against) AS wavg_away_team_away_dangerous_attacks_against,
		
		printf("'%.2f", wavg_home_team_home_dangerous_attacks_total) AS wavg_home_team_home_dangerous_attacks_total,
		printf("'%.2f", wavg_away_team_away_dangerous_attacks_total) AS wavg_away_team_away_dangerous_attacks_total,

		printf("'%.2f", wavg_home_team_dangerous_attacks_for) AS wavg_home_team_dangerous_attacks_for,
		printf("'%.2f", wavg_home_team_dangerous_attacks_against) AS wavg_home_team_dangerous_attacks_against,
		printf("'%.2f", wavg_away_team_dangerous_attacks_for) AS wavg_away_team_dangerous_attacks_for,
		printf("'%.2f", wavg_away_team_dangerous_attacks_against) AS wavg_away_team_dangerous_attacks_against,

		printf("'%.2f", wavg_home_team_dangerous_attacks_total) AS wavg_home_team_dangerous_attacks_total,
		printf("'%.2f", wavg_away_team_dangerous_attacks_total) AS wavg_away_team_dangerous_attacks_total,

		printf("'%.2f", wexpected_home_dangerous_attacks_total) AS wexpected_home_dangerous_attacks_total,
		printf("'%.2f", wexpected_away_dangerous_attacks_total) AS wexpected_away_dangerous_attacks_total,

		printf("'%.2f", wexpected_dangerous_attacks_total) AS wexpected_dangerous_attacks
	FROM all_dangerous_attack_stats
)

SELECT * FROM
-- relevant_dangerous_attack_stats
all_dangerous_attack_stats