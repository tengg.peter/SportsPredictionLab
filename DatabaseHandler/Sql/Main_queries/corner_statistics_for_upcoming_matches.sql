WITH
RECURSIVE weight_table(row_num, w) AS
(
	SELECT 1, 20.0
        UNION ALL 
    SELECT row_num + 1, w - 1.0 FROM weight_table WHERE w > 1.0
),
home_team_home_corner_stats AS
(
	SELECT
		sm_localteam_id,
		average_corners_for AS avg_home_team_home_corners_for,
		weighted_average_corners_for AS wavg_home_team_home_corners_for,
		average_corners_against AS avg_home_team_home_corners_against,
		weighted_average_corners_against AS wavg_home_team_home_corners_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_corners) OVER (PARTITION BY sm_localteam_id) AS average_corners_for,
			SUM(home_corners * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_corners_for,
			AVG(away_corners) OVER (PARTITION BY sm_localteam_id) AS average_corners_against,
			SUM(away_corners * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_corners_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_corners IS NOT NULL AND
			t1.away_corners IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
home_team_away_corner_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_corners_for AS avg_home_team_away_corners_for,
		weighted_average_corners_for AS wavg_home_team_away_corners_for,
		average_corners_against AS avg_home_team_away_corners_against,
		weighted_average_corners_against AS wavg_home_team_away_corners_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_corners) OVER (PARTITION BY sm_visitorteam_id) AS average_corners_for,
			SUM(away_corners * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_corners_for,
			AVG(home_corners) OVER (PARTITION BY sm_visitorteam_id) AS average_corners_against,
			SUM(home_corners * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_corners_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_corners IS NOT NULL AND
			t1.away_corners IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
away_team_home_corner_stats AS
(
	SELECT
		sm_localteam_id,
		average_corners_for AS avg_away_team_home_corners_for,
		weighted_average_corners_for AS wavg_away_team_home_corners_for,
		average_corners_against AS avg_away_team_home_corners_against,
		weighted_average_corners_against AS wavg_away_team_home_corners_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_corners) OVER (PARTITION BY sm_localteam_id) AS average_corners_for,
			SUM(home_corners * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_corners_for,
			AVG(away_corners) OVER (PARTITION BY sm_localteam_id) AS average_corners_against,
			SUM(away_corners * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_corners_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_corners IS NOT NULL AND
			t1.away_corners IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
away_team_away_corner_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_corners_for AS avg_away_team_away_corners_for,
		weighted_average_corners_for AS wavg_away_team_away_corners_for,
		average_corners_against AS avg_away_team_away_corners_against,
		weighted_average_corners_against AS wavg_away_team_away_corners_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_corners) OVER (PARTITION BY sm_visitorteam_id) AS average_corners_for,
			SUM(away_corners * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_corners_for,
			AVG(home_corners) OVER (PARTITION BY sm_visitorteam_id) AS average_corners_against,
			SUM(home_corners * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_corners_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_corners IS NOT NULL AND
			t1.away_corners IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
all_corner_stats AS
(
	SELECT 
		country_name, 
		league_name,
		home_team,
		away_team,
		date_time,
		hh.avg_home_team_home_corners_for,
		hh.wavg_home_team_home_corners_for,
		hh.avg_home_team_home_corners_against,
		hh.wavg_home_team_home_corners_against,
		ha.avg_home_team_away_corners_for,
		ha.wavg_home_team_away_corners_for,
		ha.avg_home_team_away_corners_against,
		ha.wavg_home_team_away_corners_against,
		ah.avg_away_team_home_corners_for,
		ah.wavg_away_team_home_corners_for,
		ah.avg_away_team_home_corners_against,
		ah.wavg_away_team_home_corners_against,
		aa.avg_away_team_away_corners_for,
		aa.wavg_away_team_away_corners_for,
		aa.avg_away_team_away_corners_against,
		aa.wavg_away_team_away_corners_against,
		hh.avg_home_team_home_corners_for + hh.avg_home_team_home_corners_against AS avg_home_team_home_corners_total,
		hh.wavg_home_team_home_corners_for + hh.wavg_home_team_home_corners_against AS wavg_home_team_home_corners_total,
		ha.avg_home_team_away_corners_for + ha.avg_home_team_away_corners_against AS avg_home_team_away_corners_total,
		ha.wavg_home_team_away_corners_for + ha.wavg_home_team_away_corners_against AS wavg_home_team_away_corners_total,
		ah.avg_away_team_home_corners_for + ah.avg_away_team_home_corners_against AS avg_away_team_home_corners_total, 
		ah.wavg_away_team_home_corners_for + ah.wavg_away_team_home_corners_against AS wavg_away_team_home_corners_total,
		aa.avg_away_team_away_corners_for + aa.avg_away_team_away_corners_against AS avg_away_team_away_corners_total,
		aa.wavg_away_team_away_corners_for + aa.wavg_away_team_away_corners_against AS wavg_away_team_away_corners_total,
		
		(hh.avg_home_team_home_corners_for + ha.avg_home_team_away_corners_for) / 2.0 AS avg_home_team_corners_for,
		(hh.wavg_home_team_home_corners_for + ha.wavg_home_team_away_corners_for) / 2.0 AS wavg_home_team_corners_for,
		(hh.avg_home_team_home_corners_against + ha.avg_home_team_away_corners_against) / 2.0 AS avg_home_team_corners_against,
		(hh.wavg_home_team_home_corners_against + ha.wavg_home_team_away_corners_against) / 2.0 AS wavg_home_team_corners_against,
		(ah.avg_away_team_home_corners_for + aa.avg_away_team_away_corners_for) / 2.0 AS avg_away_team_corners_for,
		(ah.wavg_away_team_home_corners_for + aa.wavg_away_team_away_corners_for) / 2.0 AS wavg_away_team_corners_for,
		(ah.avg_away_team_home_corners_against + aa.avg_away_team_away_corners_against) / 2.0 AS avg_away_team_corners_against,
		(ah.wavg_away_team_home_corners_against + aa.wavg_away_team_away_corners_against) / 2.0 AS wavg_away_team_corners_against,

		(hh.avg_home_team_home_corners_for + ha.avg_home_team_away_corners_for + hh.avg_home_team_home_corners_against + ha.avg_home_team_away_corners_against) / 2.0 
			AS avg_home_team_corners_total,
		(hh.wavg_home_team_home_corners_for + ha.wavg_home_team_away_corners_for + hh.wavg_home_team_home_corners_against + ha.wavg_home_team_away_corners_against) / 2.0 
			AS wavg_home_team_corners_total,
		(ah.avg_away_team_home_corners_for + aa.avg_away_team_away_corners_for + ah.avg_away_team_home_corners_against + aa.avg_away_team_away_corners_against) / 2.0
			AS avg_away_team_corners_total,
		(ah.wavg_away_team_home_corners_for + aa.wavg_away_team_away_corners_for + ah.wavg_away_team_home_corners_against + aa.wavg_away_team_away_corners_against) / 2.0
			AS wavg_away_team_corners_total,

		(hh.avg_home_team_home_corners_for + aa.avg_away_team_away_corners_against) / 2.0 AS expected_home_corners,
		(hh.wavg_home_team_home_corners_for + aa.wavg_away_team_away_corners_against) / 2.0 AS wexpected_home_corners,
		(hh.avg_home_team_home_corners_against + aa.avg_away_team_away_corners_for) / 2.0 AS expected_away_corners,
		(hh.wavg_home_team_home_corners_against + aa.wavg_away_team_away_corners_for) / 2.0 AS wexpected_away_corners,

		(hh.avg_home_team_home_corners_for + aa.avg_away_team_away_corners_against + hh.avg_home_team_home_corners_against + aa.avg_away_team_away_corners_for) / 2.0 
			AS expected_corners_total,
		(hh.wavg_home_team_home_corners_for + aa.wavg_away_team_away_corners_against + hh.wavg_home_team_home_corners_against + aa.wavg_away_team_away_corners_for) / 2.0 
			AS wexpected_corners_total

	FROM v_upcoming_matches m
	LEFT JOIN (SELECT * FROM home_team_home_corner_stats) hh ON m.home_team_id = hh.sm_localteam_id
	LEFT JOIN (SELECT * FROM home_team_away_corner_stats) ha ON m.home_team_id = ha.sm_visitorteam_id
	LEFT JOIN (SELECT * FROM away_team_home_corner_stats) ah ON m.away_team_id = ah.sm_localteam_id
	LEFT JOIN (SELECT * FROM away_team_away_corner_stats) aa ON m.away_team_id = aa.sm_visitorteam_id
	ORDER BY date_time ASC
),
relevant_stats AS
(
	SELECT
		country_name,
		league_name,
		home_team,
		away_team,
		datetime(date_time, 'localtime') AS date_time,
		printf("'%.2f", wavg_home_team_home_corners_for) AS wavg_home_team_home_corners_for,
		printf("'%.2f", wavg_home_team_home_corners_against) AS wavg_home_team_home_corners_against,
		printf("'%.2f", wavg_away_team_away_corners_for) AS wavg_away_team_away_corners_for,
		printf("'%.2f", wavg_away_team_away_corners_against) AS wavg_away_team_away_corners_against,
		
		printf("'%.2f", wavg_home_team_home_corners_total) AS wavg_home_team_home_corners_total,
		printf("'%.2f", wavg_away_team_away_corners_total) AS wavg_away_team_away_corners_total,

		printf("'%.2f", wavg_home_team_corners_for) AS wavg_home_team_corners_for,
		printf("'%.2f", wavg_home_team_corners_against) AS wavg_home_team_corners_against,
		printf("'%.2f", wavg_away_team_corners_for) AS wavg_away_team_corners_for,
		printf("'%.2f", wavg_away_team_corners_against) AS wavg_away_team_corners_against,

		printf("'%.2f", wavg_home_team_corners_total) AS wavg_home_team_corners_total,
		printf("'%.2f", wavg_away_team_corners_total) AS wavg_away_team_corners_total,

		printf("'%.2f", wexpected_home_corners) AS wexpected_home_corners,
		printf("'%.2f", wexpected_away_corners) AS wexpected_away_corners,

		printf("'%.2f", wexpected_corners_total) AS wexpected_corners_total
	FROM all_corner_stats
)

SELECT * FROM 
-- relevant_stats
all_corner_stats