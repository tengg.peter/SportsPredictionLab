WITH
RECURSIVE weight_table(row_num, w) AS
(
	SELECT 1, 20.0
        UNION ALL 
    SELECT row_num + 1, w - 1.0 FROM weight_table WHERE w > 1.0
),
home_team_home_cross_stats AS
(
	SELECT
		sm_localteam_id,
		average_crosses_for AS avg_home_team_home_crosses_for,
		weighted_average_crosses_for AS wavg_home_team_home_crosses_for,
		average_crosses_against AS avg_home_team_home_crosses_against,
		weighted_average_crosses_against AS wavg_home_team_home_crosses_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_crosses) OVER (PARTITION BY sm_localteam_id) AS average_crosses_for,
			SUM(home_crosses * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_crosses_for,
			AVG(away_crosses) OVER (PARTITION BY sm_localteam_id) AS average_crosses_against,
			SUM(away_crosses * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_crosses_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*,
				s.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
			JOIN calculated_match_stats s ON f.sm_fixture_id = s.sm_fixture_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_crosses IS NOT NULL AND
			t1.away_crosses IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
home_team_away_cross_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_crosses_for AS avg_home_team_away_crosses_for,
		weighted_average_crosses_for AS wavg_home_team_away_crosses_for,
		average_crosses_against AS avg_home_team_away_crosses_against,
		weighted_average_crosses_against AS wavg_home_team_away_crosses_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_crosses) OVER (PARTITION BY sm_visitorteam_id) AS average_crosses_for,
			SUM(away_crosses * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_crosses_for,
			AVG(home_crosses) OVER (PARTITION BY sm_visitorteam_id) AS average_crosses_against,
			SUM(home_crosses * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_crosses_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*,
				s.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.home_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
			JOIN calculated_match_stats s ON f.sm_fixture_id = s.sm_fixture_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_crosses IS NOT NULL AND
			t1.away_crosses IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
away_team_home_cross_stats AS
(
	SELECT
		sm_localteam_id,
		average_crosses_for AS avg_away_team_home_crosses_for,
		weighted_average_crosses_for AS wavg_away_team_home_crosses_for,
		average_crosses_against AS avg_away_team_home_crosses_against,
		weighted_average_crosses_against AS wavg_away_team_home_crosses_against
	FROM
	(
		SELECT 
			*, 
			AVG(home_crosses) OVER (PARTITION BY sm_localteam_id) AS average_crosses_for,
			SUM(home_crosses * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_crosses_for,
			AVG(away_crosses) OVER (PARTITION BY sm_localteam_id) AS average_crosses_against,
			SUM(away_crosses * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_crosses_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_localteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*,
				s.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_localteam_id AND f.sm_league_id = m.sm_league_id
			JOIN calculated_match_stats s ON f.sm_fixture_id = s.sm_fixture_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_crosses IS NOT NULL AND
			t1.away_crosses IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
away_team_away_cross_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_crosses_for AS avg_away_team_away_crosses_for,
		weighted_average_crosses_for AS wavg_away_team_away_crosses_for,
		average_crosses_against AS avg_away_team_away_crosses_against,
		weighted_average_crosses_against AS wavg_away_team_away_crosses_against
	FROM
	(
		SELECT 
			*, 
			AVG(away_crosses) OVER (PARTITION BY sm_visitorteam_id) AS average_crosses_for,
			SUM(away_crosses * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_crosses_for,
			AVG(home_crosses) OVER (PARTITION BY sm_visitorteam_id) AS average_crosses_against,
			SUM(home_crosses * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_crosses_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY f.sm_visitorteam_id
					ORDER BY f.date_time DESC
				) row_num,
				f.*,
				s.*
			FROM v_upcoming_matches m
			JOIN v_fixtures_with_stats f ON m.away_team_id = f.sm_visitorteam_id AND f.sm_league_id = m.sm_league_id
			JOIN calculated_match_stats s ON f.sm_fixture_id = s.sm_fixture_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.home_crosses IS NOT NULL AND
			t1.away_crosses IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
all_cross_stats AS
(
	SELECT 
		country_name, 
		league_name,
		home_team,
		away_team,
		date_time,
		hh.avg_home_team_home_crosses_for,
		hh.wavg_home_team_home_crosses_for,
		hh.avg_home_team_home_crosses_against,
		hh.wavg_home_team_home_crosses_against,
		ha.avg_home_team_away_crosses_for,
		ha.wavg_home_team_away_crosses_for,
		ha.avg_home_team_away_crosses_against,
		ha.wavg_home_team_away_crosses_against,
		ah.avg_away_team_home_crosses_for,
		ah.wavg_away_team_home_crosses_for,
		ah.avg_away_team_home_crosses_against,
		ah.wavg_away_team_home_crosses_against,
		aa.avg_away_team_away_crosses_for,
		aa.wavg_away_team_away_crosses_for,
		aa.avg_away_team_away_crosses_against,
		aa.wavg_away_team_away_crosses_against,
		hh.avg_home_team_home_crosses_for + hh.avg_home_team_home_crosses_against AS avg_home_team_home_crosses_total,
		hh.wavg_home_team_home_crosses_for + hh.wavg_home_team_home_crosses_against AS wavg_home_team_home_crosses_total,
		ha.avg_home_team_away_crosses_for + ha.avg_home_team_away_crosses_against AS avg_home_team_away_crosses_total,
		ha.wavg_home_team_away_crosses_for + ha.wavg_home_team_away_crosses_against AS wavg_home_team_away_crosses_total,
		ah.avg_away_team_home_crosses_for + ah.avg_away_team_home_crosses_against AS avg_away_team_home_crosses_total,
		ah.wavg_away_team_home_crosses_for + ah.wavg_away_team_home_crosses_against AS wavg_away_team_home_crosses_total,
		aa.avg_away_team_away_crosses_for + aa.avg_away_team_away_crosses_against AS avg_away_team_away_crosses_total,
		aa.wavg_away_team_away_crosses_for + aa.wavg_away_team_away_crosses_against AS wavg_away_team_away_crosses_total,
		
		(hh.avg_home_team_home_crosses_for + ha.avg_home_team_away_crosses_for) / 2.0 AS avg_home_team_crosses_for,
		(hh.wavg_home_team_home_crosses_for + ha.wavg_home_team_away_crosses_for) / 2.0 AS wavg_home_team_crosses_for,
		(hh.avg_home_team_home_crosses_against + ha.avg_home_team_away_crosses_against) / 2.0 AS avg_home_team_crosses_against,
		(hh.wavg_home_team_home_crosses_against + ha.wavg_home_team_away_crosses_against) / 2.0 AS wavg_home_team_crosses_against,
		(ah.avg_away_team_home_crosses_for + aa.avg_away_team_away_crosses_for) / 2.0 AS avg_away_team_crosses_for,
		(ah.wavg_away_team_home_crosses_for + aa.wavg_away_team_away_crosses_for) / 2.0 AS wavg_away_team_crosses_for,
		(ah.avg_away_team_home_crosses_against + aa.avg_away_team_away_crosses_against) / 2.0 AS avg_away_team_crosses_against,
		(ah.wavg_away_team_home_crosses_against + aa.wavg_away_team_away_crosses_against) / 2.0 AS wavg_away_team_crosses_against,

		(hh.avg_home_team_home_crosses_for + ha.avg_home_team_away_crosses_for + hh.avg_home_team_home_crosses_against + ha.avg_home_team_away_crosses_against) / 2.0 
			AS avg_home_team_crosses_total,
		(hh.wavg_home_team_home_crosses_for + ha.wavg_home_team_away_crosses_for + hh.wavg_home_team_home_crosses_against + ha.wavg_home_team_away_crosses_against) / 2.0 
			AS wavg_home_team_crosses_total,
		(ah.avg_away_team_home_crosses_for + aa.avg_away_team_away_crosses_for + ah.avg_away_team_home_crosses_against + aa.avg_away_team_away_crosses_against) / 2.0
			AS avg_away_team_crosses_total,
		(ah.wavg_away_team_home_crosses_for + aa.wavg_away_team_away_crosses_for + ah.wavg_away_team_home_crosses_against + aa.wavg_away_team_away_crosses_against) / 2.0
			AS wavg_away_team_crosses_total,

		(hh.avg_home_team_home_crosses_for + aa.avg_away_team_away_crosses_against) / 2.0 AS expected_home_crosses,
		(hh.wavg_home_team_home_crosses_for + aa.wavg_away_team_away_crosses_against) / 2.0 AS wexpected_home_crosses,
		(hh.avg_home_team_home_crosses_against + aa.avg_away_team_away_crosses_for) / 2.0 AS expected_away_crosses,
		(hh.wavg_home_team_home_crosses_against + aa.wavg_away_team_away_crosses_for) / 2.0 AS wexpected_away_crosses,

		(hh.avg_home_team_home_crosses_for + aa.avg_away_team_away_crosses_against + hh.avg_home_team_home_crosses_against + aa.avg_away_team_away_crosses_for) / 2.0 
			AS expected_crosses,
		(hh.wavg_home_team_home_crosses_for + aa.wavg_away_team_away_crosses_against + hh.wavg_home_team_home_crosses_against + aa.wavg_away_team_away_crosses_for) / 2.0 
			AS wexpected_crosses

	FROM v_upcoming_matches m
	LEFT JOIN (SELECT * FROM home_team_home_cross_stats) hh ON m.home_team_id = hh.sm_localteam_id
	LEFT JOIN (SELECT * FROM home_team_away_cross_stats) ha ON m.home_team_id = ha.sm_visitorteam_id
	LEFT JOIN (SELECT * FROM away_team_home_cross_stats) ah ON m.away_team_id = ah.sm_localteam_id
	LEFT JOIN (SELECT * FROM away_team_away_cross_stats) aa ON m.away_team_id = aa.sm_visitorteam_id
	ORDER BY date_time ASC
),
relevant_cross_stats AS
(
	SELECT
		country_name,
		league_name,
		home_team,
		away_team,
		datetime(date_time, 'localtime') AS date_time,
		printf("'%.2f", wavg_home_team_home_crosses_for) AS wavg_home_team_home_crosses_for,
		printf("'%.2f", wavg_home_team_home_crosses_against) AS wavg_home_team_home_crosses_against,
		printf("'%.2f", wavg_away_team_away_crosses_for) AS wavg_away_team_away_crosses_for,
		printf("'%.2f", wavg_away_team_away_crosses_against) AS wavg_away_team_away_crosses_against,
		
		printf("'%.2f", wavg_home_team_home_crosses_total) AS wavg_home_team_home_crosses_total,
		printf("'%.2f", wavg_away_team_away_crosses_total) AS wavg_away_team_away_crosses_total,

		printf("'%.2f", wavg_home_team_crosses_for) AS wavg_home_team_crosses_for,
		printf("'%.2f", wavg_home_team_crosses_against) AS wavg_home_team_crosses_against,
		printf("'%.2f", wavg_away_team_crosses_for) AS wavg_away_team_crosses_for,
		printf("'%.2f", wavg_away_team_crosses_against) AS wavg_away_team_crosses_against,

		printf("'%.2f", wavg_home_team_crosses_total) AS wavg_home_team_crosses_total,
		printf("'%.2f", wavg_away_team_crosses_total) AS wavg_away_team_crosses_total,

		printf("'%.2f", wexpected_home_crosses) AS wexpected_home_crosses,
		printf("'%.2f", wexpected_away_crosses) AS wexpected_away_crosses,

		printf("'%.2f", wexpected_crosses) AS wexpected_crosses
	FROM all_cross_stats
)

SELECT * FROM
-- relevant_cross_stats
all_cross_stats