WITH
home_team_results AS
(
    SELECT 
        ROW_NUMBER() OVER
        (
            PARTITION BY m.home_team_id
            ORDER BY fs.date_time DESC
        ) row_num,
        home_team,
        home_team_id,
        fs.sm_league_id,
        home_corners,
        away_corners
    FROM v_upcoming_matches m
    JOIN v_fixtures_with_stats fs ON fs.sm_localteam_id = m.home_team_id AND
        fs.sm_league_id = m.sm_league_id
),
away_team_results AS
(
    SELECT 
        ROW_NUMBER() OVER
        (
            PARTITION BY m.away_team_id
            ORDER BY fs.date_time DESC
        ) row_num,
        away_team,
        away_team_id,
        fs.sm_league_id,
        home_corners,
        away_corners
    FROM v_upcoming_matches m
    JOIN v_fixtures_with_stats fs ON fs.sm_visitorteam_id = m.away_team_id AND
        fs.sm_league_id = m.sm_league_id
),
home_team_home_corner_distribution_for AS
(
    SELECT
        home_team || ' home corners for',
        home_team_id,
        sm_league_id,
        home_corners,
        COUNT(home_corners),
        COUNT(home_corners) / 20.0 AS frequency
    FROM (SELECT * FROM home_team_results)
    WHERE row_num <= 20
    GROUP BY home_team_id, home_corners
),
home_team_home_corner_distribution_against AS
(
    SELECT
        home_team || ' home corners against',
        home_team_id,
        sm_league_id,
        away_corners,
        COUNT(away_corners),
        COUNT(away_corners) / 20.0 AS frequency
    FROM (SELECT * FROM home_team_results)
    WHERE row_num <= 20
    GROUP BY home_team_id, away_corners
),
home_team_home_corner_distribution_total AS
(
    SELECT
        home_team || ' home corners total',
        home_team_id,
        sm_league_id,
        home_corners + away_corners,
        COUNT(home_corners + away_corners),
        COUNT(home_corners + away_corners) / 20.0 AS frequency
    FROM (SELECT * FROM home_team_results)
    WHERE row_num <= 20
    GROUP BY home_team_id, (home_corners + away_corners)
),
away_team_away_corner_distribution_for AS
(
    SELECT
        away_team || ' away corners for',
        away_team_id,
        sm_league_id,
        away_corners,
        COUNT(away_corners),
        COUNT(away_corners) / 20.0 AS frequency
    FROM (SELECT * FROM away_team_results)
    WHERE row_num <= 20
    GROUP BY away_team_id, away_corners
),
away_team_away_corner_distribution_against AS
(
    SELECT
        away_team || ' away corners against',
        away_team_id,
        sm_league_id,
        home_corners,
        COUNT(home_corners),
        COUNT(home_corners) / 20.0 AS frequency
    FROM (SELECT * FROM away_team_results)
    WHERE row_num <= 20
    GROUP BY away_team_id, home_corners
),
away_team_away_corner_distribution_total AS
(
    SELECT
        away_team || ' away corners total',
        away_team_id,
        sm_league_id,
        home_corners + away_corners,
        COUNT(home_corners + away_corners),
        COUNT(home_corners + away_corners) / 20.0 AS frequency
    FROM (SELECT * FROM away_team_results)
    WHERE row_num <= 20
    GROUP BY away_team_id, (home_corners + away_corners)
)

SELECT
    home_team,
    away_team,
    date_time,
    [home_team || ' home corners for'] AS description,
    home_corners AS corners,
    [COUNT(home_corners)] AS times,
    frequency
FROM
(
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM home_team_home_corner_distribution_for) hhf 
    ON m.home_team_id = hhf.home_team_id
    UNION
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM home_team_home_corner_distribution_against) hha 
    ON m.home_team_id = hha.home_team_id
    UNION
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM home_team_home_corner_distribution_total) hht 
    ON m.home_team_id = hht.home_team_id
    UNION
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM away_team_away_corner_distribution_for) aaf 
    ON m.away_team_id = aaf.away_team_id
    UNION
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM away_team_away_corner_distribution_against) aaa
    ON m.away_team_id = aaa.away_team_id
    UNION
    SELECT * FROM v_upcoming_matches m
    JOIN (SELECT * FROM away_team_away_corner_distribution_total) aat
    ON m.away_team_id = aat.away_team_id
    ORDER BY date_time ASC
)