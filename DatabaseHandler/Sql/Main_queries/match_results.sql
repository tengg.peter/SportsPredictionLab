WITH 
num_teams_per_league AS
(
	SELECT 
		l.league_id,
		COUNT(teams_sm_id) AS num_teams
	FROM 
	v_leagues l
	JOIN seasons_teams st ON l.current_season_id = st.seasons_sm_id
	GROUP BY league_id
),
matches AS
(
	SELECT 
		ROW_NUMBER() OVER
		(
			ORDER BY c.date_time DESC
		) row_num,
		*
	FROM
	(
		SELECT * FROM v_completed_matches
		WHERE date_time < '3000-01-01'
	) c
	JOIN seasons s ON s.sm_id = c.sm_season_id
	JOIN 
	(
		SELECT * FROM v_leagues
		WHERE
			country_name = 'England' AND
			league_name = 'Premier League'
	) l ON l.league_id = s.sm_league_id
	JOIN num_teams_per_league nt ON nt.league_id = l.league_id
)
SELECT 
	ht.name AS home_team,
	at.name AS away_team,
	m.country_name,
	m.league_name,
	m.sm_league_id,
	m.sm_season_id,
	m.name AS season_name,
	m.sm_id AS fixture_id,
	m.status,
	m.sm_localteam_id AS home_team_id,
	m.sm_visitorteam_id AS away_team_id,
	m.localteam_score AS home_goals,
	m.visitorteam_score AS away_goals,
	m.date_time,
	num_teams
FROM 
(
	SELECT * FROM matches
	WHERE row_num <= num_teams * (num_teams - 1)	
) m
LEFT JOIN teams ht ON ht.sm_id = m.sm_localteam_id
LEFT JOIN teams at ON at.sm_id = m.sm_visitorteam_id