WITH
RECURSIVE weight_table(row_num, w) AS
(
	SELECT 1, 20.0
        UNION ALL 
    SELECT row_num + 1, w - 1.0 FROM weight_table WHERE w > 1.0
),
home_team_home_goal_stats AS
(
	SELECT
		sm_localteam_id,
		average_goals_for AS avg_home_team_home_goals_for,
		weighted_average_goals_for AS wavg_home_team_home_goals_for,
		average_goals_against AS avg_home_team_home_goals_against,
		weighted_average_goals_against AS wavg_home_team_home_goals_against
	FROM
	(
		SELECT 
			*, 
			AVG(localteam_score) OVER (PARTITION BY sm_localteam_id) AS average_goals_for,
			SUM(localteam_score * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_goals_for,
			AVG(visitorteam_score) OVER (PARTITION BY sm_localteam_id) AS average_goals_against,
			SUM(visitorteam_score * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_goals_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY c.sm_localteam_id
					ORDER BY c.date_time DESC
				) row_num,
				localteam_score,
				visitorteam_score,
				c.*
			FROM v_upcoming_matches m
			JOIN v_completed_matches c ON m.home_team_id = c.sm_localteam_id
			JOIN seasons s ON c.sm_season_id = s.sm_id
			WHERE s.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.localteam_score IS NOT NULL AND
			t1.visitorteam_score IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
home_team_away_goal_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_goals_for AS avg_home_team_away_goals_for,
		weighted_average_goals_for AS wavg_home_team_away_goals_for,
		average_goals_against AS avg_home_team_away_goals_against,
		weighted_average_goals_against AS wavg_home_team_away_goals_against
	FROM
	(
		SELECT 
			*, 
			AVG(visitorteam_score) OVER (PARTITION BY sm_visitorteam_id) AS average_goals_for,
			SUM(visitorteam_score * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_goals_for,
			AVG(localteam_score) OVER (PARTITION BY sm_visitorteam_id) AS average_goals_against,
			SUM(localteam_score * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_goals_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY c.sm_visitorteam_id
					ORDER BY c.date_time DESC
				) row_num,
				c.*
			FROM v_upcoming_matches m
			JOIN v_completed_matches c ON m.home_team_id = c.sm_visitorteam_id
			JOIN seasons s ON c.sm_season_id = s.sm_id
			WHERE s.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.localteam_score IS NOT NULL AND
			t1.visitorteam_score IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
away_team_home_goal_stats AS
(
	SELECT
		sm_localteam_id,
		average_goals_for AS avg_away_team_home_goals_for,
		weighted_average_goals_for AS wavg_away_team_home_goals_for,
		average_goals_against AS avg_away_team_home_goals_against,
		weighted_average_goals_against AS wavg_away_team_home_goals_against
	FROM
	(
		SELECT 
			*, 
			AVG(localteam_score) OVER (PARTITION BY sm_localteam_id) AS average_goals_for,
			SUM(localteam_score * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_goals_for,
			AVG(visitorteam_score) OVER (PARTITION BY sm_localteam_id) AS average_goals_against,
			SUM(visitorteam_score * w) OVER (PARTITION BY sm_localteam_id) / 
				SUM(w) OVER (PARTITION BY sm_localteam_id) AS weighted_average_goals_against,
			COUNT(w) OVER (PARTITION BY sm_localteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY c.sm_localteam_id
					ORDER BY c.date_time DESC
				) row_num,
				c.*
			FROM v_upcoming_matches m
			JOIN v_completed_matches c ON m.away_team_id = c.sm_localteam_id
			JOIN seasons s ON c.sm_season_id = s.sm_id
			WHERE s.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.localteam_score IS NOT NULL AND
			t1.visitorteam_score IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_localteam_id
),
away_team_away_goal_stats AS
(
	SELECT
		sm_visitorteam_id,
		average_goals_for AS avg_away_team_away_goals_for,
		weighted_average_goals_for AS wavg_away_team_away_goals_for,
		average_goals_against AS avg_away_team_away_goals_against,
		weighted_average_goals_against AS wavg_away_team_away_goals_against
	FROM
	(
		SELECT 
			*, 
			AVG(visitorteam_score) OVER (PARTITION BY sm_visitorteam_id) AS average_goals_for,
			SUM(visitorteam_score * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_goals_for,
			AVG(localteam_score) OVER (PARTITION BY sm_visitorteam_id) AS average_goals_against,
			SUM(localteam_score * w) OVER (PARTITION BY sm_visitorteam_id) / 
				SUM(w) OVER (PARTITION BY sm_visitorteam_id) AS weighted_average_goals_against,
			COUNT(w) OVER (PARTITION BY sm_visitorteam_id) AS sample_size
		FROM
		(
			SELECT
				ROW_NUMBER() OVER
				(
					PARTITION BY c.sm_visitorteam_id
					ORDER BY c.date_time DESC
				) row_num,
				c.*
			FROM v_upcoming_matches m
			JOIN v_completed_matches c ON m.away_team_id = c.sm_visitorteam_id
			JOIN seasons s ON c.sm_season_id = s.sm_id
			WHERE s.sm_league_id = m.sm_league_id
		) t1
		JOIN weight_table ON weight_table.row_num = t1.row_num AND 
			t1.localteam_score IS NOT NULL AND
			t1.visitorteam_score IS NOT NULL
		WHERE t1.row_num <= 20
	)
	GROUP BY sm_visitorteam_id
),
all_goal_stats AS
(
	SELECT 
		country_name, 
		league_name,
		home_team,
		away_team,
		date_time,
		hh.avg_home_team_home_goals_for,
		hh.wavg_home_team_home_goals_for,
		hh.avg_home_team_home_goals_against,
		hh.wavg_home_team_home_goals_against,
		ha.avg_home_team_away_goals_for,
		ha.wavg_home_team_away_goals_for,
		ha.avg_home_team_away_goals_against,
		ha.wavg_home_team_away_goals_against,
		ah.avg_away_team_home_goals_for,
		ah.wavg_away_team_home_goals_for,
		ah.avg_away_team_home_goals_against,
		ah.wavg_away_team_home_goals_against,
		aa.avg_away_team_away_goals_for,
		aa.wavg_away_team_away_goals_for,
		aa.avg_away_team_away_goals_against,
		aa.wavg_away_team_away_goals_against,
		hh.avg_home_team_home_goals_for + hh.avg_home_team_home_goals_against AS avg_home_team_home_goals_total,
		hh.wavg_home_team_home_goals_for + hh.wavg_home_team_home_goals_against AS wavg_home_team_home_goals_total,
		ha.avg_home_team_away_goals_for + ha.avg_home_team_away_goals_against AS avg_home_team_away_goals_total,
		ha.wavg_home_team_away_goals_for + ha.wavg_home_team_away_goals_against AS wavg_home_team_away_goals_total,
		ah.avg_away_team_home_goals_for + ah.avg_away_team_home_goals_against AS avg_away_team_home_goals_total,
		ah.wavg_away_team_home_goals_for + ah.wavg_away_team_home_goals_against AS wavg_away_team_home_goals_total,
		aa.avg_away_team_away_goals_for + aa.avg_away_team_away_goals_against AS avg_away_team_away_goals_total,
		aa.wavg_away_team_away_goals_for + aa.wavg_away_team_away_goals_against AS wavg_away_team_away_goals_total,
		
		(hh.avg_home_team_home_goals_for + ha.avg_home_team_away_goals_for) / 2.0 AS avg_home_team_goals_for,
		(hh.wavg_home_team_home_goals_for + ha.wavg_home_team_away_goals_for) / 2.0 AS wavg_home_team_goals_for,
		(hh.avg_home_team_home_goals_against + ha.avg_home_team_away_goals_against) / 2.0 AS avg_home_team_goals_against,
		(hh.wavg_home_team_home_goals_against + ha.wavg_home_team_away_goals_against) / 2.0 AS wavg_home_team_goals_against,
		(ah.avg_away_team_home_goals_for + aa.avg_away_team_away_goals_for) / 2.0 AS avg_away_team_goals_for,
		(ah.wavg_away_team_home_goals_for + aa.wavg_away_team_away_goals_for) / 2.0 AS wavg_away_team_goals_for,
		(ah.avg_away_team_home_goals_against + aa.avg_away_team_away_goals_against) / 2.0 AS avg_away_team_goals_against,
		(ah.wavg_away_team_home_goals_against + aa.wavg_away_team_away_goals_against) / 2.0 AS wavg_away_team_goals_against,

		(hh.avg_home_team_home_goals_for + ha.avg_home_team_away_goals_for + hh.avg_home_team_home_goals_against + ha.avg_home_team_away_goals_against) / 2.0 
			AS avg_home_team_goals_total,
		(hh.wavg_home_team_home_goals_for + ha.wavg_home_team_away_goals_for + hh.wavg_home_team_home_goals_against + ha.wavg_home_team_away_goals_against) / 2.0 
			AS wavg_home_team_goals_total,
		(ah.avg_away_team_home_goals_for + aa.avg_away_team_away_goals_for + ah.avg_away_team_home_goals_against + aa.avg_away_team_away_goals_against) / 2.0
			AS avg_away_team_goals_total,
		(ah.wavg_away_team_home_goals_for + aa.wavg_away_team_away_goals_for + ah.wavg_away_team_home_goals_against + aa.wavg_away_team_away_goals_against) / 2.0
			AS wavg_away_team_goals_total,

		(hh.avg_home_team_home_goals_for + aa.avg_away_team_away_goals_against) / 2.0 AS expected_home_goals,
		(hh.wavg_home_team_home_goals_for + aa.wavg_away_team_away_goals_against) / 2.0 AS wexpected_home_goals,
		(hh.avg_home_team_home_goals_against + aa.avg_away_team_away_goals_for) / 2.0 AS expected_away_goals,
		(hh.wavg_home_team_home_goals_against + aa.wavg_away_team_away_goals_for) / 2.0 AS wexpected_away_goals,

		(hh.avg_home_team_home_goals_for + aa.avg_away_team_away_goals_against + hh.avg_home_team_home_goals_against + aa.avg_away_team_away_goals_for) / 2.0 
			AS expected_goals_total,
		(hh.wavg_home_team_home_goals_for + aa.wavg_away_team_away_goals_against + hh.wavg_home_team_home_goals_against + aa.wavg_away_team_away_goals_for) / 2.0 
			AS wexpected_goals_total

	FROM v_upcoming_matches m
	LEFT JOIN (SELECT * FROM home_team_home_goal_stats) hh ON m.home_team_id = hh.sm_localteam_id
	LEFT JOIN (SELECT * FROM home_team_away_goal_stats) ha ON m.home_team_id = ha.sm_visitorteam_id
	LEFT JOIN (SELECT * FROM away_team_home_goal_stats) ah ON m.away_team_id = ah.sm_localteam_id
	LEFT JOIN (SELECT * FROM away_team_away_goal_stats) aa ON m.away_team_id = aa.sm_visitorteam_id
	ORDER BY date_time ASC
),
relevant_goal_stats AS
(
	SELECT
		country_name,
		league_name,
		home_team,
		away_team,
		datetime(date_time, 'localtime') AS date_time,
		printf("'%.2f", wavg_home_team_home_goals_for) AS wavg_home_team_home_goals_for,
		printf("'%.2f", wavg_home_team_home_goals_against) AS wavg_home_team_home_goals_against,
		printf("'%.2f", wavg_away_team_away_goals_for) AS wavg_away_team_away_goals_for,
		printf("'%.2f", wavg_away_team_away_goals_against) AS wavg_away_team_away_goals_against,
		
		printf("'%.2f", wavg_home_team_home_goals_total) AS wavg_home_team_home_goals_total,
		printf("'%.2f", wavg_away_team_away_goals_total) AS wavg_away_team_away_goals_total,

		printf("'%.2f", wavg_home_team_goals_for) AS wavg_home_team_goals_for,
		printf("'%.2f", wavg_home_team_goals_against) AS wavg_home_team_goals_against,
		printf("'%.2f", wavg_away_team_goals_for) AS wavg_away_team_goals_for,
		printf("'%.2f", wavg_away_team_goals_against) AS wavg_away_team_goals_against,

		printf("'%.2f", wavg_home_team_goals_total) AS wavg_home_team_goals_total,
		printf("'%.2f", wavg_away_team_goals_total) AS wavg_away_team_goals_total,

		printf("'%.2f", wexpected_home_goals) AS wexpected_home_goals,
		printf("'%.2f", wexpected_away_goals) AS wexpected_away_goals,

		printf("'%.2f", wexpected_goals_total) AS wexpected_goals_total
	FROM all_goal_stats
)

SELECT * FROM
-- relevant_goal_stats
all_goal_stats