#include "DatabaseHandler/Source/v2/CreateSchema.h"
#include "DatabaseHandler/Source/v2/SqlCreateIndexScripts.h"
#include "DatabaseHandler/Source/v2/SqlCreateTableScripts.h"
//##protect##"includes"
#include "DatabaseHandler/Source/Common/Create.h"
//##protect##"includes"

//3rd Party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"

namespace database_handler::v2
{
	void CreateSchema(SQLite::Database& db)
	{
		db.exec(CREATE_TABLE_BOOKMAKERS);
		db.exec(CREATE_INDEX_BOOKMAKERS);
		
		db.exec(CREATE_TABLE_COACHES);
		db.exec(CREATE_INDEX_COACHES);
		
		db.exec(CREATE_TABLE_COMMENTARIES);
		db.exec(CREATE_INDEX_COMMENTARIES);
		
		db.exec(CREATE_TABLE_CONTINENTS);
		db.exec(CREATE_INDEX_CONTINENTS);
		
		db.exec(CREATE_TABLE_COUNTRIES);
		db.exec(CREATE_INDEX_COUNTRIES);
		
		db.exec(CREATE_TABLE_FIXTURES);
		db.exec(CREATE_INDEX_FIXTURES);
		
		db.exec(CREATE_TABLE_FIXTURE_SIDELINES);
		db.exec(CREATE_INDEX_FIXTURE_SIDELINES);
		
		db.exec(CREATE_TABLE_GOAL_LINES);
		db.exec(CREATE_INDEX_GOAL_LINES);
		
		db.exec(CREATE_TABLE_GOALS_SCORED_MINUTES);
		db.exec(CREATE_INDEX_GOALS_SCORED_MINUTES);
		
		db.exec(CREATE_TABLE_LEAGUES);
		db.exec(CREATE_INDEX_LEAGUES);
		
		db.exec(CREATE_TABLE_LINEUPS);
		db.exec(CREATE_INDEX_LINEUPS);
		
		db.exec(CREATE_TABLE_MARKETS);
		db.exec(CREATE_INDEX_MARKETS);
		
		db.exec(CREATE_TABLE_MATCH_STATISTICS);
		db.exec(CREATE_INDEX_MATCH_STATISTICS);
		
		db.exec(CREATE_TABLE_ODDS);
		db.exec(CREATE_INDEX_ODDS);
		
		db.exec(CREATE_TABLE_PLAYER_PERFORMANCES);
		db.exec(CREATE_INDEX_PLAYER_PERFORMANCES);
		
		db.exec(CREATE_TABLE_PLAYERS);
		db.exec(CREATE_INDEX_PLAYERS);
		
		db.exec(CREATE_TABLE_PLAYER_SIDELINES);
		db.exec(CREATE_INDEX_PLAYER_SIDELINES);
		
		db.exec(CREATE_TABLE_PLAYER_TRANSFERS);
		db.exec(CREATE_INDEX_PLAYER_TRANSFERS);
		
		db.exec(CREATE_TABLE_ROUNDS);
		db.exec(CREATE_INDEX_ROUNDS);
		
		db.exec(CREATE_TABLE_SEASONS);
		db.exec(CREATE_INDEX_SEASONS);
		
		db.exec(CREATE_TABLE_SEASON_STATISTICS);
		db.exec(CREATE_INDEX_SEASON_STATISTICS);
		
		db.exec(CREATE_TABLE_STAGES);
		db.exec(CREATE_INDEX_STAGES);
		
		db.exec(CREATE_TABLE_STANDING_POSITIONS);
		db.exec(CREATE_INDEX_STANDING_POSITIONS);
		
		db.exec(CREATE_TABLE_STANDINGS);
		db.exec(CREATE_INDEX_STANDINGS);
		
		db.exec(CREATE_TABLE_TEAM_GOAL_LINES);
		db.exec(CREATE_INDEX_TEAM_GOAL_LINES);
		
		db.exec(CREATE_TABLE_TEAM_GOAL_MINUTES);
		db.exec(CREATE_INDEX_TEAM_GOAL_MINUTES);
		
		db.exec(CREATE_TABLE_TEAMS);
		db.exec(CREATE_INDEX_TEAMS);
		
		db.exec(CREATE_TABLE_TEAM_SEASON_STATISTICS);
		db.exec(CREATE_INDEX_TEAM_SEASON_STATISTICS);
		
		db.exec(CREATE_TABLE_TOP_ASSIST_SCORERS);
		db.exec(CREATE_INDEX_TOP_ASSIST_SCORERS);
		
		db.exec(CREATE_TABLE_TOP_CARD_SCORERS);
		db.exec(CREATE_INDEX_TOP_CARD_SCORERS);
		
		db.exec(CREATE_TABLE_TOP_GOAL_SCORERS);
		db.exec(CREATE_INDEX_TOP_GOAL_SCORERS);
		
		db.exec(CREATE_TABLE_TREND_POINTS);
		db.exec(CREATE_INDEX_TREND_POINTS);
		
		db.exec(CREATE_TABLE_TRENDS);
		db.exec(CREATE_INDEX_TRENDS);
		
		db.exec(CREATE_TABLE_VENUES);
		db.exec(CREATE_INDEX_VENUES);
		
		//##protect##"switch tables"
		common::CreateSwitchTable(db, "seasons_teams", { {"seasons", "sm_id"}, {"teams", "sm_id"} });
		common::CreateSwitchTable(db, "seasons_venues", { {"seasons", "sm_id"}, {"venues", "sm_id"} });
		//##protect##"switch tables"

	}
}