#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalMinute.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalLine.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class TeamSeasonStatistics
	{
	public:
		int64_t id = 0;
		int64_t sm_team_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_stage_id = 0;
		int home_wins = 0;
		int away_wins = 0;
		int home_draws = 0;
		int away_draws = 0;
		int home_losses = 0;
		int away_losses = 0;
		int home_goals_for = 0;
		int away_goals_for = 0;
		int home_goals_against = 0;
		int away_goals_against = 0;
		int home_clean_sheet = 0;
		int away_clean_sheet = 0;
		int home_failed_to_score = 0;
		int away_failed_to_score = 0;
		std::optional<double> home_avg_goals_per_game_scored_opt;
		std::optional<double> away_avg_goals_per_game_scored_opt;
		std::optional<double> home_avg_goals_per_game_conceded_opt;
		std::optional<double> away_avg_goals_per_game_conceded_opt;
		utils::String home_avg_first_goal_scored;
		utils::String away_avg_first_goal_scored;
		utils::String overall_avg_first_goal_scored;
		utils::String home_avg_first_goal_conceded;
		utils::String away_avg_first_goal_conceded;
		utils::String overall_avg_first_goal_conceded;
		std::optional<int> attacks_opt;
		std::optional<int> dangerous_attacks_opt;
		std::optional<double> avg_ball_possession_percentage_opt;
		std::optional<int> fouls_opt;
		std::optional<double> avg_fouls_per_game_opt;
		std::optional<int> offsides_opt;
		std::optional<int> redcards_opt;
		std::optional<int> yellowcards_opt;
		std::optional<int> shots_blocked_opt;
		std::optional<int> shots_off_target_opt;
		std::optional<double> avg_shots_off_target_per_game_opt;
		std::optional<int> shots_on_target_opt;
		std::optional<double> avg_shots_on_target_per_game_opt;
		std::optional<double> avg_corners_opt;
		std::optional<int> total_corners_opt;
		std::optional<double> btts_opt;
		//##protect##"class members"
		std::set<TeamGoalMinute, TeamGoalMinuteCmp> goalMinutes;
		std::set<TeamGoalLine, TeamGoalLineCmp> goalLines;
		//##protect##"class members"

		explicit TeamSeasonStatistics() = default;
		virtual ~TeamSeasonStatistics() {}

		explicit TeamSeasonStatistics(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_stage_id(stm.getColumn("sm_stage_id").getInt64())
			, home_wins(stm.getColumn("home_wins").getInt())
			, away_wins(stm.getColumn("away_wins").getInt())
			, home_draws(stm.getColumn("home_draws").getInt())
			, away_draws(stm.getColumn("away_draws").getInt())
			, home_losses(stm.getColumn("home_losses").getInt())
			, away_losses(stm.getColumn("away_losses").getInt())
			, home_goals_for(stm.getColumn("home_goals_for").getInt())
			, away_goals_for(stm.getColumn("away_goals_for").getInt())
			, home_goals_against(stm.getColumn("home_goals_against").getInt())
			, away_goals_against(stm.getColumn("away_goals_against").getInt())
			, home_clean_sheet(stm.getColumn("home_clean_sheet").getInt())
			, away_clean_sheet(stm.getColumn("away_clean_sheet").getInt())
			, home_failed_to_score(stm.getColumn("home_failed_to_score").getInt())
			, away_failed_to_score(stm.getColumn("away_failed_to_score").getInt())
			, home_avg_goals_per_game_scored_opt(!stm.getColumn("home_avg_goals_per_game_scored").isNull() ?
				std::optional{stm.getColumn("home_avg_goals_per_game_scored").getDouble()} : std::nullopt)
			, away_avg_goals_per_game_scored_opt(!stm.getColumn("away_avg_goals_per_game_scored").isNull() ?
				std::optional{stm.getColumn("away_avg_goals_per_game_scored").getDouble()} : std::nullopt)
			, home_avg_goals_per_game_conceded_opt(!stm.getColumn("home_avg_goals_per_game_conceded").isNull() ?
				std::optional{stm.getColumn("home_avg_goals_per_game_conceded").getDouble()} : std::nullopt)
			, away_avg_goals_per_game_conceded_opt(!stm.getColumn("away_avg_goals_per_game_conceded").isNull() ?
				std::optional{stm.getColumn("away_avg_goals_per_game_conceded").getDouble()} : std::nullopt)
			, home_avg_first_goal_scored(StdStringToUtilsString(stm.getColumn("home_avg_first_goal_scored").getString()))
			, away_avg_first_goal_scored(StdStringToUtilsString(stm.getColumn("away_avg_first_goal_scored").getString()))
			, overall_avg_first_goal_scored(StdStringToUtilsString(stm.getColumn("overall_avg_first_goal_scored").getString()))
			, home_avg_first_goal_conceded(StdStringToUtilsString(stm.getColumn("home_avg_first_goal_conceded").getString()))
			, away_avg_first_goal_conceded(StdStringToUtilsString(stm.getColumn("away_avg_first_goal_conceded").getString()))
			, overall_avg_first_goal_conceded(StdStringToUtilsString(stm.getColumn("overall_avg_first_goal_conceded").getString()))
			, attacks_opt(!stm.getColumn("attacks").isNull() ?
				std::optional{stm.getColumn("attacks").getInt()} : std::nullopt)
			, dangerous_attacks_opt(!stm.getColumn("dangerous_attacks").isNull() ?
				std::optional{stm.getColumn("dangerous_attacks").getInt()} : std::nullopt)
			, avg_ball_possession_percentage_opt(!stm.getColumn("avg_ball_possession_percentage").isNull() ?
				std::optional{stm.getColumn("avg_ball_possession_percentage").getDouble()} : std::nullopt)
			, fouls_opt(!stm.getColumn("fouls").isNull() ?
				std::optional{stm.getColumn("fouls").getInt()} : std::nullopt)
			, avg_fouls_per_game_opt(!stm.getColumn("avg_fouls_per_game").isNull() ?
				std::optional{stm.getColumn("avg_fouls_per_game").getDouble()} : std::nullopt)
			, offsides_opt(!stm.getColumn("offsides").isNull() ?
				std::optional{stm.getColumn("offsides").getInt()} : std::nullopt)
			, redcards_opt(!stm.getColumn("redcards").isNull() ?
				std::optional{stm.getColumn("redcards").getInt()} : std::nullopt)
			, yellowcards_opt(!stm.getColumn("yellowcards").isNull() ?
				std::optional{stm.getColumn("yellowcards").getInt()} : std::nullopt)
			, shots_blocked_opt(!stm.getColumn("shots_blocked").isNull() ?
				std::optional{stm.getColumn("shots_blocked").getInt()} : std::nullopt)
			, shots_off_target_opt(!stm.getColumn("shots_off_target").isNull() ?
				std::optional{stm.getColumn("shots_off_target").getInt()} : std::nullopt)
			, avg_shots_off_target_per_game_opt(!stm.getColumn("avg_shots_off_target_per_game").isNull() ?
				std::optional{stm.getColumn("avg_shots_off_target_per_game").getDouble()} : std::nullopt)
			, shots_on_target_opt(!stm.getColumn("shots_on_target").isNull() ?
				std::optional{stm.getColumn("shots_on_target").getInt()} : std::nullopt)
			, avg_shots_on_target_per_game_opt(!stm.getColumn("avg_shots_on_target_per_game").isNull() ?
				std::optional{stm.getColumn("avg_shots_on_target_per_game").getDouble()} : std::nullopt)
			, avg_corners_opt(!stm.getColumn("avg_corners").isNull() ?
				std::optional{stm.getColumn("avg_corners").getDouble()} : std::nullopt)
			, total_corners_opt(!stm.getColumn("total_corners").isNull() ?
				std::optional{stm.getColumn("total_corners").getInt()} : std::nullopt)
			, btts_opt(!stm.getColumn("btts").isNull() ?
				std::optional{stm.getColumn("btts").getDouble()} : std::nullopt)
			
		{}
	};

	using UpTeamSeasonStatistics = std::unique_ptr<TeamSeasonStatistics>;

	struct TeamSeasonStatisticsCmp
	{
		bool operator()(const TeamSeasonStatistics& a, const TeamSeasonStatistics& b) const
		{
			return a.sm_team_id != b.sm_team_id ? a.sm_team_id < b.sm_team_id :
				a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.sm_stage_id < b.sm_stage_id;
		}

		bool operator()(const UpTeamSeasonStatistics& a, const UpTeamSeasonStatistics& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//these fw declarations are needed for GCC
	//template<
	//	class T,
	//	typename std::enable_if <
	//	std::is_same<TeamSeasonStatistics, T>{} || std::is_same<UpTeamSeasonStatistics, T>{},
	//	int > ::type = 0 >
	//inline std::vector<UpTeamSeasonStatistics> QueryAll(SQLite::Database& db);

	//template<
	//	class T,
	//	typename std::enable_if <
	//	std::is_same<TeamSeasonStatistics, T>{} || std::is_same<UpTeamSeasonStatistics, T>{},
	//	int > ::type = 0 >
	//inline std::vector<UpTeamSeasonStatistics> Query(SQLite::Database& db, const std::string& where);

#pragma warning( push )
#pragma warning( disable : 4348 )
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamSeasonStatistics, T>{} || std::is_same<UpTeamSeasonStatistics, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM team_season_statistics;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamSeasonStatistics, T>{} || std::is_same<UpTeamSeasonStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamSeasonStatistics> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM team_season_statistics;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTeamSeasonStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamSeasonStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamSeasonStatistics, T>{} || std::is_same<UpTeamSeasonStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamSeasonStatistics> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<TeamSeasonStatistics>(db);
		}

		const std::string query{ "SELECT * FROM team_season_statistics WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTeamSeasonStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamSeasonStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<TeamSeasonStatistics, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamSeasonStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(teamSeasonStatistics));
		std::transform(std::cbegin(teamSeasonStatistics), std::cend(teamSeasonStatistics), std::back_inserter(pointers),
			[](const typename TContainer::value_type& teamSeasonStatistics)
			{
				return &teamSeasonStatistics;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTeamSeasonStatistics, typename TContainer::value_type>{} ||
		std::is_same<const TeamSeasonStatistics*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamSeasonStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<TeamSeasonStatistics>: Inserting " << std::size(teamSeasonStatistics) << " teamSeasonStatistics." << endl;
		}
		if (std::empty(teamSeasonStatistics) || 
			std::all_of(std::cbegin(teamSeasonStatistics), std::cend(teamSeasonStatistics), 
				[](const auto& pTeamSeasonStatistics) {return nullptr == pTeamSeasonStatistics;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 43;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(teamSeasonStatistics) / batchSize;
			if(0 != std::size(teamSeasonStatistics) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(teamSeasonStatistics);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(teamSeasonStatistics)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(teamSeasonStatistics);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO team_season_statistics VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_stage_id" << i << ", ";
					ss << ":home_wins" << i << ", ";
					ss << ":away_wins" << i << ", ";
					ss << ":home_draws" << i << ", ";
					ss << ":away_draws" << i << ", ";
					ss << ":home_losses" << i << ", ";
					ss << ":away_losses" << i << ", ";
					ss << ":home_goals_for" << i << ", ";
					ss << ":away_goals_for" << i << ", ";
					ss << ":home_goals_against" << i << ", ";
					ss << ":away_goals_against" << i << ", ";
					ss << ":home_clean_sheet" << i << ", ";
					ss << ":away_clean_sheet" << i << ", ";
					ss << ":home_failed_to_score" << i << ", ";
					ss << ":away_failed_to_score" << i << ", ";
					ss << ":home_avg_goals_per_game_scored" << i << ", ";
					ss << ":away_avg_goals_per_game_scored" << i << ", ";
					ss << ":home_avg_goals_per_game_conceded" << i << ", ";
					ss << ":away_avg_goals_per_game_conceded" << i << ", ";
					ss << ":home_avg_first_goal_scored" << i << ", ";
					ss << ":away_avg_first_goal_scored" << i << ", ";
					ss << ":overall_avg_first_goal_scored" << i << ", ";
					ss << ":home_avg_first_goal_conceded" << i << ", ";
					ss << ":away_avg_first_goal_conceded" << i << ", ";
					ss << ":overall_avg_first_goal_conceded" << i << ", ";
					ss << ":attacks" << i << ", ";
					ss << ":dangerous_attacks" << i << ", ";
					ss << ":avg_ball_possession_percentage" << i << ", ";
					ss << ":fouls" << i << ", ";
					ss << ":avg_fouls_per_game" << i << ", ";
					ss << ":offsides" << i << ", ";
					ss << ":redcards" << i << ", ";
					ss << ":yellowcards" << i << ", ";
					ss << ":shots_blocked" << i << ", ";
					ss << ":shots_off_target" << i << ", ";
					ss << ":avg_shots_off_target_per_game" << i << ", ";
					ss << ":shots_on_target" << i << ", ";
					ss << ":avg_shots_on_target_per_game" << i << ", ";
					ss << ":avg_corners" << i << ", ";
					ss << ":total_corners" << i << ", ";
					ss << ":btts" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_stage_id" + idx, (*it)->sm_stage_id);
					insert.bind(":home_wins" + idx, (*it)->home_wins);
					insert.bind(":away_wins" + idx, (*it)->away_wins);
					insert.bind(":home_draws" + idx, (*it)->home_draws);
					insert.bind(":away_draws" + idx, (*it)->away_draws);
					insert.bind(":home_losses" + idx, (*it)->home_losses);
					insert.bind(":away_losses" + idx, (*it)->away_losses);
					insert.bind(":home_goals_for" + idx, (*it)->home_goals_for);
					insert.bind(":away_goals_for" + idx, (*it)->away_goals_for);
					insert.bind(":home_goals_against" + idx, (*it)->home_goals_against);
					insert.bind(":away_goals_against" + idx, (*it)->away_goals_against);
					insert.bind(":home_clean_sheet" + idx, (*it)->home_clean_sheet);
					insert.bind(":away_clean_sheet" + idx, (*it)->away_clean_sheet);
					insert.bind(":home_failed_to_score" + idx, (*it)->home_failed_to_score);
					insert.bind(":away_failed_to_score" + idx, (*it)->away_failed_to_score);
					BindOptional(insert, ":home_avg_goals_per_game_scored" + idx, (*it)->home_avg_goals_per_game_scored_opt);
					BindOptional(insert, ":away_avg_goals_per_game_scored" + idx, (*it)->away_avg_goals_per_game_scored_opt);
					BindOptional(insert, ":home_avg_goals_per_game_conceded" + idx, (*it)->home_avg_goals_per_game_conceded_opt);
					BindOptional(insert, ":away_avg_goals_per_game_conceded" + idx, (*it)->away_avg_goals_per_game_conceded_opt);
					insert.bind(":home_avg_first_goal_scored" + idx, UtilsStringToStdString((*it)->home_avg_first_goal_scored));
					insert.bind(":away_avg_first_goal_scored" + idx, UtilsStringToStdString((*it)->away_avg_first_goal_scored));
					insert.bind(":overall_avg_first_goal_scored" + idx, UtilsStringToStdString((*it)->overall_avg_first_goal_scored));
					insert.bind(":home_avg_first_goal_conceded" + idx, UtilsStringToStdString((*it)->home_avg_first_goal_conceded));
					insert.bind(":away_avg_first_goal_conceded" + idx, UtilsStringToStdString((*it)->away_avg_first_goal_conceded));
					insert.bind(":overall_avg_first_goal_conceded" + idx, UtilsStringToStdString((*it)->overall_avg_first_goal_conceded));
					BindOptional(insert, ":attacks" + idx, (*it)->attacks_opt);
					BindOptional(insert, ":dangerous_attacks" + idx, (*it)->dangerous_attacks_opt);
					BindOptional(insert, ":avg_ball_possession_percentage" + idx, (*it)->avg_ball_possession_percentage_opt);
					BindOptional(insert, ":fouls" + idx, (*it)->fouls_opt);
					BindOptional(insert, ":avg_fouls_per_game" + idx, (*it)->avg_fouls_per_game_opt);
					BindOptional(insert, ":offsides" + idx, (*it)->offsides_opt);
					BindOptional(insert, ":redcards" + idx, (*it)->redcards_opt);
					BindOptional(insert, ":yellowcards" + idx, (*it)->yellowcards_opt);
					BindOptional(insert, ":shots_blocked" + idx, (*it)->shots_blocked_opt);
					BindOptional(insert, ":shots_off_target" + idx, (*it)->shots_off_target_opt);
					BindOptional(insert, ":avg_shots_off_target_per_game" + idx, (*it)->avg_shots_off_target_per_game_opt);
					BindOptional(insert, ":shots_on_target" + idx, (*it)->shots_on_target_opt);
					BindOptional(insert, ":avg_shots_on_target_per_game" + idx, (*it)->avg_shots_on_target_per_game_opt);
					BindOptional(insert, ":avg_corners" + idx, (*it)->avg_corners_opt);
					BindOptional(insert, ":total_corners" + idx, (*it)->total_corners_opt);
					BindOptional(insert, ":btts" + idx, (*it)->btts_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
#pragma warning( pop )
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					ss.str("");
					ss << "sm_team_id = " << (*it)->sm_team_id <<
						" AND sm_season_id = " << (*it)->sm_season_id <<
						" AND sm_stage_id = " << (*it)->sm_stage_id;
					const auto& stats = Query<TeamSeasonStatistics>(db, ss.str());
					if (1 != stats.size())
					{
						throw std::runtime_error("Exactly one matching TeamSeasonStatistics expected.");
					}

					std::vector<TeamGoalMinute> goalMinutes;
					for (auto& gm : (*it)->goalMinutes)
					{
						goalMinutes.push_back(gm);
						goalMinutes.back().team_season_statistics_id = stats.front()->id;
					}
					InsertOr(onConflict, db, goalMinutes, printInfo, false);

					std::vector<TeamGoalLine> goalLines;
					for (auto& gl : (*it)->goalLines)
					{
						goalLines.push_back(gl);
						goalLines.back().team_season_statistics_id = stats.front()->id;
					}
					InsertOr(onConflict, db, goalLines, false, false);
				}
			}
			
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(teamSeasonStatistics) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "team_season_statistics");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0