#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class GoalLine
	{
	public:
		int64_t id = 0;
		int64_t sm_season_statistics_id = 0;
		utils::String line_over;
		double percentage = 0.0;
		//##protect##"class members"
//##protect##"class members"

		explicit GoalLine() = default;
		virtual ~GoalLine() {}

		explicit GoalLine(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_season_statistics_id(stm.getColumn("sm_season_statistics_id").getInt64())
			, line_over(StdStringToUtilsString(stm.getColumn("line_over").getString()))
			, percentage(stm.getColumn("percentage").getDouble())
			
		{}
	};

	using UpGoalLine = std::unique_ptr<GoalLine>;

	struct GoalLineCmp
	{
		bool operator()(const GoalLine& a, const GoalLine& b) const
		{
			return a.sm_season_statistics_id != b.sm_season_statistics_id ? a.sm_season_statistics_id < b.sm_season_statistics_id :
				a.line_over < b.line_over;
		}

		bool operator()(const UpGoalLine& a, const UpGoalLine& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalLine, T>{} || std::is_same<UpGoalLine, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM goal_lines;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalLine, T>{} || std::is_same<UpGoalLine, T>{},
		int > ::type = 0 >
	inline std::vector<UpGoalLine> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM goal_lines;" };

		SQLite::Statement stm(db, query);
		std::vector<UpGoalLine> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<GoalLine>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalLine, T>{} || std::is_same<UpGoalLine, T>{},
		int > ::type = 0 >
	inline std::vector<UpGoalLine> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<GoalLine>(db);
		}

		const std::string query{ "SELECT * FROM goal_lines WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpGoalLine> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<GoalLine>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<GoalLine, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& goalLines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(goalLines));
		std::transform(std::cbegin(goalLines), std::cend(goalLines), std::back_inserter(pointers),
			[](const typename TContainer::value_type& goalLine)
			{
				return &goalLine;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpGoalLine, typename TContainer::value_type>{} ||
		std::is_same<const GoalLine*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& goalLines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<GoalLine>: Inserting " << std::size(goalLines) << " goalLines." << endl;
		}
		if (std::empty(goalLines) || 
			std::all_of(std::cbegin(goalLines), std::cend(goalLines), 
				[](const auto& pGoalLine) {return nullptr == pGoalLine;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 3;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(goalLines) / batchSize;
			if(0 != std::size(goalLines) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(goalLines);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(goalLines)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(goalLines);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO goal_lines VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_season_statistics_id" << i << ", ";
					ss << ":line_over" << i << ", ";
					ss << ":percentage" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_season_statistics_id" + idx, (*it)->sm_season_statistics_id);
					insert.bind(":line_over" + idx, UtilsStringToStdString((*it)->line_over));
					insert.bind(":percentage" + idx, (*it)->percentage);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(goalLines) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "goal_lines");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0