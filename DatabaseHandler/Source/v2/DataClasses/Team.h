#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/TeamSeasonStatistics.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Team
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		std::optional<int64_t> sm_legacy_id_opt;
		utils::String name;
		std::optional<utils::String> short_code_opt;
		std::optional<utils::String> twitter_opt;
		int64_t sm_country_id = 0;
		bool national_team = false;
		std::optional<int64_t> founded_opt;
		std::optional<utils::String> logo_path_opt;
		std::optional<int64_t> sm_venue_id_opt;
		std::optional<int64_t> sm_current_season_id_opt;
		//##protect##"class members"
		std::set<TeamSeasonStatistics, TeamSeasonStatisticsCmp> seasonStatistics;
		//##protect##"class members"

		explicit Team() = default;
		virtual ~Team() {}

		explicit Team(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_legacy_id_opt(!stm.getColumn("sm_legacy_id").isNull() ?
				std::optional{stm.getColumn("sm_legacy_id").getInt64()} : std::nullopt)
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, short_code_opt(!stm.getColumn("short_code").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("short_code").getString())} : std::nullopt)
			, twitter_opt(!stm.getColumn("twitter").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("twitter").getString())} : std::nullopt)
			, sm_country_id(stm.getColumn("sm_country_id").getInt64())
			, national_team(static_cast<bool>(stm.getColumn("national_team").getInt()))
			, founded_opt(!stm.getColumn("founded").isNull() ?
				std::optional{stm.getColumn("founded").getInt64()} : std::nullopt)
			, logo_path_opt(!stm.getColumn("logo_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("logo_path").getString())} : std::nullopt)
			, sm_venue_id_opt(!stm.getColumn("sm_venue_id").isNull() ?
				std::optional{stm.getColumn("sm_venue_id").getInt64()} : std::nullopt)
			, sm_current_season_id_opt(!stm.getColumn("sm_current_season_id").isNull() ?
				std::optional{stm.getColumn("sm_current_season_id").getInt64()} : std::nullopt)
			
		{}
	};

	using UpTeam = std::unique_ptr<Team>;

	struct TeamCmp
	{
		bool operator()(const Team& a, const Team& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpTeam& a, const UpTeam& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Team, T>{} || std::is_same<UpTeam, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM teams;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Team, T>{} || std::is_same<UpTeam, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeam> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM teams;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTeam> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Team>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Team, T>{} || std::is_same<UpTeam, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeam> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Team>(db);
		}

		const std::string query{ "SELECT * FROM teams WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTeam> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Team>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Team, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teams,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(teams));
		std::transform(std::cbegin(teams), std::cend(teams), std::back_inserter(pointers),
			[](const typename TContainer::value_type& team)
			{
				return &team;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTeam, typename TContainer::value_type>{} ||
		std::is_same<const Team*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teams,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Team>: Inserting " << std::size(teams) << " teams." << endl;
		}
		if (std::empty(teams) || 
			std::all_of(std::cbegin(teams), std::cend(teams), 
				[](const auto& pTeam) {return nullptr == pTeam;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 11;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(teams) / batchSize;
			if(0 != std::size(teams) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(teams);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(teams)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(teams);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO teams VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_legacy_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":short_code" << i << ", ";
					ss << ":twitter" << i << ", ";
					ss << ":sm_country_id" << i << ", ";
					ss << ":national_team" << i << ", ";
					ss << ":founded" << i << ", ";
					ss << ":logo_path" << i << ", ";
					ss << ":sm_venue_id" << i << ", ";
					ss << ":sm_current_season_id" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					BindOptional(insert, ":sm_legacy_id" + idx, (*it)->sm_legacy_id_opt);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					BindOptional(insert, ":short_code" + idx, (*it)->short_code_opt);
					BindOptional(insert, ":twitter" + idx, (*it)->twitter_opt);
					insert.bind(":sm_country_id" + idx, (*it)->sm_country_id);
					insert.bind(":national_team" + idx, (*it)->national_team);
					BindOptional(insert, ":founded" + idx, (*it)->founded_opt);
					BindOptional(insert, ":logo_path" + idx, (*it)->logo_path_opt);
					BindOptional(insert, ":sm_venue_id" + idx, (*it)->sm_venue_id_opt);
					BindOptional(insert, ":sm_current_season_id" + idx, (*it)->sm_current_season_id_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					InsertOr(onConflict, db, (*it)->seasonStatistics, false, false);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(teams) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "teams");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0