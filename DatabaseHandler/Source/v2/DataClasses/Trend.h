#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/TrendPoint.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Trend
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t sm_fixture_id = 0;
		int64_t sm_team_id = 0;
		utils::String type;
		utils::String updated_at;
		int64_t timezone_type = 0;
		utils::String timezone;
		//##protect##"class members"
		std::set<TrendPoint, TrendPointCmp> trendPoints;
		//##protect##"class members"

		explicit Trend() = default;
		virtual ~Trend() {}

		explicit Trend(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			, updated_at(StdStringToUtilsString(stm.getColumn("updated_at").getString()))
			, timezone_type(stm.getColumn("timezone_type").getInt64())
			, timezone(StdStringToUtilsString(stm.getColumn("timezone").getString()))
			
		{}
	};

	using UpTrend = std::unique_ptr<Trend>;

	struct TrendCmp
	{
		bool operator()(const Trend& a, const Trend& b) const
		{
			return a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.sm_team_id != b.sm_team_id ? a.sm_team_id < b.sm_team_id :
				a.type != b.type ? a.type < b.type :
				a.sm_id < b.sm_id;
		}

		bool operator()(const UpTrend& a, const UpTrend& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Trend, T>{} || std::is_same<UpTrend, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM trends;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Trend, T>{} || std::is_same<UpTrend, T>{},
		int > ::type = 0 >
	inline std::vector<UpTrend> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM trends;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTrend> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Trend>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Trend, T>{} || std::is_same<UpTrend, T>{},
		int > ::type = 0 >
	inline std::vector<UpTrend> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Trend>(db);
		}

		const std::string query{ "SELECT * FROM trends WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTrend> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Trend>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Trend, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& trends,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(trends));
		std::transform(std::cbegin(trends), std::cend(trends), std::back_inserter(pointers),
			[](const typename TContainer::value_type& trend)
			{
				return &trend;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTrend, typename TContainer::value_type>{} ||
		std::is_same<const Trend*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& trends,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Trend>: Inserting " << std::size(trends) << " trends." << endl;
		}
		if (std::empty(trends) || 
			std::all_of(std::cbegin(trends), std::cend(trends), 
				[](const auto& pTrend) {return nullptr == pTrend;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 7;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(trends) / batchSize;
			if(0 != std::size(trends) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(trends);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(trends)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(trends);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO trends VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":updated_at" << i << ", ";
					ss << ":timezone_type" << i << ", ";
					ss << ":timezone" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					insert.bind(":updated_at" + idx, UtilsStringToStdString((*it)->updated_at));
					insert.bind(":timezone_type" + idx, (*it)->timezone_type);
					insert.bind(":timezone" + idx, UtilsStringToStdString((*it)->timezone));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					InsertOr(onConflict, db, (*it)->trendPoints, false, false);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(trends) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "trends");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0