#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/GoalsScoredMinute.h"
#include "DatabaseHandler/Source/v2/DataClasses/GoalLine.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class SeasonStatistics
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_league_id = 0;
		int number_of_clubs = 0;
		int number_of_matches = 0;
		int number_of_matches_played = 0;
		int number_of_goals = 0;
		int matches_both_teams_scored = 0;
		int number_of_yellowcards = 0;
		int number_of_yellowredcards = 0;
		int number_of_redcards = 0;
		std::optional<double> avg_goals_per_match_opt;
		std::optional<double> avg_yellowcards_per_match_opt;
		std::optional<double> avg_yellowredcards_per_match_opt;
		std::optional<double> avg_redcards_per_match_opt;
		std::optional<int64_t> sm_team_with_most_goals_id_opt;
		std::optional<int64_t> sm_team_with_most_conceded_goals_id_opt;
		std::optional<int64_t> sm_team_with_most_goals_per_match_id_opt;
		std::optional<int64_t> sm_season_topscorer_id_opt;
		std::optional<int> season_topscorer_number_opt;
		std::optional<int64_t> sm_season_assist_topscorer_id_opt;
		std::optional<int> season_assist_topscorer_number_opt;
		std::optional<int64_t> sm_team_most_cleansheets_id_opt;
		std::optional<int> team_most_cleansheets_number_opt;
		std::optional<int64_t> sm_goalkeeper_most_cleansheets_id_opt;
		std::optional<int> goalkeeper_most_cleansheets_number_opt;
		int goal_scored_every_minutes = 0;
		std::optional<double> btts_opt;
		std::optional<double> avg_corners_per_match_opt;
		std::optional<int> team_most_corners_count_opt;
		std::optional<int64_t> sm_team_most_corners_id_opt;
		std::optional<double> win_percentage_home_opt;
		std::optional<double> win_percentage_away_opt;
		std::optional<double> draw_percentage_opt;
		std::optional<double> avg_homegoals_per_match_opt;
		std::optional<double> avg_awaygoals_per_match_opt;
		std::optional<double> avg_player_rating_opt;
		utils::String updated_date;
		int timezone_type = 0;
		utils::String timezone;
		//##protect##"class members"
		std::set<GoalsScoredMinute, GoalsScoredMinuteCmp> goalsScoredMinutes;
		std::set<GoalLine, GoalLineCmp> goalLines;
		//##protect##"class members"

		explicit SeasonStatistics() = default;
		virtual ~SeasonStatistics() {}

		explicit SeasonStatistics(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, number_of_clubs(stm.getColumn("number_of_clubs").getInt())
			, number_of_matches(stm.getColumn("number_of_matches").getInt())
			, number_of_matches_played(stm.getColumn("number_of_matches_played").getInt())
			, number_of_goals(stm.getColumn("number_of_goals").getInt())
			, matches_both_teams_scored(stm.getColumn("matches_both_teams_scored").getInt())
			, number_of_yellowcards(stm.getColumn("number_of_yellowcards").getInt())
			, number_of_yellowredcards(stm.getColumn("number_of_yellowredcards").getInt())
			, number_of_redcards(stm.getColumn("number_of_redcards").getInt())
			, avg_goals_per_match_opt(!stm.getColumn("avg_goals_per_match").isNull() ?
				std::optional{stm.getColumn("avg_goals_per_match").getDouble()} : std::nullopt)
			, avg_yellowcards_per_match_opt(!stm.getColumn("avg_yellowcards_per_match").isNull() ?
				std::optional{stm.getColumn("avg_yellowcards_per_match").getDouble()} : std::nullopt)
			, avg_yellowredcards_per_match_opt(!stm.getColumn("avg_yellowredcards_per_match").isNull() ?
				std::optional{stm.getColumn("avg_yellowredcards_per_match").getDouble()} : std::nullopt)
			, avg_redcards_per_match_opt(!stm.getColumn("avg_redcards_per_match").isNull() ?
				std::optional{stm.getColumn("avg_redcards_per_match").getDouble()} : std::nullopt)
			, sm_team_with_most_goals_id_opt(!stm.getColumn("sm_team_with_most_goals_id").isNull() ?
				std::optional{stm.getColumn("sm_team_with_most_goals_id").getInt64()} : std::nullopt)
			, sm_team_with_most_conceded_goals_id_opt(!stm.getColumn("sm_team_with_most_conceded_goals_id").isNull() ?
				std::optional{stm.getColumn("sm_team_with_most_conceded_goals_id").getInt64()} : std::nullopt)
			, sm_team_with_most_goals_per_match_id_opt(!stm.getColumn("sm_team_with_most_goals_per_match_id").isNull() ?
				std::optional{stm.getColumn("sm_team_with_most_goals_per_match_id").getInt64()} : std::nullopt)
			, sm_season_topscorer_id_opt(!stm.getColumn("sm_season_topscorer_id").isNull() ?
				std::optional{stm.getColumn("sm_season_topscorer_id").getInt64()} : std::nullopt)
			, season_topscorer_number_opt(!stm.getColumn("season_topscorer_number").isNull() ?
				std::optional{stm.getColumn("season_topscorer_number").getInt()} : std::nullopt)
			, sm_season_assist_topscorer_id_opt(!stm.getColumn("sm_season_assist_topscorer_id").isNull() ?
				std::optional{stm.getColumn("sm_season_assist_topscorer_id").getInt64()} : std::nullopt)
			, season_assist_topscorer_number_opt(!stm.getColumn("season_assist_topscorer_number").isNull() ?
				std::optional{stm.getColumn("season_assist_topscorer_number").getInt()} : std::nullopt)
			, sm_team_most_cleansheets_id_opt(!stm.getColumn("sm_team_most_cleansheets_id").isNull() ?
				std::optional{stm.getColumn("sm_team_most_cleansheets_id").getInt64()} : std::nullopt)
			, team_most_cleansheets_number_opt(!stm.getColumn("team_most_cleansheets_number").isNull() ?
				std::optional{stm.getColumn("team_most_cleansheets_number").getInt()} : std::nullopt)
			, sm_goalkeeper_most_cleansheets_id_opt(!stm.getColumn("sm_goalkeeper_most_cleansheets_id").isNull() ?
				std::optional{stm.getColumn("sm_goalkeeper_most_cleansheets_id").getInt64()} : std::nullopt)
			, goalkeeper_most_cleansheets_number_opt(!stm.getColumn("goalkeeper_most_cleansheets_number").isNull() ?
				std::optional{stm.getColumn("goalkeeper_most_cleansheets_number").getInt()} : std::nullopt)
			, goal_scored_every_minutes(stm.getColumn("goal_scored_every_minutes").getInt())
			, btts_opt(!stm.getColumn("btts").isNull() ?
				std::optional{stm.getColumn("btts").getDouble()} : std::nullopt)
			, avg_corners_per_match_opt(!stm.getColumn("avg_corners_per_match").isNull() ?
				std::optional{stm.getColumn("avg_corners_per_match").getDouble()} : std::nullopt)
			, team_most_corners_count_opt(!stm.getColumn("team_most_corners_count").isNull() ?
				std::optional{stm.getColumn("team_most_corners_count").getInt()} : std::nullopt)
			, sm_team_most_corners_id_opt(!stm.getColumn("sm_team_most_corners_id").isNull() ?
				std::optional{stm.getColumn("sm_team_most_corners_id").getInt64()} : std::nullopt)
			, win_percentage_home_opt(!stm.getColumn("win_percentage_home").isNull() ?
				std::optional{stm.getColumn("win_percentage_home").getDouble()} : std::nullopt)
			, win_percentage_away_opt(!stm.getColumn("win_percentage_away").isNull() ?
				std::optional{stm.getColumn("win_percentage_away").getDouble()} : std::nullopt)
			, draw_percentage_opt(!stm.getColumn("draw_percentage").isNull() ?
				std::optional{stm.getColumn("draw_percentage").getDouble()} : std::nullopt)
			, avg_homegoals_per_match_opt(!stm.getColumn("avg_homegoals_per_match").isNull() ?
				std::optional{stm.getColumn("avg_homegoals_per_match").getDouble()} : std::nullopt)
			, avg_awaygoals_per_match_opt(!stm.getColumn("avg_awaygoals_per_match").isNull() ?
				std::optional{stm.getColumn("avg_awaygoals_per_match").getDouble()} : std::nullopt)
			, avg_player_rating_opt(!stm.getColumn("avg_player_rating").isNull() ?
				std::optional{stm.getColumn("avg_player_rating").getDouble()} : std::nullopt)
			, updated_date(StdStringToUtilsString(stm.getColumn("updated_date").getString()))
			, timezone_type(stm.getColumn("timezone_type").getInt())
			, timezone(StdStringToUtilsString(stm.getColumn("timezone").getString()))
			
		{}
	};

	using UpSeasonStatistics = std::unique_ptr<SeasonStatistics>;

	struct SeasonStatisticsCmp
	{
		bool operator()(const SeasonStatistics& a, const SeasonStatistics& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpSeasonStatistics& a, const UpSeasonStatistics& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<SeasonStatistics, T>{} || std::is_same<UpSeasonStatistics, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM season_statistics;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<SeasonStatistics, T>{} || std::is_same<UpSeasonStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpSeasonStatistics> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM season_statistics;" };

		SQLite::Statement stm(db, query);
		std::vector<UpSeasonStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<SeasonStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<SeasonStatistics, T>{} || std::is_same<UpSeasonStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpSeasonStatistics> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<SeasonStatistics>(db);
		}

		const std::string query{ "SELECT * FROM season_statistics WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpSeasonStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<SeasonStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<SeasonStatistics, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& seasonStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(seasonStatistics));
		std::transform(std::cbegin(seasonStatistics), std::cend(seasonStatistics), std::back_inserter(pointers),
			[](const typename TContainer::value_type& seasonStatistics)
			{
				return &seasonStatistics;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpSeasonStatistics, typename TContainer::value_type>{} ||
		std::is_same<const SeasonStatistics*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& seasonStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<SeasonStatistics>: Inserting " << std::size(seasonStatistics) << " seasonStatistics." << endl;
		}
		if (std::empty(seasonStatistics) || 
			std::all_of(std::cbegin(seasonStatistics), std::cend(seasonStatistics), 
				[](const auto& pSeasonStatistics) {return nullptr == pSeasonStatistics;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 40;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(seasonStatistics) / batchSize;
			if(0 != std::size(seasonStatistics) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(seasonStatistics);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(seasonStatistics)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(seasonStatistics);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO season_statistics VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":number_of_clubs" << i << ", ";
					ss << ":number_of_matches" << i << ", ";
					ss << ":number_of_matches_played" << i << ", ";
					ss << ":number_of_goals" << i << ", ";
					ss << ":matches_both_teams_scored" << i << ", ";
					ss << ":number_of_yellowcards" << i << ", ";
					ss << ":number_of_yellowredcards" << i << ", ";
					ss << ":number_of_redcards" << i << ", ";
					ss << ":avg_goals_per_match" << i << ", ";
					ss << ":avg_yellowcards_per_match" << i << ", ";
					ss << ":avg_yellowredcards_per_match" << i << ", ";
					ss << ":avg_redcards_per_match" << i << ", ";
					ss << ":sm_team_with_most_goals_id" << i << ", ";
					ss << ":sm_team_with_most_conceded_goals_id" << i << ", ";
					ss << ":sm_team_with_most_goals_per_match_id" << i << ", ";
					ss << ":sm_season_topscorer_id" << i << ", ";
					ss << ":season_topscorer_number" << i << ", ";
					ss << ":sm_season_assist_topscorer_id" << i << ", ";
					ss << ":season_assist_topscorer_number" << i << ", ";
					ss << ":sm_team_most_cleansheets_id" << i << ", ";
					ss << ":team_most_cleansheets_number" << i << ", ";
					ss << ":sm_goalkeeper_most_cleansheets_id" << i << ", ";
					ss << ":goalkeeper_most_cleansheets_number" << i << ", ";
					ss << ":goal_scored_every_minutes" << i << ", ";
					ss << ":btts" << i << ", ";
					ss << ":avg_corners_per_match" << i << ", ";
					ss << ":team_most_corners_count" << i << ", ";
					ss << ":sm_team_most_corners_id" << i << ", ";
					ss << ":win_percentage_home" << i << ", ";
					ss << ":win_percentage_away" << i << ", ";
					ss << ":draw_percentage" << i << ", ";
					ss << ":avg_homegoals_per_match" << i << ", ";
					ss << ":avg_awaygoals_per_match" << i << ", ";
					ss << ":avg_player_rating" << i << ", ";
					ss << ":updated_date" << i << ", ";
					ss << ":timezone_type" << i << ", ";
					ss << ":timezone" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":number_of_clubs" + idx, (*it)->number_of_clubs);
					insert.bind(":number_of_matches" + idx, (*it)->number_of_matches);
					insert.bind(":number_of_matches_played" + idx, (*it)->number_of_matches_played);
					insert.bind(":number_of_goals" + idx, (*it)->number_of_goals);
					insert.bind(":matches_both_teams_scored" + idx, (*it)->matches_both_teams_scored);
					insert.bind(":number_of_yellowcards" + idx, (*it)->number_of_yellowcards);
					insert.bind(":number_of_yellowredcards" + idx, (*it)->number_of_yellowredcards);
					insert.bind(":number_of_redcards" + idx, (*it)->number_of_redcards);
					BindOptional(insert, ":avg_goals_per_match" + idx, (*it)->avg_goals_per_match_opt);
					BindOptional(insert, ":avg_yellowcards_per_match" + idx, (*it)->avg_yellowcards_per_match_opt);
					BindOptional(insert, ":avg_yellowredcards_per_match" + idx, (*it)->avg_yellowredcards_per_match_opt);
					BindOptional(insert, ":avg_redcards_per_match" + idx, (*it)->avg_redcards_per_match_opt);
					BindOptional(insert, ":sm_team_with_most_goals_id" + idx, (*it)->sm_team_with_most_goals_id_opt);
					BindOptional(insert, ":sm_team_with_most_conceded_goals_id" + idx, (*it)->sm_team_with_most_conceded_goals_id_opt);
					BindOptional(insert, ":sm_team_with_most_goals_per_match_id" + idx, (*it)->sm_team_with_most_goals_per_match_id_opt);
					BindOptional(insert, ":sm_season_topscorer_id" + idx, (*it)->sm_season_topscorer_id_opt);
					BindOptional(insert, ":season_topscorer_number" + idx, (*it)->season_topscorer_number_opt);
					BindOptional(insert, ":sm_season_assist_topscorer_id" + idx, (*it)->sm_season_assist_topscorer_id_opt);
					BindOptional(insert, ":season_assist_topscorer_number" + idx, (*it)->season_assist_topscorer_number_opt);
					BindOptional(insert, ":sm_team_most_cleansheets_id" + idx, (*it)->sm_team_most_cleansheets_id_opt);
					BindOptional(insert, ":team_most_cleansheets_number" + idx, (*it)->team_most_cleansheets_number_opt);
					BindOptional(insert, ":sm_goalkeeper_most_cleansheets_id" + idx, (*it)->sm_goalkeeper_most_cleansheets_id_opt);
					BindOptional(insert, ":goalkeeper_most_cleansheets_number" + idx, (*it)->goalkeeper_most_cleansheets_number_opt);
					insert.bind(":goal_scored_every_minutes" + idx, (*it)->goal_scored_every_minutes);
					BindOptional(insert, ":btts" + idx, (*it)->btts_opt);
					BindOptional(insert, ":avg_corners_per_match" + idx, (*it)->avg_corners_per_match_opt);
					BindOptional(insert, ":team_most_corners_count" + idx, (*it)->team_most_corners_count_opt);
					BindOptional(insert, ":sm_team_most_corners_id" + idx, (*it)->sm_team_most_corners_id_opt);
					BindOptional(insert, ":win_percentage_home" + idx, (*it)->win_percentage_home_opt);
					BindOptional(insert, ":win_percentage_away" + idx, (*it)->win_percentage_away_opt);
					BindOptional(insert, ":draw_percentage" + idx, (*it)->draw_percentage_opt);
					BindOptional(insert, ":avg_homegoals_per_match" + idx, (*it)->avg_homegoals_per_match_opt);
					BindOptional(insert, ":avg_awaygoals_per_match" + idx, (*it)->avg_awaygoals_per_match_opt);
					BindOptional(insert, ":avg_player_rating" + idx, (*it)->avg_player_rating_opt);
					insert.bind(":updated_date" + idx, UtilsStringToStdString((*it)->updated_date));
					insert.bind(":timezone_type" + idx, (*it)->timezone_type);
					insert.bind(":timezone" + idx, UtilsStringToStdString((*it)->timezone));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					InsertOr(onConflict, db, (*it)->goalsScoredMinutes, false, false);
					InsertOr(onConflict, db, (*it)->goalLines, false, false);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(seasonStatistics) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "season_statistics");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0