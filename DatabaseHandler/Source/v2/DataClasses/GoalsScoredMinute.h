#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class GoalsScoredMinute
	{
	public:
		int64_t id = 0;
		int64_t sm_season_statistics_id = 0;
		utils::String interval;
		double percentage = 0.0;
		//##protect##"class members"
		//##protect##"class members"

		explicit GoalsScoredMinute() = default;
		virtual ~GoalsScoredMinute() {}

		explicit GoalsScoredMinute(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_season_statistics_id(stm.getColumn("sm_season_statistics_id").getInt64())
			, interval(StdStringToUtilsString(stm.getColumn("interval").getString()))
			, percentage(stm.getColumn("percentage").getDouble())
			
		{}
	};

	using UpGoalsScoredMinute = std::unique_ptr<GoalsScoredMinute>;

	struct GoalsScoredMinuteCmp
	{
		bool operator()(const GoalsScoredMinute& a, const GoalsScoredMinute& b) const
		{
			return a.sm_season_statistics_id != b.sm_season_statistics_id ? a.sm_season_statistics_id < b.sm_season_statistics_id :
				a.interval < b.interval;
		}

		bool operator()(const UpGoalsScoredMinute& a, const UpGoalsScoredMinute& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalsScoredMinute, T>{} || std::is_same<UpGoalsScoredMinute, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM goals_scored_minutes;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalsScoredMinute, T>{} || std::is_same<UpGoalsScoredMinute, T>{},
		int > ::type = 0 >
	inline std::vector<UpGoalsScoredMinute> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM goals_scored_minutes;" };

		SQLite::Statement stm(db, query);
		std::vector<UpGoalsScoredMinute> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<GoalsScoredMinute>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<GoalsScoredMinute, T>{} || std::is_same<UpGoalsScoredMinute, T>{},
		int > ::type = 0 >
	inline std::vector<UpGoalsScoredMinute> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<GoalsScoredMinute>(db);
		}

		const std::string query{ "SELECT * FROM goals_scored_minutes WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpGoalsScoredMinute> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<GoalsScoredMinute>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<GoalsScoredMinute, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& goalsScoredMinutes,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(goalsScoredMinutes));
		std::transform(std::cbegin(goalsScoredMinutes), std::cend(goalsScoredMinutes), std::back_inserter(pointers),
			[](const typename TContainer::value_type& goalsScoredMinute)
			{
				return &goalsScoredMinute;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpGoalsScoredMinute, typename TContainer::value_type>{} ||
		std::is_same<const GoalsScoredMinute*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& goalsScoredMinutes,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<GoalsScoredMinute>: Inserting " << std::size(goalsScoredMinutes) << " goalsScoredMinutes." << endl;
		}
		if (std::empty(goalsScoredMinutes) || 
			std::all_of(std::cbegin(goalsScoredMinutes), std::cend(goalsScoredMinutes), 
				[](const auto& pGoalsScoredMinute) {return nullptr == pGoalsScoredMinute;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 3;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(goalsScoredMinutes) / batchSize;
			if(0 != std::size(goalsScoredMinutes) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(goalsScoredMinutes);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(goalsScoredMinutes)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(goalsScoredMinutes);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO goals_scored_minutes VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_season_statistics_id" << i << ", ";
					ss << ":interval" << i << ", ";
					ss << ":percentage" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_season_statistics_id" + idx, (*it)->sm_season_statistics_id);
					insert.bind(":interval" + idx, UtilsStringToStdString((*it)->interval));
					insert.bind(":percentage" + idx, (*it)->percentage);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(goalsScoredMinutes) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "goals_scored_minutes");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0