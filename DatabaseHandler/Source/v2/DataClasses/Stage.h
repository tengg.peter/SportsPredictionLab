#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Stage
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		utils::String name;
		utils::String type;
		int64_t sm_league_id = 0;
		int64_t sm_season_id = 0;
		std::optional<int64_t> sort_order_opt;
		bool has_standings = false;
		//##protect##"class members"
		//##protect##"class members"

		explicit Stage() = default;
		virtual ~Stage() {}

		explicit Stage(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sort_order_opt(!stm.getColumn("sort_order").isNull() ?
				std::optional{stm.getColumn("sort_order").getInt64()} : std::nullopt)
			, has_standings(static_cast<bool>(stm.getColumn("has_standings").getInt()))
			
		{}
	};

	using UpStage = std::unique_ptr<Stage>;

	struct StageCmp
	{
		bool operator()(const Stage& a, const Stage& b) const
		{
			return a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.name != b.name ? a.name < b.name :
				a.sm_id < b.sm_id;
		}

		bool operator()(const UpStage& a, const UpStage& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Stage, T>{} || std::is_same<UpStage, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM stages;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Stage, T>{} || std::is_same<UpStage, T>{},
		int > ::type = 0 >
	inline std::vector<UpStage> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM stages;" };

		SQLite::Statement stm(db, query);
		std::vector<UpStage> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Stage>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Stage, T>{} || std::is_same<UpStage, T>{},
		int > ::type = 0 >
	inline std::vector<UpStage> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Stage>(db);
		}

		const std::string query{ "SELECT * FROM stages WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpStage> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Stage>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Stage, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& stages,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(stages));
		std::transform(std::cbegin(stages), std::cend(stages), std::back_inserter(pointers),
			[](const typename TContainer::value_type& stage)
			{
				return &stage;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpStage, typename TContainer::value_type>{} ||
		std::is_same<const Stage*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& stages,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Stage>: Inserting " << std::size(stages) << " stages." << endl;
		}
		if (std::empty(stages) || 
			std::all_of(std::cbegin(stages), std::cend(stages), 
				[](const auto& pStage) {return nullptr == pStage;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 7;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(stages) / batchSize;
			if(0 != std::size(stages) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(stages);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(stages)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(stages);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO stages VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sort_order" << i << ", ";
					ss << ":has_standings" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					BindOptional(insert, ":sort_order" + idx, (*it)->sort_order_opt);
					insert.bind(":has_standings" + idx, (*it)->has_standings);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(stages) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "stages");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0