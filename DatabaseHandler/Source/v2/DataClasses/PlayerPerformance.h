#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class PlayerPerformance
	{
	public:
		int64_t id = 0;
		int64_t sm_player_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_team_id = 0;
		int64_t number = 0;
		int64_t captain = 0;
		bool injured = false;
		int64_t minutes = 0;
		int64_t appearences = 0;
		int64_t lineups = 0;
		int64_t substitute_in = 0;
		int64_t substitute_out = 0;
		int64_t substitutes_on_bench = 0;
		int64_t goals = 0;
		int64_t assists = 0;
		std::optional<int64_t> saves_opt;
		std::optional<int64_t> inside_box_saves_opt;
		std::optional<int64_t> dispossesed_opt;
		std::optional<int64_t> interceptions_opt;
		int64_t yellowcards = 0;
		int64_t yellowred = 0;
		int64_t redcards = 0;
		std::optional<int64_t> tackles_opt;
		std::optional<int64_t> blocks_opt;
		std::optional<int64_t> hit_post_opt;
		std::optional<int64_t> fouls_committed_opt;
		std::optional<int64_t> fouls_drawn_opt;
		std::optional<int64_t> crosses_total_opt;
		std::optional<int64_t> crosses_accurate_opt;
		std::optional<int64_t> dribbles_attempts_opt;
		std::optional<int64_t> dribbles_success_opt;
		std::optional<int64_t> dribbled_past_opt;
		std::optional<int64_t> duels_total_opt;
		std::optional<int64_t> duels_won_opt;
		std::optional<int64_t> passes_total_opt;
		std::optional<double> passes_accuracy_opt;
		std::optional<int64_t> key_passes_opt;
		std::optional<int64_t> penalties_won_opt;
		std::optional<int64_t> penalties_scored_opt;
		std::optional<int64_t> penalties_missed_opt;
		std::optional<int64_t> penalties_committed_opt;
		std::optional<int64_t> penalties_saved_opt;
		std::optional<int64_t> shots_total_opt;
		std::optional<int64_t> shots_on_target_opt;
		std::optional<int64_t> shots_off_target_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit PlayerPerformance() = default;
		virtual ~PlayerPerformance() {}

		explicit PlayerPerformance(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_player_id(stm.getColumn("sm_player_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, number(stm.getColumn("number").getInt64())
			, captain(stm.getColumn("captain").getInt64())
			, injured(static_cast<bool>(stm.getColumn("injured").getInt()))
			, minutes(stm.getColumn("minutes").getInt64())
			, appearences(stm.getColumn("appearences").getInt64())
			, lineups(stm.getColumn("lineups").getInt64())
			, substitute_in(stm.getColumn("substitute_in").getInt64())
			, substitute_out(stm.getColumn("substitute_out").getInt64())
			, substitutes_on_bench(stm.getColumn("substitutes_on_bench").getInt64())
			, goals(stm.getColumn("goals").getInt64())
			, assists(stm.getColumn("assists").getInt64())
			, saves_opt(!stm.getColumn("saves").isNull() ?
				std::optional{stm.getColumn("saves").getInt64()} : std::nullopt)
			, inside_box_saves_opt(!stm.getColumn("inside_box_saves").isNull() ?
				std::optional{stm.getColumn("inside_box_saves").getInt64()} : std::nullopt)
			, dispossesed_opt(!stm.getColumn("dispossesed").isNull() ?
				std::optional{stm.getColumn("dispossesed").getInt64()} : std::nullopt)
			, interceptions_opt(!stm.getColumn("interceptions").isNull() ?
				std::optional{stm.getColumn("interceptions").getInt64()} : std::nullopt)
			, yellowcards(stm.getColumn("yellowcards").getInt64())
			, yellowred(stm.getColumn("yellowred").getInt64())
			, redcards(stm.getColumn("redcards").getInt64())
			, tackles_opt(!stm.getColumn("tackles").isNull() ?
				std::optional{stm.getColumn("tackles").getInt64()} : std::nullopt)
			, blocks_opt(!stm.getColumn("blocks").isNull() ?
				std::optional{stm.getColumn("blocks").getInt64()} : std::nullopt)
			, hit_post_opt(!stm.getColumn("hit_post").isNull() ?
				std::optional{stm.getColumn("hit_post").getInt64()} : std::nullopt)
			, fouls_committed_opt(!stm.getColumn("fouls_committed").isNull() ?
				std::optional{stm.getColumn("fouls_committed").getInt64()} : std::nullopt)
			, fouls_drawn_opt(!stm.getColumn("fouls_drawn").isNull() ?
				std::optional{stm.getColumn("fouls_drawn").getInt64()} : std::nullopt)
			, crosses_total_opt(!stm.getColumn("crosses_total").isNull() ?
				std::optional{stm.getColumn("crosses_total").getInt64()} : std::nullopt)
			, crosses_accurate_opt(!stm.getColumn("crosses_accurate").isNull() ?
				std::optional{stm.getColumn("crosses_accurate").getInt64()} : std::nullopt)
			, dribbles_attempts_opt(!stm.getColumn("dribbles_attempts").isNull() ?
				std::optional{stm.getColumn("dribbles_attempts").getInt64()} : std::nullopt)
			, dribbles_success_opt(!stm.getColumn("dribbles_success").isNull() ?
				std::optional{stm.getColumn("dribbles_success").getInt64()} : std::nullopt)
			, dribbled_past_opt(!stm.getColumn("dribbled_past").isNull() ?
				std::optional{stm.getColumn("dribbled_past").getInt64()} : std::nullopt)
			, duels_total_opt(!stm.getColumn("duels_total").isNull() ?
				std::optional{stm.getColumn("duels_total").getInt64()} : std::nullopt)
			, duels_won_opt(!stm.getColumn("duels_won").isNull() ?
				std::optional{stm.getColumn("duels_won").getInt64()} : std::nullopt)
			, passes_total_opt(!stm.getColumn("passes_total").isNull() ?
				std::optional{stm.getColumn("passes_total").getInt64()} : std::nullopt)
			, passes_accuracy_opt(!stm.getColumn("passes_accuracy").isNull() ?
				std::optional{stm.getColumn("passes_accuracy").getDouble()} : std::nullopt)
			, key_passes_opt(!stm.getColumn("key_passes").isNull() ?
				std::optional{stm.getColumn("key_passes").getInt64()} : std::nullopt)
			, penalties_won_opt(!stm.getColumn("penalties_won").isNull() ?
				std::optional{stm.getColumn("penalties_won").getInt64()} : std::nullopt)
			, penalties_scored_opt(!stm.getColumn("penalties_scored").isNull() ?
				std::optional{stm.getColumn("penalties_scored").getInt64()} : std::nullopt)
			, penalties_missed_opt(!stm.getColumn("penalties_missed").isNull() ?
				std::optional{stm.getColumn("penalties_missed").getInt64()} : std::nullopt)
			, penalties_committed_opt(!stm.getColumn("penalties_committed").isNull() ?
				std::optional{stm.getColumn("penalties_committed").getInt64()} : std::nullopt)
			, penalties_saved_opt(!stm.getColumn("penalties_saved").isNull() ?
				std::optional{stm.getColumn("penalties_saved").getInt64()} : std::nullopt)
			, shots_total_opt(!stm.getColumn("shots_total").isNull() ?
				std::optional{stm.getColumn("shots_total").getInt64()} : std::nullopt)
			, shots_on_target_opt(!stm.getColumn("shots_on_target").isNull() ?
				std::optional{stm.getColumn("shots_on_target").getInt64()} : std::nullopt)
			, shots_off_target_opt(!stm.getColumn("shots_off_target").isNull() ?
				std::optional{stm.getColumn("shots_off_target").getInt64()} : std::nullopt)
			
		{}
	};

	using UpPlayerPerformance = std::unique_ptr<PlayerPerformance>;

	struct PlayerPerformanceCmp
	{
		bool operator()(const PlayerPerformance& a, const PlayerPerformance& b) const
		{
			return a.sm_player_id != b.sm_player_id ? a.sm_player_id < b.sm_player_id :
				a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.sm_team_id < b.sm_team_id;
		}

		bool operator()(const UpPlayerPerformance& a, const UpPlayerPerformance& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerPerformance, T>{} || std::is_same<UpPlayerPerformance, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM player_performances;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerPerformance, T>{} || std::is_same<UpPlayerPerformance, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayerPerformance> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM player_performances;" };

		SQLite::Statement stm(db, query);
		std::vector<UpPlayerPerformance> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<PlayerPerformance>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerPerformance, T>{} || std::is_same<UpPlayerPerformance, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayerPerformance> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<PlayerPerformance>(db);
		}

		const std::string query{ "SELECT * FROM player_performances WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpPlayerPerformance> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<PlayerPerformance>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<PlayerPerformance, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& playerPerformances,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(playerPerformances));
		std::transform(std::cbegin(playerPerformances), std::cend(playerPerformances), std::back_inserter(pointers),
			[](const typename TContainer::value_type& playerPerformance)
			{
				return &playerPerformance;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpPlayerPerformance, typename TContainer::value_type>{} ||
		std::is_same<const PlayerPerformance*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& playerPerformances,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<PlayerPerformance>: Inserting " << std::size(playerPerformances) << " playerPerformances." << endl;
		}
		if (std::empty(playerPerformances) || 
			std::all_of(std::cbegin(playerPerformances), std::cend(playerPerformances), 
				[](const auto& pPlayerPerformance) {return nullptr == pPlayerPerformance;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 44;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(playerPerformances) / batchSize;
			if(0 != std::size(playerPerformances) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(playerPerformances);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(playerPerformances)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(playerPerformances);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO player_performances VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_player_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":number" << i << ", ";
					ss << ":captain" << i << ", ";
					ss << ":injured" << i << ", ";
					ss << ":minutes" << i << ", ";
					ss << ":appearences" << i << ", ";
					ss << ":lineups" << i << ", ";
					ss << ":substitute_in" << i << ", ";
					ss << ":substitute_out" << i << ", ";
					ss << ":substitutes_on_bench" << i << ", ";
					ss << ":goals" << i << ", ";
					ss << ":assists" << i << ", ";
					ss << ":saves" << i << ", ";
					ss << ":inside_box_saves" << i << ", ";
					ss << ":dispossesed" << i << ", ";
					ss << ":interceptions" << i << ", ";
					ss << ":yellowcards" << i << ", ";
					ss << ":yellowred" << i << ", ";
					ss << ":redcards" << i << ", ";
					ss << ":tackles" << i << ", ";
					ss << ":blocks" << i << ", ";
					ss << ":hit_post" << i << ", ";
					ss << ":fouls_committed" << i << ", ";
					ss << ":fouls_drawn" << i << ", ";
					ss << ":crosses_total" << i << ", ";
					ss << ":crosses_accurate" << i << ", ";
					ss << ":dribbles_attempts" << i << ", ";
					ss << ":dribbles_success" << i << ", ";
					ss << ":dribbled_past" << i << ", ";
					ss << ":duels_total" << i << ", ";
					ss << ":duels_won" << i << ", ";
					ss << ":passes_total" << i << ", ";
					ss << ":passes_accuracy" << i << ", ";
					ss << ":key_passes" << i << ", ";
					ss << ":penalties_won" << i << ", ";
					ss << ":penalties_scored" << i << ", ";
					ss << ":penalties_missed" << i << ", ";
					ss << ":penalties_committed" << i << ", ";
					ss << ":penalties_saved" << i << ", ";
					ss << ":shots_total" << i << ", ";
					ss << ":shots_on_target" << i << ", ";
					ss << ":shots_off_target" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_player_id" + idx, (*it)->sm_player_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":number" + idx, (*it)->number);
					insert.bind(":captain" + idx, (*it)->captain);
					insert.bind(":injured" + idx, (*it)->injured);
					insert.bind(":minutes" + idx, (*it)->minutes);
					insert.bind(":appearences" + idx, (*it)->appearences);
					insert.bind(":lineups" + idx, (*it)->lineups);
					insert.bind(":substitute_in" + idx, (*it)->substitute_in);
					insert.bind(":substitute_out" + idx, (*it)->substitute_out);
					insert.bind(":substitutes_on_bench" + idx, (*it)->substitutes_on_bench);
					insert.bind(":goals" + idx, (*it)->goals);
					insert.bind(":assists" + idx, (*it)->assists);
					BindOptional(insert, ":saves" + idx, (*it)->saves_opt);
					BindOptional(insert, ":inside_box_saves" + idx, (*it)->inside_box_saves_opt);
					BindOptional(insert, ":dispossesed" + idx, (*it)->dispossesed_opt);
					BindOptional(insert, ":interceptions" + idx, (*it)->interceptions_opt);
					insert.bind(":yellowcards" + idx, (*it)->yellowcards);
					insert.bind(":yellowred" + idx, (*it)->yellowred);
					insert.bind(":redcards" + idx, (*it)->redcards);
					BindOptional(insert, ":tackles" + idx, (*it)->tackles_opt);
					BindOptional(insert, ":blocks" + idx, (*it)->blocks_opt);
					BindOptional(insert, ":hit_post" + idx, (*it)->hit_post_opt);
					BindOptional(insert, ":fouls_committed" + idx, (*it)->fouls_committed_opt);
					BindOptional(insert, ":fouls_drawn" + idx, (*it)->fouls_drawn_opt);
					BindOptional(insert, ":crosses_total" + idx, (*it)->crosses_total_opt);
					BindOptional(insert, ":crosses_accurate" + idx, (*it)->crosses_accurate_opt);
					BindOptional(insert, ":dribbles_attempts" + idx, (*it)->dribbles_attempts_opt);
					BindOptional(insert, ":dribbles_success" + idx, (*it)->dribbles_success_opt);
					BindOptional(insert, ":dribbled_past" + idx, (*it)->dribbled_past_opt);
					BindOptional(insert, ":duels_total" + idx, (*it)->duels_total_opt);
					BindOptional(insert, ":duels_won" + idx, (*it)->duels_won_opt);
					BindOptional(insert, ":passes_total" + idx, (*it)->passes_total_opt);
					BindOptional(insert, ":passes_accuracy" + idx, (*it)->passes_accuracy_opt);
					BindOptional(insert, ":key_passes" + idx, (*it)->key_passes_opt);
					BindOptional(insert, ":penalties_won" + idx, (*it)->penalties_won_opt);
					BindOptional(insert, ":penalties_scored" + idx, (*it)->penalties_scored_opt);
					BindOptional(insert, ":penalties_missed" + idx, (*it)->penalties_missed_opt);
					BindOptional(insert, ":penalties_committed" + idx, (*it)->penalties_committed_opt);
					BindOptional(insert, ":penalties_saved" + idx, (*it)->penalties_saved_opt);
					BindOptional(insert, ":shots_total" + idx, (*it)->shots_total_opt);
					BindOptional(insert, ":shots_on_target" + idx, (*it)->shots_on_target_opt);
					BindOptional(insert, ":shots_off_target" + idx, (*it)->shots_off_target_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(playerPerformances) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "player_performances");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0