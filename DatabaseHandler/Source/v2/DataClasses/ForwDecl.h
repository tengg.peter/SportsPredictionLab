#pragma once

#include <memory>

namespace database_handler::v2
{
    class Bookmaker;
	using UpBookmaker = std::unique_ptr<Bookmaker>;
    
    class Coach;
	using UpCoach = std::unique_ptr<Coach>;
    
    class Commentary;
	using UpCommentary = std::unique_ptr<Commentary>;
    
    class Continent;
	using UpContinent = std::unique_ptr<Continent>;
    
    class Country;
	using UpCountry = std::unique_ptr<Country>;
    
    class Fixture;
	using UpFixture = std::unique_ptr<Fixture>;
    
    class FixtureSideline;
	using UpFixtureSideline = std::unique_ptr<FixtureSideline>;
    
    class GoalLine;
	using UpGoalLine = std::unique_ptr<GoalLine>;
    
    class GoalsScoredMinute;
	using UpGoalsScoredMinute = std::unique_ptr<GoalsScoredMinute>;
    
    class League;
	using UpLeague = std::unique_ptr<League>;
    
    class Lineup;
	using UpLineup = std::unique_ptr<Lineup>;
    
    class Market;
	using UpMarket = std::unique_ptr<Market>;
    
    class MatchStatistics;
	using UpMatchStatistics = std::unique_ptr<MatchStatistics>;
    
    class Odds;
	using UpOdds = std::unique_ptr<Odds>;
    
    class PlayerPerformance;
	using UpPlayerPerformance = std::unique_ptr<PlayerPerformance>;
    
    class Player;
	using UpPlayer = std::unique_ptr<Player>;
    
    class PlayerSideline;
	using UpPlayerSideline = std::unique_ptr<PlayerSideline>;
    
    class PlayerTransfer;
	using UpPlayerTransfer = std::unique_ptr<PlayerTransfer>;
    
    class Round;
	using UpRound = std::unique_ptr<Round>;
    
    class Season;
	using UpSeason = std::unique_ptr<Season>;
    
    class SeasonStatistics;
	using UpSeasonStatistics = std::unique_ptr<SeasonStatistics>;
    
    class Stage;
	using UpStage = std::unique_ptr<Stage>;
    
    class StandingPosition;
	using UpStandingPosition = std::unique_ptr<StandingPosition>;
    
    class Standing;
	using UpStanding = std::unique_ptr<Standing>;
    
    class TeamGoalLine;
	using UpTeamGoalLine = std::unique_ptr<TeamGoalLine>;
    
    class TeamGoalMinute;
	using UpTeamGoalMinute = std::unique_ptr<TeamGoalMinute>;
    
    class Team;
	using UpTeam = std::unique_ptr<Team>;
    
    class TeamSeasonStatistics;
	using UpTeamSeasonStatistics = std::unique_ptr<TeamSeasonStatistics>;
    
    class TopAssistScorer;
	using UpTopAssistScorer = std::unique_ptr<TopAssistScorer>;
    
    class TopCardScorer;
	using UpTopCardScorer = std::unique_ptr<TopCardScorer>;
    
    class TopGoalScorer;
	using UpTopGoalScorer = std::unique_ptr<TopGoalScorer>;
    
    class TrendPoint;
	using UpTrendPoint = std::unique_ptr<TrendPoint>;
    
    class Trend;
	using UpTrend = std::unique_ptr<Trend>;
    
    class Venue;
	using UpVenue = std::unique_ptr<Venue>;
    
    
}