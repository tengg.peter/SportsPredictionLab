#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Country
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		std::optional<int64_t> sm_continent_id_opt;
		utils::String name;
		std::optional<utils::String> image_path_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit Country() = default;
		virtual ~Country() {}

		explicit Country(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_continent_id_opt(!stm.getColumn("sm_continent_id").isNull() ?
				std::optional{stm.getColumn("sm_continent_id").getInt64()} : std::nullopt)
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, image_path_opt(!stm.getColumn("image_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("image_path").getString())} : std::nullopt)
			
		{}
	};

	using UpCountry = std::unique_ptr<Country>;

	struct CountryCmp
	{
		bool operator()(const Country& a, const Country& b) const
		{
			return a.sm_id != b.sm_id ? a.sm_id < b.sm_id :
				a.name < b.name;
		}

		bool operator()(const UpCountry& a, const UpCountry& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Country, T>{} || std::is_same<UpCountry, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM countries;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Country, T>{} || std::is_same<UpCountry, T>{},
		int > ::type = 0 >
	inline std::vector<UpCountry> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM countries;" };

		SQLite::Statement stm(db, query);
		std::vector<UpCountry> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Country>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Country, T>{} || std::is_same<UpCountry, T>{},
		int > ::type = 0 >
	inline std::vector<UpCountry> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Country>(db);
		}

		const std::string query{ "SELECT * FROM countries WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpCountry> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Country>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Country, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& countries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(countries));
		std::transform(std::cbegin(countries), std::cend(countries), std::back_inserter(pointers),
			[](const typename TContainer::value_type& country)
			{
				return &country;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpCountry, typename TContainer::value_type>{} ||
		std::is_same<const Country*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& countries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Country>: Inserting " << std::size(countries) << " countries." << endl;
		}
		if (std::empty(countries) || 
			std::all_of(std::cbegin(countries), std::cend(countries), 
				[](const auto& pCountry) {return nullptr == pCountry;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 4;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(countries) / batchSize;
			if(0 != std::size(countries) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(countries);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(countries)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(countries);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO countries VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_continent_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":image_path" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					BindOptional(insert, ":sm_continent_id" + idx, (*it)->sm_continent_id_opt);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					BindOptional(insert, ":image_path" + idx, (*it)->image_path_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(countries) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "countries");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0//*********************************************************************
// Please find below the protected areas that the template-based script
// leading the generation hasn't recognized.
//*********************************************************************
//##protect##"InsertIfNotExists"
			//##protect##"InsertIfNotExists"
