#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class TopGoalScorer
	{
	public:
		int64_t id = 0;
		int position = 0;
		int64_t sm_season_id = 0;
		int64_t sm_player_id = 0;
		int64_t sm_team_id = 0;
		std::optional<int64_t> sm_stage_id_opt;
		int goals = 0;
		int penalty_goals = 0;
		utils::String type;
		//##protect##"class members"
//##protect##"class members"

		explicit TopGoalScorer() = default;
		virtual ~TopGoalScorer() {}

		explicit TopGoalScorer(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, position(stm.getColumn("position").getInt())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_player_id(stm.getColumn("sm_player_id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_stage_id_opt(!stm.getColumn("sm_stage_id").isNull() ?
				std::optional{stm.getColumn("sm_stage_id").getInt64()} : std::nullopt)
			, goals(stm.getColumn("goals").getInt())
			, penalty_goals(stm.getColumn("penalty_goals").getInt())
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			
		{}
	};

	using UpTopGoalScorer = std::unique_ptr<TopGoalScorer>;

	struct TopGoalScorerCmp
	{
		bool operator()(const TopGoalScorer& a, const TopGoalScorer& b) const
		{
			return a.position != b.position ? a.position < b.position :
				a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.type < b.type;
		}

		bool operator()(const UpTopGoalScorer& a, const UpTopGoalScorer& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<TopGoalScorer, T>{} || std::is_same<UpTopGoalScorer, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM top_goal_scorers;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TopGoalScorer, T>{} || std::is_same<UpTopGoalScorer, T>{},
		int > ::type = 0 >
	inline std::vector<UpTopGoalScorer> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM top_goal_scorers;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTopGoalScorer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TopGoalScorer>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TopGoalScorer, T>{} || std::is_same<UpTopGoalScorer, T>{},
		int > ::type = 0 >
	inline std::vector<UpTopGoalScorer> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<TopGoalScorer>(db);
		}

		const std::string query{ "SELECT * FROM top_goal_scorers WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTopGoalScorer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TopGoalScorer>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<TopGoalScorer, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& topGoalScorers,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(topGoalScorers));
		std::transform(std::cbegin(topGoalScorers), std::cend(topGoalScorers), std::back_inserter(pointers),
			[](const typename TContainer::value_type& topGoalScorer)
			{
				return &topGoalScorer;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTopGoalScorer, typename TContainer::value_type>{} ||
		std::is_same<const TopGoalScorer*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& topGoalScorers,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<TopGoalScorer>: Inserting " << std::size(topGoalScorers) << " topGoalScorers." << endl;
		}
		if (std::empty(topGoalScorers) || 
			std::all_of(std::cbegin(topGoalScorers), std::cend(topGoalScorers), 
				[](const auto& pTopGoalScorer) {return nullptr == pTopGoalScorer;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 8;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(topGoalScorers) / batchSize;
			if(0 != std::size(topGoalScorers) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(topGoalScorers);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(topGoalScorers)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(topGoalScorers);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO top_goal_scorers VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":position" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_player_id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_stage_id" << i << ", ";
					ss << ":goals" << i << ", ";
					ss << ":penalty_goals" << i << ", ";
					ss << ":type" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":position" + idx, (*it)->position);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_player_id" + idx, (*it)->sm_player_id);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					BindOptional(insert, ":sm_stage_id" + idx, (*it)->sm_stage_id_opt);
					insert.bind(":goals" + idx, (*it)->goals);
					insert.bind(":penalty_goals" + idx, (*it)->penalty_goals);
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(topGoalScorers) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "top_goal_scorers");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0