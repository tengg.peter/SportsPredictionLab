#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class FixtureSideline
	{
	public:
		int64_t id = 0;
		int64_t sm_team_id = 0;
		int64_t sm_fixture_id = 0;
		int64_t sm_player_id = 0;
		utils::String player_name;
		utils::String reason;
		//##protect##"class members"
//##protect##"class members"

		explicit FixtureSideline() = default;
		virtual ~FixtureSideline() {}

		explicit FixtureSideline(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, sm_player_id(stm.getColumn("sm_player_id").getInt64())
			, player_name(StdStringToUtilsString(stm.getColumn("player_name").getString()))
			, reason(StdStringToUtilsString(stm.getColumn("reason").getString()))
			
		{}
	};

	using UpFixtureSideline = std::unique_ptr<FixtureSideline>;

	struct FixtureSidelineCmp
	{
		bool operator()(const FixtureSideline& a, const FixtureSideline& b) const
		{
			return a.sm_team_id != b.sm_team_id ? a.sm_team_id < b.sm_team_id :
				a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.sm_player_id < b.sm_player_id;
		}

		bool operator()(const UpFixtureSideline& a, const UpFixtureSideline& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<FixtureSideline, T>{} || std::is_same<UpFixtureSideline, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM fixture_sidelines;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<FixtureSideline, T>{} || std::is_same<UpFixtureSideline, T>{},
		int > ::type = 0 >
	inline std::vector<UpFixtureSideline> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM fixture_sidelines;" };

		SQLite::Statement stm(db, query);
		std::vector<UpFixtureSideline> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<FixtureSideline>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<FixtureSideline, T>{} || std::is_same<UpFixtureSideline, T>{},
		int > ::type = 0 >
	inline std::vector<UpFixtureSideline> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<FixtureSideline>(db);
		}

		const std::string query{ "SELECT * FROM fixture_sidelines WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpFixtureSideline> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<FixtureSideline>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<FixtureSideline, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& fixtureSidelines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(fixtureSidelines));
		std::transform(std::cbegin(fixtureSidelines), std::cend(fixtureSidelines), std::back_inserter(pointers),
			[](const typename TContainer::value_type& fixtureSideline)
			{
				return &fixtureSideline;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpFixtureSideline, typename TContainer::value_type>{} ||
		std::is_same<const FixtureSideline*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& fixtureSidelines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<FixtureSideline>: Inserting " << std::size(fixtureSidelines) << " fixtureSidelines." << endl;
		}
		if (std::empty(fixtureSidelines) || 
			std::all_of(std::cbegin(fixtureSidelines), std::cend(fixtureSidelines), 
				[](const auto& pFixtureSideline) {return nullptr == pFixtureSideline;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 5;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(fixtureSidelines) / batchSize;
			if(0 != std::size(fixtureSidelines) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(fixtureSidelines);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(fixtureSidelines)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(fixtureSidelines);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO fixture_sidelines VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":sm_player_id" << i << ", ";
					ss << ":player_name" << i << ", ";
					ss << ":reason" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					insert.bind(":sm_player_id" + idx, (*it)->sm_player_id);
					insert.bind(":player_name" + idx, UtilsStringToStdString((*it)->player_name));
					insert.bind(":reason" + idx, UtilsStringToStdString((*it)->reason));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(fixtureSidelines) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "fixture_sidelines");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0