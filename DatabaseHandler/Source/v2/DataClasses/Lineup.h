#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Lineup
	{
	public:
		int64_t id = 0;
		int64_t sm_team_id = 0;
		int64_t sm_fixture_id = 0;
		int64_t sm_player_id = 0;
		utils::String player_name;
		int number = 0;
		utils::String position;
		std::optional<utils::String> additional_position_opt;
		std::optional<int> formation_position_opt;
		std::optional<int> posx_opt;
		std::optional<int> posy_opt;
		std::optional<bool> captain_opt;
		utils::String type;
		std::optional<int> shots_total_opt;
		std::optional<int> shots_on_goal_opt;
		std::optional<int> goals_scored_opt;
		std::optional<int> goals_assists_opt;
		std::optional<int> goals_conceded_opt;
		std::optional<int> goals_owngoals_opt;
		std::optional<int> fouls_drawn_opt;
		std::optional<int> fouls_committed_opt;
		std::optional<int> yellowcards_opt;
		std::optional<int> redcards_opt;
		std::optional<int> yellowredcards_opt;
		std::optional<int> passing_total_crosses_opt;
		std::optional<int> passing_crosses_accuracy_opt;
		std::optional<int> passing_passes_opt;
		std::optional<int> passing_accurate_passes_opt;
		std::optional<int> passing_passes_accuracy_opt;
		std::optional<int> passing_key_passes_opt;
		std::optional<int> dribbles_attempts_opt;
		std::optional<int> dribbles_success_opt;
		std::optional<int> dribbles_dribbled_past_opt;
		std::optional<int> duels_total_opt;
		std::optional<int> duels_won_opt;
		std::optional<int> aerials_won_opt;
		std::optional<int> punches_opt;
		std::optional<int> offsides_opt;
		std::optional<int> saves_opt;
		std::optional<int> inside_box_saves_opt;
		std::optional<int> penalty_scored_opt;
		std::optional<int> penalty_missed_opt;
		std::optional<int> penalty_saved_opt;
		std::optional<int> penalty_committed_opt;
		std::optional<int> penalty_won_opt;
		std::optional<int> hit_woodwork_opt;
		std::optional<int> tackles_opt;
		std::optional<int> blocks_opt;
		std::optional<int> interceptions_opt;
		std::optional<int> clearances_opt;
		std::optional<int> dispossesed_opt;
		std::optional<int> minutes_played_opt;
		std::optional<double> rating_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit Lineup() = default;
		virtual ~Lineup() {}

		explicit Lineup(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, sm_player_id(stm.getColumn("sm_player_id").getInt64())
			, player_name(StdStringToUtilsString(stm.getColumn("player_name").getString()))
			, number(stm.getColumn("number").getInt())
			, position(StdStringToUtilsString(stm.getColumn("position").getString()))
			, additional_position_opt(!stm.getColumn("additional_position").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("additional_position").getString())} : std::nullopt)
			, formation_position_opt(!stm.getColumn("formation_position").isNull() ?
				std::optional{stm.getColumn("formation_position").getInt()} : std::nullopt)
			, posx_opt(!stm.getColumn("posx").isNull() ?
				std::optional{stm.getColumn("posx").getInt()} : std::nullopt)
			, posy_opt(!stm.getColumn("posy").isNull() ?
				std::optional{stm.getColumn("posy").getInt()} : std::nullopt)
			, captain_opt(!stm.getColumn("captain").isNull() ?
				std::optional{static_cast<bool>(stm.getColumn("captain").getInt())} : std::nullopt)
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			, shots_total_opt(!stm.getColumn("shots_total").isNull() ?
				std::optional{stm.getColumn("shots_total").getInt()} : std::nullopt)
			, shots_on_goal_opt(!stm.getColumn("shots_on_goal").isNull() ?
				std::optional{stm.getColumn("shots_on_goal").getInt()} : std::nullopt)
			, goals_scored_opt(!stm.getColumn("goals_scored").isNull() ?
				std::optional{stm.getColumn("goals_scored").getInt()} : std::nullopt)
			, goals_assists_opt(!stm.getColumn("goals_assists").isNull() ?
				std::optional{stm.getColumn("goals_assists").getInt()} : std::nullopt)
			, goals_conceded_opt(!stm.getColumn("goals_conceded").isNull() ?
				std::optional{stm.getColumn("goals_conceded").getInt()} : std::nullopt)
			, goals_owngoals_opt(!stm.getColumn("goals_owngoals").isNull() ?
				std::optional{stm.getColumn("goals_owngoals").getInt()} : std::nullopt)
			, fouls_drawn_opt(!stm.getColumn("fouls_drawn").isNull() ?
				std::optional{stm.getColumn("fouls_drawn").getInt()} : std::nullopt)
			, fouls_committed_opt(!stm.getColumn("fouls_committed").isNull() ?
				std::optional{stm.getColumn("fouls_committed").getInt()} : std::nullopt)
			, yellowcards_opt(!stm.getColumn("yellowcards").isNull() ?
				std::optional{stm.getColumn("yellowcards").getInt()} : std::nullopt)
			, redcards_opt(!stm.getColumn("redcards").isNull() ?
				std::optional{stm.getColumn("redcards").getInt()} : std::nullopt)
			, yellowredcards_opt(!stm.getColumn("yellowredcards").isNull() ?
				std::optional{stm.getColumn("yellowredcards").getInt()} : std::nullopt)
			, passing_total_crosses_opt(!stm.getColumn("passing_total_crosses").isNull() ?
				std::optional{stm.getColumn("passing_total_crosses").getInt()} : std::nullopt)
			, passing_crosses_accuracy_opt(!stm.getColumn("passing_crosses_accuracy").isNull() ?
				std::optional{stm.getColumn("passing_crosses_accuracy").getInt()} : std::nullopt)
			, passing_passes_opt(!stm.getColumn("passing_passes").isNull() ?
				std::optional{stm.getColumn("passing_passes").getInt()} : std::nullopt)
			, passing_accurate_passes_opt(!stm.getColumn("passing_accurate_passes").isNull() ?
				std::optional{stm.getColumn("passing_accurate_passes").getInt()} : std::nullopt)
			, passing_passes_accuracy_opt(!stm.getColumn("passing_passes_accuracy").isNull() ?
				std::optional{stm.getColumn("passing_passes_accuracy").getInt()} : std::nullopt)
			, passing_key_passes_opt(!stm.getColumn("passing_key_passes").isNull() ?
				std::optional{stm.getColumn("passing_key_passes").getInt()} : std::nullopt)
			, dribbles_attempts_opt(!stm.getColumn("dribbles_attempts").isNull() ?
				std::optional{stm.getColumn("dribbles_attempts").getInt()} : std::nullopt)
			, dribbles_success_opt(!stm.getColumn("dribbles_success").isNull() ?
				std::optional{stm.getColumn("dribbles_success").getInt()} : std::nullopt)
			, dribbles_dribbled_past_opt(!stm.getColumn("dribbles_dribbled_past").isNull() ?
				std::optional{stm.getColumn("dribbles_dribbled_past").getInt()} : std::nullopt)
			, duels_total_opt(!stm.getColumn("duels_total").isNull() ?
				std::optional{stm.getColumn("duels_total").getInt()} : std::nullopt)
			, duels_won_opt(!stm.getColumn("duels_won").isNull() ?
				std::optional{stm.getColumn("duels_won").getInt()} : std::nullopt)
			, aerials_won_opt(!stm.getColumn("aerials_won").isNull() ?
				std::optional{stm.getColumn("aerials_won").getInt()} : std::nullopt)
			, punches_opt(!stm.getColumn("punches").isNull() ?
				std::optional{stm.getColumn("punches").getInt()} : std::nullopt)
			, offsides_opt(!stm.getColumn("offsides").isNull() ?
				std::optional{stm.getColumn("offsides").getInt()} : std::nullopt)
			, saves_opt(!stm.getColumn("saves").isNull() ?
				std::optional{stm.getColumn("saves").getInt()} : std::nullopt)
			, inside_box_saves_opt(!stm.getColumn("inside_box_saves").isNull() ?
				std::optional{stm.getColumn("inside_box_saves").getInt()} : std::nullopt)
			, penalty_scored_opt(!stm.getColumn("penalty_scored").isNull() ?
				std::optional{stm.getColumn("penalty_scored").getInt()} : std::nullopt)
			, penalty_missed_opt(!stm.getColumn("penalty_missed").isNull() ?
				std::optional{stm.getColumn("penalty_missed").getInt()} : std::nullopt)
			, penalty_saved_opt(!stm.getColumn("penalty_saved").isNull() ?
				std::optional{stm.getColumn("penalty_saved").getInt()} : std::nullopt)
			, penalty_committed_opt(!stm.getColumn("penalty_committed").isNull() ?
				std::optional{stm.getColumn("penalty_committed").getInt()} : std::nullopt)
			, penalty_won_opt(!stm.getColumn("penalty_won").isNull() ?
				std::optional{stm.getColumn("penalty_won").getInt()} : std::nullopt)
			, hit_woodwork_opt(!stm.getColumn("hit_woodwork").isNull() ?
				std::optional{stm.getColumn("hit_woodwork").getInt()} : std::nullopt)
			, tackles_opt(!stm.getColumn("tackles").isNull() ?
				std::optional{stm.getColumn("tackles").getInt()} : std::nullopt)
			, blocks_opt(!stm.getColumn("blocks").isNull() ?
				std::optional{stm.getColumn("blocks").getInt()} : std::nullopt)
			, interceptions_opt(!stm.getColumn("interceptions").isNull() ?
				std::optional{stm.getColumn("interceptions").getInt()} : std::nullopt)
			, clearances_opt(!stm.getColumn("clearances").isNull() ?
				std::optional{stm.getColumn("clearances").getInt()} : std::nullopt)
			, dispossesed_opt(!stm.getColumn("dispossesed").isNull() ?
				std::optional{stm.getColumn("dispossesed").getInt()} : std::nullopt)
			, minutes_played_opt(!stm.getColumn("minutes_played").isNull() ?
				std::optional{stm.getColumn("minutes_played").getInt()} : std::nullopt)
			, rating_opt(!stm.getColumn("rating").isNull() ?
				std::optional{stm.getColumn("rating").getDouble()} : std::nullopt)
			
		{}
	};

	using UpLineup = std::unique_ptr<Lineup>;

	struct LineupCmp
	{
		bool operator()(const Lineup& a, const Lineup& b) const
		{
			return a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.sm_team_id != b.sm_team_id ? a.sm_team_id < b.sm_team_id :
				a.sm_player_id < b.sm_player_id;
		}

		bool operator()(const UpLineup& a, const UpLineup& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Lineup, T>{} || std::is_same<UpLineup, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM lineups;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Lineup, T>{} || std::is_same<UpLineup, T>{},
		int > ::type = 0 >
	inline std::vector<UpLineup> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM lineups;" };

		SQLite::Statement stm(db, query);
		std::vector<UpLineup> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Lineup>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Lineup, T>{} || std::is_same<UpLineup, T>{},
		int > ::type = 0 >
	inline std::vector<UpLineup> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Lineup>(db);
		}

		const std::string query{ "SELECT * FROM lineups WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpLineup> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Lineup>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Lineup, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& lineups,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(lineups));
		std::transform(std::cbegin(lineups), std::cend(lineups), std::back_inserter(pointers),
			[](const typename TContainer::value_type& lineup)
			{
				return &lineup;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpLineup, typename TContainer::value_type>{} ||
		std::is_same<const Lineup*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& lineups,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Lineup>: Inserting " << std::size(lineups) << " lineups." << endl;
		}
		if (std::empty(lineups) || 
			std::all_of(std::cbegin(lineups), std::cend(lineups), 
				[](const auto& pLineup) {return nullptr == pLineup;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 52;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(lineups) / batchSize;
			if(0 != std::size(lineups) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(lineups);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(lineups)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(lineups);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO lineups VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":sm_player_id" << i << ", ";
					ss << ":player_name" << i << ", ";
					ss << ":number" << i << ", ";
					ss << ":position" << i << ", ";
					ss << ":additional_position" << i << ", ";
					ss << ":formation_position" << i << ", ";
					ss << ":posx" << i << ", ";
					ss << ":posy" << i << ", ";
					ss << ":captain" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":shots_total" << i << ", ";
					ss << ":shots_on_goal" << i << ", ";
					ss << ":goals_scored" << i << ", ";
					ss << ":goals_assists" << i << ", ";
					ss << ":goals_conceded" << i << ", ";
					ss << ":goals_owngoals" << i << ", ";
					ss << ":fouls_drawn" << i << ", ";
					ss << ":fouls_committed" << i << ", ";
					ss << ":yellowcards" << i << ", ";
					ss << ":redcards" << i << ", ";
					ss << ":yellowredcards" << i << ", ";
					ss << ":passing_total_crosses" << i << ", ";
					ss << ":passing_crosses_accuracy" << i << ", ";
					ss << ":passing_passes" << i << ", ";
					ss << ":passing_accurate_passes" << i << ", ";
					ss << ":passing_passes_accuracy" << i << ", ";
					ss << ":passing_key_passes" << i << ", ";
					ss << ":dribbles_attempts" << i << ", ";
					ss << ":dribbles_success" << i << ", ";
					ss << ":dribbles_dribbled_past" << i << ", ";
					ss << ":duels_total" << i << ", ";
					ss << ":duels_won" << i << ", ";
					ss << ":aerials_won" << i << ", ";
					ss << ":punches" << i << ", ";
					ss << ":offsides" << i << ", ";
					ss << ":saves" << i << ", ";
					ss << ":inside_box_saves" << i << ", ";
					ss << ":penalty_scored" << i << ", ";
					ss << ":penalty_missed" << i << ", ";
					ss << ":penalty_saved" << i << ", ";
					ss << ":penalty_committed" << i << ", ";
					ss << ":penalty_won" << i << ", ";
					ss << ":hit_woodwork" << i << ", ";
					ss << ":tackles" << i << ", ";
					ss << ":blocks" << i << ", ";
					ss << ":interceptions" << i << ", ";
					ss << ":clearances" << i << ", ";
					ss << ":dispossesed" << i << ", ";
					ss << ":minutes_played" << i << ", ";
					ss << ":rating" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					insert.bind(":sm_player_id" + idx, (*it)->sm_player_id);
					insert.bind(":player_name" + idx, UtilsStringToStdString((*it)->player_name));
					insert.bind(":number" + idx, (*it)->number);
					insert.bind(":position" + idx, UtilsStringToStdString((*it)->position));
					BindOptional(insert, ":additional_position" + idx, (*it)->additional_position_opt);
					BindOptional(insert, ":formation_position" + idx, (*it)->formation_position_opt);
					BindOptional(insert, ":posx" + idx, (*it)->posx_opt);
					BindOptional(insert, ":posy" + idx, (*it)->posy_opt);
					BindOptional(insert, ":captain" + idx, (*it)->captain_opt);
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					BindOptional(insert, ":shots_total" + idx, (*it)->shots_total_opt);
					BindOptional(insert, ":shots_on_goal" + idx, (*it)->shots_on_goal_opt);
					BindOptional(insert, ":goals_scored" + idx, (*it)->goals_scored_opt);
					BindOptional(insert, ":goals_assists" + idx, (*it)->goals_assists_opt);
					BindOptional(insert, ":goals_conceded" + idx, (*it)->goals_conceded_opt);
					BindOptional(insert, ":goals_owngoals" + idx, (*it)->goals_owngoals_opt);
					BindOptional(insert, ":fouls_drawn" + idx, (*it)->fouls_drawn_opt);
					BindOptional(insert, ":fouls_committed" + idx, (*it)->fouls_committed_opt);
					BindOptional(insert, ":yellowcards" + idx, (*it)->yellowcards_opt);
					BindOptional(insert, ":redcards" + idx, (*it)->redcards_opt);
					BindOptional(insert, ":yellowredcards" + idx, (*it)->yellowredcards_opt);
					BindOptional(insert, ":passing_total_crosses" + idx, (*it)->passing_total_crosses_opt);
					BindOptional(insert, ":passing_crosses_accuracy" + idx, (*it)->passing_crosses_accuracy_opt);
					BindOptional(insert, ":passing_passes" + idx, (*it)->passing_passes_opt);
					BindOptional(insert, ":passing_accurate_passes" + idx, (*it)->passing_accurate_passes_opt);
					BindOptional(insert, ":passing_passes_accuracy" + idx, (*it)->passing_passes_accuracy_opt);
					BindOptional(insert, ":passing_key_passes" + idx, (*it)->passing_key_passes_opt);
					BindOptional(insert, ":dribbles_attempts" + idx, (*it)->dribbles_attempts_opt);
					BindOptional(insert, ":dribbles_success" + idx, (*it)->dribbles_success_opt);
					BindOptional(insert, ":dribbles_dribbled_past" + idx, (*it)->dribbles_dribbled_past_opt);
					BindOptional(insert, ":duels_total" + idx, (*it)->duels_total_opt);
					BindOptional(insert, ":duels_won" + idx, (*it)->duels_won_opt);
					BindOptional(insert, ":aerials_won" + idx, (*it)->aerials_won_opt);
					BindOptional(insert, ":punches" + idx, (*it)->punches_opt);
					BindOptional(insert, ":offsides" + idx, (*it)->offsides_opt);
					BindOptional(insert, ":saves" + idx, (*it)->saves_opt);
					BindOptional(insert, ":inside_box_saves" + idx, (*it)->inside_box_saves_opt);
					BindOptional(insert, ":penalty_scored" + idx, (*it)->penalty_scored_opt);
					BindOptional(insert, ":penalty_missed" + idx, (*it)->penalty_missed_opt);
					BindOptional(insert, ":penalty_saved" + idx, (*it)->penalty_saved_opt);
					BindOptional(insert, ":penalty_committed" + idx, (*it)->penalty_committed_opt);
					BindOptional(insert, ":penalty_won" + idx, (*it)->penalty_won_opt);
					BindOptional(insert, ":hit_woodwork" + idx, (*it)->hit_woodwork_opt);
					BindOptional(insert, ":tackles" + idx, (*it)->tackles_opt);
					BindOptional(insert, ":blocks" + idx, (*it)->blocks_opt);
					BindOptional(insert, ":interceptions" + idx, (*it)->interceptions_opt);
					BindOptional(insert, ":clearances" + idx, (*it)->clearances_opt);
					BindOptional(insert, ":dispossesed" + idx, (*it)->dispossesed_opt);
					BindOptional(insert, ":minutes_played" + idx, (*it)->minutes_played_opt);
					BindOptional(insert, ":rating" + idx, (*it)->rating_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(lineups) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "lineups");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0