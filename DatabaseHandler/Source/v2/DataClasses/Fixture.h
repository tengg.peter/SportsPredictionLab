#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/Lineup.h"
#include "DatabaseHandler/Source/v2/DataClasses/MatchStatistics.h"
#include "DatabaseHandler/Source/v2/DataClasses/FixtureSideline.h"
#include "DatabaseHandler/Source/v2/DataClasses/Trend.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Fixture
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t sm_league_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_stage_id = 0;
		int64_t sm_round_id = 0;
		std::optional<int64_t> sm_group_id_opt;
		std::optional<int64_t> sm_aggregate_id_opt;
		std::optional<int64_t> sm_venue_id_opt;
		std::optional<int64_t> sm_referee_id_opt;
		int64_t sm_localteam_id = 0;
		int64_t sm_visitorteam_id = 0;
		std::optional<int64_t> sm_winner_team_id_opt;
		std::optional<utils::String> weather_code_opt;
		std::optional<utils::String> weather_type_opt;
		std::optional<utils::String> weather_icon_opt;
		std::optional<double> weather_temp_cel_opt;
		std::optional<double> weather_temp_fah_opt;
		std::optional<utils::String> weather_clouds_opt;
		std::optional<utils::String> weather_humidity_opt;
		std::optional<int64_t> weather_pressure_opt;
		std::optional<utils::String> weather_wind_speed_opt;
		std::optional<int64_t> weather_wind_degree_opt;
		std::optional<double> weather_coordinates_lat_opt;
		std::optional<double> weather_coordinates_lon_opt;
		std::optional<utils::String> weather_updated_at_opt;
		bool commentaries = false;
		std::optional<int64_t> attendance_opt;
		std::optional<utils::String> pitch_opt;
		std::optional<utils::String> details_opt;
		bool neutral_venue = false;
		bool winning_odds_calculated = false;
		std::optional<utils::String> localteam_formation_opt;
		std::optional<utils::String> visitorteam_formation_opt;
		int64_t localteam_score = 0;
		int64_t visitorteam_score = 0;
		std::optional<int64_t> localteam_pen_score_opt;
		std::optional<int64_t> visitorteam_pen_score_opt;
		std::optional<utils::String> ht_score_opt;
		utils::String ft_score;
		std::optional<utils::String> et_score_opt;
		std::optional<utils::String> ps_score_opt;
		utils::String status;
		utils::date_and_time::DateTime date_time;
		utils::String date;
		utils::String time;
		int64_t timestamp = 0;
		utils::String timezone;
		int64_t minute = 0;
		std::optional<int64_t> second_opt;
		std::optional<int64_t> added_time_opt;
		std::optional<int64_t> extra_minute_opt;
		std::optional<int64_t> injury_time_opt;
		std::optional<int64_t> sm_localteam_coach_id_opt;
		std::optional<int64_t> sm_visitorteam_coach_id_opt;
		std::optional<int64_t> localteam_position_opt;
		std::optional<int64_t> visitorteam_position_opt;
		std::optional<int64_t> sm_first_assistant_id_opt;
		std::optional<int64_t> sm_second_assistant_id_opt;
		std::optional<int64_t> sm_fourth_official_id_opt;
		utils::String leg;
		std::optional<utils::String> localteam_color_opt;
		std::optional<utils::String> localteam_kit_color_opt;
		std::optional<utils::String> visitorteam_color_opt;
		std::optional<utils::String> visitorteam_kit_color_opt;
		bool deleted = false;
		//##protect##"class members"
		std::set<MatchStatistics, MatchStatisticsCmp> matchStatistics;
		std::set<Lineup, LineupCmp> lineups;
		std::set<FixtureSideline, FixtureSidelineCmp> sidelines;
		std::set<Trend, TrendCmp> trends;
		//##protect##"class members"

		explicit Fixture() = default;
		virtual ~Fixture() {}

		explicit Fixture(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_stage_id(stm.getColumn("sm_stage_id").getInt64())
			, sm_round_id(stm.getColumn("sm_round_id").getInt64())
			, sm_group_id_opt(!stm.getColumn("sm_group_id").isNull() ?
				std::optional{stm.getColumn("sm_group_id").getInt64()} : std::nullopt)
			, sm_aggregate_id_opt(!stm.getColumn("sm_aggregate_id").isNull() ?
				std::optional{stm.getColumn("sm_aggregate_id").getInt64()} : std::nullopt)
			, sm_venue_id_opt(!stm.getColumn("sm_venue_id").isNull() ?
				std::optional{stm.getColumn("sm_venue_id").getInt64()} : std::nullopt)
			, sm_referee_id_opt(!stm.getColumn("sm_referee_id").isNull() ?
				std::optional{stm.getColumn("sm_referee_id").getInt64()} : std::nullopt)
			, sm_localteam_id(stm.getColumn("sm_localteam_id").getInt64())
			, sm_visitorteam_id(stm.getColumn("sm_visitorteam_id").getInt64())
			, sm_winner_team_id_opt(!stm.getColumn("sm_winner_team_id").isNull() ?
				std::optional{stm.getColumn("sm_winner_team_id").getInt64()} : std::nullopt)
			, weather_code_opt(!stm.getColumn("weather_code").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_code").getString())} : std::nullopt)
			, weather_type_opt(!stm.getColumn("weather_type").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_type").getString())} : std::nullopt)
			, weather_icon_opt(!stm.getColumn("weather_icon").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_icon").getString())} : std::nullopt)
			, weather_temp_cel_opt(!stm.getColumn("weather_temp_cel").isNull() ?
				std::optional{stm.getColumn("weather_temp_cel").getDouble()} : std::nullopt)
			, weather_temp_fah_opt(!stm.getColumn("weather_temp_fah").isNull() ?
				std::optional{stm.getColumn("weather_temp_fah").getDouble()} : std::nullopt)
			, weather_clouds_opt(!stm.getColumn("weather_clouds").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_clouds").getString())} : std::nullopt)
			, weather_humidity_opt(!stm.getColumn("weather_humidity").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_humidity").getString())} : std::nullopt)
			, weather_pressure_opt(!stm.getColumn("weather_pressure").isNull() ?
				std::optional{stm.getColumn("weather_pressure").getInt64()} : std::nullopt)
			, weather_wind_speed_opt(!stm.getColumn("weather_wind_speed").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_wind_speed").getString())} : std::nullopt)
			, weather_wind_degree_opt(!stm.getColumn("weather_wind_degree").isNull() ?
				std::optional{stm.getColumn("weather_wind_degree").getInt64()} : std::nullopt)
			, weather_coordinates_lat_opt(!stm.getColumn("weather_coordinates_lat").isNull() ?
				std::optional{stm.getColumn("weather_coordinates_lat").getDouble()} : std::nullopt)
			, weather_coordinates_lon_opt(!stm.getColumn("weather_coordinates_lon").isNull() ?
				std::optional{stm.getColumn("weather_coordinates_lon").getDouble()} : std::nullopt)
			, weather_updated_at_opt(!stm.getColumn("weather_updated_at").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weather_updated_at").getString())} : std::nullopt)
			, commentaries(static_cast<bool>(stm.getColumn("commentaries").getInt()))
			, attendance_opt(!stm.getColumn("attendance").isNull() ?
				std::optional{stm.getColumn("attendance").getInt64()} : std::nullopt)
			, pitch_opt(!stm.getColumn("pitch").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("pitch").getString())} : std::nullopt)
			, details_opt(!stm.getColumn("details").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("details").getString())} : std::nullopt)
			, neutral_venue(static_cast<bool>(stm.getColumn("neutral_venue").getInt()))
			, winning_odds_calculated(static_cast<bool>(stm.getColumn("winning_odds_calculated").getInt()))
			, localteam_formation_opt(!stm.getColumn("localteam_formation").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("localteam_formation").getString())} : std::nullopt)
			, visitorteam_formation_opt(!stm.getColumn("visitorteam_formation").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("visitorteam_formation").getString())} : std::nullopt)
			, localteam_score(stm.getColumn("localteam_score").getInt64())
			, visitorteam_score(stm.getColumn("visitorteam_score").getInt64())
			, localteam_pen_score_opt(!stm.getColumn("localteam_pen_score").isNull() ?
				std::optional{stm.getColumn("localteam_pen_score").getInt64()} : std::nullopt)
			, visitorteam_pen_score_opt(!stm.getColumn("visitorteam_pen_score").isNull() ?
				std::optional{stm.getColumn("visitorteam_pen_score").getInt64()} : std::nullopt)
			, ht_score_opt(!stm.getColumn("ht_score").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("ht_score").getString())} : std::nullopt)
			, ft_score(StdStringToUtilsString(stm.getColumn("ft_score").getString()))
			, et_score_opt(!stm.getColumn("et_score").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("et_score").getString())} : std::nullopt)
			, ps_score_opt(!stm.getColumn("ps_score").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("ps_score").getString())} : std::nullopt)
			, status(StdStringToUtilsString(stm.getColumn("status").getString()))
			, date_time(utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString()))
			, date(StdStringToUtilsString(stm.getColumn("date").getString()))
			, time(StdStringToUtilsString(stm.getColumn("time").getString()))
			, timestamp(stm.getColumn("timestamp").getInt64())
			, timezone(StdStringToUtilsString(stm.getColumn("timezone").getString()))
			, minute(stm.getColumn("minute").getInt64())
			, second_opt(!stm.getColumn("second").isNull() ?
				std::optional{stm.getColumn("second").getInt64()} : std::nullopt)
			, added_time_opt(!stm.getColumn("added_time").isNull() ?
				std::optional{stm.getColumn("added_time").getInt64()} : std::nullopt)
			, extra_minute_opt(!stm.getColumn("extra_minute").isNull() ?
				std::optional{stm.getColumn("extra_minute").getInt64()} : std::nullopt)
			, injury_time_opt(!stm.getColumn("injury_time").isNull() ?
				std::optional{stm.getColumn("injury_time").getInt64()} : std::nullopt)
			, sm_localteam_coach_id_opt(!stm.getColumn("sm_localteam_coach_id").isNull() ?
				std::optional{stm.getColumn("sm_localteam_coach_id").getInt64()} : std::nullopt)
			, sm_visitorteam_coach_id_opt(!stm.getColumn("sm_visitorteam_coach_id").isNull() ?
				std::optional{stm.getColumn("sm_visitorteam_coach_id").getInt64()} : std::nullopt)
			, localteam_position_opt(!stm.getColumn("localteam_position").isNull() ?
				std::optional{stm.getColumn("localteam_position").getInt64()} : std::nullopt)
			, visitorteam_position_opt(!stm.getColumn("visitorteam_position").isNull() ?
				std::optional{stm.getColumn("visitorteam_position").getInt64()} : std::nullopt)
			, sm_first_assistant_id_opt(!stm.getColumn("sm_first_assistant_id").isNull() ?
				std::optional{stm.getColumn("sm_first_assistant_id").getInt64()} : std::nullopt)
			, sm_second_assistant_id_opt(!stm.getColumn("sm_second_assistant_id").isNull() ?
				std::optional{stm.getColumn("sm_second_assistant_id").getInt64()} : std::nullopt)
			, sm_fourth_official_id_opt(!stm.getColumn("sm_fourth_official_id").isNull() ?
				std::optional{stm.getColumn("sm_fourth_official_id").getInt64()} : std::nullopt)
			, leg(StdStringToUtilsString(stm.getColumn("leg").getString()))
			, localteam_color_opt(!stm.getColumn("localteam_color").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("localteam_color").getString())} : std::nullopt)
			, localteam_kit_color_opt(!stm.getColumn("localteam_kit_color").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("localteam_kit_color").getString())} : std::nullopt)
			, visitorteam_color_opt(!stm.getColumn("visitorteam_color").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("visitorteam_color").getString())} : std::nullopt)
			, visitorteam_kit_color_opt(!stm.getColumn("visitorteam_kit_color").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("visitorteam_kit_color").getString())} : std::nullopt)
			, deleted(static_cast<bool>(stm.getColumn("deleted").getInt()))
			
		{}
	};

	using UpFixture = std::unique_ptr<Fixture>;

	struct FixtureCmp
	{
		bool operator()(const Fixture& a, const Fixture& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpFixture& a, const UpFixture& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Fixture, T>{} || std::is_same<UpFixture, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM fixtures;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Fixture, T>{} || std::is_same<UpFixture, T>{},
		int > ::type = 0 >
	inline std::vector<UpFixture> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM fixtures;" };

		SQLite::Statement stm(db, query);
		std::vector<UpFixture> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Fixture>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Fixture, T>{} || std::is_same<UpFixture, T>{},
		int > ::type = 0 >
	inline std::vector<UpFixture> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Fixture>(db);
		}

		const std::string query{ "SELECT * FROM fixtures WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpFixture> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Fixture>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Fixture, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& fixtures,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(fixtures));
		std::transform(std::cbegin(fixtures), std::cend(fixtures), std::back_inserter(pointers),
			[](const typename TContainer::value_type& fixture)
			{
				return &fixture;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpFixture, typename TContainer::value_type>{} ||
		std::is_same<const Fixture*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& fixtures,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Fixture>: Inserting " << std::size(fixtures) << " fixtures." << endl;
		}
		if (std::empty(fixtures) || 
			std::all_of(std::cbegin(fixtures), std::cend(fixtures), 
				[](const auto& pFixture) {return nullptr == pFixture;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 65;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(fixtures) / batchSize;
			if(0 != std::size(fixtures) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(fixtures);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(fixtures)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(fixtures);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO fixtures VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_stage_id" << i << ", ";
					ss << ":sm_round_id" << i << ", ";
					ss << ":sm_group_id" << i << ", ";
					ss << ":sm_aggregate_id" << i << ", ";
					ss << ":sm_venue_id" << i << ", ";
					ss << ":sm_referee_id" << i << ", ";
					ss << ":sm_localteam_id" << i << ", ";
					ss << ":sm_visitorteam_id" << i << ", ";
					ss << ":sm_winner_team_id" << i << ", ";
					ss << ":weather_code" << i << ", ";
					ss << ":weather_type" << i << ", ";
					ss << ":weather_icon" << i << ", ";
					ss << ":weather_temp_cel" << i << ", ";
					ss << ":weather_temp_fah" << i << ", ";
					ss << ":weather_clouds" << i << ", ";
					ss << ":weather_humidity" << i << ", ";
					ss << ":weather_pressure" << i << ", ";
					ss << ":weather_wind_speed" << i << ", ";
					ss << ":weather_wind_degree" << i << ", ";
					ss << ":weather_coordinates_lat" << i << ", ";
					ss << ":weather_coordinates_lon" << i << ", ";
					ss << ":weather_updated_at" << i << ", ";
					ss << ":commentaries" << i << ", ";
					ss << ":attendance" << i << ", ";
					ss << ":pitch" << i << ", ";
					ss << ":details" << i << ", ";
					ss << ":neutral_venue" << i << ", ";
					ss << ":winning_odds_calculated" << i << ", ";
					ss << ":localteam_formation" << i << ", ";
					ss << ":visitorteam_formation" << i << ", ";
					ss << ":localteam_score" << i << ", ";
					ss << ":visitorteam_score" << i << ", ";
					ss << ":localteam_pen_score" << i << ", ";
					ss << ":visitorteam_pen_score" << i << ", ";
					ss << ":ht_score" << i << ", ";
					ss << ":ft_score" << i << ", ";
					ss << ":et_score" << i << ", ";
					ss << ":ps_score" << i << ", ";
					ss << ":status" << i << ", ";
					ss << ":date_time" << i << ", ";
					ss << ":date" << i << ", ";
					ss << ":time" << i << ", ";
					ss << ":timestamp" << i << ", ";
					ss << ":timezone" << i << ", ";
					ss << ":minute" << i << ", ";
					ss << ":second" << i << ", ";
					ss << ":added_time" << i << ", ";
					ss << ":extra_minute" << i << ", ";
					ss << ":injury_time" << i << ", ";
					ss << ":sm_localteam_coach_id" << i << ", ";
					ss << ":sm_visitorteam_coach_id" << i << ", ";
					ss << ":localteam_position" << i << ", ";
					ss << ":visitorteam_position" << i << ", ";
					ss << ":sm_first_assistant_id" << i << ", ";
					ss << ":sm_second_assistant_id" << i << ", ";
					ss << ":sm_fourth_official_id" << i << ", ";
					ss << ":leg" << i << ", ";
					ss << ":localteam_color" << i << ", ";
					ss << ":localteam_kit_color" << i << ", ";
					ss << ":visitorteam_color" << i << ", ";
					ss << ":visitorteam_kit_color" << i << ", ";
					ss << ":deleted" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_stage_id" + idx, (*it)->sm_stage_id);
					insert.bind(":sm_round_id" + idx, (*it)->sm_round_id);
					BindOptional(insert, ":sm_group_id" + idx, (*it)->sm_group_id_opt);
					BindOptional(insert, ":sm_aggregate_id" + idx, (*it)->sm_aggregate_id_opt);
					BindOptional(insert, ":sm_venue_id" + idx, (*it)->sm_venue_id_opt);
					BindOptional(insert, ":sm_referee_id" + idx, (*it)->sm_referee_id_opt);
					insert.bind(":sm_localteam_id" + idx, (*it)->sm_localteam_id);
					insert.bind(":sm_visitorteam_id" + idx, (*it)->sm_visitorteam_id);
					BindOptional(insert, ":sm_winner_team_id" + idx, (*it)->sm_winner_team_id_opt);
					BindOptional(insert, ":weather_code" + idx, (*it)->weather_code_opt);
					BindOptional(insert, ":weather_type" + idx, (*it)->weather_type_opt);
					BindOptional(insert, ":weather_icon" + idx, (*it)->weather_icon_opt);
					BindOptional(insert, ":weather_temp_cel" + idx, (*it)->weather_temp_cel_opt);
					BindOptional(insert, ":weather_temp_fah" + idx, (*it)->weather_temp_fah_opt);
					BindOptional(insert, ":weather_clouds" + idx, (*it)->weather_clouds_opt);
					BindOptional(insert, ":weather_humidity" + idx, (*it)->weather_humidity_opt);
					BindOptional(insert, ":weather_pressure" + idx, (*it)->weather_pressure_opt);
					BindOptional(insert, ":weather_wind_speed" + idx, (*it)->weather_wind_speed_opt);
					BindOptional(insert, ":weather_wind_degree" + idx, (*it)->weather_wind_degree_opt);
					BindOptional(insert, ":weather_coordinates_lat" + idx, (*it)->weather_coordinates_lat_opt);
					BindOptional(insert, ":weather_coordinates_lon" + idx, (*it)->weather_coordinates_lon_opt);
					BindOptional(insert, ":weather_updated_at" + idx, (*it)->weather_updated_at_opt);
					insert.bind(":commentaries" + idx, (*it)->commentaries);
					BindOptional(insert, ":attendance" + idx, (*it)->attendance_opt);
					BindOptional(insert, ":pitch" + idx, (*it)->pitch_opt);
					BindOptional(insert, ":details" + idx, (*it)->details_opt);
					insert.bind(":neutral_venue" + idx, (*it)->neutral_venue);
					insert.bind(":winning_odds_calculated" + idx, (*it)->winning_odds_calculated);
					BindOptional(insert, ":localteam_formation" + idx, (*it)->localteam_formation_opt);
					BindOptional(insert, ":visitorteam_formation" + idx, (*it)->visitorteam_formation_opt);
					insert.bind(":localteam_score" + idx, (*it)->localteam_score);
					insert.bind(":visitorteam_score" + idx, (*it)->visitorteam_score);
					BindOptional(insert, ":localteam_pen_score" + idx, (*it)->localteam_pen_score_opt);
					BindOptional(insert, ":visitorteam_pen_score" + idx, (*it)->visitorteam_pen_score_opt);
					BindOptional(insert, ":ht_score" + idx, (*it)->ht_score_opt);
					insert.bind(":ft_score" + idx, UtilsStringToStdString((*it)->ft_score));
					BindOptional(insert, ":et_score" + idx, (*it)->et_score_opt);
					BindOptional(insert, ":ps_score" + idx, (*it)->ps_score_opt);
					insert.bind(":status" + idx, UtilsStringToStdString((*it)->status));
					insert.bind(":date_time" + idx, (*it)->date_time.ToSQLiteDateTimeFormat());
					insert.bind(":date" + idx, UtilsStringToStdString((*it)->date));
					insert.bind(":time" + idx, UtilsStringToStdString((*it)->time));
					insert.bind(":timestamp" + idx, (*it)->timestamp);
					insert.bind(":timezone" + idx, UtilsStringToStdString((*it)->timezone));
					insert.bind(":minute" + idx, (*it)->minute);
					BindOptional(insert, ":second" + idx, (*it)->second_opt);
					BindOptional(insert, ":added_time" + idx, (*it)->added_time_opt);
					BindOptional(insert, ":extra_minute" + idx, (*it)->extra_minute_opt);
					BindOptional(insert, ":injury_time" + idx, (*it)->injury_time_opt);
					BindOptional(insert, ":sm_localteam_coach_id" + idx, (*it)->sm_localteam_coach_id_opt);
					BindOptional(insert, ":sm_visitorteam_coach_id" + idx, (*it)->sm_visitorteam_coach_id_opt);
					BindOptional(insert, ":localteam_position" + idx, (*it)->localteam_position_opt);
					BindOptional(insert, ":visitorteam_position" + idx, (*it)->visitorteam_position_opt);
					BindOptional(insert, ":sm_first_assistant_id" + idx, (*it)->sm_first_assistant_id_opt);
					BindOptional(insert, ":sm_second_assistant_id" + idx, (*it)->sm_second_assistant_id_opt);
					BindOptional(insert, ":sm_fourth_official_id" + idx, (*it)->sm_fourth_official_id_opt);
					insert.bind(":leg" + idx, UtilsStringToStdString((*it)->leg));
					BindOptional(insert, ":localteam_color" + idx, (*it)->localteam_color_opt);
					BindOptional(insert, ":localteam_kit_color" + idx, (*it)->localteam_kit_color_opt);
					BindOptional(insert, ":visitorteam_color" + idx, (*it)->visitorteam_color_opt);
					BindOptional(insert, ":visitorteam_kit_color" + idx, (*it)->visitorteam_kit_color_opt);
					insert.bind(":deleted" + idx, (*it)->deleted);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					InsertOr(onConflict, db, (*it)->matchStatistics, false, false);
					InsertOr(onConflict, db, (*it)->lineups, false, false);
					InsertOr(onConflict, db, (*it)->sidelines, false, false);
					InsertOr(onConflict, db, (*it)->trends, false, false);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(fixtures) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "fixtures");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0