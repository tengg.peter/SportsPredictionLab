#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerSideline.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerTransfer.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Player
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t sm_team_id = 0;
		std::optional<int64_t> sm_country_id_opt;
		int64_t sm_position_id = 0;
		utils::String common_name;
		std::optional<utils::String> display_name_opt;
		std::optional<utils::String> fullname_opt;
		std::optional<utils::String> firstname_opt;
		std::optional<utils::String> lastname_opt;
		utils::String nationality;
		utils::String birthdate;
		std::optional<utils::String> birthcountry_opt;
		std::optional<utils::String> birthplace_opt;
		std::optional<utils::String> height_opt;
		std::optional<utils::String> weight_opt;
		std::optional<utils::String> image_path_opt;
		//##protect##"class members"
		std::set<PlayerSideline, PlayerSidelineCmp> sidelines;
		std::set<PlayerTransfer, PlayerTransferCmp> transfers;
		//##protect##"class members"

		explicit Player() = default;
		virtual ~Player() {}

		explicit Player(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_country_id_opt(!stm.getColumn("sm_country_id").isNull() ?
				std::optional{stm.getColumn("sm_country_id").getInt64()} : std::nullopt)
			, sm_position_id(stm.getColumn("sm_position_id").getInt64())
			, common_name(StdStringToUtilsString(stm.getColumn("common_name").getString()))
			, display_name_opt(!stm.getColumn("display_name").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("display_name").getString())} : std::nullopt)
			, fullname_opt(!stm.getColumn("fullname").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("fullname").getString())} : std::nullopt)
			, firstname_opt(!stm.getColumn("firstname").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("firstname").getString())} : std::nullopt)
			, lastname_opt(!stm.getColumn("lastname").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("lastname").getString())} : std::nullopt)
			, nationality(StdStringToUtilsString(stm.getColumn("nationality").getString()))
			, birthdate(StdStringToUtilsString(stm.getColumn("birthdate").getString()))
			, birthcountry_opt(!stm.getColumn("birthcountry").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("birthcountry").getString())} : std::nullopt)
			, birthplace_opt(!stm.getColumn("birthplace").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("birthplace").getString())} : std::nullopt)
			, height_opt(!stm.getColumn("height").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("height").getString())} : std::nullopt)
			, weight_opt(!stm.getColumn("weight").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("weight").getString())} : std::nullopt)
			, image_path_opt(!stm.getColumn("image_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("image_path").getString())} : std::nullopt)
			
		{}
	};

	using UpPlayer = std::unique_ptr<Player>;

	struct PlayerCmp
	{
		bool operator()(const Player& a, const Player& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpPlayer& a, const UpPlayer& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Player, T>{} || std::is_same<UpPlayer, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM players;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Player, T>{} || std::is_same<UpPlayer, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayer> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM players;" };

		SQLite::Statement stm(db, query);
		std::vector<UpPlayer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Player>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Player, T>{} || std::is_same<UpPlayer, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayer> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Player>(db);
		}

		const std::string query{ "SELECT * FROM players WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpPlayer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Player>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Player, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& players,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(players));
		std::transform(std::cbegin(players), std::cend(players), std::back_inserter(pointers),
			[](const typename TContainer::value_type& player)
			{
				return &player;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpPlayer, typename TContainer::value_type>{} ||
		std::is_same<const Player*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& players,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Player>: Inserting " << std::size(players) << " players." << endl;
		}
		if (std::empty(players) || 
			std::all_of(std::cbegin(players), std::cend(players), 
				[](const auto& pPlayer) {return nullptr == pPlayer;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 16;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(players) / batchSize;
			if(0 != std::size(players) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(players);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(players)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(players);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO players VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_country_id" << i << ", ";
					ss << ":sm_position_id" << i << ", ";
					ss << ":common_name" << i << ", ";
					ss << ":display_name" << i << ", ";
					ss << ":fullname" << i << ", ";
					ss << ":firstname" << i << ", ";
					ss << ":lastname" << i << ", ";
					ss << ":nationality" << i << ", ";
					ss << ":birthdate" << i << ", ";
					ss << ":birthcountry" << i << ", ";
					ss << ":birthplace" << i << ", ";
					ss << ":height" << i << ", ";
					ss << ":weight" << i << ", ";
					ss << ":image_path" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					BindOptional(insert, ":sm_country_id" + idx, (*it)->sm_country_id_opt);
					insert.bind(":sm_position_id" + idx, (*it)->sm_position_id);
					insert.bind(":common_name" + idx, UtilsStringToStdString((*it)->common_name));
					BindOptional(insert, ":display_name" + idx, (*it)->display_name_opt);
					BindOptional(insert, ":fullname" + idx, (*it)->fullname_opt);
					BindOptional(insert, ":firstname" + idx, (*it)->firstname_opt);
					BindOptional(insert, ":lastname" + idx, (*it)->lastname_opt);
					insert.bind(":nationality" + idx, UtilsStringToStdString((*it)->nationality));
					insert.bind(":birthdate" + idx, UtilsStringToStdString((*it)->birthdate));
					BindOptional(insert, ":birthcountry" + idx, (*it)->birthcountry_opt);
					BindOptional(insert, ":birthplace" + idx, (*it)->birthplace_opt);
					BindOptional(insert, ":height" + idx, (*it)->height_opt);
					BindOptional(insert, ":weight" + idx, (*it)->weight_opt);
					BindOptional(insert, ":image_path" + idx, (*it)->image_path_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != *it)
				{
					InsertOr(onConflict, db, (*it)->sidelines, false, false);
					InsertOr(onConflict, db, (*it)->transfers, false, false);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(players) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "players");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0