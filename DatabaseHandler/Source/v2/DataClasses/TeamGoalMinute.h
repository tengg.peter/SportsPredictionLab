#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class TeamGoalMinute
	{
	public:
		int64_t id = 0;
		int64_t team_season_statistics_id = 0;
		utils::String type;
		utils::String period;
		int count = 0;
		double percentage = 0.0;
		//##protect##"class members"
		//##protect##"class members"

		explicit TeamGoalMinute() = default;
		virtual ~TeamGoalMinute() {}

		explicit TeamGoalMinute(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, team_season_statistics_id(stm.getColumn("team_season_statistics_id").getInt64())
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			, period(StdStringToUtilsString(stm.getColumn("period").getString()))
			, count(stm.getColumn("count").getInt())
			, percentage(stm.getColumn("percentage").getDouble())
			
		{}
	};

	using UpTeamGoalMinute = std::unique_ptr<TeamGoalMinute>;

	struct TeamGoalMinuteCmp
	{
		bool operator()(const TeamGoalMinute& a, const TeamGoalMinute& b) const
		{
			return a.team_season_statistics_id != b.team_season_statistics_id ? a.team_season_statistics_id < b.team_season_statistics_id :
				a.period != b.period ? a.period < b.period :
				a.type < b.type;
		}

		bool operator()(const UpTeamGoalMinute& a, const UpTeamGoalMinute& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalMinute, T>{} || std::is_same<UpTeamGoalMinute, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM team_goal_minutes;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalMinute, T>{} || std::is_same<UpTeamGoalMinute, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamGoalMinute> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM team_goal_minutes;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTeamGoalMinute> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamGoalMinute>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalMinute, T>{} || std::is_same<UpTeamGoalMinute, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamGoalMinute> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<TeamGoalMinute>(db);
		}

		const std::string query{ "SELECT * FROM team_goal_minutes WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTeamGoalMinute> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamGoalMinute>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<TeamGoalMinute, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamGoalMinutes,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(teamGoalMinutes));
		std::transform(std::cbegin(teamGoalMinutes), std::cend(teamGoalMinutes), std::back_inserter(pointers),
			[](const typename TContainer::value_type& teamGoalMinute)
			{
				return &teamGoalMinute;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTeamGoalMinute, typename TContainer::value_type>{} ||
		std::is_same<const TeamGoalMinute*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamGoalMinutes,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<TeamGoalMinute>: Inserting " << std::size(teamGoalMinutes) << " teamGoalMinutes." << endl;
		}
		if (std::empty(teamGoalMinutes) || 
			std::all_of(std::cbegin(teamGoalMinutes), std::cend(teamGoalMinutes), 
				[](const auto& pTeamGoalMinute) {return nullptr == pTeamGoalMinute;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 5;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(teamGoalMinutes) / batchSize;
			if(0 != std::size(teamGoalMinutes) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(teamGoalMinutes);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(teamGoalMinutes)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(teamGoalMinutes);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO team_goal_minutes VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":team_season_statistics_id" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":period" << i << ", ";
					ss << ":count" << i << ", ";
					ss << ":percentage" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":team_season_statistics_id" + idx, (*it)->team_season_statistics_id);
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					insert.bind(":period" + idx, UtilsStringToStdString((*it)->period));
					insert.bind(":count" + idx, (*it)->count);
					insert.bind(":percentage" + idx, (*it)->percentage);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(teamGoalMinutes) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "team_goal_minutes");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0