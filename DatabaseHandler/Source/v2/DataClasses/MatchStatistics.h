#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class MatchStatistics
	{
	public:
		int64_t id = 0;
		int64_t sm_team_id = 0;
		int64_t sm_fixture_id = 0;
		std::optional<int> shots_total_opt;
		std::optional<int> shots_ongoal_opt;
		std::optional<int> shots_offgoal_opt;
		std::optional<int> shots_blocked_opt;
		std::optional<int> shots_insidebox_opt;
		std::optional<int> shots_outsidebox_opt;
		std::optional<int> passes_total_opt;
		std::optional<int> passes_accurate_opt;
		std::optional<double> passes_percentage_opt;
		std::optional<int> attacks_opt;
		std::optional<int> dangerous_attacks_opt;
		std::optional<int> fouls_opt;
		std::optional<int> corners_opt;
		std::optional<int> offsides_opt;
		std::optional<int> possessiontime_opt;
		std::optional<int> yellowcards_opt;
		std::optional<int> redcards_opt;
		std::optional<int> yellowredcards_opt;
		std::optional<int> saves_opt;
		std::optional<int> substitutions_opt;
		std::optional<int> goal_kick_opt;
		std::optional<int> throw_in_opt;
		std::optional<int> ball_safe_opt;
		std::optional<int> goals_opt;
		std::optional<int> penalties_opt;
		std::optional<int> injuries_opt;
		//##protect##"class members"
//##protect##"class members"

		explicit MatchStatistics() = default;
		virtual ~MatchStatistics() {}

		explicit MatchStatistics(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, shots_total_opt(!stm.getColumn("shots_total").isNull() ?
				std::optional{stm.getColumn("shots_total").getInt()} : std::nullopt)
			, shots_ongoal_opt(!stm.getColumn("shots_ongoal").isNull() ?
				std::optional{stm.getColumn("shots_ongoal").getInt()} : std::nullopt)
			, shots_offgoal_opt(!stm.getColumn("shots_offgoal").isNull() ?
				std::optional{stm.getColumn("shots_offgoal").getInt()} : std::nullopt)
			, shots_blocked_opt(!stm.getColumn("shots_blocked").isNull() ?
				std::optional{stm.getColumn("shots_blocked").getInt()} : std::nullopt)
			, shots_insidebox_opt(!stm.getColumn("shots_insidebox").isNull() ?
				std::optional{stm.getColumn("shots_insidebox").getInt()} : std::nullopt)
			, shots_outsidebox_opt(!stm.getColumn("shots_outsidebox").isNull() ?
				std::optional{stm.getColumn("shots_outsidebox").getInt()} : std::nullopt)
			, passes_total_opt(!stm.getColumn("passes_total").isNull() ?
				std::optional{stm.getColumn("passes_total").getInt()} : std::nullopt)
			, passes_accurate_opt(!stm.getColumn("passes_accurate").isNull() ?
				std::optional{stm.getColumn("passes_accurate").getInt()} : std::nullopt)
			, passes_percentage_opt(!stm.getColumn("passes_percentage").isNull() ?
				std::optional{stm.getColumn("passes_percentage").getDouble()} : std::nullopt)
			, attacks_opt(!stm.getColumn("attacks").isNull() ?
				std::optional{stm.getColumn("attacks").getInt()} : std::nullopt)
			, dangerous_attacks_opt(!stm.getColumn("dangerous_attacks").isNull() ?
				std::optional{stm.getColumn("dangerous_attacks").getInt()} : std::nullopt)
			, fouls_opt(!stm.getColumn("fouls").isNull() ?
				std::optional{stm.getColumn("fouls").getInt()} : std::nullopt)
			, corners_opt(!stm.getColumn("corners").isNull() ?
				std::optional{stm.getColumn("corners").getInt()} : std::nullopt)
			, offsides_opt(!stm.getColumn("offsides").isNull() ?
				std::optional{stm.getColumn("offsides").getInt()} : std::nullopt)
			, possessiontime_opt(!stm.getColumn("possessiontime").isNull() ?
				std::optional{stm.getColumn("possessiontime").getInt()} : std::nullopt)
			, yellowcards_opt(!stm.getColumn("yellowcards").isNull() ?
				std::optional{stm.getColumn("yellowcards").getInt()} : std::nullopt)
			, redcards_opt(!stm.getColumn("redcards").isNull() ?
				std::optional{stm.getColumn("redcards").getInt()} : std::nullopt)
			, yellowredcards_opt(!stm.getColumn("yellowredcards").isNull() ?
				std::optional{stm.getColumn("yellowredcards").getInt()} : std::nullopt)
			, saves_opt(!stm.getColumn("saves").isNull() ?
				std::optional{stm.getColumn("saves").getInt()} : std::nullopt)
			, substitutions_opt(!stm.getColumn("substitutions").isNull() ?
				std::optional{stm.getColumn("substitutions").getInt()} : std::nullopt)
			, goal_kick_opt(!stm.getColumn("goal_kick").isNull() ?
				std::optional{stm.getColumn("goal_kick").getInt()} : std::nullopt)
			, throw_in_opt(!stm.getColumn("throw_in").isNull() ?
				std::optional{stm.getColumn("throw_in").getInt()} : std::nullopt)
			, ball_safe_opt(!stm.getColumn("ball_safe").isNull() ?
				std::optional{stm.getColumn("ball_safe").getInt()} : std::nullopt)
			, goals_opt(!stm.getColumn("goals").isNull() ?
				std::optional{stm.getColumn("goals").getInt()} : std::nullopt)
			, penalties_opt(!stm.getColumn("penalties").isNull() ?
				std::optional{stm.getColumn("penalties").getInt()} : std::nullopt)
			, injuries_opt(!stm.getColumn("injuries").isNull() ?
				std::optional{stm.getColumn("injuries").getInt()} : std::nullopt)
			
		{}
	};

	using UpMatchStatistics = std::unique_ptr<MatchStatistics>;

	struct MatchStatisticsCmp
	{
		bool operator()(const MatchStatistics& a, const MatchStatistics& b) const
		{
			return a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.sm_team_id < b.sm_team_id;
		}

		bool operator()(const UpMatchStatistics& a, const UpMatchStatistics& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<MatchStatistics, T>{} || std::is_same<UpMatchStatistics, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM match_statistics;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<MatchStatistics, T>{} || std::is_same<UpMatchStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpMatchStatistics> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM match_statistics;" };

		SQLite::Statement stm(db, query);
		std::vector<UpMatchStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<MatchStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<MatchStatistics, T>{} || std::is_same<UpMatchStatistics, T>{},
		int > ::type = 0 >
	inline std::vector<UpMatchStatistics> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<MatchStatistics>(db);
		}

		const std::string query{ "SELECT * FROM match_statistics WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpMatchStatistics> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<MatchStatistics>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<MatchStatistics, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& matchStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(matchStatistics));
		std::transform(std::cbegin(matchStatistics), std::cend(matchStatistics), std::back_inserter(pointers),
			[](const typename TContainer::value_type& matchStatistics)
			{
				return &matchStatistics;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpMatchStatistics, typename TContainer::value_type>{} ||
		std::is_same<const MatchStatistics*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& matchStatistics,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<MatchStatistics>: Inserting " << std::size(matchStatistics) << " matchStatistics." << endl;
		}
		if (std::empty(matchStatistics) || 
			std::all_of(std::cbegin(matchStatistics), std::cend(matchStatistics), 
				[](const auto& pMatchStatistics) {return nullptr == pMatchStatistics;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 28;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(matchStatistics) / batchSize;
			if(0 != std::size(matchStatistics) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(matchStatistics);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(matchStatistics)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(matchStatistics);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO match_statistics VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":shots_total" << i << ", ";
					ss << ":shots_ongoal" << i << ", ";
					ss << ":shots_offgoal" << i << ", ";
					ss << ":shots_blocked" << i << ", ";
					ss << ":shots_insidebox" << i << ", ";
					ss << ":shots_outsidebox" << i << ", ";
					ss << ":passes_total" << i << ", ";
					ss << ":passes_accurate" << i << ", ";
					ss << ":passes_percentage" << i << ", ";
					ss << ":attacks" << i << ", ";
					ss << ":dangerous_attacks" << i << ", ";
					ss << ":fouls" << i << ", ";
					ss << ":corners" << i << ", ";
					ss << ":offsides" << i << ", ";
					ss << ":possessiontime" << i << ", ";
					ss << ":yellowcards" << i << ", ";
					ss << ":redcards" << i << ", ";
					ss << ":yellowredcards" << i << ", ";
					ss << ":saves" << i << ", ";
					ss << ":substitutions" << i << ", ";
					ss << ":goal_kick" << i << ", ";
					ss << ":throw_in" << i << ", ";
					ss << ":ball_safe" << i << ", ";
					ss << ":goals" << i << ", ";
					ss << ":penalties" << i << ", ";
					ss << ":injuries" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					BindOptional(insert, ":shots_total" + idx, (*it)->shots_total_opt);
					BindOptional(insert, ":shots_ongoal" + idx, (*it)->shots_ongoal_opt);
					BindOptional(insert, ":shots_offgoal" + idx, (*it)->shots_offgoal_opt);
					BindOptional(insert, ":shots_blocked" + idx, (*it)->shots_blocked_opt);
					BindOptional(insert, ":shots_insidebox" + idx, (*it)->shots_insidebox_opt);
					BindOptional(insert, ":shots_outsidebox" + idx, (*it)->shots_outsidebox_opt);
					BindOptional(insert, ":passes_total" + idx, (*it)->passes_total_opt);
					BindOptional(insert, ":passes_accurate" + idx, (*it)->passes_accurate_opt);
					BindOptional(insert, ":passes_percentage" + idx, (*it)->passes_percentage_opt);
					BindOptional(insert, ":attacks" + idx, (*it)->attacks_opt);
					BindOptional(insert, ":dangerous_attacks" + idx, (*it)->dangerous_attacks_opt);
					BindOptional(insert, ":fouls" + idx, (*it)->fouls_opt);
					BindOptional(insert, ":corners" + idx, (*it)->corners_opt);
					BindOptional(insert, ":offsides" + idx, (*it)->offsides_opt);
					BindOptional(insert, ":possessiontime" + idx, (*it)->possessiontime_opt);
					BindOptional(insert, ":yellowcards" + idx, (*it)->yellowcards_opt);
					BindOptional(insert, ":redcards" + idx, (*it)->redcards_opt);
					BindOptional(insert, ":yellowredcards" + idx, (*it)->yellowredcards_opt);
					BindOptional(insert, ":saves" + idx, (*it)->saves_opt);
					BindOptional(insert, ":substitutions" + idx, (*it)->substitutions_opt);
					BindOptional(insert, ":goal_kick" + idx, (*it)->goal_kick_opt);
					BindOptional(insert, ":throw_in" + idx, (*it)->throw_in_opt);
					BindOptional(insert, ":ball_safe" + idx, (*it)->ball_safe_opt);
					BindOptional(insert, ":goals" + idx, (*it)->goals_opt);
					BindOptional(insert, ":penalties" + idx, (*it)->penalties_opt);
					BindOptional(insert, ":injuries" + idx, (*it)->injuries_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(matchStatistics) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "match_statistics");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0