#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Coach
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t sm_team_id = 0;
		int64_t sm_country_id = 0;
		utils::String common_name;
		utils::String fullname;
		std::optional<utils::String> firstname_opt;
		std::optional<utils::String> lastname_opt;
		utils::String nationality;
		std::optional<utils::String> birthdate_opt;
		std::optional<utils::String> birthcountry_opt;
		std::optional<utils::String> birthplace_opt;
		std::optional<utils::String> image_path_opt;
		//##protect##"class members"
//##protect##"class members"

		explicit Coach() = default;
		virtual ~Coach() {}

		explicit Coach(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, sm_country_id(stm.getColumn("sm_country_id").getInt64())
			, common_name(StdStringToUtilsString(stm.getColumn("common_name").getString()))
			, fullname(StdStringToUtilsString(stm.getColumn("fullname").getString()))
			, firstname_opt(!stm.getColumn("firstname").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("firstname").getString())} : std::nullopt)
			, lastname_opt(!stm.getColumn("lastname").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("lastname").getString())} : std::nullopt)
			, nationality(StdStringToUtilsString(stm.getColumn("nationality").getString()))
			, birthdate_opt(!stm.getColumn("birthdate").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("birthdate").getString())} : std::nullopt)
			, birthcountry_opt(!stm.getColumn("birthcountry").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("birthcountry").getString())} : std::nullopt)
			, birthplace_opt(!stm.getColumn("birthplace").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("birthplace").getString())} : std::nullopt)
			, image_path_opt(!stm.getColumn("image_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("image_path").getString())} : std::nullopt)
			
		{}
	};

	using UpCoach = std::unique_ptr<Coach>;

	struct CoachCmp
	{
		bool operator()(const Coach& a, const Coach& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpCoach& a, const UpCoach& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Coach, T>{} || std::is_same<UpCoach, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM coaches;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Coach, T>{} || std::is_same<UpCoach, T>{},
		int > ::type = 0 >
	inline std::vector<UpCoach> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM coaches;" };

		SQLite::Statement stm(db, query);
		std::vector<UpCoach> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Coach>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Coach, T>{} || std::is_same<UpCoach, T>{},
		int > ::type = 0 >
	inline std::vector<UpCoach> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Coach>(db);
		}

		const std::string query{ "SELECT * FROM coaches WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpCoach> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Coach>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Coach, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& coaches,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(coaches));
		std::transform(std::cbegin(coaches), std::cend(coaches), std::back_inserter(pointers),
			[](const typename TContainer::value_type& coach)
			{
				return &coach;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpCoach, typename TContainer::value_type>{} ||
		std::is_same<const Coach*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& coaches,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Coach>: Inserting " << std::size(coaches) << " coaches." << endl;
		}
		if (std::empty(coaches) || 
			std::all_of(std::cbegin(coaches), std::cend(coaches), 
				[](const auto& pCoach) {return nullptr == pCoach;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 12;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(coaches) / batchSize;
			if(0 != std::size(coaches) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(coaches);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(coaches)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(coaches);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO coaches VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":sm_country_id" << i << ", ";
					ss << ":common_name" << i << ", ";
					ss << ":fullname" << i << ", ";
					ss << ":firstname" << i << ", ";
					ss << ":lastname" << i << ", ";
					ss << ":nationality" << i << ", ";
					ss << ":birthdate" << i << ", ";
					ss << ":birthcountry" << i << ", ";
					ss << ":birthplace" << i << ", ";
					ss << ":image_path" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":sm_country_id" + idx, (*it)->sm_country_id);
					insert.bind(":common_name" + idx, UtilsStringToStdString((*it)->common_name));
					insert.bind(":fullname" + idx, UtilsStringToStdString((*it)->fullname));
					BindOptional(insert, ":firstname" + idx, (*it)->firstname_opt);
					BindOptional(insert, ":lastname" + idx, (*it)->lastname_opt);
					insert.bind(":nationality" + idx, UtilsStringToStdString((*it)->nationality));
					BindOptional(insert, ":birthdate" + idx, (*it)->birthdate_opt);
					BindOptional(insert, ":birthcountry" + idx, (*it)->birthcountry_opt);
					BindOptional(insert, ":birthplace" + idx, (*it)->birthplace_opt);
					BindOptional(insert, ":image_path" + idx, (*it)->image_path_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(coaches) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "coaches");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0