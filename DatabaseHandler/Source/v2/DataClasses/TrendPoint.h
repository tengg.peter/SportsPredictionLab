#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class TrendPoint
	{
	public:
		int64_t id = 0;
		int64_t sm_trend_id = 0;
		double minute = 0.0;
		double amount = 0.0;
		//##protect##"class members"
//##protect##"class members"

		explicit TrendPoint() = default;
		virtual ~TrendPoint() {}

		explicit TrendPoint(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_trend_id(stm.getColumn("sm_trend_id").getInt64())
			, minute(stm.getColumn("minute").getDouble())
			, amount(stm.getColumn("amount").getDouble())
			
		{}
	};

	using UpTrendPoint = std::unique_ptr<TrendPoint>;

	struct TrendPointCmp
	{
		bool operator()(const TrendPoint& a, const TrendPoint& b) const
		{
			return a.sm_trend_id != b.sm_trend_id ? a.sm_trend_id < b.sm_trend_id :
				a.minute < b.minute;
		}

		bool operator()(const UpTrendPoint& a, const UpTrendPoint& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<TrendPoint, T>{} || std::is_same<UpTrendPoint, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM trend_points;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TrendPoint, T>{} || std::is_same<UpTrendPoint, T>{},
		int > ::type = 0 >
	inline std::vector<UpTrendPoint> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM trend_points;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTrendPoint> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TrendPoint>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TrendPoint, T>{} || std::is_same<UpTrendPoint, T>{},
		int > ::type = 0 >
	inline std::vector<UpTrendPoint> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<TrendPoint>(db);
		}

		const std::string query{ "SELECT * FROM trend_points WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTrendPoint> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TrendPoint>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<TrendPoint, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& trendPoints,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(trendPoints));
		std::transform(std::cbegin(trendPoints), std::cend(trendPoints), std::back_inserter(pointers),
			[](const typename TContainer::value_type& trendPoint)
			{
				return &trendPoint;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTrendPoint, typename TContainer::value_type>{} ||
		std::is_same<const TrendPoint*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& trendPoints,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<TrendPoint>: Inserting " << std::size(trendPoints) << " trendPoints." << endl;
		}
		if (std::empty(trendPoints) || 
			std::all_of(std::cbegin(trendPoints), std::cend(trendPoints), 
				[](const auto& pTrendPoint) {return nullptr == pTrendPoint;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 3;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(trendPoints) / batchSize;
			if(0 != std::size(trendPoints) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(trendPoints);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(trendPoints)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(trendPoints);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO trend_points VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_trend_id" << i << ", ";
					ss << ":minute" << i << ", ";
					ss << ":amount" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_trend_id" + idx, (*it)->sm_trend_id);
					insert.bind(":minute" + idx, (*it)->minute);
					insert.bind(":amount" + idx, (*it)->amount);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(trendPoints) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "trend_points");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0