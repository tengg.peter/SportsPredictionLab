#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Venue
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		utils::String name;
		std::optional<utils::String> surface_opt;
		std::optional<utils::String> address_opt;
		utils::String city;
		std::optional<int64_t> capacity_opt;
		std::optional<utils::String> image_path_opt;
		std::optional<utils::String> coordinates_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit Venue() = default;
		virtual ~Venue() {}

		explicit Venue(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, surface_opt(!stm.getColumn("surface").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("surface").getString())} : std::nullopt)
			, address_opt(!stm.getColumn("address").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("address").getString())} : std::nullopt)
			, city(StdStringToUtilsString(stm.getColumn("city").getString()))
			, capacity_opt(!stm.getColumn("capacity").isNull() ?
				std::optional{stm.getColumn("capacity").getInt64()} : std::nullopt)
			, image_path_opt(!stm.getColumn("image_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("image_path").getString())} : std::nullopt)
			, coordinates_opt(!stm.getColumn("coordinates").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("coordinates").getString())} : std::nullopt)
			
		{}
	};

	using UpVenue = std::unique_ptr<Venue>;

	struct VenueCmp
	{
		bool operator()(const Venue& a, const Venue& b) const
		{
			return a.sm_id != b.sm_id ? a.sm_id < b.sm_id :
				a.name < b.name;
		}

		bool operator()(const UpVenue& a, const UpVenue& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Venue, T>{} || std::is_same<UpVenue, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM venues;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Venue, T>{} || std::is_same<UpVenue, T>{},
		int > ::type = 0 >
	inline std::vector<UpVenue> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM venues;" };

		SQLite::Statement stm(db, query);
		std::vector<UpVenue> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Venue>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Venue, T>{} || std::is_same<UpVenue, T>{},
		int > ::type = 0 >
	inline std::vector<UpVenue> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Venue>(db);
		}

		const std::string query{ "SELECT * FROM venues WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpVenue> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Venue>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Venue, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& venues,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(venues));
		std::transform(std::cbegin(venues), std::cend(venues), std::back_inserter(pointers),
			[](const typename TContainer::value_type& venue)
			{
				return &venue;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpVenue, typename TContainer::value_type>{} ||
		std::is_same<const Venue*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& venues,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Venue>: Inserting " << std::size(venues) << " venues." << endl;
		}
		if (std::empty(venues) || 
			std::all_of(std::cbegin(venues), std::cend(venues), 
				[](const auto& pVenue) {return nullptr == pVenue;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 8;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(venues) / batchSize;
			if(0 != std::size(venues) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(venues);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(venues)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(venues);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO venues VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":surface" << i << ", ";
					ss << ":address" << i << ", ";
					ss << ":city" << i << ", ";
					ss << ":capacity" << i << ", ";
					ss << ":image_path" << i << ", ";
					ss << ":coordinates" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					BindOptional(insert, ":surface" + idx, (*it)->surface_opt);
					BindOptional(insert, ":address" + idx, (*it)->address_opt);
					insert.bind(":city" + idx, UtilsStringToStdString((*it)->city));
					BindOptional(insert, ":capacity" + idx, (*it)->capacity_opt);
					BindOptional(insert, ":image_path" + idx, (*it)->image_path_opt);
					BindOptional(insert, ":coordinates" + idx, (*it)->coordinates_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(venues) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "venues");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0