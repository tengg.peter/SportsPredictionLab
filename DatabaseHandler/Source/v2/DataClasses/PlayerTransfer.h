#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class PlayerTransfer
	{
	public:
		int64_t id = 0;
		int64_t sm_player_id = 0;
		int64_t sm_from_team_id = 0;
		int64_t sm_to_team_id = 0;
		std::optional<int64_t> sm_season_id_opt;
		utils::String transfer;
		utils::date_and_time::DateTime date;
		std::optional<utils::String> amount_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit PlayerTransfer() = default;
		virtual ~PlayerTransfer() {}

		explicit PlayerTransfer(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_player_id(stm.getColumn("sm_player_id").getInt64())
			, sm_from_team_id(stm.getColumn("sm_from_team_id").getInt64())
			, sm_to_team_id(stm.getColumn("sm_to_team_id").getInt64())
			, sm_season_id_opt(!stm.getColumn("sm_season_id").isNull() ?
				std::optional{stm.getColumn("sm_season_id").getInt64()} : std::nullopt)
			, transfer(StdStringToUtilsString(stm.getColumn("transfer").getString()))
			, date(utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date").getString()))
			, amount_opt(!stm.getColumn("amount").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("amount").getString())} : std::nullopt)
			
		{}
	};

	using UpPlayerTransfer = std::unique_ptr<PlayerTransfer>;

	struct PlayerTransferCmp
	{
		bool operator()(const PlayerTransfer& a, const PlayerTransfer& b) const
		{
			return a.sm_player_id != b.sm_player_id ? a.sm_player_id < b.sm_player_id :
				a.date < b.date;
		}

		bool operator()(const UpPlayerTransfer& a, const UpPlayerTransfer& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerTransfer, T>{} || std::is_same<UpPlayerTransfer, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM player_transfers;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerTransfer, T>{} || std::is_same<UpPlayerTransfer, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayerTransfer> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM player_transfers;" };

		SQLite::Statement stm(db, query);
		std::vector<UpPlayerTransfer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<PlayerTransfer>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<PlayerTransfer, T>{} || std::is_same<UpPlayerTransfer, T>{},
		int > ::type = 0 >
	inline std::vector<UpPlayerTransfer> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<PlayerTransfer>(db);
		}

		const std::string query{ "SELECT * FROM player_transfers WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpPlayerTransfer> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<PlayerTransfer>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<PlayerTransfer, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& playerTransfers,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(playerTransfers));
		std::transform(std::cbegin(playerTransfers), std::cend(playerTransfers), std::back_inserter(pointers),
			[](const typename TContainer::value_type& playerTransfer)
			{
				return &playerTransfer;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpPlayerTransfer, typename TContainer::value_type>{} ||
		std::is_same<const PlayerTransfer*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& playerTransfers,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<PlayerTransfer>: Inserting " << std::size(playerTransfers) << " playerTransfers." << endl;
		}
		if (std::empty(playerTransfers) || 
			std::all_of(std::cbegin(playerTransfers), std::cend(playerTransfers), 
				[](const auto& pPlayerTransfer) {return nullptr == pPlayerTransfer;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 7;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(playerTransfers) / batchSize;
			if(0 != std::size(playerTransfers) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(playerTransfers);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(playerTransfers)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(playerTransfers);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO player_transfers VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_player_id" << i << ", ";
					ss << ":sm_from_team_id" << i << ", ";
					ss << ":sm_to_team_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":transfer" << i << ", ";
					ss << ":date" << i << ", ";
					ss << ":amount" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_player_id" + idx, (*it)->sm_player_id);
					insert.bind(":sm_from_team_id" + idx, (*it)->sm_from_team_id);
					insert.bind(":sm_to_team_id" + idx, (*it)->sm_to_team_id);
					BindOptional(insert, ":sm_season_id" + idx, (*it)->sm_season_id_opt);
					insert.bind(":transfer" + idx, UtilsStringToStdString((*it)->transfer));
					insert.bind(":date" + idx, (*it)->date.ToSQLiteDateTimeFormat());
					BindOptional(insert, ":amount" + idx, (*it)->amount_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(playerTransfers) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "player_transfers");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0