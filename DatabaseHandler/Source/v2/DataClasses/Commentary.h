#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Commentary
	{
	public:
		int64_t id = 0;
		int64_t sm_fixture_id = 0;
		bool important = false;
		int64_t order_ = 0;
		bool goal = false;
		int64_t minute = 0;
		std::optional<int64_t> extra_minute_opt;
		utils::String comment;
		//##protect##"class members"
//##protect##"class members"

		explicit Commentary() = default;
		virtual ~Commentary() {}

		explicit Commentary(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, important(static_cast<bool>(stm.getColumn("important").getInt()))
			, order_(stm.getColumn("order_").getInt64())
			, goal(static_cast<bool>(stm.getColumn("goal").getInt()))
			, minute(stm.getColumn("minute").getInt64())
			, extra_minute_opt(!stm.getColumn("extra_minute").isNull() ?
				std::optional{stm.getColumn("extra_minute").getInt64()} : std::nullopt)
			, comment(StdStringToUtilsString(stm.getColumn("comment").getString()))
			
		{}
	};

	using UpCommentary = std::unique_ptr<Commentary>;

	struct CommentaryCmp
	{
		bool operator()(const Commentary& a, const Commentary& b) const
		{
			return a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.order_ != b.order_ ? a.order_ < b.order_ :
				a.minute != b.minute ? a.minute < b.minute :
				a.comment < b.comment;
		}

		bool operator()(const UpCommentary& a, const UpCommentary& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Commentary, T>{} || std::is_same<UpCommentary, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM commentaries;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Commentary, T>{} || std::is_same<UpCommentary, T>{},
		int > ::type = 0 >
	inline std::vector<UpCommentary> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM commentaries;" };

		SQLite::Statement stm(db, query);
		std::vector<UpCommentary> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Commentary>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Commentary, T>{} || std::is_same<UpCommentary, T>{},
		int > ::type = 0 >
	inline std::vector<UpCommentary> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Commentary>(db);
		}

		const std::string query{ "SELECT * FROM commentaries WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpCommentary> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Commentary>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Commentary, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& commentaries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(commentaries));
		std::transform(std::cbegin(commentaries), std::cend(commentaries), std::back_inserter(pointers),
			[](const typename TContainer::value_type& commentary)
			{
				return &commentary;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpCommentary, typename TContainer::value_type>{} ||
		std::is_same<const Commentary*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& commentaries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Commentary>: Inserting " << std::size(commentaries) << " commentaries." << endl;
		}
		if (std::empty(commentaries) || 
			std::all_of(std::cbegin(commentaries), std::cend(commentaries), 
				[](const auto& pCommentary) {return nullptr == pCommentary;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 7;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(commentaries) / batchSize;
			if(0 != std::size(commentaries) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(commentaries);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(commentaries)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(commentaries);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO commentaries VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":important" << i << ", ";
					ss << ":order_" << i << ", ";
					ss << ":goal" << i << ", ";
					ss << ":minute" << i << ", ";
					ss << ":extra_minute" << i << ", ";
					ss << ":comment" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					insert.bind(":important" + idx, (*it)->important);
					insert.bind(":order_" + idx, (*it)->order_);
					insert.bind(":goal" + idx, (*it)->goal);
					insert.bind(":minute" + idx, (*it)->minute);
					BindOptional(insert, ":extra_minute" + idx, (*it)->extra_minute_opt);
					insert.bind(":comment" + idx, UtilsStringToStdString((*it)->comment));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(commentaries) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "commentaries");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0