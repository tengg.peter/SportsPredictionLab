#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class TeamGoalLine
	{
	public:
		int64_t id = 0;
		int64_t team_season_statistics_id = 0;
		utils::String line_over;
		double home_percentage = 0.0;
		double away_percentage = 0.0;
		//##protect##"class members"
//##protect##"class members"

		explicit TeamGoalLine() = default;
		virtual ~TeamGoalLine() {}

		explicit TeamGoalLine(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, team_season_statistics_id(stm.getColumn("team_season_statistics_id").getInt64())
			, line_over(StdStringToUtilsString(stm.getColumn("line_over").getString()))
			, home_percentage(stm.getColumn("home_percentage").getDouble())
			, away_percentage(stm.getColumn("away_percentage").getDouble())
			
		{}
	};

	using UpTeamGoalLine = std::unique_ptr<TeamGoalLine>;

	struct TeamGoalLineCmp
	{
		bool operator()(const TeamGoalLine& a, const TeamGoalLine& b) const
		{
			return a.team_season_statistics_id != b.team_season_statistics_id ? a.team_season_statistics_id < b.team_season_statistics_id :
				a.line_over < b.line_over;
		}

		bool operator()(const UpTeamGoalLine& a, const UpTeamGoalLine& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalLine, T>{} || std::is_same<UpTeamGoalLine, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM team_goal_lines;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalLine, T>{} || std::is_same<UpTeamGoalLine, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamGoalLine> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM team_goal_lines;" };

		SQLite::Statement stm(db, query);
		std::vector<UpTeamGoalLine> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamGoalLine>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<TeamGoalLine, T>{} || std::is_same<UpTeamGoalLine, T>{},
		int > ::type = 0 >
	inline std::vector<UpTeamGoalLine> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<TeamGoalLine>(db);
		}

		const std::string query{ "SELECT * FROM team_goal_lines WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpTeamGoalLine> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<TeamGoalLine>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<TeamGoalLine, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamGoalLines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(teamGoalLines));
		std::transform(std::cbegin(teamGoalLines), std::cend(teamGoalLines), std::back_inserter(pointers),
			[](const typename TContainer::value_type& teamGoalLine)
			{
				return &teamGoalLine;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpTeamGoalLine, typename TContainer::value_type>{} ||
		std::is_same<const TeamGoalLine*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& teamGoalLines,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<TeamGoalLine>: Inserting " << std::size(teamGoalLines) << " teamGoalLines." << endl;
		}
		if (std::empty(teamGoalLines) || 
			std::all_of(std::cbegin(teamGoalLines), std::cend(teamGoalLines), 
				[](const auto& pTeamGoalLine) {return nullptr == pTeamGoalLine;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 4;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(teamGoalLines) / batchSize;
			if(0 != std::size(teamGoalLines) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(teamGoalLines);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(teamGoalLines)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(teamGoalLines);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO team_goal_lines VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":team_season_statistics_id" << i << ", ";
					ss << ":line_over" << i << ", ";
					ss << ":home_percentage" << i << ", ";
					ss << ":away_percentage" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":team_season_statistics_id" + idx, (*it)->team_season_statistics_id);
					insert.bind(":line_over" + idx, UtilsStringToStdString((*it)->line_over));
					insert.bind(":home_percentage" + idx, (*it)->home_percentage);
					insert.bind(":away_percentage" + idx, (*it)->away_percentage);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(teamGoalLines) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "team_goal_lines");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0