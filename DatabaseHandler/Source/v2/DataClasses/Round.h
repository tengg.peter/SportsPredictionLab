#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Round
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		int64_t name = 0;
		int64_t sm_league_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_stage_id = 0;
		utils::String start;
		utils::String end;
		//##protect##"class members"
		//##protect##"class members"

		explicit Round() = default;
		virtual ~Round() {}

		explicit Round(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, name(stm.getColumn("name").getInt64())
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_stage_id(stm.getColumn("sm_stage_id").getInt64())
			, start(StdStringToUtilsString(stm.getColumn("start").getString()))
			, end(StdStringToUtilsString(stm.getColumn("end").getString()))
			
		{}
	};

	using UpRound = std::unique_ptr<Round>;

	struct RoundCmp
	{
		bool operator()(const Round& a, const Round& b) const
		{
			return a.name != b.name ? a.name < b.name :
				a.sm_league_id != b.sm_league_id ? a.sm_league_id < b.sm_league_id :
				a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.sm_stage_id != b.sm_stage_id ? a.sm_stage_id < b.sm_stage_id :
				a.sm_id < b.sm_id;
		}

		bool operator()(const UpRound& a, const UpRound& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Round, T>{} || std::is_same<UpRound, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM rounds;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Round, T>{} || std::is_same<UpRound, T>{},
		int > ::type = 0 >
	inline std::vector<UpRound> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM rounds;" };

		SQLite::Statement stm(db, query);
		std::vector<UpRound> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Round>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Round, T>{} || std::is_same<UpRound, T>{},
		int > ::type = 0 >
	inline std::vector<UpRound> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Round>(db);
		}

		const std::string query{ "SELECT * FROM rounds WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpRound> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Round>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Round, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& rounds,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(rounds));
		std::transform(std::cbegin(rounds), std::cend(rounds), std::back_inserter(pointers),
			[](const typename TContainer::value_type& round)
			{
				return &round;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpRound, typename TContainer::value_type>{} ||
		std::is_same<const Round*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& rounds,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Round>: Inserting " << std::size(rounds) << " rounds." << endl;
		}
		if (std::empty(rounds) || 
			std::all_of(std::cbegin(rounds), std::cend(rounds), 
				[](const auto& pRound) {return nullptr == pRound;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 7;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(rounds) / batchSize;
			if(0 != std::size(rounds) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(rounds);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(rounds)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(rounds);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO rounds VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_stage_id" << i << ", ";
					ss << ":start" << i << ", ";
					ss << ":end" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":name" + idx, (*it)->name);
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_stage_id" + idx, (*it)->sm_stage_id);
					insert.bind(":start" + idx, UtilsStringToStdString((*it)->start));
					insert.bind(":end" + idx, UtilsStringToStdString((*it)->end));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(rounds) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "rounds");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0