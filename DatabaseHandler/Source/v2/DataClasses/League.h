#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class League
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		bool active = false;
		std::optional<utils::String> type_opt;
		std::optional<int64_t> sm_legacy_id_opt;
		int64_t sm_country_id = 0;
		std::optional<utils::String> logo_path_opt;
		utils::String name;
		bool is_cup = false;
		int64_t sm_current_season_id = 0;
		std::optional<int64_t> sm_current_round_id_opt;
		std::optional<int64_t> sm_current_stage_id_opt;
		bool live_standings = false;
		bool coverage_predictions = false;
		bool coverage_topscorer_goals = false;
		bool coverage_topscorer_assists = false;
		bool coverage_topscorer_cards = false;
		//##protect##"class members"
		//##protect##"class members"

		explicit League() = default;
		virtual ~League() {}

		explicit League(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, active(static_cast<bool>(stm.getColumn("active").getInt()))
			, type_opt(!stm.getColumn("type").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("type").getString())} : std::nullopt)
			, sm_legacy_id_opt(!stm.getColumn("sm_legacy_id").isNull() ?
				std::optional{stm.getColumn("sm_legacy_id").getInt64()} : std::nullopt)
			, sm_country_id(stm.getColumn("sm_country_id").getInt64())
			, logo_path_opt(!stm.getColumn("logo_path").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("logo_path").getString())} : std::nullopt)
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, is_cup(static_cast<bool>(stm.getColumn("is_cup").getInt()))
			, sm_current_season_id(stm.getColumn("sm_current_season_id").getInt64())
			, sm_current_round_id_opt(!stm.getColumn("sm_current_round_id").isNull() ?
				std::optional{stm.getColumn("sm_current_round_id").getInt64()} : std::nullopt)
			, sm_current_stage_id_opt(!stm.getColumn("sm_current_stage_id").isNull() ?
				std::optional{stm.getColumn("sm_current_stage_id").getInt64()} : std::nullopt)
			, live_standings(static_cast<bool>(stm.getColumn("live_standings").getInt()))
			, coverage_predictions(static_cast<bool>(stm.getColumn("coverage_predictions").getInt()))
			, coverage_topscorer_goals(static_cast<bool>(stm.getColumn("coverage_topscorer_goals").getInt()))
			, coverage_topscorer_assists(static_cast<bool>(stm.getColumn("coverage_topscorer_assists").getInt()))
			, coverage_topscorer_cards(static_cast<bool>(stm.getColumn("coverage_topscorer_cards").getInt()))
			
		{}
	};

	using UpLeague = std::unique_ptr<League>;

	struct LeagueCmp
	{
		bool operator()(const League& a, const League& b) const
		{
			return a.sm_country_id != b.sm_country_id ? a.sm_country_id < b.sm_country_id :
				a.name != b.name ? a.name < b.name :
				a.sm_id < b.sm_id;
		}

		bool operator()(const UpLeague& a, const UpLeague& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<League, T>{} || std::is_same<UpLeague, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM leagues;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<League, T>{} || std::is_same<UpLeague, T>{},
		int > ::type = 0 >
	inline std::vector<UpLeague> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM leagues;" };

		SQLite::Statement stm(db, query);
		std::vector<UpLeague> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<League>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<League, T>{} || std::is_same<UpLeague, T>{},
		int > ::type = 0 >
	inline std::vector<UpLeague> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<League>(db);
		}

		const std::string query{ "SELECT * FROM leagues WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpLeague> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<League>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<League, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& leagues,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(leagues));
		std::transform(std::cbegin(leagues), std::cend(leagues), std::back_inserter(pointers),
			[](const typename TContainer::value_type& league)
			{
				return &league;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpLeague, typename TContainer::value_type>{} ||
		std::is_same<const League*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& leagues,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<League>: Inserting " << std::size(leagues) << " leagues." << endl;
		}
		if (std::empty(leagues) || 
			std::all_of(std::cbegin(leagues), std::cend(leagues), 
				[](const auto& pLeague) {return nullptr == pLeague;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 16;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(leagues) / batchSize;
			if(0 != std::size(leagues) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(leagues);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(leagues)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(leagues);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO leagues VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":active" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":sm_legacy_id" << i << ", ";
					ss << ":sm_country_id" << i << ", ";
					ss << ":logo_path" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":is_cup" << i << ", ";
					ss << ":sm_current_season_id" << i << ", ";
					ss << ":sm_current_round_id" << i << ", ";
					ss << ":sm_current_stage_id" << i << ", ";
					ss << ":live_standings" << i << ", ";
					ss << ":coverage_predictions" << i << ", ";
					ss << ":coverage_topscorer_goals" << i << ", ";
					ss << ":coverage_topscorer_assists" << i << ", ";
					ss << ":coverage_topscorer_cards" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":active" + idx, (*it)->active);
					BindOptional(insert, ":type" + idx, (*it)->type_opt);
					BindOptional(insert, ":sm_legacy_id" + idx, (*it)->sm_legacy_id_opt);
					insert.bind(":sm_country_id" + idx, (*it)->sm_country_id);
					BindOptional(insert, ":logo_path" + idx, (*it)->logo_path_opt);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					insert.bind(":is_cup" + idx, (*it)->is_cup);
					insert.bind(":sm_current_season_id" + idx, (*it)->sm_current_season_id);
					BindOptional(insert, ":sm_current_round_id" + idx, (*it)->sm_current_round_id_opt);
					BindOptional(insert, ":sm_current_stage_id" + idx, (*it)->sm_current_stage_id_opt);
					insert.bind(":live_standings" + idx, (*it)->live_standings);
					insert.bind(":coverage_predictions" + idx, (*it)->coverage_predictions);
					insert.bind(":coverage_topscorer_goals" + idx, (*it)->coverage_topscorer_goals);
					insert.bind(":coverage_topscorer_assists" + idx, (*it)->coverage_topscorer_assists);
					insert.bind(":coverage_topscorer_cards" + idx, (*it)->coverage_topscorer_cards);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(leagues) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "leagues");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0