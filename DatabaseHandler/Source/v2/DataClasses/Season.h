#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Season
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		utils::String name;
		int64_t sm_league_id = 0;
		bool is_current_season = false;
		std::optional<int64_t> sm_current_round_id_opt;
		std::optional<int64_t> sm_current_stage_id_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit Season() = default;
		virtual ~Season() {}

		explicit Season(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, is_current_season(static_cast<bool>(stm.getColumn("is_current_season").getInt()))
			, sm_current_round_id_opt(!stm.getColumn("sm_current_round_id").isNull() ?
				std::optional{stm.getColumn("sm_current_round_id").getInt64()} : std::nullopt)
			, sm_current_stage_id_opt(!stm.getColumn("sm_current_stage_id").isNull() ?
				std::optional{stm.getColumn("sm_current_stage_id").getInt64()} : std::nullopt)
			
		{}
	};

	using UpSeason = std::unique_ptr<Season>;

	struct SeasonCmp
	{
		bool operator()(const Season& a, const Season& b) const
		{
			return a.sm_id < b.sm_id;
		}

		bool operator()(const UpSeason& a, const UpSeason& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Season, T>{} || std::is_same<UpSeason, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM seasons;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Season, T>{} || std::is_same<UpSeason, T>{},
		int > ::type = 0 >
	inline std::vector<UpSeason> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM seasons;" };

		SQLite::Statement stm(db, query);
		std::vector<UpSeason> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Season>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Season, T>{} || std::is_same<UpSeason, T>{},
		int > ::type = 0 >
	inline std::vector<UpSeason> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Season>(db);
		}

		const std::string query{ "SELECT * FROM seasons WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpSeason> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Season>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Season, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& seasons,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(seasons));
		std::transform(std::cbegin(seasons), std::cend(seasons), std::back_inserter(pointers),
			[](const typename TContainer::value_type& season)
			{
				return &season;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpSeason, typename TContainer::value_type>{} ||
		std::is_same<const Season*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& seasons,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Season>: Inserting " << std::size(seasons) << " seasons." << endl;
		}
		if (std::empty(seasons) || 
			std::all_of(std::cbegin(seasons), std::cend(seasons), 
				[](const auto& pSeason) {return nullptr == pSeason;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 6;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(seasons) / batchSize;
			if(0 != std::size(seasons) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(seasons);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(seasons)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(seasons);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO seasons VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":is_current_season" << i << ", ";
					ss << ":sm_current_round_id" << i << ", ";
					ss << ":sm_current_stage_id" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":is_current_season" + idx, (*it)->is_current_season);
					BindOptional(insert, ":sm_current_round_id" + idx, (*it)->sm_current_round_id_opt);
					BindOptional(insert, ":sm_current_stage_id" + idx, (*it)->sm_current_stage_id_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(seasons) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "seasons");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0//*********************************************************************
// Please find below the protected areas that the template-based script
// leading the generation hasn't recognized.
//*********************************************************************
//##protect##"InsertIfNotExists"
			//##protect##"InsertIfNotExists"
