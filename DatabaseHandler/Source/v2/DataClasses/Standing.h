#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/v2/DataClasses/StandingPosition.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Standing
	{
	public:
		int64_t id = 0;
		int64_t sm_id = 0;
		utils::String name;
		int64_t sm_league_id = 0;
		int64_t sm_season_id = 0;
		int64_t sm_round_id = 0;
		int64_t round_name = 0;
		utils::String type;
		int64_t sm_stage_id = 0;
		utils::String stage_name;
		utils::String resource;
		//##protect##"class members"
		std::set<StandingPosition, StandingPositionCmp> positions;
		//##protect##"class members"

		explicit Standing() = default;
		virtual ~Standing() {}

		explicit Standing(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_id(stm.getColumn("sm_id").getInt64())
			, name(StdStringToUtilsString(stm.getColumn("name").getString()))
			, sm_league_id(stm.getColumn("sm_league_id").getInt64())
			, sm_season_id(stm.getColumn("sm_season_id").getInt64())
			, sm_round_id(stm.getColumn("sm_round_id").getInt64())
			, round_name(stm.getColumn("round_name").getInt64())
			, type(StdStringToUtilsString(stm.getColumn("type").getString()))
			, sm_stage_id(stm.getColumn("sm_stage_id").getInt64())
			, stage_name(StdStringToUtilsString(stm.getColumn("stage_name").getString()))
			, resource(StdStringToUtilsString(stm.getColumn("resource").getString()))
			
		{}
	};

	using UpStanding = std::unique_ptr<Standing>;

	struct StandingCmp
	{
		bool operator()(const Standing& a, const Standing& b) const
		{
			return a.sm_league_id != b.sm_league_id ? a.sm_league_id < b.sm_league_id :
				a.sm_season_id != b.sm_season_id ? a.sm_season_id < b.sm_season_id :
				a.sm_round_id != b.sm_round_id ? a.sm_round_id < b.sm_round_id :
				a.sm_stage_id != b.sm_stage_id ? a.sm_stage_id < b.sm_stage_id :
				a.sm_id < b.sm_id;
		}

		bool operator()(const UpStanding& a, const UpStanding& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Standing, T>{} || std::is_same<UpStanding, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM standings;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Standing, T>{} || std::is_same<UpStanding, T>{},
		int > ::type = 0 >
	inline std::vector<UpStanding> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM standings;" };

		SQLite::Statement stm(db, query);
		std::vector<UpStanding> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Standing>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Standing, T>{} || std::is_same<UpStanding, T>{},
		int > ::type = 0 >
	inline std::vector<UpStanding> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Standing>(db);
		}

		const std::string query{ "SELECT * FROM standings WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpStanding> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Standing>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Standing, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& standings,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(standings));
		std::transform(std::cbegin(standings), std::cend(standings), std::back_inserter(pointers),
			[](const typename TContainer::value_type& standing)
			{
				return &standing;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpStanding, typename TContainer::value_type>{} ||
		std::is_same<const Standing*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& standings,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Standing>: Inserting " << std::size(standings) << " standings." << endl;
		}
		if (std::empty(standings) || 
			std::all_of(std::cbegin(standings), std::cend(standings), 
				[](const auto& pStanding) {return nullptr == pStanding;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 10;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(standings) / batchSize;
			if(0 != std::size(standings) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(standings);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(standings)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(standings);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO standings VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_id" << i << ", ";
					ss << ":name" << i << ", ";
					ss << ":sm_league_id" << i << ", ";
					ss << ":sm_season_id" << i << ", ";
					ss << ":sm_round_id" << i << ", ";
					ss << ":round_name" << i << ", ";
					ss << ":type" << i << ", ";
					ss << ":sm_stage_id" << i << ", ";
					ss << ":stage_name" << i << ", ";
					ss << ":resource" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_id" + idx, (*it)->sm_id);
					insert.bind(":name" + idx, UtilsStringToStdString((*it)->name));
					insert.bind(":sm_league_id" + idx, (*it)->sm_league_id);
					insert.bind(":sm_season_id" + idx, (*it)->sm_season_id);
					insert.bind(":sm_round_id" + idx, (*it)->sm_round_id);
					insert.bind(":round_name" + idx, (*it)->round_name);
					insert.bind(":type" + idx, UtilsStringToStdString((*it)->type));
					insert.bind(":sm_stage_id" + idx, (*it)->sm_stage_id);
					insert.bind(":stage_name" + idx, UtilsStringToStdString((*it)->stage_name));
					insert.bind(":resource" + idx, UtilsStringToStdString((*it)->resource));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
			for (auto it = batchStartIt; it != batchEndIt; it++)
			{
				if (nullptr != (*it))
				{
					InsertOr(onConflict, db, (*it)->positions);
				}
			}
			//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(standings) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "standings");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0//*********************************************************************
// Please find below the protected areas that the template-based script
// leading the generation hasn't recognized.
//*********************************************************************
//##protect##"InsertIfNotExists"
			//##protect##"InsertIfNotExists"
