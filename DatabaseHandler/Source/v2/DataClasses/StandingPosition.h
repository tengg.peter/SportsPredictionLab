#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
#include "DatabaseHandler/Source/Common/Constants.h"
#include "DatabaseHandler/Source/Common/Pragma.h"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class StandingPosition
	{
	public:
		int64_t id = 0;
		int64_t sm_standing_id = 0;
		int64_t position = 0;
		int64_t sm_team_id = 0;
		utils::String team_name;
		int64_t sm_round_id = 0;
		int64_t round_name = 0;
		std::optional<int64_t> sm_group_id_opt;
		std::optional<utils::String> group_name_opt;
		int64_t home_games_played = 0;
		int64_t home_won = 0;
		int64_t home_draw = 0;
		int64_t home_lost = 0;
		int64_t home_goals_scored = 0;
		int64_t home_goals_against = 0;
		int64_t away_games_played = 0;
		int64_t away_won = 0;
		int64_t away_draw = 0;
		int64_t away_lost = 0;
		int64_t away_goals_scored = 0;
		int64_t away_goals_against = 0;
		std::optional<utils::String> result_opt;
		std::optional<utils::String> recent_form_opt;
		std::optional<utils::String> status_opt;
		//##protect##"class members"
		//##protect##"class members"

		explicit StandingPosition() = default;
		virtual ~StandingPosition() {}

		explicit StandingPosition(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_standing_id(stm.getColumn("sm_standing_id").getInt64())
			, position(stm.getColumn("position").getInt64())
			, sm_team_id(stm.getColumn("sm_team_id").getInt64())
			, team_name(StdStringToUtilsString(stm.getColumn("team_name").getString()))
			, sm_round_id(stm.getColumn("sm_round_id").getInt64())
			, round_name(stm.getColumn("round_name").getInt64())
			, sm_group_id_opt(!stm.getColumn("sm_group_id").isNull() ?
				std::optional{stm.getColumn("sm_group_id").getInt64()} : std::nullopt)
			, group_name_opt(!stm.getColumn("group_name").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("group_name").getString())} : std::nullopt)
			, home_games_played(stm.getColumn("home_games_played").getInt64())
			, home_won(stm.getColumn("home_won").getInt64())
			, home_draw(stm.getColumn("home_draw").getInt64())
			, home_lost(stm.getColumn("home_lost").getInt64())
			, home_goals_scored(stm.getColumn("home_goals_scored").getInt64())
			, home_goals_against(stm.getColumn("home_goals_against").getInt64())
			, away_games_played(stm.getColumn("away_games_played").getInt64())
			, away_won(stm.getColumn("away_won").getInt64())
			, away_draw(stm.getColumn("away_draw").getInt64())
			, away_lost(stm.getColumn("away_lost").getInt64())
			, away_goals_scored(stm.getColumn("away_goals_scored").getInt64())
			, away_goals_against(stm.getColumn("away_goals_against").getInt64())
			, result_opt(!stm.getColumn("result").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("result").getString())} : std::nullopt)
			, recent_form_opt(!stm.getColumn("recent_form").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("recent_form").getString())} : std::nullopt)
			, status_opt(!stm.getColumn("status").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("status").getString())} : std::nullopt)
			
		{}
	};

	using UpStandingPosition = std::unique_ptr<StandingPosition>;

	struct StandingPositionCmp
	{
		bool operator()(const StandingPosition& a, const StandingPosition& b) const
		{
			return a.sm_standing_id != b.sm_standing_id ? a.sm_standing_id < b.sm_standing_id :
				a.position < b.position;
		}

		bool operator()(const UpStandingPosition& a, const UpStandingPosition& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<StandingPosition, T>{} || std::is_same<UpStandingPosition, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM standing_positions;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<StandingPosition, T>{} || std::is_same<UpStandingPosition, T>{},
		int > ::type = 0 >
	inline std::vector<UpStandingPosition> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM standing_positions;" };

		SQLite::Statement stm(db, query);
		std::vector<UpStandingPosition> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<StandingPosition>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<StandingPosition, T>{} || std::is_same<UpStandingPosition, T>{},
		int > ::type = 0 >
	inline std::vector<UpStandingPosition> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<StandingPosition>(db);
		}

		const std::string query{ "SELECT * FROM standing_positions WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpStandingPosition> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<StandingPosition>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<StandingPosition, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& standingPositions,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(standingPositions));
		std::transform(std::cbegin(standingPositions), std::cend(standingPositions), std::back_inserter(pointers),
			[](const typename TContainer::value_type& standingPosition)
			{
				return &standingPosition;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpStandingPosition, typename TContainer::value_type>{} ||
		std::is_same<const StandingPosition*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& standingPositions,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<StandingPosition>: Inserting " << std::size(standingPositions) << " standingPositions." << endl;
		}
		if (std::empty(standingPositions) || 
			std::all_of(std::cbegin(standingPositions), std::cend(standingPositions), 
				[](const auto& pStandingPosition) {return nullptr == pStandingPosition;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 23;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(standingPositions) / batchSize;
			if(0 != std::size(standingPositions) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(standingPositions);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(standingPositions)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(standingPositions);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO standing_positions VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_standing_id" << i << ", ";
					ss << ":position" << i << ", ";
					ss << ":sm_team_id" << i << ", ";
					ss << ":team_name" << i << ", ";
					ss << ":sm_round_id" << i << ", ";
					ss << ":round_name" << i << ", ";
					ss << ":sm_group_id" << i << ", ";
					ss << ":group_name" << i << ", ";
					ss << ":home_games_played" << i << ", ";
					ss << ":home_won" << i << ", ";
					ss << ":home_draw" << i << ", ";
					ss << ":home_lost" << i << ", ";
					ss << ":home_goals_scored" << i << ", ";
					ss << ":home_goals_against" << i << ", ";
					ss << ":away_games_played" << i << ", ";
					ss << ":away_won" << i << ", ";
					ss << ":away_draw" << i << ", ";
					ss << ":away_lost" << i << ", ";
					ss << ":away_goals_scored" << i << ", ";
					ss << ":away_goals_against" << i << ", ";
					ss << ":result" << i << ", ";
					ss << ":recent_form" << i << ", ";
					ss << ":status" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_standing_id" + idx, (*it)->sm_standing_id);
					insert.bind(":position" + idx, (*it)->position);
					insert.bind(":sm_team_id" + idx, (*it)->sm_team_id);
					insert.bind(":team_name" + idx, UtilsStringToStdString((*it)->team_name));
					insert.bind(":sm_round_id" + idx, (*it)->sm_round_id);
					insert.bind(":round_name" + idx, (*it)->round_name);
					BindOptional(insert, ":sm_group_id" + idx, (*it)->sm_group_id_opt);
					BindOptional(insert, ":group_name" + idx, (*it)->group_name_opt);
					insert.bind(":home_games_played" + idx, (*it)->home_games_played);
					insert.bind(":home_won" + idx, (*it)->home_won);
					insert.bind(":home_draw" + idx, (*it)->home_draw);
					insert.bind(":home_lost" + idx, (*it)->home_lost);
					insert.bind(":home_goals_scored" + idx, (*it)->home_goals_scored);
					insert.bind(":home_goals_against" + idx, (*it)->home_goals_against);
					insert.bind(":away_games_played" + idx, (*it)->away_games_played);
					insert.bind(":away_won" + idx, (*it)->away_won);
					insert.bind(":away_draw" + idx, (*it)->away_draw);
					insert.bind(":away_lost" + idx, (*it)->away_lost);
					insert.bind(":away_goals_scored" + idx, (*it)->away_goals_scored);
					insert.bind(":away_goals_against" + idx, (*it)->away_goals_against);
					BindOptional(insert, ":result" + idx, (*it)->result_opt);
					BindOptional(insert, ":recent_form" + idx, (*it)->recent_form_opt);
					BindOptional(insert, ":status" + idx, (*it)->status_opt);
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(standingPositions) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "standing_positions");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0