#include "DatabaseHandler/Source/v2/DataClasses/Bookmaker.h"
#include "DatabaseHandler/Source/v2/DataClasses/Coach.h"
#include "DatabaseHandler/Source/v2/DataClasses/Commentary.h"
#include "DatabaseHandler/Source/v2/DataClasses/Continent.h"
#include "DatabaseHandler/Source/v2/DataClasses/Country.h"
#include "DatabaseHandler/Source/v2/DataClasses/Fixture.h"
#include "DatabaseHandler/Source/v2/DataClasses/FixtureSideline.h"
#include "DatabaseHandler/Source/v2/DataClasses/GoalLine.h"
#include "DatabaseHandler/Source/v2/DataClasses/GoalsScoredMinute.h"
#include "DatabaseHandler/Source/v2/DataClasses/League.h"
#include "DatabaseHandler/Source/v2/DataClasses/Lineup.h"
#include "DatabaseHandler/Source/v2/DataClasses/Market.h"
#include "DatabaseHandler/Source/v2/DataClasses/MatchStatistics.h"
#include "DatabaseHandler/Source/v2/DataClasses/Odds.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerPerformance.h"
#include "DatabaseHandler/Source/v2/DataClasses/Player.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerSideline.h"
#include "DatabaseHandler/Source/v2/DataClasses/PlayerTransfer.h"
#include "DatabaseHandler/Source/v2/DataClasses/Round.h"
#include "DatabaseHandler/Source/v2/DataClasses/Season.h"
#include "DatabaseHandler/Source/v2/DataClasses/SeasonStatistics.h"
#include "DatabaseHandler/Source/v2/DataClasses/Stage.h"
#include "DatabaseHandler/Source/v2/DataClasses/StandingPosition.h"
#include "DatabaseHandler/Source/v2/DataClasses/Standing.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalLine.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamGoalMinute.h"
#include "DatabaseHandler/Source/v2/DataClasses/Team.h"
#include "DatabaseHandler/Source/v2/DataClasses/TeamSeasonStatistics.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopAssistScorer.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopCardScorer.h"
#include "DatabaseHandler/Source/v2/DataClasses/TopGoalScorer.h"
#include "DatabaseHandler/Source/v2/DataClasses/TrendPoint.h"
#include "DatabaseHandler/Source/v2/DataClasses/Trend.h"
#include "DatabaseHandler/Source/v2/DataClasses/Venue.h"
