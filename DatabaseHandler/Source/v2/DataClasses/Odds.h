#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;

	class Odds
	{
	public:
		int64_t id = 0;
		int64_t sm_fixture_id = 0;
		int64_t sm_market_id = 0;
		int64_t sm_bookmaker_id = 0;
		bool suspended = false;
		utils::String label;
		double value = 0.0;
		std::optional<double> probability_opt;
		std::optional<utils::String> dp3_opt;
		int64_t american = 0;
		std::optional<utils::String> fractional_opt;
		std::optional<bool> winning_opt;
		std::optional<utils::String> handicap_opt;
		std::optional<utils::String> total_opt;
		bool stop = false;
		std::optional<int64_t> bookmaker_event_id_opt;
		utils::String last_update;
		std::optional<int64_t> timezone_type_opt;
		utils::String timezone;
		//##protect##"class members"
		//##protect##"class members"

		explicit Odds() = default;
		virtual ~Odds() {}

		explicit Odds(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, sm_fixture_id(stm.getColumn("sm_fixture_id").getInt64())
			, sm_market_id(stm.getColumn("sm_market_id").getInt64())
			, sm_bookmaker_id(stm.getColumn("sm_bookmaker_id").getInt64())
			, suspended(static_cast<bool>(stm.getColumn("suspended").getInt()))
			, label(StdStringToUtilsString(stm.getColumn("label").getString()))
			, value(stm.getColumn("value").getDouble())
			, probability_opt(!stm.getColumn("probability").isNull() ?
				std::optional{stm.getColumn("probability").getDouble()} : std::nullopt)
			, dp3_opt(!stm.getColumn("dp3").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("dp3").getString())} : std::nullopt)
			, american(stm.getColumn("american").getInt64())
			, fractional_opt(!stm.getColumn("fractional").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("fractional").getString())} : std::nullopt)
			, winning_opt(!stm.getColumn("winning").isNull() ?
				std::optional{static_cast<bool>(stm.getColumn("winning").getInt())} : std::nullopt)
			, handicap_opt(!stm.getColumn("handicap").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("handicap").getString())} : std::nullopt)
			, total_opt(!stm.getColumn("total").isNull() ?
				std::optional{StdStringToUtilsString(stm.getColumn("total").getString())} : std::nullopt)
			, stop(static_cast<bool>(stm.getColumn("stop").getInt()))
			, bookmaker_event_id_opt(!stm.getColumn("bookmaker_event_id").isNull() ?
				std::optional{stm.getColumn("bookmaker_event_id").getInt64()} : std::nullopt)
			, last_update(StdStringToUtilsString(stm.getColumn("last_update").getString()))
			, timezone_type_opt(!stm.getColumn("timezone_type").isNull() ?
				std::optional{stm.getColumn("timezone_type").getInt64()} : std::nullopt)
			, timezone(StdStringToUtilsString(stm.getColumn("timezone").getString()))
			
		{}
	};

	using UpOdds = std::unique_ptr<Odds>;

	struct OddsCmp
	{
		bool operator()(const Odds& a, const Odds& b) const
		{
			return a.sm_fixture_id != b.sm_fixture_id ? a.sm_fixture_id < b.sm_fixture_id :
				a.sm_market_id != b.sm_market_id ? a.sm_market_id < b.sm_market_id :
				a.sm_bookmaker_id != b.sm_bookmaker_id ? a.sm_bookmaker_id < b.sm_bookmaker_id :
				a.last_update < b.last_update;
		}

		bool operator()(const UpOdds& a, const UpOdds& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
	//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<Odds, T>{} || std::is_same<UpOdds, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM odds;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Odds, T>{} || std::is_same<UpOdds, T>{},
		int > ::type = 0 >
	inline std::vector<UpOdds> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM odds;" };

		SQLite::Statement stm(db, query);
		std::vector<UpOdds> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Odds>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<Odds, T>{} || std::is_same<UpOdds, T>{},
		int > ::type = 0 >
	inline std::vector<UpOdds> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<Odds>(db);
		}

		const std::string query{ "SELECT * FROM odds WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpOdds> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<Odds>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<Odds, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& odds,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(odds));
		std::transform(std::cbegin(odds), std::cend(odds), std::back_inserter(pointers),
			[](const typename TContainer::value_type& odds)
			{
				return &odds;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpOdds, typename TContainer::value_type>{} ||
		std::is_same<const Odds*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& odds,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<Odds>: Inserting " << std::size(odds) << " odds." << endl;
		}
		if (std::empty(odds) || 
			std::all_of(std::cbegin(odds), std::cend(odds), 
				[](const auto& pOdds) {return nullptr == pOdds;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 18;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(odds) / batchSize;
			if(0 != std::size(odds) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(odds);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(odds)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(odds);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO odds VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":sm_fixture_id" << i << ", ";
					ss << ":sm_market_id" << i << ", ";
					ss << ":sm_bookmaker_id" << i << ", ";
					ss << ":suspended" << i << ", ";
					ss << ":label" << i << ", ";
					ss << ":value" << i << ", ";
					ss << ":probability" << i << ", ";
					ss << ":dp3" << i << ", ";
					ss << ":american" << i << ", ";
					ss << ":fractional" << i << ", ";
					ss << ":winning" << i << ", ";
					ss << ":handicap" << i << ", ";
					ss << ":total" << i << ", ";
					ss << ":stop" << i << ", ";
					ss << ":bookmaker_event_id" << i << ", ";
					ss << ":last_update" << i << ", ";
					ss << ":timezone_type" << i << ", ";
					ss << ":timezone" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":sm_fixture_id" + idx, (*it)->sm_fixture_id);
					insert.bind(":sm_market_id" + idx, (*it)->sm_market_id);
					insert.bind(":sm_bookmaker_id" + idx, (*it)->sm_bookmaker_id);
					insert.bind(":suspended" + idx, (*it)->suspended);
					insert.bind(":label" + idx, UtilsStringToStdString((*it)->label));
					insert.bind(":value" + idx, (*it)->value);
					BindOptional(insert, ":probability" + idx, (*it)->probability_opt);
					BindOptional(insert, ":dp3" + idx, (*it)->dp3_opt);
					insert.bind(":american" + idx, (*it)->american);
					BindOptional(insert, ":fractional" + idx, (*it)->fractional_opt);
					BindOptional(insert, ":winning" + idx, (*it)->winning_opt);
					BindOptional(insert, ":handicap" + idx, (*it)->handicap_opt);
					BindOptional(insert, ":total" + idx, (*it)->total_opt);
					insert.bind(":stop" + idx, (*it)->stop);
					BindOptional(insert, ":bookmaker_event_id" + idx, (*it)->bookmaker_event_id_opt);
					insert.bind(":last_update" + idx, UtilsStringToStdString((*it)->last_update));
					BindOptional(insert, ":timezone_type" + idx, (*it)->timezone_type_opt);
					insert.bind(":timezone" + idx, UtilsStringToStdString((*it)->timezone));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(odds) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "odds");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0