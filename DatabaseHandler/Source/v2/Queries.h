#include "DatabaseHandler/Source/v2/DataClasses/Standing.h"
#include "Utils/Utils.h"

#include <set>
#include <string>

namespace SQLite { class Database; }

namespace database_handler::v2
{
	std::set<int64_t> QueryNotImportedCoachIds(SQLite::Database& db);

	struct SeasonTeam
	{
		int64_t seasonId = 0;
		int64_t teamId = 0;
		bool operator < (const SeasonTeam& other) const
		{
			return seasonId != other.seasonId ? seasonId < other.seasonId :
				teamId < other.teamId;
				
		}
	};

	std::set<SeasonTeam> QuerySeasonsTeamsForPlayerPerformances(SQLite::Database& db, bool activeSeasons);

	std::set<int64_t> QueryFixtureIdsWithoutOdds(SQLite::Database& db, bool activeSeasons);

	struct LeagueInfo
	{
		int64_t league_id = 0;
		std::string country;
		std::string league;
		bool is_cup = false;
		int64_t current_season_id = 0;
		int64_t current_round_id = 0;
		int numTeams = 0;

		bool operator<(const LeagueInfo& other) const
		{
			return country != other.country ? country < other.country :
				league < other.league;
		}

		bool operator==(const LeagueInfo& other) const
		{
			return !(*this < other || other < *this);
		}

		bool operator!=(const LeagueInfo& other) const
		{
			return !(*this == other );
		}
	};

	struct FixtureInfo
	{
		std::string homeTeam;
		std::string awayTeam;
		utils::date_and_time::DateTime dateTime;
		int64_t fixtureId = 0;
		int64_t seasonId = 0;
		LeagueInfo leagueInfo;

		bool operator<(const FixtureInfo& other) const
		{
			return dateTime != other.dateTime ? dateTime < other.dateTime :
				homeTeam != other.homeTeam ? homeTeam < other.homeTeam :
				awayTeam < other.awayTeam;
		}

		bool operator==(const FixtureInfo& other) const
		{
			return !(*this < other || other < *this);
		}
	};

	struct DiscreteDistributionEntry
	{
		int value = 0;
		int occurances = 0;
		double frequency = 0.0;

		bool operator<(const DiscreteDistributionEntry& other) const
		{
			return value < other.value;
		}
	};

	struct DiscreteDistribution
	{
		FixtureInfo fixtureInfo;
		utils::String description;
		std::set<DiscreteDistributionEntry> entries;
	};

	std::vector<DiscreteDistribution> QueryCornerDistributionsForUpcomingMatches(SQLite::Database& db, int sampleSize);

	struct PreMatchStats
	{
		FixtureInfo fixtureInfo;
		std::string description;

		std::optional<double> homeTeamHomeForOpt;
		std::optional<double> homeTeamHomeAgainstOpt;
		std::optional<double> homeTeamAwayForOpt;
		std::optional<double> homeTeamAwayAgainstOpt;
		std::optional<double> awayTeamHomeForOpt;
		std::optional<double> awayTeamHomeAgainstOpt;
		std::optional<double> awayTeamAwayForOpt;
		std::optional<double> awayTeamAwayAgainstOpt;

		std::optional<double> homeTeamHomeTotalOpt;
		std::optional<double> homeTeamAwayTotalOpt;
		std::optional<double> awayTeamHomeTotalOpt;
		std::optional<double> awayTeamAwayTotalOpt;

		std::optional<double> homeTeamAnywhereForOpt;
		std::optional<double> homeTeamAnywhereAgainstOpt;
		std::optional<double> awayTeamAnywhereForOpt;
		std::optional<double> awayTeamAnywhereAgainstOpt;

		std::optional<double> homeTeamAnywhereTotalOpt;
		std::optional<double> awayTeamAnywhereTotalOpt;

		PreMatchStats operator/(const PreMatchStats& other) const;
	};

	std::vector<PreMatchStats> QueryWeightedPreMatchCornerStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchGoalStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchAttackStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchDangerousAttackStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchShotStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchShotsOnGoalStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchPossessionStats(SQLite::Database& db, int sampleSize);
	std::vector<PreMatchStats> QueryWeightedPreMatchCrossStats(SQLite::Database& db, int sampleSize);

	std::map<LeagueInfo, Standing> QueryCurrentStandings(SQLite::Database& db);

	struct MatchResult
	{
		FixtureInfo fixtureInfo;
		int64_t league_id = 0;
		int home_goals = 0;
		int away_goals = 0;

		bool operator<(const MatchResult& other) const
		{
			return fixtureInfo < other.fixtureInfo;
		}
	};

	std::set<MatchResult> QueryMatchResults(
		SQLite::Database& db, 
		const std::string& countryName,
		const std::string& leagueName,
		const std::string& beforeThisDate);

	struct TeamReplacements
	{
		std::vector<std::string> teamsOut;
		std::vector<std::string> teamsIn;
	};

	TeamReplacements QueryTeamReplacements(SQLite::Database& db, int64_t oldSeasonId, int64_t newSeasonId);

	std::vector<LeagueInfo> QueryLeaguesView(SQLite::Database& db);

	std::map<LeagueInfo, std::set<MatchResult>> QueryAllMatchResults(SQLite::Database& db);

	std::set<FixtureInfo> QueryUpcomingMatches(SQLite::Database& db);

	std::set<int64_t> QueryPotentiallyDeletedMatches(SQLite::Database& db);

	struct OddsMarket
	{
		double value = 0.0;
		bool winning = false;
		utils::String handicap;
	};

	using FixtureOddsMap = std::map<utils::String, /*market*/
		std::map<utils::String /*bookmaker*/,
		std::map<utils::String /*label*/,
		OddsMarket>>>;

	using OddsMap = std::map<int64_t /*fixture_id*/, FixtureOddsMap>;

	OddsMap	QueryOdds(SQLite::Database& db);
}