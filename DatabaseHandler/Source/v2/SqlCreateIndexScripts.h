#pragma once

#include <string>

namespace database_handler::v2
{
    const std::string CREATE_INDEX_BOOKMAKERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_bookmakers__sm_id "
        "ON bookmakers (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_COACHES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_coaches__sm_id "
        "ON coaches (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_COMMENTARIES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_commentaries__sm_fixture_id__order___minute__comment "
        "ON commentaries (sm_fixture_id, order_, minute, comment); "
        
	};
    
    const std::string CREATE_INDEX_CONTINENTS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_continents__sm_id "
        "ON continents (sm_id); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_continents__name "
        "ON continents (name); "
        
	};
    
    const std::string CREATE_INDEX_COUNTRIES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_countries__sm_id "
        "ON countries (sm_id); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_countries__name "
        "ON countries (name); "
        
	};
    
    const std::string CREATE_INDEX_FIXTURES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_fixtures__sm_id "
        "ON fixtures (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_FIXTURE_SIDELINES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_fixture_sidelines__sm_team_id__sm_fixture_id__sm_player_id "
        "ON fixture_sidelines (sm_team_id, sm_fixture_id, sm_player_id); "
        
	};
    
    const std::string CREATE_INDEX_GOAL_LINES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_goal_lines__sm_season_statistics_id__line_over "
        "ON goal_lines (sm_season_statistics_id, line_over); "
        
	};
    
    const std::string CREATE_INDEX_GOALS_SCORED_MINUTES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_goals_scored_minutes__sm_season_statistics_id__interval "
        "ON goals_scored_minutes (sm_season_statistics_id, interval); "
        
	};
    
    const std::string CREATE_INDEX_LEAGUES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_leagues__sm_country_id__name "
        "ON leagues (sm_country_id, name); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_leagues__sm_id "
        "ON leagues (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_LINEUPS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_lineups__sm_fixture_id__sm_team_id__sm_player_id "
        "ON lineups (sm_fixture_id, sm_team_id, sm_player_id); "
        
	};
    
    const std::string CREATE_INDEX_MARKETS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_markets__sm_id "
        "ON markets (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_MATCH_STATISTICS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_match_statistics__sm_fixture_id__sm_team_id "
        "ON match_statistics (sm_fixture_id, sm_team_id); "
        
	};
    
    const std::string CREATE_INDEX_ODDS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_odds__sm_fixture_id__sm_market_id__sm_bookmaker_id__last_update "
        "ON odds (sm_fixture_id, sm_market_id, sm_bookmaker_id, last_update); "
        
	};
    
    const std::string CREATE_INDEX_PLAYER_PERFORMANCES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_player_performances__sm_player_id__sm_season_id__sm_team_id "
        "ON player_performances (sm_player_id, sm_season_id, sm_team_id); "
        
	};
    
    const std::string CREATE_INDEX_PLAYERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_players__sm_id "
        "ON players (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_PLAYER_SIDELINES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_player_sidelines__sm_player_id__start_date "
        "ON player_sidelines (sm_player_id, start_date); "
        
	};
    
    const std::string CREATE_INDEX_PLAYER_TRANSFERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_player_transfers__sm_player_id__date "
        "ON player_transfers (sm_player_id, date); "
        
	};
    
    const std::string CREATE_INDEX_ROUNDS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_rounds__name__sm_league_id__sm_season_id__sm_stage_id "
        "ON rounds (name, sm_league_id, sm_season_id, sm_stage_id); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_rounds__sm_id "
        "ON rounds (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_SEASONS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_seasons__sm_id "
        "ON seasons (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_SEASON_STATISTICS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_season_statistics__sm_id "
        "ON season_statistics (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_STAGES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_stages__sm_season_id__name "
        "ON stages (sm_season_id, name); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_stages__sm_id "
        "ON stages (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_STANDING_POSITIONS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_standing_positions__sm_standing_id__position "
        "ON standing_positions (sm_standing_id, position); "
        
	};
    
    const std::string CREATE_INDEX_STANDINGS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_standings__sm_league_id__sm_season_id__sm_round_id__sm_stage_id "
        "ON standings (sm_league_id, sm_season_id, sm_round_id, sm_stage_id); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_standings__sm_id "
        "ON standings (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_TEAM_GOAL_LINES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_team_goal_lines__team_season_statistics_id__line_over "
        "ON team_goal_lines (team_season_statistics_id, line_over); "
        
	};
    
    const std::string CREATE_INDEX_TEAM_GOAL_MINUTES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_team_goal_minutes__team_season_statistics_id__period__type "
        "ON team_goal_minutes (team_season_statistics_id, period, type); "
        
	};
    
    const std::string CREATE_INDEX_TEAMS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_teams__sm_id "
        "ON teams (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_TEAM_SEASON_STATISTICS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_team_season_statistics__sm_team_id__sm_season_id__sm_stage_id "
        "ON team_season_statistics (sm_team_id, sm_season_id, sm_stage_id); "
        
	};
    
    const std::string CREATE_INDEX_TOP_ASSIST_SCORERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_top_assist_scorers__position__sm_season_id__type "
        "ON top_assist_scorers (position, sm_season_id, type); "
        
	};
    
    const std::string CREATE_INDEX_TOP_CARD_SCORERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_top_card_scorers__position__sm_season_id__type "
        "ON top_card_scorers (position, sm_season_id, type); "
        
	};
    
    const std::string CREATE_INDEX_TOP_GOAL_SCORERS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_top_goal_scorers__position__sm_season_id__type "
        "ON top_goal_scorers (position, sm_season_id, type); "
        
	};
    
    const std::string CREATE_INDEX_TREND_POINTS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_trend_points__sm_trend_id__minute "
        "ON trend_points (sm_trend_id, minute); "
        
	};
    
    const std::string CREATE_INDEX_TRENDS
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_trends__sm_fixture_id__sm_team_id__type "
        "ON trends (sm_fixture_id, sm_team_id, type); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_trends__sm_id "
        "ON trends (sm_id); "
        
	};
    
    const std::string CREATE_INDEX_VENUES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_venues__sm_id "
        "ON venues (sm_id); "
        " "
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_venues__name "
        "ON venues (name); "
        
	};
    
    
}