#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "DatabaseHandler/Source/Common/LoadScript.h"
#include "DatabaseHandler/Source/v2/Queries.h"
//3rd party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/Statement.h"

namespace database_handler::v2
{
	using utils::string_conversion::StdStringToUtilsString;
	using std::endl;

	std::set<int64_t> QueryNotImportedCoachIds(SQLite::Database& db)
	{
		const std::string query
		{
			"SELECT DISTINCT sm_localteam_coach_id FROM		 "
			"(												 "
			"	SELECT sm_localteam_coach_id FROM fixtures	 "
			"	WHERE sm_localteam_coach_id IS NOT NULL		 "
			"	UNION										 "
			"	SELECT sm_visitorteam_coach_id FROM fixtures "
			"	WHERE sm_visitorteam_coach_id IS NOT NULL	 "
			")												 "
			"WHERE sm_localteam_coach_id NOT IN				 "
			"(												 "
			"	SELECT sm_id FROM coaches					 "
			")												 "
		};
		SQLite::Statement stm{ db, query };
		std::set<int64_t> retVal;
		while (stm.executeStep())
		{
			retVal.insert(stm.getColumn(0).getInt64());
		}
		common::CheckThrow(stm);
		return retVal;
	}

	std::set<SeasonTeam> QuerySeasonsTeamsForPlayerPerformances(SQLite::Database& db, bool activeSeasons)
	{
		std::string query
		{
			"SELECT DISTINCT seasons_sm_id, teams_sm_id FROM											"
			"seasons_teams																				"
			"LEFT JOIN player_performances ON sm_season_id = seasons_sm_id AND sm_team_id = teams_sm_id "
			"JOIN seasons ON seasons.sm_id = seasons_sm_id												"
			"WHERE sm_player_id IS NULL AND is_current_season = 										"
		};
		query += activeSeasons ? "1;" : "0;";
		SQLite::Statement stm{ db, query };
		std::set<SeasonTeam> retVal;
		while (stm.executeStep())
		{
			retVal.insert({ stm.getColumn(0).getInt64(), stm.getColumn(1).getInt64() });
		}
		common::CheckThrow(stm);
		return retVal;
	}
	std::set<int64_t> QueryFixtureIdsWithoutOdds(SQLite::Database& db, bool activeSeasons)
	{
		std::string query
		{
			"SELECT sm_id FROM fixtures					"
			"WHERE sm_id NOT IN							"
			"(											"
			"	SELECT sm_fixture_id FROM odds			"
			")											"
			"AND status NOT IN('TBA', 'NS', 'POSTP')	"
			"AND sm_season_id IN						"
			"(											"
			"	SELECT sm_id FROM seasons				"
			"	WHERE is_current_season = 				"
		};
		query += activeSeasons ? "1);" : "0);";
		SQLite::Statement stm{ db, query };
		std::set<int64_t> ret;
		while (stm.executeStep())
		{
			ret.insert(stm.getColumn(0).getInt64());
		}
		common::CheckThrow(stm);
		return ret;
	}
	std::vector<DiscreteDistribution> QueryCornerDistributionsForUpcomingMatches(SQLite::Database& db, int sampleSize)
	{
		SQLite::Statement stm{ db, common::LoadScript(
					"../DatabaseHandler/Sql/Main_queries/corner_distribution_upcoming.sql",
					{ {"20", std::to_string(sampleSize)} }) };

		std::vector<DiscreteDistribution> ret;
		while (stm.executeStep())
		{
			utils::String desc = utils::string_conversion::StdStringToUtilsString(stm.getColumn("description").getString());
			if (ret.empty() || ret.back().description != desc)
			{
				ret.emplace_back();
				ret.back().description = desc;
				ret.back().fixtureInfo =
				{
					stm.getColumn("home_team").getString(),
					stm.getColumn("away_team").getString(),
					utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString())
				};
			}
			DiscreteDistributionEntry entry
			{
				stm.getColumn("corners").getInt(),	//value
				stm.getColumn("times").getInt(),	//occurances
				stm.getColumn("frequency").getDouble()	//frequency
			};
			ret.back().entries.insert(entry);
		}

		common::CheckThrow(stm);
		return ret;
	}

	PreMatchStats PreMatchStats::operator/(const PreMatchStats& other) const
	{
		PreMatchStats ret;

		if (fixtureInfo == other.fixtureInfo)
		{
			ret.fixtureInfo = fixtureInfo;
		}
		if (description == other.description)
		{
			ret.description = description;
		}

		//=========================================================================================
		if (this->homeTeamHomeForOpt.has_value() && other.homeTeamHomeForOpt.has_value())
		{
			ret.homeTeamHomeForOpt = this->homeTeamHomeForOpt.value() / other.homeTeamHomeForOpt.value();
		}
		if (this->homeTeamHomeAgainstOpt.has_value() && other.homeTeamHomeAgainstOpt.has_value())
		{
			ret.homeTeamHomeAgainstOpt = this->homeTeamHomeAgainstOpt.value() / other.homeTeamHomeAgainstOpt.value();
		}
		if (this->homeTeamAwayForOpt.has_value() && other.homeTeamAwayForOpt.has_value())
		{
			ret.homeTeamAwayForOpt = this->homeTeamAwayForOpt.value() / other.homeTeamAwayForOpt.value();
		}
		if (this->homeTeamAwayAgainstOpt.has_value() && other.homeTeamAwayAgainstOpt.has_value())
		{
			ret.homeTeamAwayAgainstOpt = this->homeTeamAwayAgainstOpt.value() / other.homeTeamAwayAgainstOpt.value();
		}
		if (this->awayTeamHomeForOpt.has_value() && other.awayTeamHomeForOpt.has_value())
		{
			ret.awayTeamHomeForOpt = this->awayTeamHomeForOpt.value() / other.awayTeamHomeForOpt.value();
		}
		if (this->awayTeamHomeAgainstOpt.has_value() && other.awayTeamHomeAgainstOpt.has_value())
		{
			ret.awayTeamHomeAgainstOpt = this->awayTeamHomeAgainstOpt.value() / other.awayTeamHomeAgainstOpt.value();
		}
		if (this->awayTeamAwayForOpt.has_value() && other.awayTeamAwayForOpt.has_value())
		{
			ret.awayTeamAwayForOpt = this->awayTeamAwayForOpt.value() / other.awayTeamAwayForOpt.value();
		}
		if (this->awayTeamAwayAgainstOpt.has_value() && other.awayTeamAwayAgainstOpt.has_value())
		{
			ret.awayTeamAwayAgainstOpt = this->awayTeamAwayAgainstOpt.value() / other.awayTeamAwayAgainstOpt.value();
		}

		//=========================================================================================
		if (this->homeTeamHomeTotalOpt.has_value() && other.homeTeamHomeTotalOpt.has_value())
		{
			ret.homeTeamHomeTotalOpt = this->homeTeamHomeTotalOpt.value() / other.homeTeamHomeTotalOpt.value();
		}
		if (this->homeTeamAwayTotalOpt.has_value() && other.homeTeamAwayTotalOpt.has_value())
		{
			ret.homeTeamAwayTotalOpt = this->homeTeamAwayTotalOpt.value() / other.homeTeamAwayTotalOpt.value();
		}
		if (this->awayTeamHomeTotalOpt.has_value() && other.awayTeamHomeTotalOpt.has_value())
		{
			ret.awayTeamHomeTotalOpt = this->awayTeamHomeTotalOpt.value() / other.awayTeamHomeTotalOpt.value();
		}
		if (this->awayTeamAwayTotalOpt.has_value() && other.awayTeamAwayTotalOpt.has_value())
		{
			ret.awayTeamAwayTotalOpt = this->awayTeamAwayTotalOpt.value() / other.awayTeamAwayTotalOpt.value();
		}

		//=========================================================================================
		if (this->homeTeamAnywhereForOpt.has_value() && other.homeTeamAnywhereForOpt.has_value())
		{
			ret.homeTeamAnywhereForOpt = this->homeTeamAnywhereForOpt.value() / other.homeTeamAnywhereForOpt.value();
		}
		if (this->homeTeamAnywhereAgainstOpt.has_value() && other.homeTeamAnywhereAgainstOpt.has_value())
		{
			ret.homeTeamAnywhereAgainstOpt = this->homeTeamAnywhereAgainstOpt.value() / other.homeTeamAnywhereAgainstOpt.value();
		}
		if (this->awayTeamAnywhereForOpt.has_value() && other.awayTeamAnywhereForOpt.has_value())
		{
			ret.awayTeamAnywhereForOpt = this->awayTeamAnywhereForOpt.value() / other.awayTeamAnywhereForOpt.value();
		}
		if (this->awayTeamAnywhereAgainstOpt.has_value() && other.awayTeamAnywhereAgainstOpt.has_value())
		{
			ret.awayTeamAnywhereAgainstOpt = this->awayTeamAnywhereAgainstOpt.value() / other.awayTeamAnywhereAgainstOpt.value();
		}

		//=========================================================================================
		if (this->homeTeamAnywhereTotalOpt.has_value() && other.homeTeamAnywhereTotalOpt.has_value())
		{
			ret.homeTeamAnywhereTotalOpt = this->homeTeamAnywhereTotalOpt.value() / other.homeTeamAnywhereTotalOpt.value();
		}
		if (this->awayTeamAnywhereTotalOpt.has_value() && other.awayTeamAnywhereTotalOpt.has_value())
		{
			ret.awayTeamAnywhereTotalOpt = this->awayTeamAnywhereTotalOpt.value() / other.awayTeamAnywhereTotalOpt.value();
		}

		return ret;
	}

	namespace
	{
		std::optional<double> GetDoubleOpt(const SQLite::Column& col)
		{
			return col.isNull() ? std::nullopt : std::make_optional(col.getDouble());
		}

		std::vector<PreMatchStats> QueryPreMatchStats(
			SQLite::Database& db,
			const std::string& scriptFile,
			const std::vector<std::string>& columnNames,
			int sampleSize,
			const std::string& description)
		{
			const std::string script = common::LoadScript(scriptFile, { {"20", std::to_string(sampleSize)} });

			SQLite::Statement stm{ db, script };
			std::vector<PreMatchStats> ret;
			while (stm.executeStep())
			{
				ret.emplace_back();
				ret.back().description = description;
				ret.back().fixtureInfo.homeTeam = stm.getColumn("home_team").getString();
				ret.back().fixtureInfo.awayTeam = stm.getColumn("away_team").getString();
				ret.back().fixtureInfo.dateTime = utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString());
				ret.back().fixtureInfo.leagueInfo.country = stm.getColumn("country_name").getString();
				ret.back().fixtureInfo.leagueInfo.league = stm.getColumn("league_name").getString();

				ret.back().homeTeamHomeForOpt = GetDoubleOpt(stm.getColumn(columnNames[0].c_str()));
				ret.back().homeTeamHomeAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[1].c_str()));
				ret.back().homeTeamAwayForOpt = GetDoubleOpt(stm.getColumn(columnNames[2].c_str()));
				ret.back().homeTeamAwayAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[3].c_str()));
				ret.back().awayTeamHomeForOpt = GetDoubleOpt(stm.getColumn(columnNames[4].c_str()));
				ret.back().awayTeamHomeAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[5].c_str()));
				ret.back().awayTeamAwayForOpt = GetDoubleOpt(stm.getColumn(columnNames[6].c_str()));
				ret.back().awayTeamAwayAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[7].c_str()));

				ret.back().homeTeamHomeTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[8].c_str()));
				ret.back().homeTeamAwayTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[9].c_str()));
				ret.back().awayTeamHomeTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[10].c_str()));
				ret.back().awayTeamAwayTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[11].c_str()));

				ret.back().homeTeamAnywhereForOpt = GetDoubleOpt(stm.getColumn(columnNames[12].c_str()));
				ret.back().homeTeamAnywhereAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[13].c_str()));
				ret.back().awayTeamAnywhereForOpt = GetDoubleOpt(stm.getColumn(columnNames[14].c_str()));
				ret.back().awayTeamAnywhereAgainstOpt = GetDoubleOpt(stm.getColumn(columnNames[15].c_str()));

				ret.back().homeTeamAnywhereTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[16].c_str()));
				ret.back().awayTeamAnywhereTotalOpt = GetDoubleOpt(stm.getColumn(columnNames[17].c_str()));
			}
			common::CheckThrow(stm);
			return ret;
		}
	}

	std::vector<PreMatchStats> QueryWeightedPreMatchCornerStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_corners_for",
			"wavg_home_team_home_corners_against",
			"wavg_home_team_away_corners_for",
			"wavg_home_team_away_corners_against",
			"wavg_away_team_home_corners_for",
			"wavg_away_team_home_corners_against",
			"wavg_away_team_away_corners_for",
			"wavg_away_team_away_corners_against",

			"wavg_home_team_home_corners_total",
			"wavg_home_team_away_corners_total",
			"wavg_away_team_home_corners_total",
			"wavg_away_team_away_corners_total",

			"wavg_home_team_corners_for",
			"wavg_home_team_corners_against",
			"wavg_away_team_corners_for",
			"wavg_away_team_corners_against",

			"wavg_home_team_corners_total",
			"wavg_away_team_corners_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/corner_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted corner stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchGoalStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_goals_for",
			"wavg_home_team_home_goals_against",
			"wavg_home_team_away_goals_for",
			"wavg_home_team_away_goals_against",
			"wavg_away_team_home_goals_for",
			"wavg_away_team_home_goals_against",
			"wavg_away_team_away_goals_for",
			"wavg_away_team_away_goals_against",

			"wavg_home_team_home_goals_total",
			"wavg_home_team_away_goals_total",
			"wavg_away_team_home_goals_total",
			"wavg_away_team_away_goals_total",

			"wavg_home_team_goals_for",
			"wavg_home_team_goals_against",
			"wavg_away_team_goals_for",
			"wavg_away_team_goals_against",

			"wavg_home_team_goals_total",
			"wavg_away_team_goals_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/goal_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted goal stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchAttackStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_attacks_for",
			"wavg_home_team_home_attacks_against",
			"wavg_home_team_away_attacks_for",
			"wavg_home_team_away_attacks_against",
			"wavg_away_team_home_attacks_for",
			"wavg_away_team_home_attacks_against",
			"wavg_away_team_away_attacks_for",
			"wavg_away_team_away_attacks_against",

			"wavg_home_team_home_attacks_total",
			"wavg_home_team_away_attacks_total",
			"wavg_away_team_home_attacks_total",
			"wavg_away_team_away_attacks_total",

			"wavg_home_team_attacks_for",
			"wavg_home_team_attacks_against",
			"wavg_away_team_attacks_for",
			"wavg_away_team_attacks_against",

			"wavg_home_team_attacks_total",
			"wavg_away_team_attacks_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/attack_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted attack stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchDangerousAttackStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_dangerous_attacks_for",
			"wavg_home_team_home_dangerous_attacks_against",
			"wavg_home_team_away_dangerous_attacks_for",
			"wavg_home_team_away_dangerous_attacks_against",
			"wavg_away_team_home_dangerous_attacks_for",
			"wavg_away_team_home_dangerous_attacks_against",
			"wavg_away_team_away_dangerous_attacks_for",
			"wavg_away_team_away_dangerous_attacks_against",

			"wavg_home_team_home_dangerous_attacks_total",
			"wavg_home_team_away_dangerous_attacks_total",
			"wavg_away_team_home_dangerous_attacks_total",
			"wavg_away_team_away_dangerous_attacks_total",

			"wavg_home_team_dangerous_attacks_for",
			"wavg_home_team_dangerous_attacks_against",
			"wavg_away_team_dangerous_attacks_for",
			"wavg_away_team_dangerous_attacks_against",

			"wavg_home_team_dangerous_attacks_total",
			"wavg_away_team_dangerous_attacks_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/dangerous_attack_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted dangerous attack stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchShotStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_shots_for",
			"wavg_home_team_home_shots_against",
			"wavg_home_team_away_shots_for",
			"wavg_home_team_away_shots_against",
			"wavg_away_team_home_shots_for",
			"wavg_away_team_home_shots_against",
			"wavg_away_team_away_shots_for",
			"wavg_away_team_away_shots_against",

			"wavg_home_team_home_shots_total",
			"wavg_home_team_away_shots_total",
			"wavg_away_team_home_shots_total",
			"wavg_away_team_away_shots_total",

			"wavg_home_team_shots_for",
			"wavg_home_team_shots_against",
			"wavg_away_team_shots_for",
			"wavg_away_team_shots_against",

			"wavg_home_team_shots_total",
			"wavg_away_team_shots_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/shot_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted shot stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchShotsOnGoalStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_shots_ongoal_for",
			"wavg_home_team_home_shots_ongoal_against",
			"wavg_home_team_away_shots_ongoal_for",
			"wavg_home_team_away_shots_ongoal_against",
			"wavg_away_team_home_shots_ongoal_for",
			"wavg_away_team_home_shots_ongoal_against",
			"wavg_away_team_away_shots_ongoal_for",
			"wavg_away_team_away_shots_ongoal_against",

			"wavg_home_team_home_shots_ongoal_total",
			"wavg_home_team_away_shots_ongoal_total",
			"wavg_away_team_home_shots_ongoal_total",
			"wavg_away_team_away_shots_ongoal_total",

			"wavg_home_team_shots_ongoal_for",
			"wavg_home_team_shots_ongoal_against",
			"wavg_away_team_shots_ongoal_for",
			"wavg_away_team_shots_ongoal_against",

			"wavg_home_team_shots_ongoal_total",
			"wavg_away_team_shots_ongoal_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/shots_on_goal_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted shots on goal stats");
	}
	std::vector<PreMatchStats> QueryWeightedPreMatchPossessionStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_possession_for",
			"wavg_home_team_home_possession_against",
			"wavg_home_team_away_possession_for",
			"wavg_home_team_away_possession_against",
			"wavg_away_team_home_possession_for",
			"wavg_away_team_home_possession_against",
			"wavg_away_team_away_possession_for",
			"wavg_away_team_away_possession_against",

			"wavg_home_team_home_possession_total",
			"wavg_home_team_away_possession_total",
			"wavg_away_team_home_possession_total",
			"wavg_away_team_away_possession_total",

			"wavg_home_team_possession_for",
			"wavg_home_team_possession_against",
			"wavg_away_team_possession_for",
			"wavg_away_team_possession_against",

			"wavg_home_team_possession_total",
			"wavg_away_team_possession_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/possession_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted possession stats");
	}

	std::vector<PreMatchStats> QueryWeightedPreMatchCrossStats(SQLite::Database& db, int sampleSize)
	{
		std::vector<std::string> columnNames
		{
			"wavg_home_team_home_crosses_for",
			"wavg_home_team_home_crosses_against",
			"wavg_home_team_away_crosses_for",
			"wavg_home_team_away_crosses_against",
			"wavg_away_team_home_crosses_for",
			"wavg_away_team_home_crosses_against",
			"wavg_away_team_away_crosses_for",
			"wavg_away_team_away_crosses_against",

			"wavg_home_team_home_crosses_total",
			"wavg_home_team_away_crosses_total",
			"wavg_away_team_home_crosses_total",
			"wavg_away_team_away_crosses_total",

			"wavg_home_team_crosses_for",
			"wavg_home_team_crosses_against",
			"wavg_away_team_crosses_for",
			"wavg_away_team_crosses_against",

			"wavg_home_team_crosses_total",
			"wavg_away_team_crosses_total"
		};

		return QueryPreMatchStats(
			db,
			"../DatabaseHandler/Sql/Main_queries/cross_statistics_for_upcoming_matches.sql",
			columnNames,
			sampleSize,
			"weighted crosses stats");
	}

	std::map<LeagueInfo, Standing> QueryCurrentStandings(SQLite::Database& db)
	{
		const std::string script = common::LoadScript("../DatabaseHandler/Sql/Main_queries/current_standings.sql", { });
		SQLite::Statement stm{ db, script };
		std::map<LeagueInfo, Standing> ret;

		while (stm.executeStep())
		{
			LeagueInfo leagueInfo;
			leagueInfo.country = stm.getColumn("country").getString();
			leagueInfo.league = stm.getColumn("league").getString();

			StandingPosition pos;
			pos.position = stm.getColumn("position").getInt();
			pos.team_name = StdStringToUtilsString(stm.getColumn("team_name").getString());
			pos.round_name = stm.getColumn("round_name").getInt();
			pos.home_games_played = stm.getColumn("home_games_played").getInt();
			pos.home_won = stm.getColumn("home_won").getInt();
			pos.home_draw = stm.getColumn("home_draw").getInt();
			pos.home_lost = stm.getColumn("home_lost").getInt();
			pos.home_goals_scored = stm.getColumn("home_goals_scored").getInt();
			pos.home_goals_against = stm.getColumn("home_goals_against").getInt();
			pos.away_games_played = stm.getColumn("away_games_played").getInt();
			pos.away_won = stm.getColumn("away_won").getInt();
			pos.away_draw = stm.getColumn("away_draw").getInt();
			pos.away_lost = stm.getColumn("away_lost").getInt();
			pos.away_goals_scored = stm.getColumn("away_goals_scored").getInt();
			pos.away_goals_against = stm.getColumn("away_goals_against").getInt();
			pos.recent_form_opt = StdStringToUtilsString(stm.getColumn("recent_form").getString());
			pos.result_opt = StdStringToUtilsString(stm.getColumn("result").getString());

			ret[leagueInfo].positions.insert(pos);
		}

		common::CheckThrow(stm);
		return ret;
	}

	std::set<MatchResult> QueryMatchResults(
		SQLite::Database& db,
		const std::string& countryName,
		const std::string& leagueName,
		const std::string& beforeThisDate)
	{
		const std::string script = common::LoadScript("../DatabaseHandler/Sql/Main_queries/match_results.sql",
			{
				{"'3000-01-01'", "'" + utils::date_and_time::DateTime::FromSQLiteDateFormat(beforeThisDate).ToSQLiteDateTimeFormat() + "'"},
				{"'England'", "'" + countryName + "'"},
				{"'Premier League'", "'" + leagueName + "'"}
			});

		SQLite::Statement stm{ db, script };
		std::set<MatchResult> ret;
		while (stm.executeStep())
		{
			MatchResult result;
			result.fixtureInfo.leagueInfo.country = stm.getColumn("country_name").getString();
			result.fixtureInfo.leagueInfo.league = stm.getColumn("league_name").getString();
			result.fixtureInfo.dateTime = utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString());
			result.fixtureInfo.homeTeam = stm.getColumn("home_team").getString();
			result.fixtureInfo.awayTeam = stm.getColumn("away_team").getString();
			result.fixtureInfo.fixtureId = stm.getColumn("fixture_id").getInt64();
			result.fixtureInfo.seasonId = stm.getColumn("sm_season_id").getInt64();
			result.league_id = stm.getColumn("sm_league_id").getInt64();
			result.home_goals = stm.getColumn("home_goals").getInt();
			result.away_goals = stm.getColumn("away_goals").getInt();
			ret.insert(std::move(result));
		}
		common::CheckThrow(stm);
		return ret;
	}
	TeamReplacements QueryTeamReplacements(SQLite::Database& db, int64_t oldSeasonId, int64_t newSeasonId)
	{
		const std::string script = common::LoadScript("../DatabaseHandler/Sql/Main_queries/replacing_teams.sql",
			{
				{"oldSeasonId", std::to_string(oldSeasonId)},
				{"newSeasonId", std::to_string(newSeasonId)}
			});

		SQLite::Statement stm{ db, script };
		TeamReplacements ret;
		while (stm.executeStep())
		{
			const std::string direction{ stm.getColumn("direction").getString() };
			const std::string teamName{ stm.getColumn("name").getString() };
			if ("out" == direction)
			{
				ret.teamsOut.emplace_back(teamName);
			}
			else if ("in" == direction)
			{
				ret.teamsIn.emplace_back(teamName);
			}
		}
		common::CheckThrow(stm);
		return ret;
	}

	std::vector<LeagueInfo> QueryLeaguesView(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM v_leagues" };
		SQLite::Statement stm{ db, query };
		std::vector<LeagueInfo> ret;

		while (stm.executeStep())
		{
			ret.emplace_back();
			ret.back().league_id = stm.getColumn("league_id").getInt64();
			ret.back().country = stm.getColumn("country_name").getString();
			ret.back().league = stm.getColumn("league_name").getString();
			ret.back().is_cup = static_cast<bool>(stm.getColumn("is_cup").getInt());
			ret.back().current_season_id = stm.getColumn("current_season_id").getInt64();
			ret.back().current_round_id = stm.getColumn("sm_current_round_id").getInt64();
			ret.back().numTeams = stm.getColumn("num_teams").getInt();
		}

		common::CheckThrow(stm);
		return ret;
	}

	std::map<LeagueInfo, std::set<MatchResult>> QueryAllMatchResults(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM v_all_match_results" };

		SQLite::Statement stm{ db, query };
		std::map<LeagueInfo, std::set<MatchResult>> ret;
		while (stm.executeStep())
		{
			MatchResult result;
			result.fixtureInfo.leagueInfo.country = stm.getColumn("country_name").getString();
			result.fixtureInfo.leagueInfo.league = stm.getColumn("league_name").getString();
			result.fixtureInfo.leagueInfo.numTeams = stm.getColumn("num_teams").getInt();
			result.fixtureInfo.dateTime = utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString());
			result.fixtureInfo.homeTeam = stm.getColumn("home_team").getString();
			result.fixtureInfo.awayTeam = stm.getColumn("away_team").getString();
			result.fixtureInfo.fixtureId = stm.getColumn("fixture_id").getInt64();
			result.fixtureInfo.seasonId = stm.getColumn("sm_season_id").getInt64();
			result.league_id = stm.getColumn("sm_league_id").getInt64();
			result.home_goals = stm.getColumn("home_goals").getInt();
			result.away_goals = stm.getColumn("away_goals").getInt();
			ret[result.fixtureInfo.leagueInfo].insert(std::move(result));
		}
		common::CheckThrow(stm);
		return ret;
	}

	std::set<FixtureInfo> QueryUpcomingMatches(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM v_upcoming_matches" };
		SQLite::Statement stm{ db, query };
		std::set<FixtureInfo> ret;

		while (stm.executeStep())
		{
			FixtureInfo fi;
			fi.dateTime = utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn("date_time").getString());
			fi.homeTeam = stm.getColumn("home_team").getString();
			fi.awayTeam = stm.getColumn("away_team").getString();
			fi.fixtureId = stm.getColumn("fixture_id").getInt64();
			fi.seasonId = stm.getColumn("sm_season_id").getInt64();
			fi.leagueInfo.country = stm.getColumn("country_name").getString();
			fi.leagueInfo.league = stm.getColumn("league_name").getString();
			fi.leagueInfo.league_id = stm.getColumn("sm_league_id").getInt64();
			ret.insert(std::move(fi));
		}

		common::CheckThrow(stm);
		return ret;
	}

	std::set<int64_t> QueryPotentiallyDeletedMatches(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM v_potentially_deleted_fixtures" };
		SQLite::Statement stm{ db, query };

		std::set<int64_t> ret;
		while (stm.executeStep())
		{
			ret.insert(stm.getColumn("sm_id").getInt64());
		}

		common::CheckThrow(stm);
		return ret;
	}

	OddsMap QueryOdds(SQLite::Database& db)
	{
		utils::date_and_time::StopWatch stopWatch;
		size_t numRecords = 0;
		int previousElapsed = 0;
		TCOUT << "QueryOdds() is starting" << endl;
		stopWatch.Start();

		const std::string query{ "SELECT * FROM v_odds" };
		SQLite::Statement stm{ db, query };
		OddsMap ret;

		while (stm.executeStep()/* && numRecords < 10000*/)
		{
			const int64_t fixtureId = stm.getColumn("sm_fixture_id").getInt64();
			const utils::String market = StdStringToUtilsString(stm.getColumn("market").getString());
			const utils::String bookmaker = StdStringToUtilsString(stm.getColumn("bookmaker").getString());
			const utils::String label = StdStringToUtilsString(stm.getColumn("label").getString());

			OddsMarket om;
			om.value = stm.getColumn("value").getDouble();
			om.winning = static_cast<bool>(stm.getColumn("winning").getInt());
			om.handicap = StdStringToUtilsString(stm.getColumn("handicap").getString());
			ret[fixtureId][market][bookmaker][label] = std::move(om);
			++numRecords;
			const size_t printEveryNRecords = 10000;
			if (0 == numRecords % printEveryNRecords)
			{
				int elapsed = stopWatch.Elapsed<std::chrono::seconds>();
				TCOUT << "Loaded " << numRecords << 
					" records in " << elapsed << 
					" seconds. " << elapsed - previousElapsed << 
					" since last " << printEveryNRecords << endl;
				previousElapsed = elapsed;
			}
		}
		TCOUT << "QueryOdds() finished after " << stopWatch.Elapsed<std::chrono::seconds>() << " seconds. Loaded " << numRecords << " records." << endl;

		common::CheckThrow(stm);
		return ret;
	}
}