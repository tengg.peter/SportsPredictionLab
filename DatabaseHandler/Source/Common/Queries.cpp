#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "DatabaseHandler/Source/Common/Queries.h"
//lib
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
#include "Utils/Utils.h"

#include <sstream>

namespace database_handler::common
{
	std::vector<std::vector<int64_t>> QuerySwitchTable(SQLite::Database& db, const std::string& tableName, const std::string& whereClause)
	{
		std::string query{ "SELECT * FROM " + tableName };
		if (!whereClause.empty())
		{
			query += " WHERE " + whereClause;
		}
		query += ";";

		SQLite::Statement stm(db, query);
		std::vector<std::vector<int64_t>> retVec;
		while (stm.executeStep())
		{
			std::vector<int64_t> row;
			for (size_t i = 0; i < stm.getColumnCount(); i++)
			{
				row.push_back(stm.getColumn(i));
			}
			retVec.emplace_back(std::move(row));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	std::vector<int64_t> QuerySingleColumnFromTable(
		SQLite::Database& db,
		const std::string& tableName,
		const std::string& columnName,
		const std::string& whereClause,
		int64_t firstRowId,
		size_t numRows)
	{
		std::vector<int64_t> ret;
		std::stringstream query;
		constexpr size_t MAX_SIZE = std::numeric_limits<size_t>::max();
		bool overflow = firstRowId > 0 && numRows > MAX_SIZE - firstRowId;
		const size_t endRowId = overflow ? MAX_SIZE : firstRowId + numRows;

		query << "SELECT " << columnName << " FROM " << tableName;
		std::vector<std::string> whereExpr;
		if (0 != firstRowId || MAX_SIZE != endRowId)
		{
			whereExpr.push_back("id >= " + std::to_string(firstRowId));
			whereExpr.push_back("id < " + std::to_string(endRowId));
		}
		if (!whereClause.empty())
		{
			whereExpr.push_back(whereClause);
		}
		if (!whereExpr.empty())
		{
			query << " WHERE " << utils::string_operations::JoinStrings(whereExpr, std::string{ " AND " });
		}
		query << ";";
		SQLite::Statement stm(db, query.str());
		while (stm.executeStep())
		{
			ret.push_back(stm.getColumn(0));
		}

		ret.shrink_to_fit();
		return ret;
	}

	std::vector<int64_t> QueryGreatestRowId(
		SQLite::Database& db,
		const std::string& tableName,
		const std::vector<std::string>& columnNames)
	{
		std::vector<int64_t> ret;
		std::stringstream query;
		query << "SELECT ";
		for (size_t i = 0; i < columnNames.size(); i++)
		{
			if (0 < i)
			{
				query << ", ";
			}
			query << columnNames[i];
		}
		query << " FROM " << tableName << " ORDER BY id DESC LIMIT 1;";
		SQLite::Statement stm{ db, query.str() };
		if (stm.executeStep())
		{
			for (size_t i = 0; i < columnNames.size(); i++)
			{
				ret.push_back(stm.getColumn(i).getInt64());
			}
		}
		CheckThrow(stm);
		return ret;
	}

	utils::date_and_time::DateTime QueryExtremeDate(
		SQLite::Database& db,
		const std::string& tableName,
		const std::string& columnName,
		const std::string& whereClause,
		const std::string& ordering)
	{
		//SELECT id, date FROM fixtures
		//WHERE status = 'NS' OR status = 'TBA'
		//ORDER BY date ASC LIMIT 1
		std::stringstream query;
		query << "SELECT " << columnName << " FROM " << tableName;
		if (!whereClause.empty())
		{
			query << " WHERE " << whereClause;
		}
		query << " ORDER BY " << columnName << " " << ordering << " LIMIT 1;";
		SQLite::Statement stm{ db, query.str() };
		utils::date_and_time::DateTime date;
		if (stm.executeStep())
		{
			return utils::date_and_time::DateTime::FromSQLiteDateFormat(stm.getColumn(0).getString());
		}
		CheckThrow(stm);
	}

	std::set<int64_t> QueryWhereNotIn(
		SQLite::Database& db,
		const std::string& firstTableName,
		const std::string& firstColumnName,
		const std::string& secondTableName,
		const std::string& secondColumnName,
		const std::string& firstWhereClause)
	{
		//SELECT DISTINCT sm_id FROM fixtures
		//	WHERE sm_id NOT IN
		//	(SELECT DISTINCT sm_fixture_id FROM commentaries)
		//	AND commentaries = 1;

		std::stringstream query;
		query << "SELECT DISTINCT " << firstColumnName << " FROM " << firstTableName <<
			" WHERE " << firstColumnName << " NOT IN (SELECT DISTINCT " << secondColumnName <<
			" FROM " << secondTableName << ")";
		if (!firstWhereClause.empty())
		{
			query << " AND " << firstWhereClause;
		}
		query << ";";

		SQLite::Statement stm{ db, query.str() };
		std::set<int64_t> retVal;
		while (stm.executeStep())
		{
			retVal.insert(stm.getColumn(0).getInt64());
		}
		CheckThrow(stm);
		return retVal;
	}
}