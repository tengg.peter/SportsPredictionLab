#include "DatabaseHandler/Source/Common/Delete.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
//lib
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
//std
#include <sstream>

namespace database_handler::common
{
	void DeleteFromTable(
		SQLite::Database& db, 
		const std::string& tableName, 
		const std::string& columnName, 
		const std::set<int64_t>& ids)
	{
		if (ids.empty())
		{
			return;
		}

		std::string query{"DELETE FROM " + tableName + " WHERE " + columnName + " IN ( "};
		for (auto it = ids.cbegin(); ids.cend() != it; ++it)
		{
			if (ids.cbegin() != it)
			{
				query += ", ";
			}
			query += std::to_string(*it);
		}
		query += ");";

		SQLite::Statement stm(db, query);
		stm.exec();
		common::CheckThrow(stm);
	}
}