//own
#include "DatabaseHandler/Source/Common/GetAndCloseDb.h"
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "DatabaseHandler/Source/Common/TableInfo/TableInfo.h"
#include "Utils/Utils.h"
//std
#include <iostream>

namespace database_handler::debug
{
	using std::endl;
	using std::vector;
	using namespace common;

	static void PrintColumnValue(SQLite::Statement& statement, const ColumnInfo& columnInfo, int columnIndex)
	{
		if (SQLiteType::BLOB == columnInfo.type)
		{
			TCOUT << "BLOB: " << static_cast<int64_t>(statement.getColumn(columnIndex));
		}
		else if (SQLiteType::INTEGER == columnInfo.type)
		{
			TCOUT << static_cast<int64_t>(statement.getColumn(columnIndex));
		}
		else if (SQLiteType::NUMERIC == columnInfo.type)
		{
			TCOUT << "NUMERIC: " << static_cast<int64_t>(statement.getColumn(columnIndex));
		}
		else if (SQLiteType::REAL == columnInfo.type)
		{
			TCOUT << static_cast<double>(statement.getColumn(columnIndex));
		}
		else if (SQLiteType::TEXT == columnInfo.type)
		{
			TCOUT << utils::string_conversion::StdStringToUtilsString(statement.getColumn(columnIndex));
		}
	}
	static void PrintColumnValue(const SQLite::Column& column)
	{
		if (column.isBlob())
		{
			TCOUT << "BLOB: " << static_cast<int64_t>(column);
		}
		else if (column.isInteger())
		{
			TCOUT << static_cast<int64_t>(column);
		}
		else if (column.isNull())
		{
			TCOUT << "NULL";
		}
		else if (column.isFloat())
		{
			TCOUT << static_cast<double>(column);
		}
		else if (column.isText())
		{
			TCOUT << utils::string_conversion::StdStringToUtilsString(column);
		}
	}

	void PrintTable(SQLite::Database& db, std::string tableName)
	{
		TableInfo tableInfo = GetTableInfo(db, tableName);
		if (0 == tableInfo.columnInfos.size())
		{
			TCOUT << "\"" << utils::string_conversion::StdStringToUtilsString(tableName) << "\" table not found." << endl;
			return;
		}
		TCOUT << "===========================================================================================" << endl;
		TCOUT << "Table name: " << tableName.c_str() << endl;
		for (const ColumnInfo& columnInfo : tableInfo.columnInfos)
		{
			TCOUT << columnInfo.columnName.c_str() << "\t";
		}
		TCOUT << endl;

		SQLite::Statement query(db, "SELECT * FROM " + tableName);

		bool emptyTable = true;
		while (query.executeStep())
		{
			emptyTable = false;
			for (int i = 0; i < tableInfo.columnInfos.size(); i++)
			{
				PrintColumnValue(query, tableInfo.columnInfos[i], i);
				TCOUT << "\t";
			}
			TCOUT << endl;
		}
		if (emptyTable)
		{
			TCOUT << "<empty table>" << endl;
		}
		TCOUT << "===========================================================================================" << endl << endl;
	}
	void PrintQueryResult(std::string query)
	{
		SQLite::Statement q(database_handler::common::GetDb(DbType::V1), query);
		TCOUT << "===========================================================================================" << endl;
		TCOUT << query.c_str() << endl;

		bool emptyResult = true;
		while (q.executeStep())
		{
			emptyResult = false;
			for (int i = 0; i < q.getColumnCount(); i++)
			{
				PrintColumnValue(q.getColumn(i));
				TCOUT << "\t";
			}
			TCOUT << endl;
		}
		if (emptyResult)
		{
			TCOUT << "<empty result>" << endl;
		}
		TCOUT << "===========================================================================================" << endl << endl;
	}

	void PrintTables(SQLite::Database& db)
	{
		TCOUT << "===================== PrintTables =========================-" << endl;
		SQLite::Statement query(db, "SELECT * FROM sqlite_master;");
		while (query.executeStep())
		{
			if ("table" == query.getColumn("type").getString())
			{
				TCOUT << utils::string_conversion::StdStringToUtilsString(query.getColumn("name").getString()) << endl;
			}
		}

		TCOUT << "===================== PrintTables =========================-" << endl;
	}

	void PrintPragmaSettings(SQLite::Database& db)
	{
		TCOUT << "===================== PragmaSettings =========================-" << endl;
		TCOUT << "foreign_keys: " << db.execAndGet("PRAGMA foreign_keys").getInt() << endl;
		TCOUT << "===================== PragmaSettings =========================-" << endl;

	}
}