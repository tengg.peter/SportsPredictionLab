#pragma once
#include <string>

namespace SQLite { class Database; }

namespace database_handler::debug
{
	void PrintTable(SQLite::Database& db, std::string tableName);

	void PrintQueryResult(SQLite::Database& db, std::string query);

	void PrintTables(SQLite::Database& db);

	void PrintPragmaSettings(SQLite::Database& db);
}