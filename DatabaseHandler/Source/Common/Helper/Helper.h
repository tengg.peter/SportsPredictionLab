#pragma once
//own
//lib
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/sqlite3/sqlite3.h"
#include "Utils/Utils.h"
//std
#include <ctime>
#include <optional>
#include <variant>
#include <vector>

namespace database_handler::common
{

	inline bool IsSqliteError(int code)
	{
		return SQLITE_OK != code && SQLITE_ROW != code && SQLITE_DONE != code;
	}

	void CheckThrow(const SQLite::Statement& stm);

	//void BindVariant(
	//	SQLite::Statement& stm,
	//	const std::string& fieldName,
	//	const std::variant<std::monostate, int64_t, double, bool, std::string>& value);

	template<typename T>
	void BindOptional(
		SQLite::Statement& stm,
		const std::string& fieldName,
		const std::optional<T> optValue)
	{
		if (optValue.has_value())
		{
			if constexpr (std::is_same<utils::String, T>{})
			{
				stm.bind(fieldName, utils::string_conversion::UtilsStringToStdString(optValue.value()));
			}
			else if constexpr (std::is_same<utils::date_and_time::DateTime, T>{})
			{
				stm.bind(fieldName, optValue.value().ToSQLiteDateTimeFormat());
			}
			else
			{
				stm.bind(fieldName, optValue.value());
			}
			return;
		}
		stm.bind(fieldName);
	}
}