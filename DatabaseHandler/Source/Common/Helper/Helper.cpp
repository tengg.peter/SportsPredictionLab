//own
#include "DatabaseHandler/Source/Common/GetAndCloseDb.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"

namespace database_handler::common
{
	using std::vector;
	using utils::String;

	void CheckThrow(const SQLite::Statement& stm)
	{
		if (IsSqliteError(stm.getErrorCode()))
		{
			std::stringstream ss;
			ss << "SQLite error code: " << stm.getErrorCode() << '\n';
			ss << "SQLite extended error code: " << stm.getExtendedErrorCode() << '\n';
			ss << "SQLite error message: " << stm.getErrorMsg() << '\n';

			utils::Logger::LogError(ss.str(), true);
			throw std::runtime_error(ss.str());
		}
	}

	void BindVariant(
		SQLite::Statement& stm, 
		const std::string& fieldName, 
		const std::variant<std::monostate, int64_t, double, bool, std::string>& value)
	{
		if (const std::monostate* const pVal = std::get_if<std::monostate>(&value))
		{
			stm.bind(fieldName);	//field is not set. Bind NULL value
		}
		else if (const int64_t* const pVal = std::get_if<int64_t>(&value))
		{
			stm.bind(fieldName, *pVal);
		}
		else if (const double* const pVal = std::get_if<double>(&value))
		{
			stm.bind(fieldName, *pVal);
		}
		else if (const bool* const pVal = std::get_if<bool>(&value))
		{
			stm.bind(fieldName, *pVal);
		}
		else if (const std::string* const pVal = std::get_if<std::string>(&value))
		{
			stm.bind(fieldName, *pVal);
		}
	}
}