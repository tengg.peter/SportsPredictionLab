#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "DatabaseHandler/Source/Common/TableInfo/TableInfo.h"
//3rd party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
namespace database_handler::common
{
	ColumnInfo::ColumnInfo(
		int columnId, 
		const std::string& name, 
		SQLiteType type, 
		bool notNull, 
		const std::string& defaultValue,
		int primaryKeyIndex)
		:
		columnId(columnId),
		columnName(name),
		type(type),
		notNull(notNull),
		defaultValue(defaultValue),
		primaryKeyIndex(primaryKeyIndex)
	{
	}

	TableInfo GetTableInfo(SQLite::Database& db, const std::string tableName)
	{
		TableInfo tableInfo{tableName};

		SQLite::Statement query(db, "pragma table_info(" + tableName + ")");

		while (query.executeStep())
		{
			utils::String type = static_cast<utils::String>(utils::string_conversion::StdStringToUtilsString(query.getColumn(2)));

			tableInfo.columnInfos.emplace_back(
				static_cast<int>(query.getColumn(0)),
				query.getColumn(1).getString(),
				SQLiteType::FromUtilsString(type),
				static_cast<bool>(static_cast<int>(query.getColumn(3))),
				query.getColumn(4).getString(),
				static_cast<int>(query.getColumn(5))
			);
		}
		CheckThrow(query);

		return tableInfo;
	}
}