#include "SQLiteType.h"

namespace database_handler::common
{
	const SQLiteType SQLiteType::INTEGER(STR("INTEGER"));
	const SQLiteType SQLiteType::TEXT(STR("TEXT"));
	const SQLiteType SQLiteType::BLOB(STR("BLOB"));
	const SQLiteType SQLiteType::REAL(STR("REAL"));
	const SQLiteType SQLiteType::NUMERIC(STR("NUMERIC"));
	const SQLiteType SQLiteType::BOOLEAN(STR("BOOLEAN"));

	SQLiteType SQLiteType::FromUtilsString(const utils::String & s)
	{
		using utils::string_operations::EqualsIgnoreCase;
		if (EqualsIgnoreCase(INTEGER.ToUtilsString(), s))
		{
			return INTEGER;
		}
		if (EqualsIgnoreCase(TEXT.ToUtilsString(), s))
		{
			return TEXT;
		}
		if (EqualsIgnoreCase(BLOB.ToUtilsString(), s))
		{
			return BLOB;
		}
		if (EqualsIgnoreCase(REAL.ToUtilsString(), s))
		{
			return REAL;
		}
		if (EqualsIgnoreCase(NUMERIC.ToUtilsString(), s))
		{
			return NUMERIC;
		}
		if (EqualsIgnoreCase(BOOLEAN.ToUtilsString(), s))
		{
			return BOOLEAN;
		}
		return SQLiteType(STR("unknown column type"));
	}
}