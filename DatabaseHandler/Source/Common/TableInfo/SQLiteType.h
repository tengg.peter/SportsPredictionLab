#pragma once

#include "Utils/Utils.h"

namespace database_handler::common
{
	class SQLiteType : public utils::enums::StringEnum
	{
	public:
		static const SQLiteType INTEGER;
		static const SQLiteType TEXT;
		static const SQLiteType BLOB;
		static const SQLiteType REAL;
		static const SQLiteType NUMERIC;
		static const SQLiteType BOOLEAN;

		static SQLiteType FromUtilsString(const utils::String& ws);

	protected:
		explicit SQLiteType(const utils::String& enumValue) : StringEnum(enumValue) {}
	};
}