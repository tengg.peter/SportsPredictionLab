#pragma once
//own
#include "Utils/Utils.h"
//std
#include <string>

namespace database_handler::common
{
	class SqlTypeName : public utils::enums::StringEnum
	{
	public:
		static const SqlTypeName INT;
		static const SqlTypeName INTEGER;
		static const SqlTypeName TINYINT;
		static const SqlTypeName SMALLINT;
		static const SqlTypeName MEDIUMINT;
		static const SqlTypeName BIGINT;
		static const SqlTypeName UNSIGNED_BIG_INT;
		static const SqlTypeName INT2;
		static const SqlTypeName INT8;

		static const SqlTypeName CHARACTER_20;
		static const SqlTypeName VARCHAR_255;
		static const SqlTypeName VARYING_CHARACTER_255;
		static const SqlTypeName NCHAR_55;
		static const SqlTypeName NATIVE_CHARACTER_70;
		static const SqlTypeName NVARCHAR_100;
		static const SqlTypeName TEXT;
		static const SqlTypeName CLOB;

		static const SqlTypeName BLOB;

		static const SqlTypeName REAL;
		static const SqlTypeName DOUBLE;
		static const SqlTypeName DOUBLE_PRECISION;
		static const SqlTypeName FLOAT;

		static const SqlTypeName NUMERIC;
		static const SqlTypeName DECIMAL_10_5;
		static const SqlTypeName BOOLEAN;
		static const SqlTypeName DATE;
		static const SqlTypeName DATETIME;

	protected:
		explicit SqlTypeName(const utils::String enumValue) : StringEnum(enumValue) {	}
	};
}