#include "SqlTypeName.h"

namespace database_handler::common
{
	const SqlTypeName SqlTypeName::INT(STR("INT"));
	const SqlTypeName SqlTypeName::INTEGER(STR("INTEGER"));
	const SqlTypeName SqlTypeName::TINYINT(STR("TINYINT"));
	const SqlTypeName SqlTypeName::SMALLINT(STR("SMALLINT"));
	const SqlTypeName SqlTypeName::MEDIUMINT(STR("MEDIUMINT"));
	const SqlTypeName SqlTypeName::BIGINT(STR("BIGINT"));
	const SqlTypeName SqlTypeName::UNSIGNED_BIG_INT(STR("UNSIGNED BIG INT"));
	const SqlTypeName SqlTypeName::INT2(STR("INT2"));
	const SqlTypeName SqlTypeName::INT8(STR("INT8"));

	const SqlTypeName SqlTypeName::CHARACTER_20(STR("CHARACTER(20)"));
	const SqlTypeName SqlTypeName::VARCHAR_255(STR("VARCHAR(255)"));
	const SqlTypeName SqlTypeName::VARYING_CHARACTER_255(STR("VARYING CHARACTER(255)"));
	const SqlTypeName SqlTypeName::NCHAR_55(STR("NCHAR(55)"));
	const SqlTypeName SqlTypeName::NATIVE_CHARACTER_70(STR("NATIVE CHARACTER(70)"));
	const SqlTypeName SqlTypeName::NVARCHAR_100(STR("NVARCHAR(100)"));
	const SqlTypeName SqlTypeName::TEXT(STR("TEXT"));
	const SqlTypeName SqlTypeName::CLOB(STR("CLOB"));

	const SqlTypeName SqlTypeName::BLOB(STR("BLOB"));

	const SqlTypeName SqlTypeName::REAL(STR("REAL"));
	const SqlTypeName SqlTypeName::DOUBLE(STR("DOUBLE"));
	const SqlTypeName SqlTypeName::DOUBLE_PRECISION(STR("DOUBLE PRECISION"));
	const SqlTypeName SqlTypeName::FLOAT(STR("FLOAT"));

	const SqlTypeName SqlTypeName::NUMERIC(STR("NUMERIC"));
	const SqlTypeName SqlTypeName::DECIMAL_10_5(STR("DECIMAL(10,5)"));
	const SqlTypeName SqlTypeName::BOOLEAN(STR("BOOLEAN"));
	const SqlTypeName SqlTypeName::DATE(STR("DATE"));
	const SqlTypeName SqlTypeName::DATETIME(STR("DATETIME"));
}