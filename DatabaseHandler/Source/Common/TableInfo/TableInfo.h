#pragma once
//own
#include "SQLiteType.h"
//std
#include <string>
#include <vector>

namespace SQLite { class Database; }

namespace database_handler::common
{
	struct ColumnInfo
	{
		explicit ColumnInfo(
			int columnId,
			const std::string& columnName,
			SQLiteType type,
			bool notNull,
			const std::string& defaultValue,
			int primaryKeyIndex);

		const int columnId = 0;
		std::string columnName;
		SQLiteType type;
		bool notNull;
		std::string defaultValue;
		//0 if not part of the primary key. Else, the index of the column in the primary key.
		const int primaryKeyIndex = 0;
	};

	struct TableInfo
	{
		explicit TableInfo(const std::string& tableName) : tableName(tableName) {}

		std::string tableName;
		std::vector<ColumnInfo> columnInfos;
	};

	TableInfo GetTableInfo(SQLite::Database& db, const std::string tableName);
}