#include "DatabaseHandler/Source/Common/Constants.h"
#include "DatabaseHandler/Source/Common/CreateAndDeleteDb.h"
#include "DatabaseHandler/Source/Common/GetAndCloseDb.h"
//std
#include <memory>

namespace database_handler::common
{
	namespace
	{
		std::unique_ptr<SQLite::Database> g_upDbV1;
		std::unique_ptr<SQLite::Database> g_upDbV2;
		std::unique_ptr<SQLite::Database> g_upSportMonksCacheDb;

		SQLite::Database& GetDb(const std::string& dbFileName)
		{
			std::unique_ptr<SQLite::Database>* pDb = nullptr;

			if (FILENAME_FOOTBALL_V1_DB == dbFileName)
			{
				if (nullptr == g_upDbV1)
				{
					g_upDbV1 = std::make_unique<SQLite::Database>(FILEPATH_FOOTBALL_V1_DB, SQLite::OPEN_READWRITE);
				}
				return *g_upDbV1.get();
			}
			else if (FILENAME_FOOTBALL_V2_DB == dbFileName)
			{
				if (nullptr == g_upDbV2)
				{
					g_upDbV2 = std::make_unique<SQLite::Database>(FILEPATH_FOOTBALL_V2_DB, SQLite::OPEN_READWRITE);
				}
				return *g_upDbV2.get();
			}
			else if (FILENAME_SPORT_MONKS_CACHE_DB == dbFileName)
			{
				if (nullptr == g_upSportMonksCacheDb)
				{
					g_upSportMonksCacheDb = std::make_unique<SQLite::Database>(FILEPATH_SPORT_MONKS_CACHE_DB, SQLite::OPEN_READWRITE);
				}
				return *g_upSportMonksCacheDb.get();
			}

			throw std::runtime_error("Unknown DB: " + dbFileName);
		}
	}

	SQLite::Database& GetDb(DbType dbType)
	{
		switch (dbType)
		{
		case database_handler::common::DbType::V1:
			CreateDbIfNotExists(FILENAME_FOOTBALL_V1_DB);
			return GetDb(FILENAME_FOOTBALL_V1_DB);
		case database_handler::common::DbType::V2:
			CreateDbIfNotExists(FILENAME_FOOTBALL_V2_DB);
			return GetDb(FILENAME_FOOTBALL_V2_DB);
		case database_handler::common::DbType::SportMonksCache:
			CreateDbIfNotExists(FILENAME_SPORT_MONKS_CACHE_DB);
			return GetDb(FILENAME_SPORT_MONKS_CACHE_DB);
		default:
			throw std::runtime_error("Unknown DB type.");
		}
	}

	void CloseDb(DbType dbType)
	{
		switch (dbType)
		{
		case database_handler::common::DbType::V1:
			g_upDbV1.reset();
			break;
		case database_handler::common::DbType::V2:
			g_upDbV2.reset();
			break;
		case database_handler::common::DbType::SportMonksCache:
			g_upSportMonksCacheDb.reset();
			break;
		default:
			throw std::runtime_error("Unknown DB type.");
		}
	}
}