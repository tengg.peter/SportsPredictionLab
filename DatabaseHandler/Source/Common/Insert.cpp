#include "DatabaseHandler/Source/Common/Insert.h"
#include "DatabaseHandler/Source/Common/GetAndCloseDb.h"
#include "DatabaseHandler/Source/Common/Pragma.h"
#include "DatabaseHandler/Source/Common/TableInfo/TableInfo.h"

#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"

#include <sstream>

namespace database_handler::common
{
	using std::vector;

	void InsertIntoSwitchTable(SQLite::Database& db, const std::string& tableName, const vector<vector<int64_t>>& keys)
	{
		//https://modern-sql.com/feature/with
		//http://cc.davelozinski.com/sql/fastest-way-to-insert-new-records-where-one-doesnt-already-exist

		if (tableName.empty())
		{
			throw std::invalid_argument("Table name cannot be empty.");
		}

		if (keys.empty() || keys.front().empty())
		{
			return;
		}

		common::PragmaTempStoreMemory(db);
		std::stringstream ss;
		//builds the query to create the temporary table. It is created with the required number of columns for the keys
		ss << "CREATE TEMP TABLE insert_candidates (\n";
		size_t numColumns = keys.front().size();
		for (size_t i = 0; i < numColumns; i++)
		{
			ss << "id" << i << " INTEGER NOT NULL";
			if (i < numColumns - 1)
			{
				ss << ",\n";
			}
		}
		ss << ");";
		db.exec(ss.str());

		//builds the query to insert data to the temp table
		ss.str("");
		ss << "INSERT INTO insert_candidates VALUES ";
		//here comes the dynamically built where condition:
		for (size_t i = 0; i < keys.size(); i++)
		{
			ss << "(";
			for (size_t j = 0; j < numColumns; j++)
			{
				ss << keys[i][j];
				if (j < numColumns - 1)
				{
					ss << ", ";
				}
			}
			ss << ")";
			if (i < keys.size() - 1)
			{
				ss << ", ";
			}
			else
			{
				ss << ";";
			}
		}

		db.exec(ss.str());
		ss.str("");
		const TableInfo targetTableInfo = GetTableInfo(db, tableName);
		if (targetTableInfo.columnInfos.size() != keys.front().size())
		{
			throw std::logic_error("The target table and the temporary table must have the same number of columns");
		}
		//INSERT INTO #table1(id, guidd, TimeAdded, ExtraData)
		//	SELECT #table2.id, #table2.guidd, #table2.TimeAdded, #table2.ExtraData
		//	FROM #table2
		//	LEFT JOIN #table1 on #table1.id = #table2.id
		//	WHERE #table1.id is null
		ss << "INSERT INTO " << tableName << "\n"
			"SELECT ";
		for (size_t i = 0; i < numColumns; i++)
		{
			ss << "insert_candidates.id" << i;
			if (i < numColumns - 1)
			{
				ss << ", ";
			}
		}
		ss << "\nFROM insert_candidates\n"
			"LEFT JOIN " << tableName << "\nON\n";

		using namespace utils::string_conversion;
		for (int i = 0; i < targetTableInfo.columnInfos.size(); i++)
		{
			ss << tableName << "." << targetTableInfo.columnInfos[i].columnName << " = insert_candidates.id" << i;
			if (i < targetTableInfo.columnInfos.size() - 1)
			{
				ss << " AND\n";
			}
		}
		ss << "\nWHERE\n";
		for (int i = 0; i < targetTableInfo.columnInfos.size(); i++)
		{
			ss << tableName << "." << targetTableInfo.columnInfos[i].columnName << " IS NULL";
			if (i < targetTableInfo.columnInfos.size() - 1)
			{
				ss << " AND\n";
			}
		}
		ss << ";";
		db.exec(ss.str());
		db.exec("DROP TABLE IF EXISTS insert_candidates");
	}
}