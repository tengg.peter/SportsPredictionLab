#pragma once

#include <string>
#include <utility>
#include <vector>

namespace SQLite { class Database; }

namespace database_handler::common
{
	void CreateSwitchTable(
		SQLite::Database& db, 
		const std::string& tableName, 
		const std::vector<std::pair<std::string, std::string>>& foreignKeys);
}