#pragma once
#include "Utils/Utils.h"

#include <string>

namespace database_handler::common
{
	enum class DbType
	{
		V1, V2, SportMonksCache
	};

	class OnConflict : public utils::enums::StringEnum
	{
	public:
		static const OnConflict Rollback;
		static const OnConflict Abort;
		static const OnConflict Fail;
		static const OnConflict Ignore;
		static const OnConflict Replace;

	protected:
		explicit OnConflict(const utils::String& enumValue) : StringEnum(enumValue) {}
	};

	const inline OnConflict OnConflict::Rollback{ STR("ROLLBACK") };
	const inline OnConflict OnConflict::Abort{ STR("ABORT") };
	const inline OnConflict OnConflict::Fail{ STR("FAIL") };
	const inline OnConflict OnConflict::Ignore{ STR("IGNORE") };
	const inline OnConflict OnConflict::Replace{ STR("REPLACE") };

	
	const std::string FILENAME_FOOTBALL_V1_DB{ "football_v1.db" };
	const std::string FILENAME_FOOTBALL_V2_DB{ "football_v2.db" };
	const std::string FILENAME_SPORT_MONKS_CACHE_DB{ "SportMonksCache.db" };

	const std::string FILEPATH_DATABASES_DIR{ "Databases/" };

	const std::string FILEPATH_FOOTBALL_V1_DB{ FILEPATH_DATABASES_DIR + FILENAME_FOOTBALL_V1_DB };
	const std::string FILEPATH_FOOTBALL_V2_DB{ FILEPATH_DATABASES_DIR + FILENAME_FOOTBALL_V2_DB };
	const std::string FILEPATH_SPORT_MONKS_CACHE_DB{ FILEPATH_DATABASES_DIR + FILENAME_SPORT_MONKS_CACHE_DB };
}