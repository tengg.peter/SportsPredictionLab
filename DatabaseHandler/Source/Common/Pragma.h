#pragma once

namespace SQLite { class Database; }

namespace database_handler::common
{
	void PragmaTempStoreMemory(SQLite::Database& db);

	void PragmaForeignKeys(SQLite::Database& db, bool enforceForeignKeys);
}