#pragma once

#include <set>
#include <string>

namespace SQLite { class Database; }

namespace database_handler::common
{
	void DeleteFromTable(
		SQLite::Database& db,
		const std::string& tableName,
		const std::string& columnName,
		const std::set<int64_t>& ids);
}