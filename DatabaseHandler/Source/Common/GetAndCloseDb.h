#pragma once

#include "DatabaseHandler/Source/Common/Constants.h"
//3rd party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"

namespace database_handler::common
{
	SQLite::Database& GetDb(DbType dbType);
	void CloseDb(DbType dbType);
}