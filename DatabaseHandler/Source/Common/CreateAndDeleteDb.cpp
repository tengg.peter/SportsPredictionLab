//own
#include "DatabaseHandler/Source/Common/Constants.h"
#include "DatabaseHandler/Source/Common/CreateAndDeleteDb.h"
#include "DatabaseHandler/Source/SportMonksCache/CreateSchema.h"
#include "DatabaseHandler/Source/v2/CreateSchema.h"
#include "Utils/Utils.h"
//3rd party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/VariadicBind.h"
//std
#include <filesystem>	//exists()
#include <iostream>
#include "CreateAndDeleteDb.h"

namespace database_handler::common
{
	using SQLite::Database;
	using std::endl;

	void CreateDbIfNotExists(const std::string& fileName)
	{
		std::filesystem::create_directory(FILEPATH_DATABASES_DIR);	//does nothing if the directory already exists
		if (!std::filesystem::exists(FILEPATH_DATABASES_DIR + fileName))
		{
			utils::Logger::LogWarning(FILEPATH_DATABASES_DIR + fileName + " does not exist. Creating an empty one.", true);
			Database db(FILEPATH_DATABASES_DIR + fileName, SQLite::OPEN_CREATE | SQLite::OPEN_READWRITE);
			if (common::FILENAME_FOOTBALL_V2_DB == fileName)	//this is a hack. It is not set up for db v1 at the moment.
			{
				v2::CreateSchema(db);
			}
			else if (common::FILENAME_SPORT_MONKS_CACHE_DB == fileName)	//this is a hack. It is not set up for db v1 at the moment.
			{
				sm_cache::CreateSchema(db);
			}
		}
	}

	void DeleteDb(const std::string& fileName)
	{
		if (std::filesystem::exists(FILEPATH_DATABASES_DIR + fileName))
		{
			std::filesystem::remove(FILEPATH_DATABASES_DIR + fileName);
		}
	}

	void DeleteDbFolder()
	{
		if (std::filesystem::exists(FILEPATH_DATABASES_DIR))
		{
			std::filesystem::remove_all(FILEPATH_DATABASES_DIR);
		}
	}
}