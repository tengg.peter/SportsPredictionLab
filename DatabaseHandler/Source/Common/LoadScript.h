#pragma once

#include <map>
#include <string>

namespace database_handler::common
{
	std::string LoadScript(
		const std::string& scriptFile, 
		const std::map<std::string, std::string>& replace);
}