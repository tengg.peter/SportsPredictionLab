#include "Utils/Utils.h"

#include <limits>
#include <set>
#include <string>
#include <vector>

namespace SQLite { class Database; }

namespace database_handler::common
{
	std::vector<std::vector<int64_t>> QuerySwitchTable(
		SQLite::Database& db, 
		const std::string& tableName, 
		const std::string& whereClause = "");

	std::vector<int64_t> QuerySingleColumnFromTable(
		SQLite::Database& db,
		const std::string& tableName,
		const std::string& columnName,
		const std::string& whereClause = "",
		int64_t firstRowId = 0,
		size_t numRows = std::numeric_limits<size_t>::max());

	std::vector<int64_t> QueryGreatestRowId(
		SQLite::Database& db,
		const std::string& tableName,
		const std::vector<std::string>& columnNames
		);

	utils::date_and_time::DateTime QueryExtremeDate(
		SQLite::Database& db,
		const std::string& tableName,
		const std::string& columnName,
		const std::string& whereClause = "",
		const std::string& ordering = "DESC"
		);

	std::set<int64_t> QueryWhereNotIn(
		SQLite::Database& db,
		const std::string& firstTableName,
		const std::string& firstColumnName,
		const std::string& secondTableName,
		const std::string& secondColumnName,
		const std::string& firstWhereClause = ""
	);
}