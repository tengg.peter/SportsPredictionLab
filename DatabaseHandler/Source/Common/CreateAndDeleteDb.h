#pragma once

#include <string>

namespace database_handler::common
{
	//database files are created under Databases/
	void CreateDbIfNotExists(const std::string& fileName);

	void DeleteDb(const std::string& fileName);
	
	void DeleteDbFolder();
}