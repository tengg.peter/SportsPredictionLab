//own
#include "DatabaseHandler/Source/Common/Create.h"
#include "Utils/Utils.h"
//3rd party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"
//std
#include <exception>
#include <sstream>

namespace database_handler::common
{
	void CreateSwitchTable(
		SQLite::Database& db,
		const std::string& tableName,
		const std::vector<std::pair<std::string, std::string>>& foreignKeys)
	{
		if (2 > foreignKeys.size())
		{
			throw std::invalid_argument("columnNames: a switch table must have at least two columns.");
		}
		
		std::stringstream query;
		query << "CREATE TABLE " << tableName << "(";

		std::vector<std::string> columnNames;
		for (const auto& [tableName, columnName] : foreignKeys)
		{
			columnNames.push_back({tableName + "_" + columnName});
		}
		using utils::string_operations::JoinStrings;
		using namespace std::string_literals;
		query << JoinStrings(columnNames, " INTEGER NOT NULL, "s) << " INTEGER NOT NULL, ";
		query << "UNIQUE (" << JoinStrings(columnNames, ", "s) << "), ";
		for (size_t i = 0; i < foreignKeys.size(); i++)
		{
			if (0 < i)
			{
				query << ", ";
			}
			query << "FOREIGN KEY(" << columnNames[i] << ") REFERENCES " << foreignKeys[i].first << "(" << foreignKeys[i].second << ")";
		}
		query << ");";
		db.exec(query.str());
	}
}

//CREATE TABLE seasons_teams(
//	seasons_sm_id INTEGER NOT NULL,
//	teams_sm_id INTEGER NOT NULL,
//
//	UNIQUE(sm_season_id, sm_team_id),
//	FOREIGN KEY(seasons_sm_id) REFERENCES seasons(sm_id),
//	FOREIGN KEY(teams_sm_id) REFERENCES teams(sm_id)
//)