#include <string>
#include <vector>

namespace SQLite { class Database; }

namespace database_handler::common
{
	//inserts only the ones that are not in the db yet.
	void InsertIntoSwitchTable(SQLite::Database& db, const std::string& tableName, const std::vector<std::vector<int64_t>>& keys);
}