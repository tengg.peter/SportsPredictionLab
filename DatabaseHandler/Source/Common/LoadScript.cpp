#include "DatabaseHandler/Source/Common/LoadScript.h"

//std
#include <fstream>
#include <sstream>
#include <regex>

namespace database_handler::common
{
	std::string LoadScript(
		const std::string& scriptFile, 
		const std::map<std::string, std::string>& replace)
	{
		std::fstream file{ scriptFile };
		std::stringstream buf;
		buf << file.rdbuf();
		std::string script{ buf.str() };

		for (const auto& [toReplace, replaceWith] : replace)
		{
			script = std::regex_replace(script, std::regex(toReplace), replaceWith);
		}

		return script;
	}
}