//own
#include "Pragma.h"
//3rdParty
#include <SQLiteCpp/SQLiteCpp.h>
#include <SQLiteCpp/VariadicBind.h>

namespace database_handler::common
{
	void PragmaTempStoreMemory(SQLite::Database& db)
	{
		db.exec("PRAGMA temp_store=MEMORY");
	}

	void PragmaForeignKeys(SQLite::Database& db, bool enforceForeignKeys)
	{
		db.exec("PRAGMA foreign_keys=" + std::to_string(enforceForeignKeys ? 1 : 0));
	}
}