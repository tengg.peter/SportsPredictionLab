#pragma once

#include <string>

namespace database_handler::sm_cache
{
    const std::string CREATE_INDEX_CACHE_ENTRIES
    {
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_cache_entries__query "
        "ON cache_entries (query); "
        
	};
    
    
}