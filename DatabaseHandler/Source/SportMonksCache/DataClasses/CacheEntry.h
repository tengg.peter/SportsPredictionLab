#pragma once
#include "DatabaseHandler/Source/Common/Helper/Debug.h"
#include "DatabaseHandler/Source/Common/Helper/Helper.h"
#include "Utils/Utils.h"
//##protect##"includes"
//##protect##"includes"

#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <vector>

#define ENABLE_DEBUG_PRINTS 0

namespace database_handler::sm_cache
{
	using utils::string_conversion::StdStringToUtilsString;

	class CacheEntry
	{
	public:
		int64_t id = 0;
		utils::String query;
		utils::String updated;
		utils::String json_response;
		//##protect##"class members"
//##protect##"class members"

		explicit CacheEntry() = default;
		virtual ~CacheEntry() {}

		explicit CacheEntry(SQLite::Statement& stm)
			: id(stm.getColumn("id").getInt64())
			, query(StdStringToUtilsString(stm.getColumn("query").getString()))
			, updated(StdStringToUtilsString(stm.getColumn("updated").getString()))
			, json_response(StdStringToUtilsString(stm.getColumn("json_response").getString()))
			
		{}
	};

	using UpCacheEntry = std::unique_ptr<CacheEntry>;

	struct CacheEntryCmp
	{
		bool operator()(const CacheEntry& a, const CacheEntry& b) const
		{
			return a.query < b.query;
		}

		bool operator()(const UpCacheEntry& a, const UpCacheEntry& b) const
		{
			return nullptr == b ? false : 
				nullptr == a ? true : 
				a == b ? false : operator()(*a, *b);
		}
	};

	//##protect##"functions"
//##protect##"functions"

	template<
		class T,
		typename std::enable_if <
		std::is_same<CacheEntry, T>{} || std::is_same<UpCacheEntry, T>{},
		int > ::type = 0 >
	inline int64_t QueryCount(SQLite::Database& db)
	{
		const std::string query{ "SELECT COUNT(id) FROM cache_entries;" };
		SQLite::Statement stm(db, query);
		if (0 == stm.executeStep())
		{
			common::CheckThrow(stm);
		}
		return stm.getColumn(0).getInt64();
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<CacheEntry, T>{} || std::is_same<UpCacheEntry, T>{},
		int > ::type = 0 >
	inline std::vector<UpCacheEntry> QueryAll(SQLite::Database& db)
	{
		const std::string query{ "SELECT * FROM cache_entries;" };

		SQLite::Statement stm(db, query);
		std::vector<UpCacheEntry> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<CacheEntry>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class T,
		typename std::enable_if <
		std::is_same<CacheEntry, T>{} || std::is_same<UpCacheEntry, T>{},
		int > ::type = 0 >
	inline std::vector<UpCacheEntry> Query(SQLite::Database& db, const std::string& where)
	{
		if (where.empty())
		{
			return QueryAll<CacheEntry>(db);
		}

		const std::string query{ "SELECT * FROM cache_entries WHERE " + where + ";"};

		SQLite::Statement stm(db, query);
		std::vector<UpCacheEntry> retVec;
		while (stm.executeStep())
		{
			retVec.emplace_back(std::make_unique<CacheEntry>(stm));
		}
		common::CheckThrow(stm);

		return retVec;
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<CacheEntry, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& cacheEntries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		std::vector<const typename TContainer::value_type*> pointers;
		pointers.reserve(std::size(cacheEntries));
		std::transform(std::cbegin(cacheEntries), std::cend(cacheEntries), std::back_inserter(pointers),
			[](const typename TContainer::value_type& cacheEntry)
			{
				return &cacheEntry;
			});
		InsertOr(onConflict, db, pointers, printInfo, startNewTransaction);
	}

	template<
		class TContainer,
		typename std::enable_if <
		std::is_same<UpCacheEntry, typename TContainer::value_type>{} ||
		std::is_same<const CacheEntry*, typename TContainer::value_type>{},
		int > ::type = 0 >
	inline void InsertOr(
		const common::OnConflict& onConflict, 
		SQLite::Database& db, 
		const TContainer& cacheEntries,
		bool printInfo = false,
		bool startNewTransaction = true)
	{
		using std::endl;

		if (printInfo)
		{
			TCOUT << "InsertOr" << onConflict.ToUtilsString() << "<CacheEntry>: Inserting " << std::size(cacheEntries) << " cacheEntries." << endl;
		}
		if (std::empty(cacheEntries) || 
			std::all_of(std::cbegin(cacheEntries), std::cend(cacheEntries), 
				[](const auto& pCacheEntry) {return nullptr == pCacheEntry;}))
		{
			return;
		}


		std::stringstream ss;
		
		//creating temp table
		common::PragmaTempStoreMemory(db);
		constexpr int numColumns = 3;
		constexpr size_t batchSize = 999 / numColumns;
		size_t numProcessed = 0;

		if (printInfo)
		{
			size_t numBatches = std::size(cacheEntries) / batchSize;
			if(0 != std::size(cacheEntries) % batchSize)
			{
				numBatches += 1;
			}
			TCOUT << "Batches: " << numBatches << endl;
		}

		utils::date_and_time::StopWatch timer;
		double totalIteratorOperationTimeMs = 0;
		double totalQueryBuildingTimeMs = 0;
		double totalBindingTimeMs = 0;
		double totalInsertionTimeMs = 0;
		double totalConnectionInsertionTimeMs = 0;

		if (startNewTransaction)
		{
			db.exec("BEGIN TRANSACTION;");
		}
		do
		{
			timer.Restart();
			auto batchStartIt = std::cbegin(cacheEntries);
			std::advance(batchStartIt, numProcessed);
			auto batchEndIt = batchStartIt;
			if (batchSize <= std::distance(batchEndIt, std::cend(cacheEntries)))
			{
				std::advance(batchEndIt, batchSize);
			}
			else
			{
				batchEndIt = std::cend(cacheEntries);
			}
			timer.Stop();
			totalIteratorOperationTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			timer.Restart();
			//query to insert into temp table
			{
				ss.str("");
				ss << "INSERT OR "<< onConflict.ToStdString() << " INTO cache_entries VALUES\n";
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; ++it)
				{
					if (nullptr == *it)
					{
						continue;
					}

					if (0 < i)
					{
						ss << ",\n";
					}
					ss << "(";
					ss << ":id" << i << ", ";
					ss << ":query" << i << ", ";
					ss << ":updated" << i << ", ";
					ss << ":json_response" << i;
					ss << ")";
					++i;
				}
				ss << ";";
			}

			timer.Stop();
			totalQueryBuildingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			//binding the insert_candidate query
			{
#if ENABLE_DEBUG_PRINTS
				TCOUT << ss.str().c_str() << "\n" << std::endl;
#endif
				timer.Restart();
				SQLite::Statement insert(db, ss.str());
				size_t i = 0;
				for (auto it = batchStartIt; it != batchEndIt; it++)
				{
					if(nullptr == *it)
					{
						continue;
					}

					using common::BindOptional;
					using utils::string_conversion::UtilsStringToStdString;
					const std::string idx{ std::to_string(i++) };

					insert.bind(":id" + idx);
					insert.bind(":query" + idx, UtilsStringToStdString((*it)->query));
					insert.bind(":updated" + idx, UtilsStringToStdString((*it)->updated));
					insert.bind(":json_response" + idx, UtilsStringToStdString((*it)->json_response));
					
				}
				timer.Stop();
				totalBindingTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				timer.Restart();
				insert.exec();
				timer.Stop();
				totalInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
				common::CheckThrow(insert);
			}

			timer.Restart();
			//##protect##"InsertOr"
//##protect##"InsertOr"

			timer.Stop();
			totalConnectionInsertionTimeMs += timer.Elapsed<std::chrono::milliseconds>();
			numProcessed += std::distance(batchStartIt, batchEndIt);
		} while (std::size(cacheEntries) > numProcessed);
		if (startNewTransaction)
		{
			db.exec("COMMIT;");
		}

		if (printInfo)
		{
			TCOUT << "Total iterator time: " << totalIteratorOperationTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total query building time: " << totalQueryBuildingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total binding time: " << totalBindingTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total insertion time: " << totalInsertionTimeMs / 1000 << " seconds." << endl;
			TCOUT << "Total connection insertion time: " << totalConnectionInsertionTimeMs / 1000 << " seconds." << endl;
		}
#if ENABLE_DEBUG_PRINTS
		debug::PrintTable(db, "cache_entries");
#endif
	}
}

#undef ENABLE_DEBUG_PRINTS
#define ENABLE_DEBUG_PRINTS 0