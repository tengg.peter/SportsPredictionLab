#pragma once

#include <string>

namespace database_handler::sm_cache
{
    const std::string CREATE_TABLE_CACHE_ENTRIES
    {
        "CREATE TABLE cache_entries( "
        "id INTEGER PRIMARY KEY, "
        "query TEXT NOT NULL UNIQUE, "
        "updated TEXT NOT NULL, "
        "json_response TEXT NOT NULL "
        "); "
        
	};
    
    
}