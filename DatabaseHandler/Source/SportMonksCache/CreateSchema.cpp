#include "DatabaseHandler/Source/SportMonksCache/CreateSchema.h"
#include "DatabaseHandler/Source/SportMonksCache/SqlCreateIndexScripts.h"
#include "DatabaseHandler/Source/SportMonksCache/SqlCreateTableScripts.h"
//##protect##"includes"
//##protect##"includes"

//3rd Party
#include "DatabaseHandler/Source/3rdParty/SQLiteCpp/include/SQLiteCpp/SQLiteCpp.h"

namespace database_handler::sm_cache
{
	void CreateSchema(SQLite::Database& db)
	{
		db.exec(CREATE_TABLE_CACHE_ENTRIES);
		db.exec(CREATE_INDEX_CACHE_ENTRIES);
		
		//##protect##"switch tables"
//##protect##"switch tables"

	}
}