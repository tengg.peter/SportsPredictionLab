Sportmonks token: mG4rJvhR8dHGVfRBPail3VcEHyXKFEo9lrrjBvVTfvrfxH8v6GYmo58JfMg9

Project directory tree

.
├── 3rdParty
│   ├── curl
│   │   ├── linux
│   │   │   ├── include
│   │   │   │   └── curl
│   │   │   │       ├── curl.h
│   │   │   │       ├── curlver.h
│   │   │   │       ├── easy.h
│   │   │   │       ├── mprintf.h
│   │   │   │       ├── multi.h
│   │   │   │       ├── stdcheaders.h
│   │   │   │       ├── system.h
│   │   │   │       └── typecheck-gcc.h
│   │   │   └── linbcurl.a
│   │   └── windows
│   │       ├── include
│   │       │   ├── curl.h
│   │       │   ├── curlver.h
│   │       │   ├── easy.h
│   │       │   ├── mprintf.h
│   │       │   ├── multi.h
│   │       │   ├── stdcheaders.h
│   │       │   ├── system.h
│   │       │   └── typecheck-gcc.h
│   │       ├── libcurl_a.lib
│   │       └── libcurl-d.lib (debug build)
│   ├── googletest (as cloned from Git)
│   │   └── googletest
│   │       └── include
│   │           └── gtest
│   │               ├── internal
│   │               │   ├── custom
│   │               │   │   └── ...(3 headers)
│   │               │   └── ...(8 headers)
│   │               └── ...(11 headers)
│   └── yaml-cpp-master   (as got from their Git repo)
│       ├── CMakeLists.txt
│       └── ...(a lot more stuff)
├── AiLab
│   ├── CmakeLists.txt
│   └── ...
├── Utils
│   ├── CmakeLists.txt
│   └── ..
└── SportsPredictionLab
    ├── CmakeLists.txt
    └── ..

yaml-cpp is built from the main CMake file automatically.

To build curl on Linux (uses OpenSSL by default. It is needed for https)
- cmake .. -DBUILD_CURL_EXE=OFF -DCURL_STATICLIB=ON -DCURL_STATIC_CRT=ON -DHTTP_ONLY=ON -DBUILD_TESTING=OFF
- you need to link to "ssl" and "crypto" when you build your application

To build curl on Windows (WinSsl needs to be specified):
Debug:
- cmake .. -DBUILD_CURL_EXE=OFF -DCURL_STATICLIB=ON -DCURL_STATIC_CRT=ON -DHTTP_ONLY=ON -DBUILD_TESTING=OFF -DCMAKE_USE_WINSSL=ON
- you need to link to ws2_32.lib (winsocket) and crypt32.lib when you build your application
Release: (source: https://medium.com/@chuy.max/compile-libcurl-on-windows-with-visual-studio-2017-x64-and-ssl-winssl-cff41ac7971d and winbuild/BUILD.WINDOWS.txt)
Open cmd. Go to the curl directory.
- "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
- cd winbuild
- nmake /f Makefile.vc mode=static ENABLE_WINSSL=yes DEBUG=no MACHINE=x64 VC=16 RTLIBCFG=static
- The built binaries will be under curl/builds. Copy builds/{long-name-without-obj}/lib/libcurl_a.lib to curl/windows/

 Known issues:
 - When you build Utils/Tests and AiLab/Tests on Linux, at the end you will get a "no rule to make build/all", message. You can ignore that and just run the tests.
